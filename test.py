from uspto.pbd.client import UsptoPatentExaminationDataSystemClient
from uspto.pbd.document import UsptoPairBulkDataDocuments
import os

client = UsptoPairBulkDataClient()

expression = 'patentTitle:(retail) AND appStatus_txt:(patented)'
filter     = 'appFilingDate:[1997-01-01T00:00:00Z TO 2017-11-24T23:59:59Z]'

result = client.download_document('PP28532')

document = UsptoPairBulkDataDocument(result)

print(result)

