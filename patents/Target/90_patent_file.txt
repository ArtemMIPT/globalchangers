DOCUMENT_BODY:
	appGrpArtNumber: 3683
	appGrpArtNumberFacet: 3683
	transactions:
		№ 0
			recordDate: 2010-10-14 00:00:00
			code: PCTFR
			description: PCT Formality Review
		№ 1
			recordDate: 2010-08-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 2
			recordDate: 2010-07-07 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 3
			recordDate: 2010-06-30 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 4
			recordDate: 2010-06-28 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 5
			recordDate: 2009-01-05 00:00:00
			code: MP210
			description: Mail International Search Report
		№ 6
			recordDate: 2009-01-02 00:00:00
			code: P237
			description: ISA Written Opinion
		№ 7
			recordDate: 2009-01-02 00:00:00
			code: P210
			description: International Search Report Ready to be Mailed
		№ 8
			recordDate: 2008-12-31 00:00:00
			code: CTSF
			description: PCT - Chapter 1 P210 / P237 (Full count)
		№ 9
			recordDate: 2008-12-30 00:00:00
			code: 210I
			description: 210I
		№ 10
			recordDate: 2008-12-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 11
			recordDate: 2008-12-04 00:00:00
			code: D5003
			description: Dispatch from PCT Operations -Chapter I
		№ 12
			recordDate: 2008-12-04 00:00:00
			code: D5003
			description: Dispatch from PCT Operations -Chapter I
		№ 13
			recordDate: 2008-12-03 00:00:00
			code: TF01
			description: TF01
		№ 14
			recordDate: 2008-11-24 00:00:00
			code: D5003
			description: Dispatch from PCT Operations -Chapter I
		№ 15
			recordDate: 2008-11-19 00:00:00
			code: P202
			description: Notification of Receipt of Search Copy
		№ 16
			recordDate: 2008-11-18 00:00:00
			code: P202
			description: Notification of Receipt of Search Copy
		№ 17
			recordDate: 2008-09-10 00:00:00
			code: P132
			description: Miscellaneous Communication
		№ 18
			recordDate: 2008-09-09 00:00:00
			code: P132
			description: Miscellaneous Communication
		№ 19
			recordDate: 2008-08-15 00:00:00
			code: P105
			description: Notification of Intntl. Appl. Number and Intntl. Filing Date
		№ 20
			recordDate: 2008-08-15 00:00:00
			code: P102
			description: Notification Concerning Payment of Fees
		№ 21
			recordDate: 2008-08-15 00:00:00
			code: MLIB
			description: Record Copy Mailed
		№ 22
			recordDate: 2008-08-01 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 23
			recordDate: 2008-07-31 00:00:00
			code: RCDT
			description: Receipt Date
		№ 24
			recordDate: 2008-07-31 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 25
			recordDate: 2008-07-31 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: PCT - International Search Report Mailed to IB
	appStatus_txt: PCT - International Search Report Mailed to IB
	appStatusFacet: PCT - International Search Report Mailed to IB
	wipoEarlyPubDate: 2009-02-05T05:00:00Z
	patentNumber: 
	applIdStr: PCT/US08/71746
	appl_id_txt: PCT/US08/71746
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: PCT/US08/71746
	appSubCls: 330000
	wipoEarlyPubNumber: 2009018437
	appLocation: ELECTRONIC
	appAttrDockNumber: 2006-004773-
	appType: PCT
	appTypeFacet: PCT
	appCustNumber: 67261
	applicants:
		№ 0
			nameLineOne: C/O TARGET BRANDS, INC.
			nameLineTwo: 1000 NICOLLET MALL 
			suffix: 
			streetOne: 
			streetTwo: 
			city: MINNEAPOLIS, 
			geoCode: MN
			country: (US)
			rankNo: 0
	applicantsFacet: {1000 NICOLLET MALL|C/O TARGET BRANDS, INC.||||MINNEAPOLIS|MN|US|0}
	appStatusDate: 2009-01-02T19:44:40Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	APP_IND: 2
	appExamName: NGUYEN, NGA B
	appExamNameFacet: NGUYEN, NGA B
	firstInventorFile: Other
	appClsSubCls: 705/330000
	appClsSubClsFacet: 705/330000
	applId: PCT/US08/71746
	patentTitle: TRANSPORTATION MANAGEMENT SYSTEM
	appIntlPubNumber: 
	appConfrNumber: 5788
	appFilingDate: 2008-07-31T04:00:00Z
	firstNamedApplicant: 1000 NICOLLET MALL C/O TARGET BRANDS, INC.
	firstNamedApplicantFacet: 1000 NICOLLET MALL C/O TARGET BRANDS, INC.
	appCls: 705
	_version_: 1580434319227224068
	lastUpdatedTimestamp: 2017-10-05T16:06:59.022Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 537468de-e1bf-4bd1-bcc4-2db951c3bc2d
	responseHeader:
			status: 0
			QTime: 13
