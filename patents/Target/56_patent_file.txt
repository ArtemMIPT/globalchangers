DOCUMENT_BODY:
	appGrpArtNumber: 2918
	appGrpArtNumberFacet: 2918
	transactions:
		№ 0
			recordDate: 2015-05-26 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-05-07 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 2
			recordDate: 2015-05-06 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 3
			recordDate: 2015-04-21 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 4
			recordDate: 2015-04-06 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 5
			recordDate: 2015-04-03 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 6
			recordDate: 2015-04-03 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 7
			recordDate: 2015-03-16 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 8
			recordDate: 2015-03-16 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 9
			recordDate: 2015-03-16 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 10
			recordDate: 2015-03-09 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 11
			recordDate: 2015-02-25 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 12
			recordDate: 2015-02-23 00:00:00
			code: ELC.
			description: Response to Election / Restriction Filed
		№ 13
			recordDate: 2015-02-23 00:00:00
			code: LTDR
			description: Incoming Letter Pertaining to the Drawings
		№ 14
			recordDate: 2015-01-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 15
			recordDate: 2015-01-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 16
			recordDate: 2015-01-23 00:00:00
			code: MCTRS
			description: Mail Restriction Requirement
		№ 17
			recordDate: 2015-01-13 00:00:00
			code: CTRS
			description: Restriction/Election Requirement
		№ 18
			recordDate: 2015-01-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 19
			recordDate: 2015-01-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 20
			recordDate: 2014-05-16 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 21
			recordDate: 2014-05-16 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 22
			recordDate: 2014-01-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 23
			recordDate: 2013-06-02 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 24
			recordDate: 2013-03-15 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 25
			recordDate: 2013-03-15 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 26
			recordDate: 2013-03-15 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 27
			recordDate: 2013-03-15 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 28
			recordDate: 2013-03-05 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 29
			recordDate: 2013-03-04 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 30
			recordDate: 2013-03-04 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 31
			recordDate: 2013-03-04 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D730115
	applIdStr: 29447471
	appl_id_txt: 29447471
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29447471
	appSubCls: 396400
	appLocation: ELECTRONIC
	appAttrDockNumber: 201204749
	appType: Design
	appTypeFacet: Design
	appCustNumber: 70829
	applicants:
		№ 0
			nameLineOne: TARGET BRANDS, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 3
	applicantsFacet: {|TARGET BRANDS, INC.||||Minneapolis|MN|US|3}
	appStatusDate: 2015-05-06T10:11:09Z
	LAST_MOD_TS: 2017-04-18T10:12:30Z
	patentIssueDate: 2015-05-26T04:00:00Z
	APP_IND: 1
	appExamName: MILLER, BRETT A
	appExamNameFacet: MILLER, BRETT A
	firstInventorFile: No
	appClsSubCls: D07/396400
	appClsSubClsFacet: D07/396400
	applId: 29447471
	patentTitle: RIM FOR TABLEWARE
	appIntlPubNumber: 
	appConfrNumber: 8819
	appFilingDate: 2013-03-04T05:00:00Z
	firstNamedApplicant:  TARGET BRANDS, INC.
	firstNamedApplicantFacet:  TARGET BRANDS, INC.
	inventors:
		№ 0
			nameLineOne: Amanda Marie MILLER
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Kelly Margaret BERGE
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapois, 
			geoCode: MN
			country: (US)
			rankNo: 2
	appCls: D07
	_version_: 1580433807164571649
	lastUpdatedTimestamp: 2017-10-05T15:58:50.674Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 95463814-a5ed-47d1-a0d3-38229449d3d0
	responseHeader:
			status: 0
			QTime: 13
