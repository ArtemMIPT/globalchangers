DOCUMENT_BODY:
	appGrpArtNumber: 2912
	appGrpArtNumberFacet: 2912
	transactions:
		№ 0
			recordDate: 2015-04-28 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-04-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 2
			recordDate: 2015-04-08 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 3
			recordDate: 2015-03-23 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 4
			recordDate: 2015-03-19 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 5
			recordDate: 2015-03-11 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 6
			recordDate: 2015-03-11 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 7
			recordDate: 2015-02-27 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 8
			recordDate: 2015-02-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 9
			recordDate: 2015-02-27 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 10
			recordDate: 2015-02-23 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 11
			recordDate: 2015-02-23 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 12
			recordDate: 2015-02-20 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 13
			recordDate: 2013-08-08 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 14
			recordDate: 2013-01-23 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 15
			recordDate: 2013-01-23 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 16
			recordDate: 2013-01-23 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 17
			recordDate: 2013-01-23 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 18
			recordDate: 2013-01-14 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 19
			recordDate: 2013-01-14 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 20
			recordDate: 2013-01-14 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 21
			recordDate: 2013-01-14 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 22
			recordDate: 2013-01-14 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 23
			recordDate: 2013-01-14 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D728035
	applIdStr: 29443120
	appl_id_txt: 29443120
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29443120
	appSubCls: 472000
	appLocation: ELECTRONIC
	appAttrDockNumber: 201202720
	appType: Design
	appTypeFacet: Design
	appCustNumber: 70829
	applicants:
		№ 0
			nameLineOne: TARGET BRANDS, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 4
	applicantsFacet: {|TARGET BRANDS, INC.||||Minneapolis|MN|US|4}
	appStatusDate: 2015-04-08T13:58:48Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-04-28T04:00:00Z
	APP_IND: 1
	appExamName: TUTTLE, CATHERINE A
	appExamNameFacet: TUTTLE, CATHERINE A
	firstInventorFile: No
	appClsSubCls: D21/472000
	appClsSubClsFacet: D21/472000
	applId: 29443120
	patentTitle: VESSEL WITH EXTERNAL RINGS
	appIntlPubNumber: 
	appConfrNumber: 1825
	appFilingDate: 2013-01-14T05:00:00Z
	firstNamedApplicant:  TARGET BRANDS, INC.
	firstNamedApplicantFacet:  TARGET BRANDS, INC.
	inventors:
		№ 0
			nameLineOne: Sheila Kelly ZWETTLER
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Eden Prairie, 
			geoCode: MN
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Ashley L. KNELLER
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 1
		№ 2
			nameLineOne: Siu Ling  MARTINEZ
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 3
	appCls: D21
	_version_: 1580433806409596933
	lastUpdatedTimestamp: 2017-10-05T15:58:49.957Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 95463814-a5ed-47d1-a0d3-38229449d3d0
	responseHeader:
			status: 0
			QTime: 13
