DOCUMENT_BODY:
	appGrpArtNumber: 2422
	appGrpArtNumberFacet: 2422
	transactions:
		№ 0
			recordDate: 2015-11-10 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-11-10 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 2
			recordDate: 2015-11-10 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2015-10-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2015-10-21 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2015-10-09 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2015-10-08 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2015-10-05 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2015-10-05 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2015-09-21 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2015-09-21 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2015-09-21 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 12
			recordDate: 2015-09-17 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 13
			recordDate: 2015-09-16 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 14
			recordDate: 2015-08-18 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 15
			recordDate: 2015-08-13 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 16
			recordDate: 2015-05-20 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 17
			recordDate: 2015-05-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 18
			recordDate: 2015-05-20 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 19
			recordDate: 2015-05-15 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 20
			recordDate: 2015-05-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 21
			recordDate: 2015-03-09 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 22
			recordDate: 2015-02-11 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 23
			recordDate: 2014-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 24
			recordDate: 2014-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 25
			recordDate: 2014-03-14 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 26
			recordDate: 2013-06-17 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 27
			recordDate: 2013-06-04 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 28
			recordDate: 2013-06-04 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 29
			recordDate: 2013-06-04 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 30
			recordDate: 2013-06-03 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 31
			recordDate: 2013-06-03 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 32
			recordDate: 2013-04-25 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 33
			recordDate: 2013-04-23 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 34
			recordDate: 2013-04-23 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 35
			recordDate: 2013-04-23 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 36
			recordDate: 2013-04-23 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 37
			recordDate: 2013-04-23 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 38
			recordDate: 2013-04-23 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 39
			recordDate: 2013-04-23 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9185359
	applIdStr: 13868673
	appl_id_txt: 13868673
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13868673
	appSubCls: 159000
	appLocation: ELECTRONIC
	appAttrDockNumber: 201203460
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 67260
	applicants:
		№ 0
			nameLineOne: Target Brands, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 3
	applicantsFacet: {|Target Brands, Inc.||||Minneapolis|MN|US|3}
	appStatusDate: 2015-10-21T09:11:16Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-11-10T05:00:00Z
	APP_IND: 1
	appExamName: LEE, MICHAEL
	appExamNameFacet: LEE, MICHAEL
	firstInventorFile: Yes
	appClsSubCls: 348/159000
	appClsSubClsFacet: 348/159000
	applId: 13868673
	patentTitle: Enterprise-Wide Camera Data
	appIntlPubNumber: 
	appConfrNumber: 2631
	appFilingDate: 2013-04-23T04:00:00Z
	firstNamedApplicant:  Target Brands, Inc.
	firstNamedApplicantFacet:  Target Brands, Inc.
	inventors:
		№ 0
			nameLineOne: Christopher Michael McGee
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Robert F. Foster
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Ramsey, 
			geoCode: MN
			country: (US)
			rankNo: 2
	appCls: 348
	_version_: 1580433237886369794
	lastUpdatedTimestamp: 2017-10-05T15:49:47.769Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 229ca736-4b25-4baa-97a7-d6c8e2eb0309
	responseHeader:
			status: 0
			QTime: 13
