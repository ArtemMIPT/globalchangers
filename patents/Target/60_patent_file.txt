DOCUMENT_BODY:
	appGrpArtNumber: 2913
	appGrpArtNumberFacet: 2913
	transactions:
		№ 0
			recordDate: 2013-10-08 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2013-09-18 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 2
			recordDate: 2013-09-03 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 3
			recordDate: 2013-08-26 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 4
			recordDate: 2013-08-20 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 5
			recordDate: 2013-08-20 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 6
			recordDate: 2013-08-01 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 7
			recordDate: 2013-07-31 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 8
			recordDate: 2013-07-17 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 9
			recordDate: 2013-07-17 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 10
			recordDate: 2013-07-17 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 11
			recordDate: 2013-07-17 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 12
			recordDate: 2013-07-17 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 13
			recordDate: 2013-07-17 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 14
			recordDate: 2013-07-17 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 15
			recordDate: 2013-07-17 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 16
			recordDate: 2013-07-17 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 17
			recordDate: 2013-07-17 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 18
			recordDate: 2013-07-17 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 19
			recordDate: 2013-06-10 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 20
			recordDate: 2013-04-09 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 21
			recordDate: 2013-03-28 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 22
			recordDate: 2013-03-28 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 23
			recordDate: 2013-03-28 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 24
			recordDate: 2013-03-28 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 25
			recordDate: 2013-03-28 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 26
			recordDate: 2013-03-17 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 27
			recordDate: 2013-03-13 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 28
			recordDate: 2013-03-12 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 29
			recordDate: 2013-03-12 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 30
			recordDate: 2013-03-12 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 31
			recordDate: 2013-03-12 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D690996
	applIdStr: 29448445
	appl_id_txt: 29448445
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29448445
	appSubCls: 554300
	appLocation: ELECTRONIC
	appAttrDockNumber: 201300142
	appType: Design
	appTypeFacet: Design
	appCustNumber: 70829
	applicants:
		№ 0
			nameLineOne: TARGET BRANDS, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 3
	applicantsFacet: {|TARGET BRANDS, INC.||||Minneapolis|MN|US|3}
	appStatusDate: 2013-09-18T10:27:48Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2013-10-08T04:00:00Z
	APP_IND: 1
	appExamName: DONNELLY, KELLEY A
	appExamNameFacet: DONNELLY, KELLEY A
	firstInventorFile: No
	appClsSubCls: D07/554300
	appClsSubClsFacet: D07/554300
	applId: 29448445
	patentTitle: TRAY
	appIntlPubNumber: 
	appConfrNumber: 1015
	appFilingDate: 2013-03-12T04:00:00Z
	firstNamedApplicant:  TARGET BRANDS, INC.
	firstNamedApplicantFacet:  TARGET BRANDS, INC.
	inventors:
		№ 0
			nameLineOne: Stacy Lee ABEL
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Maple Grove, 
			geoCode: MN
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Bonnie Leigh VIG
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 2
	appCls: D07
	_version_: 1580433807291449345
	lastUpdatedTimestamp: 2017-10-05T15:58:50.793Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 15643a99-bd41-42c2-b3a7-4350025e7055
	responseHeader:
			status: 0
			QTime: 16
