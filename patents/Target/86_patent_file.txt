DOCUMENT_BODY:
	appGrpArtNumber: 2915
	appGrpArtNumberFacet: 2915
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D711707
	applIdStr: 29456346
	appl_id_txt: 29456346
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29456346
	appSubCls: 018000
	appLocation: ELECTRONIC
	appAttrDockNumber: 201300217
	appType: Design
	appTypeFacet: Design
	appCustNumber: 67260
	applicants:
		№ 0
			nameLineOne: Target Brands, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 3
	applicantsFacet: {|Target Brands, Inc.||||Minneapolis|MN|US|3}
	appStatusDate: 2014-08-06T13:48:32Z
	LAST_MOD_TS: 2017-11-06T11:50:07Z
	patentIssueDate: 2014-08-26T04:00:00Z
	APP_IND: 1
	appExamName: PRICE, IEISHA N
	appExamNameFacet: PRICE, IEISHA N
	firstInventorFile: Yes
	appClsSubCls: D08/018000
	appClsSubClsFacet: D08/018000
	applId: 29456346
	patentTitle: BOTTLE OPENER
	appIntlPubNumber: 
	transactions:
		№ 0
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 1
			recordDate: 2014-08-26 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2014-08-07 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2014-08-06 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2014-07-24 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2014-07-22 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2014-07-07 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2014-07-07 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2014-06-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2014-06-18 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2014-06-18 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2014-06-15 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2014-06-11 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 13
			recordDate: 2014-05-28 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 14
			recordDate: 2014-05-27 00:00:00
			code: LTDR
			description: Incoming Letter Pertaining to the Drawings
		№ 15
			recordDate: 2014-05-27 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 16
			recordDate: 2014-02-26 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 17
			recordDate: 2014-02-10 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 18
			recordDate: 2013-12-16 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 19
			recordDate: 2013-12-13 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 20
			recordDate: 2013-08-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 21
			recordDate: 2013-06-12 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 22
			recordDate: 2013-06-12 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 23
			recordDate: 2013-06-12 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 24
			recordDate: 2013-06-12 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 25
			recordDate: 2013-06-12 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 26
			recordDate: 2013-06-03 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 27
			recordDate: 2013-05-30 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 28
			recordDate: 2013-05-30 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 29
			recordDate: 2013-05-30 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 30
			recordDate: 2013-05-30 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 31
			recordDate: 2013-05-30 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 32
			recordDate: 2013-05-30 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appConfrNumber: 7945
	appFilingDate: 2013-05-30T04:00:00Z
	firstNamedApplicant:  Target Brands, Inc.
	firstNamedApplicantFacet:  Target Brands, Inc.
	inventors:
		№ 0
			nameLineOne: Daniel J. Hoyord
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: St. Paul, 
			geoCode: MN
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Emily T. Brown
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 2
	appCls: D08
	_version_: 1583336840361934854
	lastUpdatedTimestamp: 2017-11-06T17:01:18.876Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 537468de-e1bf-4bd1-bcc4-2db951c3bc2d
	responseHeader:
			status: 0
			QTime: 13
