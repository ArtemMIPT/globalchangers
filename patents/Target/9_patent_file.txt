DOCUMENT_BODY:
	appGrpArtNumber: 2911
	appGrpArtNumberFacet: 2911
	transactions:
		№ 0
			recordDate: 2013-07-23 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2013-07-03 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 2
			recordDate: 2013-06-24 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 3
			recordDate: 2013-06-18 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 4
			recordDate: 2013-06-17 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 5
			recordDate: 2013-06-17 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 6
			recordDate: 2013-06-13 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 7
			recordDate: 2013-06-12 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 8
			recordDate: 2013-06-11 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 9
			recordDate: 2013-05-31 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 10
			recordDate: 2013-05-31 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 11
			recordDate: 2013-05-31 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 12
			recordDate: 2013-05-30 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 13
			recordDate: 2013-05-30 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 14
			recordDate: 2013-05-09 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 15
			recordDate: 2013-05-06 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 16
			recordDate: 2012-12-20 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 17
			recordDate: 2012-11-29 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 18
			recordDate: 2012-11-29 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 19
			recordDate: 2012-11-29 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 20
			recordDate: 2012-11-26 00:00:00
			code: FLFEE
			description: Payment of additional filing fee/Preexam
		№ 21
			recordDate: 2012-11-26 00:00:00
			code: OATHDECL
			description: A statement by one or more inventors satisfying the requirement under 35 USC 115, Oath of the Applic
		№ 22
			recordDate: 2012-10-31 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 23
			recordDate: 2012-10-31 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 24
			recordDate: 2012-10-16 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 25
			recordDate: 2012-10-16 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 26
			recordDate: 2012-10-16 00:00:00
			code: INCD
			description: Notice Mailed--Application Incomplete--Filing Date Assigned 
		№ 27
			recordDate: 2012-10-01 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 28
			recordDate: 2012-10-01 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 29
			recordDate: 2012-10-01 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 30
			recordDate: 2012-09-29 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 31
			recordDate: 2012-09-28 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 32
			recordDate: 2012-09-28 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 33
			recordDate: 2012-09-28 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D686629
	applIdStr: 29433411
	appl_id_txt: 29433411
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29433411
	appSubCls: 447000
	appLocation: ELECTRONIC
	appAttrDockNumber: 201105106
	appType: Design
	appTypeFacet: Design
	appCustNumber: 70829
	applicants:
		№ 0
			nameLineOne: TARGET BRANDS, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 4
	applicantsFacet: {|TARGET BRANDS, INC.||||Minneapolis|MN|US|4}
	appStatusDate: 2013-07-03T11:26:47Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2013-07-23T04:00:00Z
	APP_IND: 1
	appExamName: LEE, ANGELA J
	appExamNameFacet: LEE, ANGELA J
	firstInventorFile: No
	appClsSubCls: D14/447000
	appClsSubClsFacet: D14/447000
	applId: 29433411
	patentTitle: DEVICE DISPLAY FIXTURE
	appIntlPubNumber: 
	appConfrNumber: 1047
	appFilingDate: 2012-09-28T04:00:00Z
	firstNamedApplicant:  TARGET BRANDS, INC.
	firstNamedApplicantFacet:  TARGET BRANDS, INC.
	inventors:
		№ 0
			nameLineOne: Nick Q. Trinh
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Joseph H. Bowser
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Justin T. Werth
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Nowthen, 
			geoCode: MN
			country: (US)
			rankNo: 3
	appCls: D14
	_version_: 1580433804783255554
	lastUpdatedTimestamp: 2017-10-05T15:58:48.382Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 229ca736-4b25-4baa-97a7-d6c8e2eb0309
	responseHeader:
			status: 0
			QTime: 13
