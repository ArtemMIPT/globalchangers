DOCUMENT_BODY:
	appGrpArtNumber: 2916
	appGrpArtNumberFacet: 2916
	transactions:
		№ 0
			recordDate: 2014-06-24 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2014-06-05 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 2
			recordDate: 2014-06-04 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 3
			recordDate: 2014-05-19 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 4
			recordDate: 2014-05-17 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 5
			recordDate: 2014-05-02 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 6
			recordDate: 2014-05-02 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 7
			recordDate: 2014-04-08 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 8
			recordDate: 2014-04-06 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 9
			recordDate: 2014-04-04 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 10
			recordDate: 2014-02-11 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 11
			recordDate: 2013-04-03 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 12
			recordDate: 2013-04-03 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 13
			recordDate: 2013-04-03 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 14
			recordDate: 2013-04-03 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 15
			recordDate: 2013-04-03 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 16
			recordDate: 2013-03-19 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 17
			recordDate: 2013-03-16 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 18
			recordDate: 2013-03-14 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 19
			recordDate: 2013-03-14 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D707498
	applIdStr: 29449030
	appl_id_txt: 29449030
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29449030
	appSubCls: 622000
	appLocation: ELECTRONIC
	appAttrDockNumber: 201300144
	appType: Design
	appTypeFacet: Design
	appCustNumber: 70829
	applicants:
		№ 0
			nameLineOne: TARGET BRANDS, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 2
	applicantsFacet: {|TARGET BRANDS, INC.||||Minneapolis|MN|US|2}
	appStatusDate: 2014-06-04T14:18:03Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2014-06-24T04:00:00Z
	APP_IND: 1
	appExamName: WALLACE, TERRY A
	appExamNameFacet: WALLACE, TERRY A
	firstInventorFile: No
	appClsSubCls: D07/622000
	appClsSubClsFacet: D07/622000
	applId: 29449030
	patentTitle: CUP HANDLE
	appIntlPubNumber: 
	appConfrNumber: 8609
	appFilingDate: 2013-03-14T04:00:00Z
	firstNamedApplicant:  TARGET BRANDS, INC.
	firstNamedApplicantFacet:  TARGET BRANDS, INC.
	inventors:
		№ 0
			nameLineOne: Jessica Mary WILSON
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 1
	appCls: D07
	_version_: 1580433807372189700
	lastUpdatedTimestamp: 2017-10-05T15:58:50.889Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 15643a99-bd41-42c2-b3a7-4350025e7055
	responseHeader:
			status: 0
			QTime: 16
