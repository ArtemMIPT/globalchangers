DOCUMENT_BODY:
	appGrpArtNumber: 2913
	appGrpArtNumberFacet: 2913
	transactions:
		№ 0
			recordDate: 2013-08-06 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2013-07-17 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 2
			recordDate: 2013-07-01 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 3
			recordDate: 2013-06-28 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 4
			recordDate: 2013-06-26 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 5
			recordDate: 2013-06-26 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 6
			recordDate: 2013-05-31 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 7
			recordDate: 2013-05-30 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 8
			recordDate: 2013-05-30 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 9
			recordDate: 2012-12-17 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 10
			recordDate: 2012-12-10 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 11
			recordDate: 2012-11-14 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 12
			recordDate: 2012-11-14 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 13
			recordDate: 2012-11-13 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 14
			recordDate: 2012-11-05 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 15
			recordDate: 2012-10-31 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 16
			recordDate: 2012-10-31 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 17
			recordDate: 2012-10-31 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 18
			recordDate: 2012-10-31 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 19
			recordDate: 2012-10-31 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D687233
	applIdStr: 29435996
	appl_id_txt: 29435996
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29435996
	appSubCls: 369000
	appLocation: ELECTRONIC
	appAttrDockNumber: 201202874
	appType: Design
	appTypeFacet: Design
	appCustNumber: 67260
	applicants:
		№ 0
			nameLineOne: Target Brands, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 5
	applicantsFacet: {|Target Brands, Inc.||||Minneapolis|MN|US|5}
	appStatusDate: 2013-07-17T09:47:50Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2013-08-06T04:00:00Z
	APP_IND: 1
	appExamName: DE, MIMOSA
	appExamNameFacet: DE, MIMOSA
	firstInventorFile: No
	appClsSubCls: D06/369000
	appClsSubClsFacet: D06/369000
	applId: 29435996
	patentTitle: CHAIR
	appIntlPubNumber: 
	appConfrNumber: 8425
	appFilingDate: 2012-10-31T04:00:00Z
	firstNamedApplicant:  Target Brands, Inc.
	firstNamedApplicantFacet:  Target Brands, Inc.
	inventors:
		№ 0
			nameLineOne: Sara L. Pedersen
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Katherine R. Wittenberg
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Saint Paul, 
			geoCode: MN
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Chad M. Bogdan
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Saint Paul, 
			geoCode: MN
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: George K. Smithwick
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Blaine, 
			geoCode: MN
			country: (US)
			rankNo: 4
	appCls: D06
	_version_: 1580433805255114752
	lastUpdatedTimestamp: 2017-10-05T15:58:48.859Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 229ca736-4b25-4baa-97a7-d6c8e2eb0309
	responseHeader:
			status: 0
			QTime: 13
