DOCUMENT_BODY:
	appGrpArtNumber: 2916
	appGrpArtNumberFacet: 2916
	transactions:
		№ 0
			recordDate: 2014-10-07 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2014-09-18 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 2
			recordDate: 2014-09-17 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 3
			recordDate: 2014-08-28 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 4
			recordDate: 2014-08-26 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 5
			recordDate: 2014-08-21 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 6
			recordDate: 2014-08-21 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 7
			recordDate: 2014-08-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 8
			recordDate: 2014-08-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 9
			recordDate: 2014-08-14 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 10
			recordDate: 2014-08-11 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 11
			recordDate: 2014-08-11 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 12
			recordDate: 2014-07-16 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 13
			recordDate: 2014-07-16 00:00:00
			code: ELC.
			description: Response to Election / Restriction Filed
		№ 14
			recordDate: 2014-06-04 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 15
			recordDate: 2014-06-04 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 16
			recordDate: 2014-06-04 00:00:00
			code: MCTRS
			description: Mail Restriction Requirement
		№ 17
			recordDate: 2014-05-30 00:00:00
			code: CTRS
			description: Restriction/Election Requirement
		№ 18
			recordDate: 2014-05-30 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 19
			recordDate: 2014-05-30 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 20
			recordDate: 2013-12-06 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 21
			recordDate: 2013-12-06 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 22
			recordDate: 2012-12-12 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 23
			recordDate: 2012-11-25 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 24
			recordDate: 2012-10-12 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 25
			recordDate: 2012-10-12 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 26
			recordDate: 2012-10-12 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 27
			recordDate: 2012-09-27 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 28
			recordDate: 2012-09-26 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 29
			recordDate: 2012-09-26 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 30
			recordDate: 2012-09-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 31
			recordDate: 2012-09-26 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 32
			recordDate: 2012-09-26 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D714644
	applIdStr: 29433119
	appl_id_txt: 29433119
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29433119
	appSubCls: 500000
	appLocation: ELECTRONIC
	appAttrDockNumber: 201202601
	appType: Design
	appTypeFacet: Design
	appCustNumber: 67260
	applicants:
		№ 0
			nameLineOne: Target Brands, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 5
	applicantsFacet: {|Target Brands, Inc.||||Minneapolis|MN|US|5}
	appStatusDate: 2014-09-17T14:40:13Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2014-10-07T04:00:00Z
	APP_IND: 1
	appExamName: MEYROW, DANA LYNNE
	appExamNameFacet: MEYROW, DANA LYNNE
	firstInventorFile: No
	appClsSubCls: D09/500000
	appClsSubClsFacet: D09/500000
	applId: 29433119
	patentTitle: BOTTLE
	appIntlPubNumber: 
	appConfrNumber: 7573
	appFilingDate: 2012-09-26T04:00:00Z
	firstNamedApplicant:  Target Brands, Inc.
	firstNamedApplicantFacet:  Target Brands, Inc.
	inventors:
		№ 0
			nameLineOne: Pathakone  Wagner
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Jennifer J. Gunderson
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: St. Paul, 
			geoCode: MN
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Jessica M. Wilson
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Stacy L. Abel
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Maple Grove, 
			geoCode: MN
			country: (US)
			rankNo: 4
	appCls: D09
	_version_: 1580433804759138309
	lastUpdatedTimestamp: 2017-10-05T15:58:48.382Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 229ca736-4b25-4baa-97a7-d6c8e2eb0309
	responseHeader:
			status: 0
			QTime: 13
