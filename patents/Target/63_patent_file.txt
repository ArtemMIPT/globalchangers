DOCUMENT_BODY:
	appGrpArtNumber: 2911
	appGrpArtNumberFacet: 2911
	transactions:
		№ 0
			recordDate: 2014-04-29 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2014-04-09 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 2
			recordDate: 2014-03-28 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 3
			recordDate: 2014-03-28 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 4
			recordDate: 2014-03-04 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 5
			recordDate: 2014-03-04 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 6
			recordDate: 2014-01-22 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 7
			recordDate: 2014-01-20 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 8
			recordDate: 2014-01-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 9
			recordDate: 2014-01-14 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 10
			recordDate: 2014-01-14 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 11
			recordDate: 2014-01-14 00:00:00
			code: ACPA
			description: Continuing Prosecution Application - Continuation (ACPA)
		№ 12
			recordDate: 2014-01-14 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 13
			recordDate: 2014-01-14 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 14
			recordDate: 2014-01-02 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 15
			recordDate: 2013-12-30 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 16
			recordDate: 2013-08-05 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 17
			recordDate: 2013-06-25 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 18
			recordDate: 2013-06-25 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 19
			recordDate: 2013-06-25 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 20
			recordDate: 2013-04-02 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 21
			recordDate: 2013-04-02 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 22
			recordDate: 2013-04-02 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 23
			recordDate: 2013-04-01 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 24
			recordDate: 2013-04-01 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 25
			recordDate: 2013-04-01 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 26
			recordDate: 2013-03-18 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 27
			recordDate: 2013-03-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 28
			recordDate: 2013-03-14 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 29
			recordDate: 2013-03-14 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 30
			recordDate: 2013-03-14 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 31
			recordDate: 2013-03-13 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D703904
	applIdStr: 29448707
	appl_id_txt: 29448707
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29448707
	appSubCls: 020000
	appLocation: ELECTRONIC
	appAttrDockNumber: 201203034
	appType: Design
	appTypeFacet: Design
	appCustNumber: 67260
	applicants:
		№ 0
			nameLineOne: Target Brands, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 5
	applicantsFacet: {|Target Brands, Inc.||||Minneapolis|MN|US|5}
	appStatusDate: 2014-04-09T14:07:04Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2014-04-29T04:00:00Z
	APP_IND: 1
	appExamName: RAMIREZ, CYNTHIA
	appExamNameFacet: RAMIREZ, CYNTHIA
	firstInventorFile: No
	appClsSubCls: D34/020000
	appClsSubClsFacet: D34/020000
	applId: 29448707
	patentTitle: POINT-OF-SALE CART
	appIntlPubNumber: 
	appConfrNumber: 8411
	appFilingDate: 2013-03-13T04:00:00Z
	firstNamedApplicant:  Target Brands, Inc.
	firstNamedApplicantFacet:  Target Brands, Inc.
	inventors:
		№ 0
			nameLineOne: Robert D. Peota
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: David J. Floersch
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 4
		№ 2
			nameLineOne: Mitchell  Knoll
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Eagan, 
			geoCode: MN
			country: (US)
			rankNo: 1
		№ 3
			nameLineOne: Timothy J. Martell
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Brooklyn Park, 
			geoCode: MN
			country: (US)
			rankNo: 3
	appCls: D34
	_version_: 1580433807315566596
	lastUpdatedTimestamp: 2017-10-05T15:58:50.824Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 15643a99-bd41-42c2-b3a7-4350025e7055
	responseHeader:
			status: 0
			QTime: 16
