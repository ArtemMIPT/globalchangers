DOCUMENT_BODY:
	appGrpArtNumber: 2913
	appGrpArtNumberFacet: 2913
	transactions:
		№ 0
			recordDate: 2013-06-18 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2013-05-29 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 2
			recordDate: 2013-05-14 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 3
			recordDate: 2013-05-14 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 4
			recordDate: 2013-05-13 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 5
			recordDate: 2013-05-09 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 6
			recordDate: 2013-05-09 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2013-05-09 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2013-04-16 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 9
			recordDate: 2013-04-15 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 10
			recordDate: 2013-04-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 11
			recordDate: 2013-03-25 00:00:00
			code: CTRT
			description: Telephonic restriction
		№ 12
			recordDate: 2013-03-25 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 13
			recordDate: 2012-12-16 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 14
			recordDate: 2012-12-12 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 15
			recordDate: 2012-11-01 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 16
			recordDate: 2012-11-01 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 17
			recordDate: 2012-11-01 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 18
			recordDate: 2012-10-17 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 19
			recordDate: 2012-10-16 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 20
			recordDate: 2012-10-16 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D684410
	applIdStr: 29434714
	appl_id_txt: 29434714
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29434714
	appSubCls: 661000
	appLocation: ELECTRONIC
	appAttrDockNumber: 201201450
	appType: Design
	appTypeFacet: Design
	appCustNumber: 67260
	applicants:
		№ 0
			nameLineOne: Target Brands, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 2
	applicantsFacet: {|Target Brands, Inc.||||Minneapolis|MN|US|2}
	appStatusDate: 2013-05-29T09:08:41Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2013-06-18T04:00:00Z
	APP_IND: 1
	appExamName: DONNELLY, KELLEY A
	appExamNameFacet: DONNELLY, KELLEY A
	firstInventorFile: No
	appClsSubCls: D06/661000
	appClsSubClsFacet: D06/661000
	applId: 29434714
	patentTitle: DISPLAY SHELF
	appIntlPubNumber: 
	appConfrNumber: 7228
	appFilingDate: 2012-10-16T04:00:00Z
	firstNamedApplicant:  Target Brands, Inc.
	firstNamedApplicantFacet:  Target Brands, Inc.
	inventors:
		№ 0
			nameLineOne: Laura L. Hawkins
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 1
	appCls: D06
	_version_: 1580433804976193539
	lastUpdatedTimestamp: 2017-10-05T15:58:48.588Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 229ca736-4b25-4baa-97a7-d6c8e2eb0309
	responseHeader:
			status: 0
			QTime: 13
