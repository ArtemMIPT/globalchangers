DOCUMENT_BODY:
	appGrpArtNumber: 2918
	appGrpArtNumberFacet: 2918
	transactions:
		№ 0
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 1
			recordDate: 2013-11-12 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2013-10-23 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 3
			recordDate: 2013-10-14 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 4
			recordDate: 2013-10-10 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 5
			recordDate: 2013-09-25 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 6
			recordDate: 2013-09-25 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 7
			recordDate: 2013-09-10 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 8
			recordDate: 2013-09-08 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 9
			recordDate: 2013-09-05 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 10
			recordDate: 2013-08-02 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 11
			recordDate: 2013-06-13 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 12
			recordDate: 2013-06-13 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 13
			recordDate: 2013-06-13 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 14
			recordDate: 2013-06-13 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 15
			recordDate: 2013-06-13 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 16
			recordDate: 2013-06-03 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 17
			recordDate: 2013-05-30 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 18
			recordDate: 2013-05-30 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 19
			recordDate: 2013-05-30 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 20
			recordDate: 2013-05-30 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 21
			recordDate: 2013-05-30 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 22
			recordDate: 2013-05-30 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D693180
	applIdStr: 29456322
	appl_id_txt: 29456322
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29456322
	appSubCls: 555000
	appLocation: ELECTRONIC
	appAttrDockNumber: 201301296
	appType: Design
	appTypeFacet: Design
	appCustNumber: 70829
	applicants:
		№ 0
			nameLineOne: TARGET BRANDS, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 3
	applicantsFacet: {|TARGET BRANDS, INC.||||Minneapolis|MN|US|3}
	appStatusDate: 2013-10-23T08:54:11Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2013-11-12T05:00:00Z
	APP_IND: 1
	appExamName: UNDERWOOD, CYNTHIA R
	appExamNameFacet: UNDERWOOD, CYNTHIA R
	firstInventorFile: Yes
	appClsSubCls: D07/555000
	appClsSubClsFacet: D07/555000
	applId: 29456322
	patentTitle: DIVIDED DISH
	appIntlPubNumber: 
	appConfrNumber: 2219
	appFilingDate: 2013-05-30T04:00:00Z
	firstNamedApplicant:  TARGET BRANDS, INC.
	firstNamedApplicantFacet:  TARGET BRANDS, INC.
	inventors:
		№ 0
			nameLineOne: William  PETERSON
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Kent D. KATTERHEINRICH
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Shoreview, 
			geoCode: MN
			country: (US)
			rankNo: 2
	appCls: D07
	_version_: 1580433808575954948
	lastUpdatedTimestamp: 2017-10-05T15:58:52.026Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 537468de-e1bf-4bd1-bcc4-2db951c3bc2d
	responseHeader:
			status: 0
			QTime: 13
