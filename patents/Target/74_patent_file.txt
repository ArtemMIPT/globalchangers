DOCUMENT_BODY:
	appGrpArtNumber: 2918
	appGrpArtNumberFacet: 2918
	transactions:
		№ 0
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 1
			recordDate: 2014-07-22 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2014-07-03 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2014-07-02 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2014-06-12 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2014-06-12 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2014-06-04 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2014-06-04 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2014-05-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2014-05-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2014-05-23 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2014-05-16 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 12
			recordDate: 2014-05-16 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 13
			recordDate: 2014-05-16 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 14
			recordDate: 2014-05-10 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 15
			recordDate: 2014-05-07 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 16
			recordDate: 2014-05-07 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 17
			recordDate: 2014-01-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 18
			recordDate: 2013-07-17 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 19
			recordDate: 2013-07-17 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 20
			recordDate: 2013-07-01 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 21
			recordDate: 2013-05-14 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 22
			recordDate: 2013-05-14 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 23
			recordDate: 2013-05-14 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 24
			recordDate: 2013-05-14 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 25
			recordDate: 2013-05-14 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 26
			recordDate: 2013-05-02 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 27
			recordDate: 2013-05-01 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 28
			recordDate: 2013-05-01 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 29
			recordDate: 2013-05-01 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 30
			recordDate: 2013-05-01 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 31
			recordDate: 2013-05-01 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D709332
	applIdStr: 29453680
	appl_id_txt: 29453680
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29453680
	appSubCls: 566000
	appLocation: ELECTRONIC
	appAttrDockNumber: 201300880
	appType: Design
	appTypeFacet: Design
	appCustNumber: 70829
	applicants:
		№ 0
			nameLineOne: TARGET BRANDS, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 3
	applicantsFacet: {|TARGET BRANDS, INC.||||Minneapolis|MN|US|3}
	appStatusDate: 2014-07-02T13:43:28Z
	LAST_MOD_TS: 2017-04-18T10:12:30Z
	patentIssueDate: 2014-07-22T04:00:00Z
	APP_IND: 1
	appExamName: MILLER, BRETT A
	appExamNameFacet: MILLER, BRETT A
	firstInventorFile: Yes
	appClsSubCls: D07/566000
	appClsSubClsFacet: D07/566000
	applId: 29453680
	patentTitle: BOWL
	appIntlPubNumber: 
	appConfrNumber: 8739
	appFilingDate: 2013-05-01T04:00:00Z
	firstNamedApplicant:  TARGET BRANDS, INC.
	firstNamedApplicantFacet:  TARGET BRANDS, INC.
	inventors:
		№ 0
			nameLineOne: Kent D. KATTERHEINRICH
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Shoreview, 
			geoCode: MN
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Amanda  MILLER
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 2
	appCls: D07
	_version_: 1580433808086269953
	lastUpdatedTimestamp: 2017-10-05T15:58:51.564Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 15643a99-bd41-42c2-b3a7-4350025e7055
	responseHeader:
			status: 0
			QTime: 16
