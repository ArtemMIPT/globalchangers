DOCUMENT_BODY:
	appGrpArtNumber: 2913
	appGrpArtNumberFacet: 2913
	transactions:
		№ 0
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 1
			recordDate: 2014-12-02 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2014-11-13 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2014-11-12 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2014-10-31 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2014-10-29 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2014-10-28 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2014-10-28 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2014-10-10 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2014-10-10 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2014-10-10 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2014-10-07 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2014-08-28 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 13
			recordDate: 2013-08-12 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 14
			recordDate: 2013-04-22 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 15
			recordDate: 2013-04-22 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 16
			recordDate: 2013-04-22 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 17
			recordDate: 2013-04-22 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 18
			recordDate: 2013-04-22 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 19
			recordDate: 2013-04-14 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 20
			recordDate: 2013-04-11 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 21
			recordDate: 2013-04-10 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 22
			recordDate: 2013-04-10 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 23
			recordDate: 2013-04-10 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 24
			recordDate: 2013-04-10 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D718896
	applIdStr: 29451941
	appl_id_txt: 29451941
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29451941
	appSubCls: 093000
	appLocation: ELECTRONIC
	appAttrDockNumber: 201202920
	appType: Design
	appTypeFacet: Design
	appCustNumber: 67260
	applicants:
		№ 0
			nameLineOne: Target Brands, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 5
	applicantsFacet: {|Target Brands, Inc.||||Minneapolis|MN|US|5}
	appStatusDate: 2014-11-12T15:17:22Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2014-12-02T05:00:00Z
	APP_IND: 1
	appExamName: VINSON, BRIAN N
	appExamNameFacet: VINSON, BRIAN N
	firstInventorFile: Yes
	appClsSubCls: D26/093000
	appClsSubClsFacet: D26/093000
	applId: 29451941
	patentTitle: LAMP BASE
	appIntlPubNumber: 
	appConfrNumber: 4077
	appFilingDate: 2013-04-10T04:00:00Z
	firstNamedApplicant:  Target Brands, Inc.
	firstNamedApplicantFacet:  Target Brands, Inc.
	inventors:
		№ 0
			nameLineOne: Scott A. Meyer
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Waconia, 
			geoCode: MN
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Patrick M. Greenwell
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Woodbury, 
			geoCode: MN
			country: (US)
			rankNo: 4
		№ 2
			nameLineOne: Joseph P. Foley
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: St. Paul, 
			geoCode: MN
			country: (US)
			rankNo: 1
		№ 3
			nameLineOne: Pathakone  Wagner
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 3
	appCls: D26
	_version_: 1580433807816785921
	lastUpdatedTimestamp: 2017-10-05T15:58:51.289Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 15643a99-bd41-42c2-b3a7-4350025e7055
	responseHeader:
			status: 0
			QTime: 16
