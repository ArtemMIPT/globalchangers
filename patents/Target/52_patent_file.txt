DOCUMENT_BODY:
	appGrpArtNumber: 2913
	appGrpArtNumberFacet: 2913
	transactions:
		№ 0
			recordDate: 2013-09-24 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2013-09-04 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 2
			recordDate: 2013-08-19 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 3
			recordDate: 2013-08-07 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 4
			recordDate: 2013-08-06 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 5
			recordDate: 2013-08-06 00:00:00
			code: DRWF
			description: Workflow - Drawings Finished
		№ 6
			recordDate: 2013-08-06 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 7
			recordDate: 2013-07-25 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 8
			recordDate: 2013-07-24 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 9
			recordDate: 2013-07-23 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 10
			recordDate: 2013-05-09 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 11
			recordDate: 2013-03-18 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 12
			recordDate: 2013-03-18 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 13
			recordDate: 2013-03-18 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 14
			recordDate: 2013-03-15 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 15
			recordDate: 2013-03-05 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 16
			recordDate: 2013-03-04 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 17
			recordDate: 2013-03-04 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 18
			recordDate: 2013-03-04 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 19
			recordDate: 2013-03-04 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 20
			recordDate: 2013-03-04 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 21
			recordDate: 2013-03-04 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D690156
	applIdStr: 29447463
	appl_id_txt: 29447463
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29447463
	appSubCls: 401200
	appLocation: ELECTRONIC
	appAttrDockNumber: 201204756
	appType: Design
	appTypeFacet: Design
	appCustNumber: 70829
	applicants:
		№ 0
			nameLineOne: TARGET BRANDS, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 3
	applicantsFacet: {|TARGET BRANDS, INC.||||Minneapolis|MN|US|3}
	appStatusDate: 2013-09-04T09:37:19Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2013-09-24T04:00:00Z
	APP_IND: 1
	appExamName: PHAM, RICKY NGOC
	appExamNameFacet: PHAM, RICKY NGOC
	firstInventorFile: No
	appClsSubCls: D07/401200
	appClsSubClsFacet: D07/401200
	applId: 29447463
	patentTitle: EATING UTENSIL
	appIntlPubNumber: 
	appConfrNumber: 1005
	appFilingDate: 2013-03-04T05:00:00Z
	firstNamedApplicant:  TARGET BRANDS, INC.
	firstNamedApplicantFacet:  TARGET BRANDS, INC.
	inventors:
		№ 0
			nameLineOne: Elizabeth H. APPLE
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Kent D. KATTERHEINRICH
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Shoreview, 
			geoCode: MN
			country: (US)
			rankNo: 2
	appCls: D07
	_version_: 1580433807127871491
	lastUpdatedTimestamp: 2017-10-05T15:58:50.648Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 95463814-a5ed-47d1-a0d3-38229449d3d0
	responseHeader:
			status: 0
			QTime: 13
