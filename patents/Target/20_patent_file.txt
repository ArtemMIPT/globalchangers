DOCUMENT_BODY:
	appGrpArtNumber: 2918
	appGrpArtNumberFacet: 2918
	transactions:
		№ 0
			recordDate: 2014-04-08 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2014-03-19 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 2
			recordDate: 2014-03-11 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 3
			recordDate: 2014-03-10 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 4
			recordDate: 2014-02-04 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 5
			recordDate: 2014-02-04 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 6
			recordDate: 2014-01-13 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 7
			recordDate: 2014-01-12 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 8
			recordDate: 2014-01-09 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 9
			recordDate: 2014-01-09 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 10
			recordDate: 2014-01-09 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 11
			recordDate: 2013-02-20 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 12
			recordDate: 2012-11-14 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 13
			recordDate: 2012-11-14 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 14
			recordDate: 2012-11-14 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 15
			recordDate: 2012-11-14 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 16
			recordDate: 2012-11-05 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 17
			recordDate: 2012-10-31 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 18
			recordDate: 2012-10-31 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 19
			recordDate: 2012-10-31 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 20
			recordDate: 2012-10-31 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 21
			recordDate: 2012-10-31 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D702081
	applIdStr: 29436052
	appl_id_txt: 29436052
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29436052
	appSubCls: 396500
	appLocation: ELECTRONIC
	appAttrDockNumber: 201202683
	appType: Design
	appTypeFacet: Design
	appCustNumber: 70829
	applicants:
		№ 0
			nameLineOne: TARGET BRANDS, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 3
	applicantsFacet: {|TARGET BRANDS, INC.||||Minneapolis|MN|US|3}
	appStatusDate: 2014-03-19T09:18:00Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2014-04-08T04:00:00Z
	APP_IND: 1
	appExamName: PANDOZZI, MARIANNE N
	appExamNameFacet: PANDOZZI, MARIANNE N
	firstInventorFile: No
	appClsSubCls: D07/396500
	appClsSubClsFacet: D07/396500
	applId: 29436052
	patentTitle: PLATTER
	appIntlPubNumber: 
	appConfrNumber: 3663
	appFilingDate: 2012-10-31T04:00:00Z
	firstNamedApplicant:  TARGET BRANDS, INC.
	firstNamedApplicantFacet:  TARGET BRANDS, INC.
	inventors:
		№ 0
			nameLineOne: Richard P. RIEDEL
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Blaine, 
			geoCode: MN
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Elizabeth H. APPLE
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 2
	appCls: D07
	_version_: 1580433805243580416
	lastUpdatedTimestamp: 2017-10-05T15:58:48.836Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: ac106c6a-b99e-45dc-a00a-0a8286dadf47
	responseHeader:
			status: 0
			QTime: 13
