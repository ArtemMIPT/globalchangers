DOCUMENT_BODY:
	appGrpArtNumber: 2913
	appGrpArtNumberFacet: 2913
	transactions:
		№ 0
			recordDate: 2013-05-21 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2013-05-01 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 2
			recordDate: 2013-04-11 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 3
			recordDate: 2013-04-10 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 4
			recordDate: 2013-04-09 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 5
			recordDate: 2013-04-09 00:00:00
			code: DRWF
			description: Workflow - Drawings Finished
		№ 6
			recordDate: 2013-04-09 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 7
			recordDate: 2013-02-20 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 8
			recordDate: 2013-02-11 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 9
			recordDate: 2013-02-11 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 10
			recordDate: 2013-01-22 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 11
			recordDate: 2012-12-21 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 12
			recordDate: 2012-12-21 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 13
			recordDate: 2012-12-21 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 14
			recordDate: 2012-12-21 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 15
			recordDate: 2012-12-12 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 16
			recordDate: 2012-12-11 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 17
			recordDate: 2012-12-11 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 18
			recordDate: 2012-12-11 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 19
			recordDate: 2012-12-11 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 20
			recordDate: 2012-12-11 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D682583
	applIdStr: 29439449
	appl_id_txt: 29439449
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29439449
	appSubCls: 475000
	appLocation: ELECTRONIC
	appAttrDockNumber: 201202869
	appType: Design
	appTypeFacet: Design
	appCustNumber: 67260
	applicants:
		№ 0
			nameLineOne: Target Brands, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 2
	applicantsFacet: {|Target Brands, Inc.||||Minneapolis|MN|US|2}
	appStatusDate: 2013-05-01T10:44:16Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2013-05-21T04:00:00Z
	APP_IND: 1
	appExamName: DONNELLY, KELLEY A
	appExamNameFacet: DONNELLY, KELLEY A
	firstInventorFile: No
	appClsSubCls: D06/475000
	appClsSubClsFacet: D06/475000
	applId: 29439449
	patentTitle: DISPLAY RACK
	appIntlPubNumber: 
	appConfrNumber: 3324
	appFilingDate: 2012-12-11T05:00:00Z
	firstNamedApplicant:  Target Brands, Inc.
	firstNamedApplicantFacet:  Target Brands, Inc.
	inventors:
		№ 0
			nameLineOne: Laura L. Hawkins
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 1
	appCls: D06
	_version_: 1580433805818200065
	lastUpdatedTimestamp: 2017-10-05T15:58:49.388Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 95463814-a5ed-47d1-a0d3-38229449d3d0
	responseHeader:
			status: 0
			QTime: 13
