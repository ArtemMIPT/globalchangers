DOCUMENT_BODY:
	appGrpArtNumber: 2917
	appGrpArtNumberFacet: 2917
	transactions:
		№ 0
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 1
			recordDate: 2013-10-15 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2013-09-25 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 3
			recordDate: 2013-09-11 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 4
			recordDate: 2013-09-07 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 5
			recordDate: 2013-08-28 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 6
			recordDate: 2013-08-28 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 7
			recordDate: 2013-08-19 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 8
			recordDate: 2013-08-16 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 9
			recordDate: 2013-08-12 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 10
			recordDate: 2013-08-12 00:00:00
			code: CTRT
			description: Telephonic restriction
		№ 11
			recordDate: 2013-07-16 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 12
			recordDate: 2013-05-10 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 13
			recordDate: 2013-05-10 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 14
			recordDate: 2013-05-10 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 15
			recordDate: 2013-05-10 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 16
			recordDate: 2013-05-10 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 17
			recordDate: 2013-04-30 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 18
			recordDate: 2013-04-29 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 19
			recordDate: 2013-04-29 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 20
			recordDate: 2013-04-29 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 21
			recordDate: 2013-04-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 22
			recordDate: 2013-04-29 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 23
			recordDate: 2013-04-29 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 24
			recordDate: 2013-04-29 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D691674
	applIdStr: 29453420
	appl_id_txt: 29453420
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29453420
	appSubCls: 506000
	appLocation: ELECTRONIC
	appAttrDockNumber: 201301088
	appType: Design
	appTypeFacet: Design
	appCustNumber: 70829
	applicants:
		№ 0
			nameLineOne: TARGET BRANDS, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 3
	applicantsFacet: {|TARGET BRANDS, INC.||||Minneapolis|MN|US|3}
	appStatusDate: 2013-09-25T12:52:47Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2013-10-15T04:00:00Z
	APP_IND: 1
	appExamName: CHIN, CYNTHIA M
	appExamNameFacet: CHIN, CYNTHIA M
	firstInventorFile: No
	appClsSubCls: D21/506000
	appClsSubClsFacet: D21/506000
	applId: 29453420
	patentTitle: DOLLHOUSE UNIT
	appIntlPubNumber: 
	appConfrNumber: 6601
	appFilingDate: 2013-04-29T04:00:00Z
	firstNamedApplicant:  TARGET BRANDS, INC.
	firstNamedApplicantFacet:  TARGET BRANDS, INC.
	inventors:
		№ 0
			nameLineOne: Mark  CHASE
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Minneapolis, 
			geoCode: MN
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Jill  SANDHEI
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: St. Paul, 
			geoCode: MN
			country: (US)
			rankNo: 1
	appCls: D21
	_version_: 1580433808046424070
	lastUpdatedTimestamp: 2017-10-05T15:58:51.496Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 15643a99-bd41-42c2-b3a7-4350025e7055
	responseHeader:
			status: 0
			QTime: 16
