DOCUMENT_BODY:
	appGrpArtNumber: 3743
	appGrpArtNumberFacet: 3743
	appStatus: Notice of Allowance Mailed -- Application Received in Office of Publications
	appStatus_txt: Notice of Allowance Mailed -- Application Received in Office of Publications
	appStatusFacet: Notice of Allowance Mailed -- Application Received in Office of Publications
	patentNumber: 
	applIdStr: 14797662
	appl_id_txt: 14797662
	appEarlyPubNumber: US20160010876A1
	appEntityStatus: SMALL
	id: 14797662
	appSubCls: 633000
	appLocation: ELECTRONIC
	appAttrDockNumber: 571522: VST-003US
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 12779
	applicants:
		№ 0
			nameLineOne: Kroger, Regine
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Stellenbosch, 
			geoCode: 
			country: (ZA)
			rankNo: 2
	applicantsFacet: {|Kroger, Regine||||Stellenbosch||ZA|2}
	appStatusDate: 2017-10-30T14:23:12Z
	LAST_MOD_TS: 2017-11-20T21:21:40Z
	appEarlyPubDate: 2016-01-14T05:00:00Z
	APP_IND: 1
	appExamName: BASICHAS, ALFRED
	appExamNameFacet: BASICHAS, ALFRED
	firstInventorFile: Yes
	appClsSubCls: 126/633000
	appClsSubClsFacet: 126/633000
	applId: 14797662
	patentTitle: AIR TEMPERATURE REGULATING ASSEMBLY USING THERMAL STORAGE
	appIntlPubNumber: 
	transactions:
		№ 0
			recordDate: 2017-11-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 1
			recordDate: 2017-11-15 00:00:00
			code: FLRCPT.C
			description: Filing Receipt - Corrected
		№ 2
			recordDate: 2017-11-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 3
			recordDate: 2017-11-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 4
			recordDate: 2017-11-01 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 5
			recordDate: 2017-10-29 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 6
			recordDate: 2017-10-13 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 7
			recordDate: 2017-08-10 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 8
			recordDate: 2017-06-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 9
			recordDate: 2017-06-30 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 10
			recordDate: 2016-01-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 11
			recordDate: 2016-01-14 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 12
			recordDate: 2016-01-14 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 13
			recordDate: 2015-12-05 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 14
			recordDate: 2015-11-04 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 15
			recordDate: 2015-07-27 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 16
			recordDate: 2015-07-27 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 17
			recordDate: 2015-07-27 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 18
			recordDate: 2015-07-27 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 19
			recordDate: 2015-07-24 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 20
			recordDate: 2015-07-24 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 21
			recordDate: 2015-07-24 00:00:00
			code: SMAL
			description: Applicant Has Filed a Verified Statement of Small Entity Status in Compliance with 37 CFR 1.27
		№ 22
			recordDate: 2015-07-15 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 23
			recordDate: 2015-07-13 00:00:00
			code: PTA.RFE
			description: Patent Term Adjustment - Ready for Examination
		№ 24
			recordDate: 2015-07-13 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 25
			recordDate: 2015-07-13 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 26
			recordDate: 2015-07-13 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 27
			recordDate: 2015-07-13 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appConfrNumber: 9437
	appFilingDate: 2015-07-13T04:00:00Z
	firstNamedApplicant:  Kroger, Regine
	firstNamedApplicantFacet:  Kroger, Regine
	inventors:
		№ 0
			nameLineOne: Hanno Carl Rudolf Reuter
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Stellenbosch, 
			geoCode: 
			country: (ZA)
			rankNo: 1
		№ 1
			nameLineOne: Johann Pierre Terblanche
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Stellenbosch, 
			geoCode: 
			country: (ZA)
			rankNo: 2
		№ 2
			nameLineOne: Detlev Gustav Kröger
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Stellenbosch, 
			geoCode: 
			country: (ZA)
			rankNo: 3
	appCls: 126
	_version_: 1584642984040726532
	lastUpdatedTimestamp: 2017-11-21T03:01:54.539Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: f5bbbdef-8cb0-463d-a2b1-af8398edb84d
	responseHeader:
			status: 0
			QTime: 13
