DOCUMENT_BODY:
	appGrpArtNumber: 3901
	appGrpArtNumberFacet: 3901
	transactions:
		№ 0
			recordDate: 2011-05-11 00:00:00
			code: PCTFR
			description: PCT Formality Review
		№ 1
			recordDate: 2011-02-02 00:00:00
			code: MP210
			description: Mail International Search Report
		№ 2
			recordDate: 2011-01-31 00:00:00
			code: PCTFR
			description: PCT Formality Review
		№ 3
			recordDate: 2011-01-31 00:00:00
			code: P237
			description: ISA Written Opinion
		№ 4
			recordDate: 2011-01-31 00:00:00
			code: P210
			description: International Search Report Ready to be Mailed
		№ 5
			recordDate: 2011-01-24 00:00:00
			code: CTSF
			description: PCT - Chapter 1 P210 / P237 (Full count)
		№ 6
			recordDate: 2011-01-21 00:00:00
			code: 210I
			description: 210I
		№ 7
			recordDate: 2010-12-27 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 8
			recordDate: 2010-12-14 00:00:00
			code: TF06
			description: TF06  
		№ 9
			recordDate: 2010-12-09 00:00:00
			code: P105
			description: Notification of Intntl. Appl. Number and Intntl. Filing Date
		№ 10
			recordDate: 2010-12-09 00:00:00
			code: P102
			description: Notification Concerning Payment of Fees
		№ 11
			recordDate: 2010-12-09 00:00:00
			code: MLIB
			description: Record Copy Mailed
		№ 12
			recordDate: 2010-12-09 00:00:00
			code: P202
			description: Notification of Receipt of Search Copy
		№ 13
			recordDate: 2010-12-08 00:00:00
			code: D5003
			description: Dispatch from PCT Operations -Chapter I
		№ 14
			recordDate: 2010-12-04 00:00:00
			code: L128
			description: Cleared by L&R (LARS)
		№ 15
			recordDate: 2010-11-18 00:00:00
			code: L198
			description: Referred to Level 2 (LARS) by OIPE CSR
		№ 16
			recordDate: 2010-11-17 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 17
			recordDate: 2010-11-15 00:00:00
			code: IEXX
			description: Initial Exam Team nn
		№ 18
			recordDate: 2010-11-12 00:00:00
			code: RCDT
			description: Receipt Date
	appStatus: PCT - International Search Report Mailed to IB
	appStatus_txt: PCT - International Search Report Mailed to IB
	appStatusFacet: PCT - International Search Report Mailed to IB
	wipoEarlyPubDate: 2011-05-19T04:00:00Z
	patentNumber: 
	applIdStr: PCT/US10/02967
	appl_id_txt: PCT/US10/02967
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: PCT/US10/02967
	appSubCls: PCT006
	wipoEarlyPubNumber: 2011059502
	appLocation: ELECTRONIC
	appAttrDockNumber: VW-101T
	appType: PCT
	appTypeFacet: PCT
	appCustNumber: 32488
	applicants:
		№ 0
			nameLineOne: 7 SEARS ROAD
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: WAYLAND, 
			geoCode: MA
			country: (US)
			rankNo: 1
	applicantsFacet: {|7 SEARS ROAD||||WAYLAND|MA|US|1}
	appStatusDate: 2011-02-01T16:25:48Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	APP_IND: 2
	appExamName: COPENHEAVER, BLAINE R
	appExamNameFacet: COPENHEAVER, BLAINE R
	firstInventorFile: Other
	appClsSubCls: 001/PCT006
	appClsSubClsFacet: 001/PCT006
	applId: PCT/US10/02967
	patentTitle: MONITORING AND CAMERA SYSTEM AND METHOD
	appIntlPubNumber: 
	appConfrNumber: 6997
	appFilingDate: 2010-11-12T05:00:00Z
	firstNamedApplicant:  7 SEARS ROAD
	firstNamedApplicantFacet:  7 SEARS ROAD
	appCls: 001
	_version_: 1580434331283750915
	lastUpdatedTimestamp: 2017-10-05T16:07:10.497Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: ecbb0316-0aa3-47ba-80c8-320b6d84dbcf
	responseHeader:
			status: 0
			QTime: 12
