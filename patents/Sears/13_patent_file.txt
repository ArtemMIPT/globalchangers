DOCUMENT_BODY:
	appGrpArtNumber: 2617
	appGrpArtNumberFacet: 2617
	transactions:
		№ 0
			recordDate: 2009-08-25 00:00:00
			code: P105
			description: Notification of Intntl. Appl. Number and Intntl. Filing Date
		№ 1
			recordDate: 2009-08-25 00:00:00
			code: P102
			description: Notification Concerning Payment of Fees
		№ 2
			recordDate: 2009-08-25 00:00:00
			code: MLIB
			description: Record Copy Mailed
		№ 3
			recordDate: 2009-08-18 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 4
			recordDate: 2009-08-17 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 5
			recordDate: 2009-08-13 00:00:00
			code: IEXX
			description: Initial Exam Team nn
		№ 6
			recordDate: 2009-08-12 00:00:00
			code: RCDT
			description: Receipt Date
	appStatus: Application Undergoing Preexam Processing
	appStatus_txt: Application Undergoing Preexam Processing
	appStatusFacet: Application Undergoing Preexam Processing
	wipoEarlyPubDate: 2010-03-04T05:00:00Z
	patentNumber: 
	applIdStr: PCT/US09/04605
	appl_id_txt: PCT/US09/04605
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: PCT/US09/04605
	appSubCls: 466000
	wipoEarlyPubNumber: 2010024854
	appLocation: ELECTRONIC
	appAttrDockNumber: JONES 4-2
	appType: PCT
	appTypeFacet: PCT
	appCustNumber: 22046
	applicants:
		№ 0
			nameLineOne: 13203 SEARS ROAD
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: PLANO, 
			geoCode: IL
			country: (US)
			rankNo: 0
		№ 1
			nameLineOne: 16531 PARK PLACE
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: ORLAND HILLS, 
			geoCode: IL
			country: (US)
			rankNo: 0
	applicantsFacet: {|13203 SEARS ROAD||||PLANO|IL|US|0},{|16531 PARK PLACE||||ORLAND HILLS|IL|US|0}
	appStatusDate: 2009-08-13T18:42:26Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	APP_IND: 2
	firstInventorFile: Other
	appClsSubCls: 455/466000
	appClsSubClsFacet: 455/466000
	applId: PCT/US09/04605
	patentTitle: METHOD FOR INDICATING SUBSCRIPTION TO A GREEN SERVICE IN A MESSAGE
	appIntlPubNumber: 
	appConfrNumber: 9675
	appFilingDate: 2009-08-12T04:00:00Z
	firstNamedApplicant:  13203 SEARS ROAD
	firstNamedApplicantFacet:  13203 SEARS ROAD
	appCls: 455
	_version_: 1580434322990563331
	lastUpdatedTimestamp: 2017-10-05T16:07:02.614Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: ecbb0316-0aa3-47ba-80c8-320b6d84dbcf
	responseHeader:
			status: 0
			QTime: 12
