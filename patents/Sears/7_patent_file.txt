DOCUMENT_BODY:
	appGrpArtNumber: 2911
	appGrpArtNumberFacet: 2911
	transactions:
		№ 0
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 1
			recordDate: 2015-06-09 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2015-05-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-05-20 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-05-07 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-04-28 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-04-27 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-04-27 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-01-27 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2015-01-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2015-01-27 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2015-01-18 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2015-01-14 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 13
			recordDate: 2014-10-20 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 14
			recordDate: 2014-04-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 15
			recordDate: 2013-10-29 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 16
			recordDate: 2013-08-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 17
			recordDate: 2013-05-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 18
			recordDate: 2013-05-17 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 19
			recordDate: 2013-05-17 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 20
			recordDate: 2013-05-16 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 21
			recordDate: 2013-05-16 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 22
			recordDate: 2013-05-02 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 23
			recordDate: 2013-05-01 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 24
			recordDate: 2013-05-01 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 25
			recordDate: 2013-05-01 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D731540
	applIdStr: 29453694
	appl_id_txt: 29453694
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29453694
	appSubCls: 489000
	appLocation: ELECTRONIC
	appAttrDockNumber: 26683US01
	appType: Design
	appTypeFacet: Design
	appCustNumber: 23446
	applicants:
		№ 0
			nameLineOne: Sears Brands, L.L.C.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Hoffman Estates, 
			geoCode: IL
			country: (US)
			rankNo: 3
	applicantsFacet: {|Sears Brands, L.L.C.||||Hoffman Estates|IL|US|3}
	appStatusDate: 2015-05-20T13:39:09Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-06-09T04:00:00Z
	APP_IND: 1
	appExamName: LEE, ANGELA J
	appExamNameFacet: LEE, ANGELA J
	firstInventorFile: Yes
	appClsSubCls: D14/489000
	appClsSubClsFacet: D14/489000
	applId: 29453694
	patentTitle: Display Screen or Portion Thereof With an Icon
	appIntlPubNumber: 
	appConfrNumber: 8113
	appFilingDate: 2013-05-01T04:00:00Z
	firstNamedApplicant:  Sears Brands, L.L.C.
	firstNamedApplicantFacet:  Sears Brands, L.L.C.
	inventors:
		№ 0
			nameLineOne: Guy  Haimovitch
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Hoffman Estates, 
			geoCode: IL
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Itzik  Kadosh
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Hoffman Estates, 
			geoCode: IL
			country: (US)
			rankNo: 1
	appCls: D14
	_version_: 1580433808089415681
	lastUpdatedTimestamp: 2017-10-05T15:58:51.561Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: ecbb0316-0aa3-47ba-80c8-320b6d84dbcf
	responseHeader:
			status: 0
			QTime: 12
