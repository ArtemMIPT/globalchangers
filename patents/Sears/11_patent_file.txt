DOCUMENT_BODY:
	appGrpArtNumber: 3902
	appGrpArtNumberFacet: 3902
	transactions:
		№ 0
			recordDate: 2008-05-15 00:00:00
			code: MP210
			description: Mail International Search Report
		№ 1
			recordDate: 2008-05-08 00:00:00
			code: P210
			description: International Search Report Ready to be Mailed
		№ 2
			recordDate: 2008-05-08 00:00:00
			code: CTSF
			description: PCT - Chapter 1 P210 / P237 (Full count)
		№ 3
			recordDate: 2008-04-21 00:00:00
			code: 210I
			description: 210I
		№ 4
			recordDate: 2008-03-31 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 5
			recordDate: 2008-03-12 00:00:00
			code: TF04
			description: TF04 
		№ 6
			recordDate: 2008-01-28 00:00:00
			code: P146
			description: Notification of Ex-officio correction
		№ 7
			recordDate: 2008-01-28 00:00:00
			code: P132
			description: Miscellaneous Communication
		№ 8
			recordDate: 2008-01-28 00:00:00
			code: P105
			description: Notification of Intntl. Appl. Number and Intntl. Filing Date
		№ 9
			recordDate: 2008-01-28 00:00:00
			code: P102
			description: Notification Concerning Payment of Fees
		№ 10
			recordDate: 2008-01-28 00:00:00
			code: MLIB
			description: Record Copy Mailed
		№ 11
			recordDate: 2008-01-28 00:00:00
			code: P202
			description: Notification of Receipt of Search Copy
		№ 12
			recordDate: 2008-01-22 00:00:00
			code: D5003
			description: Dispatch from PCT Operations -Chapter I
		№ 13
			recordDate: 2008-01-16 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 14
			recordDate: 2008-01-12 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 15
			recordDate: 2007-12-31 00:00:00
			code: IEXX
			description: Initial Exam Team nn
		№ 16
			recordDate: 2007-12-21 00:00:00
			code: RCDT
			description: Receipt Date
	appStatus: PCT - International Search Report Mailed to IB
	appStatus_txt: PCT - International Search Report Mailed to IB
	appStatusFacet: PCT - International Search Report Mailed to IB
	wipoEarlyPubDate: 2008-07-17T04:00:00Z
	patentNumber: 
	applIdStr: PCT/US07/26247
	appl_id_txt: PCT/US07/26247
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: PCT/US07/26247
	appSubCls: PCT004
	wipoEarlyPubNumber: 2008085436
	appLocation: ELECTRONIC
	appAttrDockNumber: 2007-1361-PCT
	appType: PCT
	appTypeFacet: PCT
	appCustNumber: 
	applicants:
		№ 0
			nameLineOne: 10900 EUCLID AVENUE
			nameLineTwo: SEARS BUILDING, SIXTH FLOOR 
			suffix: 
			streetOne: 
			streetTwo: 
			city: CLEVELAND, 
			geoCode: OH
			country: (US)
			rankNo: 0
		№ 1
			nameLineOne: 10900 EUCLID AVENUE
			nameLineTwo: SEARS BUILDING, SIXTH FLOOR 
			suffix: 
			streetOne: 
			streetTwo: 
			city: CLEVELAND, 
			geoCode: OH
			country: (US)
			rankNo: 0
	applicantsFacet: {SEARS BUILDING, SIXTH FLOOR|10900 EUCLID AVENUE||||CLEVELAND|OH|US|0},{SEARS BUILDING, SIXTH FLOOR|10900 EUCLID AVENUE||||CLEVELAND|OH|US|0}
	appStatusDate: 2008-05-14T17:24:51Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	APP_IND: 2
	appExamName: YOUNG, LEE W
	appExamNameFacet: YOUNG, LEE W
	firstInventorFile: Other
	appClsSubCls: 001/PCT004
	appClsSubClsFacet: 001/PCT004
	applId: PCT/US07/26247
	patentTitle: SITUATED SIMULATION FOR TRAINING, EDUCATION, AND THERAPY
	appIntlPubNumber: 
	appConfrNumber: 9793
	appFilingDate: 2007-12-21T05:00:00Z
	firstNamedApplicant: SEARS BUILDING, SIXTH FLOOR 10900 EUCLID AVENUE
	firstNamedApplicantFacet: SEARS BUILDING, SIXTH FLOOR 10900 EUCLID AVENUE
	appCls: 001
	_version_: 1580434307657236480
	lastUpdatedTimestamp: 2017-10-05T16:06:47.979Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: ecbb0316-0aa3-47ba-80c8-320b6d84dbcf
	responseHeader:
			status: 0
			QTime: 12
