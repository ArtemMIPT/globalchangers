DOCUMENT_BODY:
	appGrpArtNumber: 2472
	appGrpArtNumberFacet: 2472
	transactions:
		№ 0
			recordDate: 2009-10-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 1
			recordDate: 2008-10-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 2
			recordDate: 2006-10-12 00:00:00
			code: MP210
			description: Mail International Search Report
		№ 3
			recordDate: 2006-09-27 00:00:00
			code: P237
			description: ISA Written Opinion
		№ 4
			recordDate: 2006-09-27 00:00:00
			code: P210
			description: International Search Report Ready to be Mailed
		№ 5
			recordDate: 2006-09-18 00:00:00
			code: CTSF
			description: PCT - Chapter 1 P210 / P237 (Full count)
		№ 6
			recordDate: 2006-08-31 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 7
			recordDate: 2006-08-23 00:00:00
			code: CLMPCT1N
			description: Claim comparison Ch I - not similar
		№ 8
			recordDate: 2006-08-14 00:00:00
			code: D5003
			description: Dispatch from PCT Operations -Chapter I
		№ 9
			recordDate: 2006-08-14 00:00:00
			code: P105
			description: Notification of Intntl. Appl. Number and Intntl. Filing Date
		№ 10
			recordDate: 2006-08-14 00:00:00
			code: P102
			description: Notification Concerning Payment of Fees
		№ 11
			recordDate: 2006-08-14 00:00:00
			code: MLIB
			description: Record Copy Mailed
		№ 12
			recordDate: 2006-08-14 00:00:00
			code: P202
			description: Notification of Receipt of Search Copy
		№ 13
			recordDate: 2006-07-17 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 14
			recordDate: 2006-07-05 00:00:00
			code: IEXX
			description: Initial Exam Team nn
		№ 15
			recordDate: 2006-06-23 00:00:00
			code: RCDT
			description: Receipt Date
	appStatus: PCT - International Search Report Mailed to IB
	appStatus_txt: PCT - International Search Report Mailed to IB
	appStatusFacet: PCT - International Search Report Mailed to IB
	wipoEarlyPubDate: 2007-04-12T04:00:00Z
	patentNumber: 
	applIdStr: PCT/US06/24615
	appl_id_txt: PCT/US06/24615
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: PCT/US06/24615
	appSubCls: 503000
	wipoEarlyPubNumber: 2007040685
	appLocation: FILE REPOSITORY (FRANCONIA)
	appAttrDockNumber: CE14404W
	appType: PCT
	appTypeFacet: PCT
	appCustNumber: 
	applicants:
		№ 0
			nameLineOne: 234 GREGORY M. SEARS DRIVE
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: GILBERTS, 
			geoCode: 
			country: (US)
			rankNo: 0
	applicantsFacet: {|234 GREGORY M. SEARS DRIVE||||GILBERTS||US|0}
	appStatusDate: 2006-10-12T18:03:19Z
	LAST_MOD_TS: 2017-09-06T21:28:51Z
	APP_IND: 2
	appExamName: SHAND, ROBERTA A
	appExamNameFacet: SHAND, ROBERTA A
	firstInventorFile: Other
	appClsSubCls: 370/503000
	appClsSubClsFacet: 370/503000
	applId: PCT/US06/24615
	patentTitle: METHOD AND SYSTEM FOR ACHIEVING ALIGNMENT BETWEEN A CENTRALIZED BASE STATION CONTROLLER (CBSC) AND A BASE TRANSCEIVER SITE (BTS) IN A NETWORK
	appIntlPubNumber: 
	appLocationDate: 2011-12-27T05:00:00Z
	appConfrNumber: 4198
	appFilingDate: 2006-06-23T04:00:00Z
	firstNamedApplicant:  234 GREGORY M. SEARS DRIVE
	firstNamedApplicantFacet:  234 GREGORY M. SEARS DRIVE
	appCls: 370
	_version_: 1580434297945325570
	lastUpdatedTimestamp: 2017-10-05T16:06:38.703Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: ecbb0316-0aa3-47ba-80c8-320b6d84dbcf
	responseHeader:
			status: 0
			QTime: 12
