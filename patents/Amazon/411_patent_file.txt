DOCUMENT_BODY:
	appGrpArtNumber: 2192
	appGrpArtNumberFacet: 2192
	transactions:
		№ 0
			recordDate: 2015-06-02 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-06-02 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-05-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-05-13 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-05-02 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-05-01 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-04-29 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-04-29 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-02-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2015-02-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2015-02-13 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2015-02-09 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2015-02-04 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 13
			recordDate: 2015-01-26 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 14
			recordDate: 2015-01-22 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 15
			recordDate: 2015-01-22 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 16
			recordDate: 2014-12-17 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 17
			recordDate: 2014-12-12 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 18
			recordDate: 2014-12-12 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 19
			recordDate: 2014-09-22 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 20
			recordDate: 2014-09-22 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 21
			recordDate: 2014-09-22 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 22
			recordDate: 2014-09-15 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 23
			recordDate: 2014-09-09 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 24
			recordDate: 2014-09-07 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 25
			recordDate: 2014-09-05 00:00:00
			code: ELC.
			description: Response to Election / Restriction Filed
		№ 26
			recordDate: 2014-09-02 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 27
			recordDate: 2014-09-02 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 28
			recordDate: 2014-07-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 29
			recordDate: 2014-07-09 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 30
			recordDate: 2014-07-09 00:00:00
			code: MCTRS
			description: Mail Restriction Requirement
		№ 31
			recordDate: 2014-06-23 00:00:00
			code: CTRS
			description: Restriction/Election Requirement
		№ 32
			recordDate: 2014-05-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 33
			recordDate: 2014-01-28 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 34
			recordDate: 2013-05-07 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 35
			recordDate: 2013-04-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 36
			recordDate: 2013-04-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 37
			recordDate: 2013-04-22 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 38
			recordDate: 2013-04-22 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 39
			recordDate: 2013-04-22 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 40
			recordDate: 2013-04-19 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 41
			recordDate: 2013-04-19 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 42
			recordDate: 2013-03-17 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 43
			recordDate: 2013-03-14 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 44
			recordDate: 2013-03-13 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 45
			recordDate: 2013-03-13 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9047404
	applIdStr: 13799654
	appl_id_txt: 13799654
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13799654
	appSubCls: 127000
	appLocation: ELECTRONIC
	appAttrDockNumber: 101058.000041
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 23377
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2015-05-13T12:20:39Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-06-02T04:00:00Z
	APP_IND: 1
	appExamName: LUU, CUONG V
	appExamNameFacet: LUU, CUONG V
	firstInventorFile: No
	appClsSubCls: 717/127000
	appClsSubClsFacet: 717/127000
	applId: 13799654
	patentTitle: BRIDGE TO CONNECT AN EXTENDED DEVELOPMENT CAPABILITY DEVICE TO A TARGET DEVICE
	appIntlPubNumber: 
	appConfrNumber: 5053
	appFilingDate: 2013-03-13T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Rahul  Ravikumar
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Irvine, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Abdullah Mohammed Jibaly
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Foothill Ranch, 
			geoCode: CA
			country: (US)
			rankNo: 1
	appCls: 717
	_version_: 1580433223347863553
	lastUpdatedTimestamp: 2017-10-05T15:49:33.919Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: cc915edb-47a7-45a5-937d-e5065d213ba1
	responseHeader:
			status: 0
			QTime: 16
