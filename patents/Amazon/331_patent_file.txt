DOCUMENT_BODY:
	appGrpArtNumber: 3752
	appGrpArtNumberFacet: 3752
	transactions:
		№ 0
			recordDate: 2017-08-03 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 1
			recordDate: 2017-08-03 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 2
			recordDate: 2017-08-03 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2017-08-03 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 4
			recordDate: 2017-08-01 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 5
			recordDate: 2017-07-31 00:00:00
			code: AFNE
			description: After Final Consideration Program Amendment too Extensive
		№ 6
			recordDate: 2017-07-31 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 7
			recordDate: 2017-07-09 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 8
			recordDate: 2017-07-03 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 9
			recordDate: 2017-07-03 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 10
			recordDate: 2017-05-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 11
			recordDate: 2017-05-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 12
			recordDate: 2017-05-01 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 13
			recordDate: 2017-04-26 00:00:00
			code: CTFR
			description: Final Rejection
		№ 14
			recordDate: 2017-03-25 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 15
			recordDate: 2017-03-21 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 16
			recordDate: 2016-12-21 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 17
			recordDate: 2016-12-21 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 18
			recordDate: 2016-12-21 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 19
			recordDate: 2016-12-15 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 20
			recordDate: 2016-11-18 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 21
			recordDate: 2016-11-18 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 22
			recordDate: 2016-11-14 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 23
			recordDate: 2016-11-14 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 24
			recordDate: 2016-11-14 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 25
			recordDate: 2016-10-28 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 26
			recordDate: 2016-10-28 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 27
			recordDate: 2016-10-25 00:00:00
			code: AFNE
			description: After Final Consideration Program Amendment too Extensive
		№ 28
			recordDate: 2016-10-25 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 29
			recordDate: 2016-10-23 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 30
			recordDate: 2016-10-14 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 31
			recordDate: 2016-10-14 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 32
			recordDate: 2016-09-08 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 33
			recordDate: 2016-09-01 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 34
			recordDate: 2016-07-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 35
			recordDate: 2016-07-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 36
			recordDate: 2016-07-14 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 37
			recordDate: 2016-07-03 00:00:00
			code: CTFR
			description: Final Rejection
		№ 38
			recordDate: 2016-06-08 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 39
			recordDate: 2016-06-02 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 40
			recordDate: 2016-03-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 41
			recordDate: 2016-03-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 42
			recordDate: 2016-03-02 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 43
			recordDate: 2016-02-23 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 44
			recordDate: 2016-02-17 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 45
			recordDate: 2016-01-28 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 46
			recordDate: 2015-10-28 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 47
			recordDate: 2015-10-28 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 48
			recordDate: 2015-10-28 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 49
			recordDate: 2015-10-21 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 50
			recordDate: 2015-10-21 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 51
			recordDate: 2015-10-21 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 52
			recordDate: 2015-10-06 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 53
			recordDate: 2015-10-05 00:00:00
			code: ELC.
			description: Response to Election / Restriction Filed
		№ 54
			recordDate: 2015-08-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 55
			recordDate: 2015-08-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 56
			recordDate: 2015-08-05 00:00:00
			code: MCTRS
			description: Mail Restriction Requirement
		№ 57
			recordDate: 2015-07-28 00:00:00
			code: CTRS
			description: Restriction/Election Requirement
		№ 58
			recordDate: 2014-08-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 59
			recordDate: 2014-08-28 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 60
			recordDate: 2014-07-15 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 61
			recordDate: 2014-07-15 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 62
			recordDate: 2014-06-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 63
			recordDate: 2014-03-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 64
			recordDate: 2014-03-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 65
			recordDate: 2014-03-24 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 66
			recordDate: 2014-03-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 67
			recordDate: 2014-03-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 68
			recordDate: 2014-03-13 00:00:00
			code: PG-PB-DT
			description: PG-Pub Notice of new or Revised projected publication date
		№ 69
			recordDate: 2014-02-27 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 70
			recordDate: 2013-12-18 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 71
			recordDate: 2013-12-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 72
			recordDate: 2013-09-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 73
			recordDate: 2013-09-04 00:00:00
			code: TI1050
			description: Transfer Inquiry to GAU
		№ 74
			recordDate: 2013-05-06 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 75
			recordDate: 2013-03-28 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 76
			recordDate: 2013-03-28 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 77
			recordDate: 2013-03-28 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 78
			recordDate: 2013-03-28 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 79
			recordDate: 2013-03-28 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 80
			recordDate: 2013-03-27 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 81
			recordDate: 2013-03-27 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 82
			recordDate: 2013-03-02 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 83
			recordDate: 2013-02-27 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 84
			recordDate: 2013-02-27 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 85
			recordDate: 2013-02-27 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 86
			recordDate: 2013-02-27 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Advisory Action Mailed
	appStatus_txt: Advisory Action Mailed
	appStatusFacet: Advisory Action Mailed
	patentNumber: 
	applIdStr: 13779411
	appl_id_txt: 13779411
	appEarlyPubNumber: US20140238705A1
	appEntityStatus: UNDISCOUNTED
	id: 13779411
	appSubCls: 046000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-47900
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|3}
	appStatusDate: 2017-08-01T13:56:58Z
	LAST_MOD_TS: 2017-09-20T15:30:59Z
	appEarlyPubDate: 2014-08-28T04:00:00Z
	APP_IND: 1
	appExamName: VALVIS, ALEXANDER M
	appExamNameFacet: VALVIS, ALEXANDER M
	firstInventorFile: No
	appClsSubCls: 169/046000
	appClsSubClsFacet: 169/046000
	applId: 13779411
	patentTitle: FIRE SUPPRESSION SYSTEM FOR SUB-FLOOR SPACE
	appIntlPubNumber: 
	appConfrNumber: 7019
	appFilingDate: 2013-02-27T05:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: BROCK ROBERT GARDNER
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: MICHAEL PHILLIP CZAMARA
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 169
	_version_: 1580433219660021760
	lastUpdatedTimestamp: 2017-10-05T15:49:30.382Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 0a00969b-d87b-44b3-8fd5-1d76e5a8df75
	responseHeader:
			status: 0
			QTime: 18
