DOCUMENT_BODY:
	appGrpArtNumber: 2623
	appGrpArtNumberFacet: 2623
	transactions:
		№ 0
			recordDate: 2016-03-29 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-03-29 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-03-09 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 3
			recordDate: 2016-02-24 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 4
			recordDate: 2016-02-24 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 5
			recordDate: 2016-02-19 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 6
			recordDate: 2016-02-19 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 7
			recordDate: 2015-11-27 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 8
			recordDate: 2015-11-24 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 9
			recordDate: 2015-11-19 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 10
			recordDate: 2015-11-18 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 11
			recordDate: 2015-11-18 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 12
			recordDate: 2015-09-18 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 13
			recordDate: 2015-08-31 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 14
			recordDate: 2015-06-24 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 15
			recordDate: 2015-06-24 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 16
			recordDate: 2015-06-23 00:00:00
			code: PST_CRD
			description: Mail Post Card
		№ 17
			recordDate: 2015-06-15 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 18
			recordDate: 2015-06-15 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 19
			recordDate: 2015-06-10 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 20
			recordDate: 2014-07-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 21
			recordDate: 2014-03-31 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 22
			recordDate: 2014-02-04 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 23
			recordDate: 2013-07-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 24
			recordDate: 2013-07-09 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 25
			recordDate: 2013-06-26 00:00:00
			code: C602
			description: Oath or Declaration Filed (Including Supplemental)
		№ 26
			recordDate: 2013-05-08 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 27
			recordDate: 2013-05-06 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 28
			recordDate: 2013-04-15 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 29
			recordDate: 2013-04-15 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 30
			recordDate: 2013-04-15 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 31
			recordDate: 2013-04-15 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 32
			recordDate: 2013-03-15 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 33
			recordDate: 2013-03-12 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 34
			recordDate: 2013-03-11 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 35
			recordDate: 2013-03-11 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 36
			recordDate: 2013-03-11 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9298324
	applIdStr: 13794591
	appl_id_txt: 13794591
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13794591
	appSubCls: 174000
	appLocation: ELECTRONIC
	appAttrDockNumber: P8167-US
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136714
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2016-03-09T11:08:52Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-03-29T04:00:00Z
	APP_IND: 1
	appExamName: ABDULSELAM, ABBAS I
	appExamNameFacet: ABDULSELAM, ABBAS I
	firstInventorFile: No
	appClsSubCls: 345/174000
	appClsSubClsFacet: 345/174000
	applId: 13794591
	patentTitle: CAPACITIVE TOUCH WITH TACTILE FEEDBACK
	appIntlPubNumber: 
	appConfrNumber: 9810
	appFilingDate: 2013-03-11T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: GABRIEL ISAIAH ROWE
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Fremont, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Lakshman  Rathnam
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Mountain View, 
			geoCode: CA
			country: (US)
			rankNo: 2
	appCls: 345
	_version_: 1580433222441893892
	lastUpdatedTimestamp: 2017-10-05T15:49:33.026Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 6073073c-cbc6-49b0-b9b5-714ce578e5c8
	responseHeader:
			status: 0
			QTime: 17
