DOCUMENT_BODY:
	appGrpArtNumber: 2166
	appGrpArtNumberFacet: 2166
	transactions:
		№ 0
			recordDate: 2015-10-27 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-10-27 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-10-08 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-10-07 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-09-25 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-09-24 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-09-23 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-09-23 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-06-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2015-06-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2015-06-23 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2015-06-18 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2015-06-16 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 13
			recordDate: 2015-06-16 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 14
			recordDate: 2015-06-15 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 15
			recordDate: 2015-06-15 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 16
			recordDate: 2015-06-05 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 17
			recordDate: 2015-06-04 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 18
			recordDate: 2015-06-02 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 19
			recordDate: 2015-06-02 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 20
			recordDate: 2015-06-01 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 21
			recordDate: 2015-06-01 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 22
			recordDate: 2015-04-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 23
			recordDate: 2015-04-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 24
			recordDate: 2015-04-02 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 25
			recordDate: 2015-03-27 00:00:00
			code: CTFR
			description: Final Rejection
		№ 26
			recordDate: 2015-03-01 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 27
			recordDate: 2015-02-24 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 28
			recordDate: 2014-11-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 29
			recordDate: 2014-11-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 30
			recordDate: 2014-11-24 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 31
			recordDate: 2014-11-19 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 32
			recordDate: 2014-11-18 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 33
			recordDate: 2014-07-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 34
			recordDate: 2013-04-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 35
			recordDate: 2013-04-16 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 36
			recordDate: 2013-03-15 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 37
			recordDate: 2013-03-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 38
			recordDate: 2013-03-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 39
			recordDate: 2013-03-15 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 40
			recordDate: 2013-03-15 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 41
			recordDate: 2013-03-14 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 42
			recordDate: 2013-02-25 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 43
			recordDate: 2013-02-20 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 44
			recordDate: 2013-02-20 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 45
			recordDate: 2013-02-20 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 46
			recordDate: 2013-02-19 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 47
			recordDate: 2013-02-19 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 48
			recordDate: 2013-02-19 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9171019
	applIdStr: 13770569
	appl_id_txt: 13770569
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13770569
	appSubCls: 704000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-48100
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|2}
	appStatusDate: 2015-10-07T09:36:57Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-10-27T04:00:00Z
	APP_IND: 1
	appExamName: PHAM, KHANH B
	appExamNameFacet: PHAM, KHANH B
	firstInventorFile: No
	appClsSubCls: 707/704000
	appClsSubClsFacet: 707/704000
	applId: 13770569
	patentTitle: DISTRIBUTED LOCK SERVICE WITH EXTERNAL LOCK INFORMATION DATABASE
	appIntlPubNumber: 
	appConfrNumber: 4570
	appFilingDate: 2013-02-19T05:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: BRYAN JAMES DONLAN
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 707
	_version_: 1580433217730641920
	lastUpdatedTimestamp: 2017-10-05T15:49:28.511Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 0a00969b-d87b-44b3-8fd5-1d76e5a8df75
	responseHeader:
			status: 0
			QTime: 18
