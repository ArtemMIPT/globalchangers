DOCUMENT_BODY:
	appGrpArtNumber: 3651
	appGrpArtNumberFacet: 3651
	transactions:
		№ 0
			recordDate: 2016-03-18 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2015-04-14 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2015-04-14 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2015-03-26 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2015-03-25 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2015-03-13 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2015-03-13 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2015-03-12 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2015-03-12 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2014-12-16 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2014-12-16 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2014-12-16 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 12
			recordDate: 2014-12-10 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 13
			recordDate: 2014-12-08 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 14
			recordDate: 2014-12-08 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 15
			recordDate: 2014-12-07 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 16
			recordDate: 2014-11-26 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 17
			recordDate: 2014-11-26 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 18
			recordDate: 2014-11-24 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 19
			recordDate: 2014-11-06 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 20
			recordDate: 2014-10-30 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 21
			recordDate: 2014-10-30 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 22
			recordDate: 2014-10-23 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 23
			recordDate: 2014-10-23 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 24
			recordDate: 2014-08-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 25
			recordDate: 2014-08-29 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 26
			recordDate: 2014-08-29 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 27
			recordDate: 2014-08-25 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 28
			recordDate: 2014-08-24 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 29
			recordDate: 2014-08-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 30
			recordDate: 2014-07-31 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 31
			recordDate: 2014-07-30 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 32
			recordDate: 2014-07-25 00:00:00
			code: ELC.
			description: Response to Election / Restriction Filed
		№ 33
			recordDate: 2014-07-18 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 34
			recordDate: 2014-07-18 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 35
			recordDate: 2014-07-18 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 36
			recordDate: 2014-07-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 37
			recordDate: 2014-07-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 38
			recordDate: 2014-06-20 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 39
			recordDate: 2014-06-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 40
			recordDate: 2014-06-20 00:00:00
			code: MCTRS
			description: Mail Restriction Requirement
		№ 41
			recordDate: 2014-06-16 00:00:00
			code: CTRS
			description: Restriction/Election Requirement
		№ 42
			recordDate: 2014-06-02 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 43
			recordDate: 2014-06-02 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 44
			recordDate: 2014-02-05 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 45
			recordDate: 2014-02-05 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 46
			recordDate: 2014-02-05 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 47
			recordDate: 2014-02-03 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 48
			recordDate: 2014-02-03 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 49
			recordDate: 2014-02-03 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 50
			recordDate: 2014-02-03 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 51
			recordDate: 2014-01-31 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 52
			recordDate: 2014-01-30 00:00:00
			code: PG-PB-DT
			description: PG-Pub Notice of new or Revised projected publication date
		№ 53
			recordDate: 2014-01-24 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 54
			recordDate: 2013-08-21 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 55
			recordDate: 2013-05-13 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 56
			recordDate: 2013-05-13 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 57
			recordDate: 2013-03-28 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 58
			recordDate: 2013-03-20 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 59
			recordDate: 2013-03-20 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 60
			recordDate: 2013-03-20 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 61
			recordDate: 2013-03-19 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 62
			recordDate: 2013-03-07 00:00:00
			code: ADDFLFEE
			description: Additional Application Filing Fees
		№ 63
			recordDate: 2013-03-07 00:00:00
			code: CORRSPEC
			description: Applicant has submitted a new specification to correct Corrected Papers problems
		№ 64
			recordDate: 2013-03-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 65
			recordDate: 2013-03-05 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 66
			recordDate: 2013-03-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 67
			recordDate: 2013-03-05 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 68
			recordDate: 2013-03-05 00:00:00
			code: CPAP
			description: Corrected Paper
		№ 69
			recordDate: 2013-01-30 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 70
			recordDate: 2013-01-28 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 71
			recordDate: 2013-01-28 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 72
			recordDate: 2013-01-28 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 73
			recordDate: 2013-01-28 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 74
			recordDate: 2013-01-28 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9008829
	applIdStr: 13751635
	appl_id_txt: 13751635
	appEarlyPubNumber: US20140214195A1
	appEntityStatus: UNDISCOUNTED
	id: 13751635
	appSubCls: 217000
	appLocation: ELECTRONIC
	appAttrDockNumber: 080663.0256 (P7972)
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136595
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2015-03-25T12:38:23Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-07-31T04:00:00Z
	patentIssueDate: 2015-04-14T04:00:00Z
	APP_IND: 1
	appExamName: CUMBESS, YOLANDA RENEE
	appExamNameFacet: CUMBESS, YOLANDA RENEE
	firstInventorFile: No
	appClsSubCls: 700/217000
	appClsSubClsFacet: 700/217000
	applId: 13751635
	patentTitle: Inventory System with Connectable Inventory Holders
	appIntlPubNumber: 
	appConfrNumber: 8874
	appFilingDate: 2013-01-28T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Timothy Craig Worsley
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 700
	_version_: 1580433213718790148
	lastUpdatedTimestamp: 2017-10-05T15:49:24.69Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 043f6711-5f32-4b47-91a2-1be2571e614b
	responseHeader:
			status: 0
			QTime: 16
