DOCUMENT_BODY:
	appGrpArtNumber: 2835
	appGrpArtNumberFacet: 2835
	transactions:
		№ 0
			recordDate: 2016-08-09 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-08-09 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-07-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-07-20 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-07-06 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-07-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2016-07-06 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 7
			recordDate: 2016-06-28 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 8
			recordDate: 2016-06-28 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 9
			recordDate: 2016-06-21 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 10
			recordDate: 2016-06-21 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 11
			recordDate: 2016-06-20 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 12
			recordDate: 2016-06-17 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 13
			recordDate: 2016-03-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 14
			recordDate: 2016-03-28 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 15
			recordDate: 2016-03-28 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 16
			recordDate: 2016-03-22 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 17
			recordDate: 2016-03-16 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 18
			recordDate: 2016-02-22 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 19
			recordDate: 2016-02-09 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 20
			recordDate: 2016-02-09 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 21
			recordDate: 2016-01-12 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 22
			recordDate: 2016-01-06 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 23
			recordDate: 2015-11-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 24
			recordDate: 2015-11-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 25
			recordDate: 2015-11-17 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 26
			recordDate: 2015-11-12 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 27
			recordDate: 2015-10-11 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 28
			recordDate: 2015-09-30 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 29
			recordDate: 2015-09-30 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 30
			recordDate: 2015-09-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 31
			recordDate: 2015-09-24 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 32
			recordDate: 2015-09-18 00:00:00
			code: AFNE
			description: After Final Consideration Program Amendment too Extensive
		№ 33
			recordDate: 2015-09-18 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 34
			recordDate: 2015-09-15 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 35
			recordDate: 2015-09-10 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 36
			recordDate: 2015-09-10 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 37
			recordDate: 2015-07-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 38
			recordDate: 2015-07-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 39
			recordDate: 2015-07-14 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 40
			recordDate: 2015-07-07 00:00:00
			code: CTFR
			description: Final Rejection
		№ 41
			recordDate: 2015-06-24 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 42
			recordDate: 2015-06-18 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 43
			recordDate: 2015-04-16 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 44
			recordDate: 2015-04-07 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 45
			recordDate: 2015-04-07 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 46
			recordDate: 2015-03-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 47
			recordDate: 2015-03-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 48
			recordDate: 2015-03-24 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 49
			recordDate: 2015-03-18 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 50
			recordDate: 2015-02-24 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 51
			recordDate: 2015-02-23 00:00:00
			code: ELC.
			description: Response to Election / Restriction Filed
		№ 52
			recordDate: 2014-12-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 53
			recordDate: 2014-12-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 54
			recordDate: 2014-12-24 00:00:00
			code: MCTRS
			description: Mail Restriction Requirement
		№ 55
			recordDate: 2014-12-16 00:00:00
			code: CTRS
			description: Restriction/Election Requirement
		№ 56
			recordDate: 2014-08-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 57
			recordDate: 2013-06-27 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 58
			recordDate: 2013-03-25 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 59
			recordDate: 2013-02-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 60
			recordDate: 2013-02-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 61
			recordDate: 2013-02-14 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 62
			recordDate: 2013-02-14 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 63
			recordDate: 2013-02-14 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 64
			recordDate: 2013-02-13 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 65
			recordDate: 2013-02-05 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 66
			recordDate: 2013-01-21 00:00:00
			code: L128
			description: Cleared by L&R (LARS)
		№ 67
			recordDate: 2012-12-22 00:00:00
			code: L198
			description: Referred to Level 2 (LARS) by OIPE CSR
		№ 68
			recordDate: 2012-12-18 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 69
			recordDate: 2012-12-18 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 70
			recordDate: 2012-12-18 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9414530
	applIdStr: 13718661
	appl_id_txt: 13718661
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13718661
	appSubCls: 715000
	appLocation: ELECTRONIC
	appAttrDockNumber: 579-7002
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 109263
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2016-07-20T14:52:43Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-08-09T04:00:00Z
	APP_IND: 1
	appExamName: PAPE, ZACHARY
	appExamNameFacet: PAPE, ZACHARY
	firstInventorFile: No
	appClsSubCls: 361/715000
	appClsSubClsFacet: 361/715000
	applId: 13718661
	patentTitle: ALTERING THERMAL CONDUCTIVITY IN DEVICES
	appIntlPubNumber: 
	appConfrNumber: 2019
	appFilingDate: 2012-12-18T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: JOHN AVERY HOWARD
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: PALO ALTO, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: DAVID ERIC PETERS
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SAN JOSE, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: ROSS KENNETH THAYER
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SANTA CLARA, 
			geoCode: CA
			country: (US)
			rankNo: 3
	appCls: 361
	_version_: 1580433206984835075
	lastUpdatedTimestamp: 2017-10-05T15:49:18.284Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 6127cd1c-1e1f-490f-8308-652c641ee1b3
	responseHeader:
			status: 0
			QTime: 15
