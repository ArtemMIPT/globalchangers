DOCUMENT_BODY:
	appGrpArtNumber: 2433
	appGrpArtNumberFacet: 2433
	transactions:
		№ 0
			recordDate: 2017-03-07 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2017-03-07 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2017-02-16 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2017-02-15 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2017-02-03 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2017-02-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2017-02-01 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 7
			recordDate: 2017-01-26 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 8
			recordDate: 2017-01-26 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 9
			recordDate: 2017-01-26 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 10
			recordDate: 2017-01-24 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 11
			recordDate: 2017-01-23 00:00:00
			code: FRCE
			description: Workflow - Request for RCE - Finish
		№ 12
			recordDate: 2017-01-23 00:00:00
			code: FRCE
			description: Workflow - Request for RCE - Finish
		№ 13
			recordDate: 2017-01-23 00:00:00
			code: QPREQ
			description: Quick Path IDS Request
		№ 14
			recordDate: 2017-01-23 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 15
			recordDate: 2017-01-23 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 16
			recordDate: 2017-01-23 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 17
			recordDate: 2017-01-23 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 18
			recordDate: 2017-01-23 00:00:00
			code: MP015
			description: Mail-Record Petition Decision of Granted to Withdraw from Issue - with assigned Patent NO.
		№ 19
			recordDate: 2017-01-23 00:00:00
			code: P015
			description: Record Petition Decision of Granted to Withdraw from Issue - with assigned Patent NO.
		№ 20
			recordDate: 2017-01-23 00:00:00
			code: WFIS
			description: Withdrawal Patent Case from Issue
		№ 21
			recordDate: 2017-01-23 00:00:00
			code: PET.
			description: Petition Entered
		№ 22
			recordDate: 2017-01-23 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 23
			recordDate: 2017-01-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 24
			recordDate: 2017-01-04 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 25
			recordDate: 2016-12-28 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 26
			recordDate: 2016-12-28 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 27
			recordDate: 2016-12-27 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 28
			recordDate: 2016-12-27 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 29
			recordDate: 2016-12-27 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 30
			recordDate: 2016-12-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 31
			recordDate: 2016-12-09 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 32
			recordDate: 2016-12-09 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 33
			recordDate: 2016-12-05 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 34
			recordDate: 2016-12-05 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 35
			recordDate: 2016-12-05 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 36
			recordDate: 2016-11-30 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 37
			recordDate: 2016-11-29 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 38
			recordDate: 2016-11-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 39
			recordDate: 2016-10-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 40
			recordDate: 2016-10-24 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 41
			recordDate: 2016-10-24 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 42
			recordDate: 2016-10-19 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 43
			recordDate: 2016-10-19 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 44
			recordDate: 2016-10-19 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 45
			recordDate: 2016-10-17 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 46
			recordDate: 2016-10-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 47
			recordDate: 2016-10-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 48
			recordDate: 2016-10-06 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 49
			recordDate: 2016-10-04 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 50
			recordDate: 2016-10-03 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 51
			recordDate: 2016-09-29 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 52
			recordDate: 2016-09-29 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 53
			recordDate: 2016-08-22 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 54
			recordDate: 2016-08-18 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 55
			recordDate: 2016-08-18 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 56
			recordDate: 2016-08-18 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 57
			recordDate: 2016-08-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 58
			recordDate: 2016-08-18 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 59
			recordDate: 2016-06-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 60
			recordDate: 2016-06-06 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 61
			recordDate: 2016-06-06 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 62
			recordDate: 2016-06-01 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 63
			recordDate: 2016-06-01 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 64
			recordDate: 2016-05-23 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 65
			recordDate: 2016-05-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 66
			recordDate: 2016-05-18 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 67
			recordDate: 2016-05-18 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 68
			recordDate: 2016-05-15 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 69
			recordDate: 2016-05-12 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 70
			recordDate: 2016-05-12 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 71
			recordDate: 2016-04-26 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 72
			recordDate: 2016-04-26 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 73
			recordDate: 2016-04-05 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 74
			recordDate: 2016-04-05 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 75
			recordDate: 2016-04-05 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 76
			recordDate: 2016-04-04 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 77
			recordDate: 2016-04-04 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 78
			recordDate: 2016-01-04 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 79
			recordDate: 2016-01-04 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 80
			recordDate: 2016-01-04 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 81
			recordDate: 2015-12-29 00:00:00
			code: CTFR
			description: Final Rejection
		№ 82
			recordDate: 2015-12-29 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 83
			recordDate: 2015-10-30 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 84
			recordDate: 2015-10-30 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 85
			recordDate: 2015-10-30 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 86
			recordDate: 2015-10-29 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 87
			recordDate: 2015-07-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 88
			recordDate: 2015-07-29 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 89
			recordDate: 2015-07-29 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 90
			recordDate: 2015-07-24 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 91
			recordDate: 2015-07-24 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 92
			recordDate: 2015-07-10 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 93
			recordDate: 2015-07-10 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 94
			recordDate: 2015-07-07 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 95
			recordDate: 2015-07-07 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 96
			recordDate: 2015-06-30 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 97
			recordDate: 2015-06-30 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 98
			recordDate: 2015-06-30 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 99
			recordDate: 2015-06-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 100
			recordDate: 2015-06-29 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 101
			recordDate: 2015-06-24 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 102
			recordDate: 2015-06-17 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 103
			recordDate: 2015-06-12 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 104
			recordDate: 2015-05-20 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 105
			recordDate: 2015-04-28 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 106
			recordDate: 2015-04-28 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 107
			recordDate: 2015-03-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 108
			recordDate: 2015-03-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 109
			recordDate: 2015-03-13 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 110
			recordDate: 2015-03-08 00:00:00
			code: CTFR
			description: Final Rejection
		№ 111
			recordDate: 2015-03-06 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 112
			recordDate: 2015-03-06 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 113
			recordDate: 2015-03-06 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 114
			recordDate: 2015-01-07 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 115
			recordDate: 2014-12-31 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 116
			recordDate: 2014-12-31 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 117
			recordDate: 2014-10-29 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 118
			recordDate: 2014-10-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 119
			recordDate: 2014-10-23 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 120
			recordDate: 2014-10-16 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 121
			recordDate: 2014-10-16 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 122
			recordDate: 2014-09-16 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 123
			recordDate: 2014-09-16 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 124
			recordDate: 2014-08-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 125
			recordDate: 2014-08-14 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 126
			recordDate: 2014-07-31 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 127
			recordDate: 2014-07-31 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 128
			recordDate: 2014-07-31 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 129
			recordDate: 2014-07-28 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 130
			recordDate: 2014-07-28 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 131
			recordDate: 2014-07-27 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 132
			recordDate: 2014-02-20 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 133
			recordDate: 2014-02-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 134
			recordDate: 2014-02-20 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 135
			recordDate: 2014-02-07 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 136
			recordDate: 2014-02-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 137
			recordDate: 2013-10-25 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 138
			recordDate: 2013-10-25 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 139
			recordDate: 2013-10-21 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 140
			recordDate: 2013-07-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 141
			recordDate: 2013-04-29 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 142
			recordDate: 2013-04-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 143
			recordDate: 2013-04-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 144
			recordDate: 2013-04-29 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 145
			recordDate: 2013-04-29 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 146
			recordDate: 2013-03-18 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 147
			recordDate: 2013-03-18 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 148
			recordDate: 2013-03-18 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 149
			recordDate: 2013-03-15 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 150
			recordDate: 2013-02-13 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 151
			recordDate: 2013-02-12 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 152
			recordDate: 2013-02-12 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 153
			recordDate: 2013-02-12 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 154
			recordDate: 2013-02-12 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9590959
	applIdStr: 13764963
	appl_id_txt: 13764963
	appEarlyPubNumber: US20140229729A1
	appEntityStatus: UNDISCOUNTED
	id: 13764963
	appSubCls: 153000
	appLocation: ELECTRONIC
	appAttrDockNumber: 0097749-094US0
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 113507
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 5
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|5}
	appStatusDate: 2017-02-15T08:48:27Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-08-14T04:00:00Z
	patentIssueDate: 2017-03-07T05:00:00Z
	APP_IND: 1
	appExamName: BROWN, ANTHONY D
	appExamNameFacet: BROWN, ANTHONY D
	firstInventorFile: No
	appClsSubCls: 713/153000
	appClsSubClsFacet: 713/153000
	applId: 13764963
	patentTitle: DATA SECURITY SERVICE
	appIntlPubNumber: 
	appConfrNumber: 1759
	appFilingDate: 2013-02-12T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Gregory Branchek Roth
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Matthew James Wren
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Eric Jason Brandwine
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Haymarket, 
			geoCode: VA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Brian Irl Pratt
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 4
	appCls: 713
	_version_: 1580433216567771139
	lastUpdatedTimestamp: 2017-10-05T15:49:27.441Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 4619a0f3-746d-490b-bd96-48bc8c9d71bb
	responseHeader:
			status: 0
			QTime: 16
