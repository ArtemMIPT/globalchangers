DOCUMENT_BODY:
	appGrpArtNumber: 1729
	appGrpArtNumberFacet: 1729
	transactions:
		№ 0
			recordDate: 2016-06-14 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-06-14 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-05-26 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-05-25 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-05-16 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-02-03 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2016-02-03 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 7
			recordDate: 2016-02-03 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 8
			recordDate: 2016-02-02 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 9
			recordDate: 2016-02-02 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 10
			recordDate: 2016-02-01 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 11
			recordDate: 2016-01-28 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 12
			recordDate: 2016-01-22 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 13
			recordDate: 2016-01-22 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 14
			recordDate: 2016-01-15 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 15
			recordDate: 2016-01-08 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 16
			recordDate: 2016-01-08 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 17
			recordDate: 2016-01-08 00:00:00
			code: DRWF
			description: Workflow - Drawings Finished
		№ 18
			recordDate: 2015-12-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 19
			recordDate: 2015-12-14 00:00:00
			code: MM327
			description: Mail Miscellaneous Communication to Applicant
		№ 20
			recordDate: 2015-12-08 00:00:00
			code: M327
			description: Miscellaneous Communication to Applicant - No Action Count
		№ 21
			recordDate: 2015-11-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 22
			recordDate: 2015-11-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 23
			recordDate: 2015-11-05 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 24
			recordDate: 2015-11-02 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 25
			recordDate: 2015-10-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 26
			recordDate: 2015-10-29 00:00:00
			code: MEXET
			description: Mail Interview Summary - Examiner Initiated - Telephonic
		№ 27
			recordDate: 2015-10-27 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 28
			recordDate: 2015-10-27 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 29
			recordDate: 2015-10-26 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 30
			recordDate: 2015-10-13 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 31
			recordDate: 2015-10-13 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 32
			recordDate: 2015-10-08 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 33
			recordDate: 2015-10-08 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 34
			recordDate: 2015-10-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 35
			recordDate: 2015-10-01 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 36
			recordDate: 2015-09-21 00:00:00
			code: AFNE
			description: After Final Consideration Program Amendment too Extensive
		№ 37
			recordDate: 2015-09-21 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 38
			recordDate: 2015-09-03 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 39
			recordDate: 2015-09-02 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 40
			recordDate: 2015-09-02 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 41
			recordDate: 2015-07-08 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 42
			recordDate: 2015-07-08 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 43
			recordDate: 2015-07-08 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 44
			recordDate: 2015-06-29 00:00:00
			code: CTFR
			description: Final Rejection
		№ 45
			recordDate: 2015-06-08 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 46
			recordDate: 2015-06-05 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 47
			recordDate: 2015-03-27 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 48
			recordDate: 2015-03-27 00:00:00
			code: MEXIA
			description: Mail Applicant Initiated Interview Summary
		№ 49
			recordDate: 2015-03-23 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 50
			recordDate: 2015-03-23 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 51
			recordDate: 2015-03-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 52
			recordDate: 2015-03-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 53
			recordDate: 2015-03-13 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 54
			recordDate: 2015-03-09 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 55
			recordDate: 2015-02-27 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 56
			recordDate: 2015-02-26 00:00:00
			code: ELC.
			description: Response to Election / Restriction Filed
		№ 57
			recordDate: 2015-01-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 58
			recordDate: 2015-01-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 59
			recordDate: 2015-01-02 00:00:00
			code: MCTRS
			description: Mail Restriction Requirement
		№ 60
			recordDate: 2014-12-29 00:00:00
			code: CTRS
			description: Restriction/Election Requirement
		№ 61
			recordDate: 2014-09-03 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 62
			recordDate: 2014-01-28 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 63
			recordDate: 2013-06-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 64
			recordDate: 2013-06-06 00:00:00
			code: FLRCPT.C
			description: Filing Receipt - Corrected
		№ 65
			recordDate: 2013-05-25 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 66
			recordDate: 2013-05-24 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 67
			recordDate: 2013-04-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 68
			recordDate: 2013-04-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 69
			recordDate: 2013-04-24 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 70
			recordDate: 2013-04-24 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 71
			recordDate: 2013-04-24 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 72
			recordDate: 2013-04-23 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 73
			recordDate: 2013-04-23 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 74
			recordDate: 2013-03-23 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 75
			recordDate: 2013-03-17 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 76
			recordDate: 2013-03-14 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 77
			recordDate: 2013-03-14 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9368285
	applIdStr: 13827282
	appl_id_txt: 13827282
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13827282
	appSubCls: 508000
	appLocation: ELECTRONIC
	appAttrDockNumber: 579-7035
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 109263
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: RENO, 
			geoCode: NV
			country: (US)
			rankNo: 6
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||RENO|NV|US|6}
	appStatusDate: 2016-05-25T10:50:55Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-06-14T04:00:00Z
	APP_IND: 1
	appExamName: CHMIELECKI, SCOTT J
	appExamNameFacet: CHMIELECKI, SCOTT J
	firstInventorFile: No
	appClsSubCls: 429/508000
	appClsSubClsFacet: 429/508000
	applId: 13827282
	patentTitle: POWER CELL EMBEDDED IN ENCLOSURE
	appIntlPubNumber: 
	appConfrNumber: 9278
	appFilingDate: 2013-03-14T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: ERIK AVY VAKNINE
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SAN JOSE, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: LIFENG  CUI
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: PALO ALTO, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: ROBERT NASRY HASBUN
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: FALL CITY, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: YUTING  YEH
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SUNNYVALE, 
			geoCode: CA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: POON-KEONG  ANG
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: CUPERTINO, 
			geoCode: CA
			country: (US)
			rankNo: 5
	appCls: 429
	_version_: 1580433229261832193
	lastUpdatedTimestamp: 2017-10-05T15:49:39.554Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 15a4ef0e-add3-4358-9cd3-7d1e69d9afa9
	responseHeader:
			status: 0
			QTime: 18
