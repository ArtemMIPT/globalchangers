DOCUMENT_BODY:
	appGrpArtNumber: 2491
	appGrpArtNumberFacet: 2491
	appStatus: Final Rejection Mailed
	appStatus_txt: Final Rejection Mailed
	appStatusFacet: Final Rejection Mailed
	patentNumber: 
	applIdStr: 13765239
	appl_id_txt: 13765239
	appEarlyPubNumber: US20140229739A1
	appEntityStatus: UNDISCOUNTED
	id: 13765239
	appSubCls: 189000
	appLocation: ELECTRONIC
	appAttrDockNumber: 0097749-098US0
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 113507
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 5
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|5}
	appStatusDate: 2017-08-17T13:11:08Z
	LAST_MOD_TS: 2017-11-20T21:32:06Z
	appEarlyPubDate: 2014-08-14T04:00:00Z
	APP_IND: 1
	appExamName: GRACIA, GARY S
	appExamNameFacet: GRACIA, GARY S
	firstInventorFile: No
	appClsSubCls: 713/189000
	appClsSubClsFacet: 713/189000
	applId: 13765239
	patentTitle: DELAYED DATA ACCESS
	appIntlPubNumber: 
	transactions:
		№ 0
			recordDate: 2017-10-27 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 1
			recordDate: 2017-08-21 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 2
			recordDate: 2017-08-21 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 3
			recordDate: 2017-08-21 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 4
			recordDate: 2017-08-17 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 5
			recordDate: 2017-08-17 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 6
			recordDate: 2017-08-04 00:00:00
			code: CTFR
			description: Final Rejection
		№ 7
			recordDate: 2017-08-03 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 8
			recordDate: 2017-08-03 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 9
			recordDate: 2017-05-18 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 10
			recordDate: 2017-05-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 11
			recordDate: 2017-05-16 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 12
			recordDate: 2017-05-12 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 13
			recordDate: 2017-05-12 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 14
			recordDate: 2017-01-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 15
			recordDate: 2017-01-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 16
			recordDate: 2017-01-23 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 17
			recordDate: 2017-01-17 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 18
			recordDate: 2017-01-10 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 19
			recordDate: 2016-12-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 20
			recordDate: 2016-12-22 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 21
			recordDate: 2016-12-22 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 22
			recordDate: 2016-11-18 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 23
			recordDate: 2016-11-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 24
			recordDate: 2016-08-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 25
			recordDate: 2016-08-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 26
			recordDate: 2016-08-23 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 27
			recordDate: 2016-08-18 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 28
			recordDate: 2016-08-18 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 29
			recordDate: 2016-08-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 30
			recordDate: 2016-08-17 00:00:00
			code: CTFR
			description: Final Rejection
		№ 31
			recordDate: 2016-08-16 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 32
			recordDate: 2016-05-25 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 33
			recordDate: 2016-05-17 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 34
			recordDate: 2016-05-17 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 35
			recordDate: 2016-05-17 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 36
			recordDate: 2016-05-17 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 37
			recordDate: 2016-05-17 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 38
			recordDate: 2016-05-17 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 39
			recordDate: 2015-12-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 40
			recordDate: 2015-12-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 41
			recordDate: 2015-12-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 42
			recordDate: 2015-12-17 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 43
			recordDate: 2015-12-14 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 44
			recordDate: 2015-12-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 45
			recordDate: 2015-12-10 00:00:00
			code: MPREV
			description: Mail-Petition to Revive Application - Granted
		№ 46
			recordDate: 2015-12-09 00:00:00
			code: PREV
			description: Petition to Revive Application - Granted
		№ 47
			recordDate: 2015-05-05 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 48
			recordDate: 2015-05-05 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 49
			recordDate: 2015-04-13 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 50
			recordDate: 2015-04-13 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 51
			recordDate: 2015-04-10 00:00:00
			code: PET.
			description: Petition Entered
		№ 52
			recordDate: 2015-04-10 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 53
			recordDate: 2015-04-10 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 54
			recordDate: 2015-04-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 55
			recordDate: 2015-04-10 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 56
			recordDate: 2015-04-06 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 57
			recordDate: 2015-04-04 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 58
			recordDate: 2015-04-03 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 59
			recordDate: 2015-04-03 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 60
			recordDate: 2015-03-17 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 61
			recordDate: 2015-02-24 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 62
			recordDate: 2015-02-24 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 63
			recordDate: 2014-10-08 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 64
			recordDate: 2014-10-08 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 65
			recordDate: 2014-10-08 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 66
			recordDate: 2014-09-29 00:00:00
			code: CTFR
			description: Final Rejection
		№ 67
			recordDate: 2014-09-24 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 68
			recordDate: 2014-09-24 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 69
			recordDate: 2014-09-24 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 70
			recordDate: 2014-09-17 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 71
			recordDate: 2014-09-16 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 72
			recordDate: 2014-09-16 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 73
			recordDate: 2014-09-16 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 74
			recordDate: 2014-09-15 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 75
			recordDate: 2014-09-15 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 76
			recordDate: 2014-09-03 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 77
			recordDate: 2014-09-03 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 78
			recordDate: 2014-08-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 79
			recordDate: 2014-08-14 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 80
			recordDate: 2014-07-28 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 81
			recordDate: 2014-07-28 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 82
			recordDate: 2014-07-28 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 83
			recordDate: 2014-07-28 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 84
			recordDate: 2014-06-16 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 85
			recordDate: 2014-06-16 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 86
			recordDate: 2014-06-16 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 87
			recordDate: 2014-06-09 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 88
			recordDate: 2014-02-20 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 89
			recordDate: 2014-02-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 90
			recordDate: 2014-02-20 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 91
			recordDate: 2014-02-11 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 92
			recordDate: 2014-02-07 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 93
			recordDate: 2013-10-25 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 94
			recordDate: 2013-10-25 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 95
			recordDate: 2013-10-21 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 96
			recordDate: 2013-07-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 97
			recordDate: 2013-07-17 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 98
			recordDate: 2013-07-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 99
			recordDate: 2013-04-16 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 100
			recordDate: 2013-03-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 101
			recordDate: 2013-03-06 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 102
			recordDate: 2013-03-06 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 103
			recordDate: 2013-03-05 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 104
			recordDate: 2013-02-13 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 105
			recordDate: 2013-02-12 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 106
			recordDate: 2013-02-12 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 107
			recordDate: 2013-02-12 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 108
			recordDate: 2013-02-12 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appConfrNumber: 8848
	appFilingDate: 2013-02-12T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Gregory Branchek Roth
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Matthew James Wren
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Eric Jason Brandwine
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Haymarket, 
			geoCode: VA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Brian Irl Pratt
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 4
	appCls: 713
	_version_: 1584646134649847812
	lastUpdatedTimestamp: 2017-11-21T03:51:59.202Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 4e446174-b94c-4fff-9a7f-c35223ddaa58
	responseHeader:
			status: 0
			QTime: 18
