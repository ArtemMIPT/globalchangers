DOCUMENT_BODY:
	appGrpArtNumber: 2687
	appGrpArtNumberFacet: 2687
	transactions:
		№ 0
			recordDate: 2015-08-17 00:00:00
			code: N423
			description: Post Issue Communication - Certificate of Correction
		№ 1
			recordDate: 2014-06-24 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2014-06-24 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2014-06-05 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2014-06-04 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2014-05-21 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2014-05-20 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2014-05-13 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 8
			recordDate: 2014-05-13 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2014-05-13 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2014-05-12 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 11
			recordDate: 2014-04-01 00:00:00
			code: MM327-O
			description: Mail PUBS Notice Requiring Inventors Oath or Declaration
		№ 12
			recordDate: 2014-03-28 00:00:00
			code: M327-O
			description: PUBS Notice Requiring Inventors Oath or Declaration
		№ 13
			recordDate: 2014-02-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 14
			recordDate: 2014-02-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 15
			recordDate: 2014-02-14 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 16
			recordDate: 2014-02-11 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 17
			recordDate: 2014-02-02 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 18
			recordDate: 2014-01-31 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 19
			recordDate: 2014-01-31 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 20
			recordDate: 2014-01-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 21
			recordDate: 2014-01-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 22
			recordDate: 2014-01-06 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 23
			recordDate: 2013-12-02 00:00:00
			code: CTFR
			description: Final Rejection
		№ 24
			recordDate: 2013-11-22 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 25
			recordDate: 2013-11-15 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 26
			recordDate: 2013-08-15 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 27
			recordDate: 2013-08-15 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 28
			recordDate: 2013-08-15 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 29
			recordDate: 2013-06-03 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 30
			recordDate: 2013-05-16 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 31
			recordDate: 2013-05-16 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 32
			recordDate: 2013-05-16 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 33
			recordDate: 2013-05-09 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 34
			recordDate: 2013-05-07 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 35
			recordDate: 2013-04-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 36
			recordDate: 2013-04-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 37
			recordDate: 2013-04-19 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 38
			recordDate: 2013-04-19 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 39
			recordDate: 2013-04-19 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 40
			recordDate: 2013-04-18 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 41
			recordDate: 2013-04-18 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 42
			recordDate: 2013-03-17 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 43
			recordDate: 2013-03-14 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 44
			recordDate: 2013-03-13 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 45
			recordDate: 2013-03-13 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8760287
	applIdStr: 13799230
	appl_id_txt: 13799230
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13799230
	appSubCls: 539130
	appLocation: ELECTRONIC
	appAttrDockNumber: 170101-1622
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2014-06-04T10:05:21Z
	LAST_MOD_TS: 2017-06-15T12:19:50Z
	patentIssueDate: 2014-06-24T04:00:00Z
	APP_IND: 1
	appExamName: POPE, DARYL C
	appExamNameFacet: POPE, DARYL C
	firstInventorFile: No
	appClsSubCls: 340/539130
	appClsSubClsFacet: 340/539130
	applId: 13799230
	patentTitle: LOCATION AWARE REMINDERS
	appIntlPubNumber: 
	appConfrNumber: 8918
	appFilingDate: 2013-03-13T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Luan K. Nguyen
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Christopher L. Scofield
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 340
	_version_: 1580433223273414661
	lastUpdatedTimestamp: 2017-10-05T15:49:33.843Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: cc915edb-47a7-45a5-937d-e5065d213ba1
	responseHeader:
			status: 0
			QTime: 16
