DOCUMENT_BODY:
	appGrpArtNumber: 2131
	appGrpArtNumberFacet: 2131
	transactions:
		№ 0
			recordDate: 2016-02-23 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-02-23 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-02-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-02-03 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-01-22 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-01-22 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-01-13 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-10-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 8
			recordDate: 2015-10-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 9
			recordDate: 2015-10-13 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 10
			recordDate: 2015-10-08 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 11
			recordDate: 2015-10-04 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 12
			recordDate: 2015-10-04 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 13
			recordDate: 2015-07-13 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 14
			recordDate: 2015-07-13 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 15
			recordDate: 2015-06-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 16
			recordDate: 2015-06-15 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 17
			recordDate: 2015-06-15 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 18
			recordDate: 2015-02-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 19
			recordDate: 2015-02-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 20
			recordDate: 2015-02-13 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 21
			recordDate: 2015-02-08 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 22
			recordDate: 2015-02-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 23
			recordDate: 2015-02-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 24
			recordDate: 2015-02-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 25
			recordDate: 2014-05-12 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 26
			recordDate: 2013-05-10 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 27
			recordDate: 2013-01-31 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 28
			recordDate: 2013-01-31 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 29
			recordDate: 2012-12-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 30
			recordDate: 2012-12-19 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 31
			recordDate: 2012-12-10 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 32
			recordDate: 2012-12-04 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 33
			recordDate: 2012-11-28 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 34
			recordDate: 2012-11-28 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 35
			recordDate: 2012-11-28 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 36
			recordDate: 2012-11-28 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 37
			recordDate: 2012-11-28 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 38
			recordDate: 2012-11-27 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 39
			recordDate: 2012-11-04 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 40
			recordDate: 2012-10-31 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 41
			recordDate: 2012-10-31 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 42
			recordDate: 2012-10-31 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 43
			recordDate: 2012-10-31 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 44
			recordDate: 2012-10-31 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9268652
	applIdStr: 13665708
	appl_id_txt: 13665708
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13665708
	appSubCls: 123000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-45100
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 5
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|5}
	appStatusDate: 2016-02-03T10:41:17Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-02-23T05:00:00Z
	APP_IND: 1
	appExamName: MACKALL, LARRY T
	appExamNameFacet: MACKALL, LARRY T
	firstInventorFile: No
	appClsSubCls: 711/123000
	appClsSubClsFacet: 711/123000
	applId: 13665708
	patentTitle: CACHED VOLUMES AT STORAGE GATEWAYS
	appIntlPubNumber: 
	appConfrNumber: 1048
	appFilingDate: 2012-10-31T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: DAVID CARL SALYERS
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: PRADEEP  VINCENT
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: KENMORE, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: ANKUR  KHETRAPAL
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: KESTUTIS  PATIEJUNAS
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SAMMAMISH, 
			geoCode: WA
			country: (US)
			rankNo: 4
	appCls: 711
	_version_: 1580433196048187394
	lastUpdatedTimestamp: 2017-10-05T15:49:07.814Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: a90b2916-4a41-40a9-9c00-a319afeb849f
	responseHeader:
			status: 0
			QTime: 15
