DOCUMENT_BODY:
	appGrpArtNumber: 2176
	appGrpArtNumberFacet: 2176
	transactions:
		№ 0
			recordDate: 2016-08-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 1
			recordDate: 2016-08-11 00:00:00
			code: MABN2
			description: Mail Abandonment for Failure to Respond to Office Action
		№ 2
			recordDate: 2016-08-08 00:00:00
			code: ABN2
			description: Aband. for Failure to Respond to O. A.
		№ 3
			recordDate: 2016-02-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 4
			recordDate: 2016-02-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 5
			recordDate: 2016-02-02 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 6
			recordDate: 2016-01-27 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 7
			recordDate: 2016-01-08 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 8
			recordDate: 2016-01-08 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 9
			recordDate: 2015-12-28 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 10
			recordDate: 2015-12-28 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 11
			recordDate: 2015-11-24 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 12
			recordDate: 2015-11-17 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 13
			recordDate: 2015-11-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 14
			recordDate: 2015-11-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 15
			recordDate: 2015-11-05 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 16
			recordDate: 2015-10-30 00:00:00
			code: CTFR
			description: Final Rejection
		№ 17
			recordDate: 2015-08-20 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 18
			recordDate: 2015-08-17 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 19
			recordDate: 2015-08-07 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 20
			recordDate: 2015-08-03 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 21
			recordDate: 2015-08-03 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 22
			recordDate: 2015-05-20 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 23
			recordDate: 2015-05-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 24
			recordDate: 2015-05-20 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 25
			recordDate: 2015-05-16 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 26
			recordDate: 2015-05-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 27
			recordDate: 2015-05-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 28
			recordDate: 2015-03-18 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 29
			recordDate: 2015-03-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 30
			recordDate: 2015-01-20 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 31
			recordDate: 2015-01-13 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 32
			recordDate: 2014-07-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 33
			recordDate: 2014-07-10 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 34
			recordDate: 2014-06-13 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 35
			recordDate: 2014-06-13 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 36
			recordDate: 2014-01-10 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 37
			recordDate: 2014-01-10 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 38
			recordDate: 2014-01-10 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 39
			recordDate: 2014-01-10 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 40
			recordDate: 2014-01-09 00:00:00
			code: PG-PB-DT
			description: PG-Pub Notice of new or Revised projected publication date
		№ 41
			recordDate: 2014-01-07 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 42
			recordDate: 2013-05-13 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 43
			recordDate: 2013-04-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 44
			recordDate: 2013-04-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 45
			recordDate: 2013-04-02 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 46
			recordDate: 2013-04-02 00:00:00
			code: MPEN
			description: Mail Pre-Exam Notice
		№ 47
			recordDate: 2013-04-02 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 48
			recordDate: 2013-03-20 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 49
			recordDate: 2013-02-08 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 50
			recordDate: 2013-02-08 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 51
			recordDate: 2013-02-08 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 52
			recordDate: 2013-02-07 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 53
			recordDate: 2013-01-15 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 54
			recordDate: 2013-01-09 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 55
			recordDate: 2013-01-09 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 56
			recordDate: 2013-01-09 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Abandoned  --  Failure to Respond to an Office Action
	appStatus_txt: Abandoned  --  Failure to Respond to an Office Action
	appStatusFacet: Abandoned  --  Failure to Respond to an Office Action
	patentNumber: 
	applIdStr: 13737596
	appl_id_txt: 13737596
	appEarlyPubNumber: US20140195890A1
	appEntityStatus: UNDISCOUNTED
	id: 13737596
	appSubCls: 234000
	appLocation: ELECTRONIC
	appAttrDockNumber: SPARC.750A
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 20995
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 9
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|9}
	appStatusDate: 2016-08-08T13:31:56Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-07-10T04:00:00Z
	APP_IND: 1
	appExamName: MILLS, FRANK D
	appExamNameFacet: MILLS, FRANK D
	firstInventorFile: No
	appClsSubCls: 715/234000
	appClsSubClsFacet: 715/234000
	applId: 13737596
	patentTitle: BROWSER INTERFACE FOR ACCESSING SUPPLEMENTAL CONTENT ASSOCIATED WITH CONTENT PAGES
	appIntlPubNumber: 
	appConfrNumber: 6184
	appFilingDate: 2013-01-09T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Brett Richard Taylor
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bainbridge Island, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Ameet Nirmal Vaswani
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Peter Frank Hill
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Jason Daniel Landry
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Ranganath  Atreya
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Kirkland, 
			geoCode: WA
			country: (US)
			rankNo: 5
		№ 5
			nameLineOne: Yang  Xu
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 6
		№ 6
			nameLineOne: Charley  Ames
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 7
		№ 7
			nameLineOne: Christopher James Sullins
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 8
	appCls: 715
	_version_: 1580433210774388737
	lastUpdatedTimestamp: 2017-10-05T15:49:21.922Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 38365122-7fb9-41d5-b2d7-e4dacdf0b560
	responseHeader:
			status: 0
			QTime: 16
