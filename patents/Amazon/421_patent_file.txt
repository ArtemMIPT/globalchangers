DOCUMENT_BODY:
	appGrpArtNumber: 2156
	appGrpArtNumberFacet: 2156
	transactions:
		№ 0
			recordDate: 2016-01-19 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-01-19 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-12-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-12-29 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-12-14 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-12-14 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-12-09 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-12-09 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-09-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2015-09-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2015-09-23 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2015-09-19 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2015-09-16 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 13
			recordDate: 2015-09-16 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 14
			recordDate: 2015-09-11 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 15
			recordDate: 2015-09-11 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 16
			recordDate: 2015-09-04 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 17
			recordDate: 2015-09-04 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 18
			recordDate: 2015-09-04 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 19
			recordDate: 2015-04-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 20
			recordDate: 2015-04-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 21
			recordDate: 2015-04-06 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 22
			recordDate: 2015-04-01 00:00:00
			code: CTFR
			description: Final Rejection
		№ 23
			recordDate: 2015-03-01 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 24
			recordDate: 2015-02-24 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 25
			recordDate: 2014-11-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 26
			recordDate: 2014-11-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 27
			recordDate: 2014-11-24 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 28
			recordDate: 2014-11-19 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 29
			recordDate: 2014-09-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 30
			recordDate: 2013-07-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 31
			recordDate: 2013-07-22 00:00:00
			code: FLRCPT.C
			description: Filing Receipt - Corrected
		№ 32
			recordDate: 2013-05-14 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 33
			recordDate: 2013-05-13 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 34
			recordDate: 2013-04-23 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 35
			recordDate: 2013-04-23 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 36
			recordDate: 2013-04-23 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 37
			recordDate: 2013-04-23 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 38
			recordDate: 2013-04-23 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 39
			recordDate: 2013-04-22 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 40
			recordDate: 2013-04-22 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 41
			recordDate: 2013-03-19 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 42
			recordDate: 2013-03-15 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 43
			recordDate: 2013-03-13 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 44
			recordDate: 2013-03-13 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9239852
	applIdStr: 13802069
	appl_id_txt: 13802069
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13802069
	appSubCls: 756000
	appLocation: ELECTRONIC
	appAttrDockNumber: 101058.000062
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 23377
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 7
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|7}
	appStatusDate: 2015-12-29T11:35:53Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-01-19T05:00:00Z
	APP_IND: 1
	appExamName: VO, TRUONG V
	appExamNameFacet: VO, TRUONG V
	firstInventorFile: No
	appClsSubCls: 707/756000
	appClsSubClsFacet: 707/756000
	applId: 13802069
	patentTitle: Item Collections
	appIntlPubNumber: 
	appConfrNumber: 6675
	appFilingDate: 2013-03-13T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Xianglong  Huang
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Stefano  Stefani
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Issaquah, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 2
			nameLineOne: Somasundaram  Perianayagam
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 6
		№ 3
			nameLineOne: David Alan Lutz
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Renton, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 4
			nameLineOne: Wei  Xiao
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Kirkland, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 5
			nameLineOne: Timothy Andrew Rath
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 5
	appCls: 707
	_version_: 1580433224043069440
	lastUpdatedTimestamp: 2017-10-05T15:49:34.565Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 15a4ef0e-add3-4358-9cd3-7d1e69d9afa9
	responseHeader:
			status: 0
			QTime: 18
