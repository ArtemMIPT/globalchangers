DOCUMENT_BODY:
	appGrpArtNumber: 2682
	appGrpArtNumberFacet: 2682
	transactions:
		№ 0
			recordDate: 2016-10-04 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-10-04 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-09-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-09-14 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-09-01 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-09-01 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-08-31 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 7
			recordDate: 2016-08-31 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2016-08-31 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2016-06-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2016-06-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2016-06-14 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 12
			recordDate: 2016-06-10 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 13
			recordDate: 2016-06-06 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 14
			recordDate: 2016-06-06 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 15
			recordDate: 2016-06-01 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 16
			recordDate: 2016-06-01 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 17
			recordDate: 2016-05-29 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 18
			recordDate: 2016-05-29 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 19
			recordDate: 2016-05-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 20
			recordDate: 2016-02-26 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 21
			recordDate: 2016-02-26 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 22
			recordDate: 2016-02-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 23
			recordDate: 2016-02-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 24
			recordDate: 2016-02-17 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 25
			recordDate: 2016-02-10 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 26
			recordDate: 2016-02-10 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 27
			recordDate: 2016-02-09 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 28
			recordDate: 2016-02-09 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 29
			recordDate: 2016-02-08 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 30
			recordDate: 2016-02-08 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 31
			recordDate: 2016-02-08 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 32
			recordDate: 2016-01-20 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 33
			recordDate: 2016-01-19 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 34
			recordDate: 2016-01-19 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 35
			recordDate: 2015-12-16 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 36
			recordDate: 2015-12-10 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 37
			recordDate: 2015-11-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 38
			recordDate: 2015-11-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 39
			recordDate: 2015-11-17 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 40
			recordDate: 2015-11-11 00:00:00
			code: CTFR
			description: Final Rejection
		№ 41
			recordDate: 2015-11-09 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 42
			recordDate: 2015-11-09 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 43
			recordDate: 2015-09-16 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 44
			recordDate: 2015-09-16 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 45
			recordDate: 2015-09-16 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 46
			recordDate: 2015-08-27 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 47
			recordDate: 2015-08-24 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 48
			recordDate: 2015-07-14 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 49
			recordDate: 2015-07-07 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 50
			recordDate: 2015-07-07 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 51
			recordDate: 2015-06-06 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 52
			recordDate: 2015-06-06 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 53
			recordDate: 2015-06-06 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 54
			recordDate: 2015-05-22 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 55
			recordDate: 2015-05-22 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 56
			recordDate: 2015-05-22 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 57
			recordDate: 2015-05-18 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 58
			recordDate: 2014-12-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 59
			recordDate: 2014-02-10 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 60
			recordDate: 2013-11-30 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 61
			recordDate: 2013-11-20 00:00:00
			code: TI1050
			description: Transfer Inquiry to GAU
		№ 62
			recordDate: 2013-10-08 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 63
			recordDate: 2013-05-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 64
			recordDate: 2013-02-19 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 65
			recordDate: 2013-01-09 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 66
			recordDate: 2013-01-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 67
			recordDate: 2013-01-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 68
			recordDate: 2013-01-09 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 69
			recordDate: 2013-01-09 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 70
			recordDate: 2013-01-08 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 71
			recordDate: 2012-12-05 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 72
			recordDate: 2012-12-04 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 73
			recordDate: 2012-12-04 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 74
			recordDate: 2012-12-04 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 75
			recordDate: 2012-12-04 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9461873
	applIdStr: 13693640
	appl_id_txt: 13693640
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13693640
	appSubCls: 005200
	appLocation: ELECTRONIC
	appAttrDockNumber: 090204-0855761 (061500US)
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 107508
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2016-09-14T09:53:00Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-10-04T04:00:00Z
	APP_IND: 1
	appExamName: AKKI, MUNEAR T
	appExamNameFacet: AKKI, MUNEAR T
	firstInventorFile: No
	appClsSubCls: 340/005200
	appClsSubClsFacet: 340/005200
	applId: 13693640
	patentTitle: LAYERED DATACENTER
	appIntlPubNumber: 
	appConfrNumber: 1272
	appFilingDate: 2012-12-04T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Michael David Marr
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Monroe, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: David Edward Bryan
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Max Jesse Wishman
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
	appCls: 340
	_version_: 1580433201826889730
	lastUpdatedTimestamp: 2017-10-05T15:49:13.356Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 397b5564-109f-402b-a2a8-4568b73be568
	responseHeader:
			status: 0
			QTime: 15
