DOCUMENT_BODY:
	appGrpArtNumber: 2657
	appGrpArtNumberFacet: 2657
	transactions:
		№ 0
			recordDate: 2016-02-12 00:00:00
			code: N423
			description: Post Issue Communication - Certificate of Correction
		№ 1
			recordDate: 2015-06-30 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2015-06-30 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2015-06-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2015-06-10 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2015-05-29 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2015-05-28 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2015-05-26 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2015-05-26 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2015-03-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2015-03-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2015-03-05 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 12
			recordDate: 2015-03-02 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 13
			recordDate: 2015-02-27 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 14
			recordDate: 2015-01-28 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 15
			recordDate: 2015-01-27 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 16
			recordDate: 2015-01-15 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 17
			recordDate: 2015-01-15 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 18
			recordDate: 2014-10-27 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 19
			recordDate: 2014-10-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 20
			recordDate: 2014-10-27 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 21
			recordDate: 2014-10-17 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 22
			recordDate: 2014-07-11 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 23
			recordDate: 2014-06-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 24
			recordDate: 2013-08-12 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 25
			recordDate: 2013-04-25 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 26
			recordDate: 2013-04-09 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 27
			recordDate: 2013-04-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 28
			recordDate: 2013-04-09 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 29
			recordDate: 2013-04-08 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 30
			recordDate: 2013-03-22 00:00:00
			code: ADDFLFEE
			description: Additional Application Filing Fees
		№ 31
			recordDate: 2013-01-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 32
			recordDate: 2013-01-23 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 33
			recordDate: 2013-01-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 34
			recordDate: 2013-01-23 00:00:00
			code: INCD
			description: Notice Mailed--Application Incomplete--Filing Date Assigned 
		№ 35
			recordDate: 2013-01-23 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 36
			recordDate: 2013-01-21 00:00:00
			code: OATHDECL
			description: A statement by one or more inventors satisfying the requirement under 35 USC 115, Oath of the Applic
		№ 37
			recordDate: 2012-12-26 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 38
			recordDate: 2012-12-20 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 39
			recordDate: 2012-12-19 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 40
			recordDate: 2012-12-19 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9070366
	applIdStr: 13720909
	appl_id_txt: 13720909
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13720909
	appSubCls: 009000
	appLocation: ELECTRONIC
	appAttrDockNumber: SEAZN.756A
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 79502
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 6
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|6}
	appStatusDate: 2015-06-10T11:48:40Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-06-30T04:00:00Z
	APP_IND: 1
	appExamName: PULLIAS, JESSE SCOTT
	appExamNameFacet: PULLIAS, JESSE SCOTT
	firstInventorFile: No
	appClsSubCls: 704/009000
	appClsSubClsFacet: 704/009000
	applId: 13720909
	patentTitle: ARCHITECTURE FOR MULTI-DOMAIN UTTERANCE PROCESSING
	appIntlPubNumber: 
	appConfrNumber: 3369
	appFilingDate: 2012-12-19T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Lambert  Mathias
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Arlington, 
			geoCode: MA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Ying  Shi
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Imre Attila Kiss
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Arlington, 
			geoCode: MA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Ryan Paul Thomas
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Redmond, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Frederic Johan Georges Deramat
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 5
	appCls: 704
	_version_: 1580433207359176708
	lastUpdatedTimestamp: 2017-10-05T15:49:18.661Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 08b71c7b-e732-4071-856a-34b5a913df41
	responseHeader:
			status: 0
			QTime: 17
