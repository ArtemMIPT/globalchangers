DOCUMENT_BODY:
	appGrpArtNumber: 2196
	appGrpArtNumberFacet: 2196
	transactions:
		№ 0
			recordDate: 2016-02-09 00:00:00
			code: CDEN
			description: Post Issue Communication - Certificate of Correction Denied
		№ 1
			recordDate: 2016-02-09 00:00:00
			code: M327-G
			description: POST ISSUE OTHER COMMUNICATION TO APPLICANT- CERTIFICATE OF CORRECTION
		№ 2
			recordDate: 2015-11-10 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 3
			recordDate: 2015-11-10 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 4
			recordDate: 2015-10-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2015-10-21 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 6
			recordDate: 2015-10-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 7
			recordDate: 2015-10-06 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 8
			recordDate: 2015-10-06 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 9
			recordDate: 2015-10-05 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 10
			recordDate: 2015-09-30 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 11
			recordDate: 2015-09-30 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 12
			recordDate: 2015-09-29 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 13
			recordDate: 2015-09-28 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 14
			recordDate: 2015-09-28 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 15
			recordDate: 2015-09-28 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 16
			recordDate: 2015-09-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 17
			recordDate: 2015-09-15 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 18
			recordDate: 2015-09-14 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 19
			recordDate: 2015-09-14 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 20
			recordDate: 2015-08-13 00:00:00
			code: MM327-O
			description: Mail PUBS Notice Requiring Inventors Oath or Declaration
		№ 21
			recordDate: 2015-08-13 00:00:00
			code: MM327-O
			description: Mail PUBS Notice Requiring Inventors Oath or Declaration
		№ 22
			recordDate: 2015-08-11 00:00:00
			code: M327-O
			description: PUBS Notice Requiring Inventors Oath or Declaration
		№ 23
			recordDate: 2015-08-11 00:00:00
			code: M327-O
			description: PUBS Notice Requiring Inventors Oath or Declaration
		№ 24
			recordDate: 2015-07-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 25
			recordDate: 2015-07-09 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 26
			recordDate: 2015-07-09 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 27
			recordDate: 2015-07-06 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 28
			recordDate: 2015-07-06 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 29
			recordDate: 2015-07-06 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 30
			recordDate: 2015-07-04 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 31
			recordDate: 2015-07-01 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 32
			recordDate: 2015-07-01 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 33
			recordDate: 2015-07-01 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 34
			recordDate: 2015-07-01 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 35
			recordDate: 2015-07-01 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 36
			recordDate: 2015-05-26 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 37
			recordDate: 2015-05-26 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 38
			recordDate: 2015-05-26 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 39
			recordDate: 2015-05-20 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 40
			recordDate: 2015-05-15 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 41
			recordDate: 2015-05-15 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 42
			recordDate: 2015-05-14 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 43
			recordDate: 2015-05-14 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 44
			recordDate: 2015-04-10 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 45
			recordDate: 2015-04-08 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 46
			recordDate: 2015-04-08 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 47
			recordDate: 2015-04-07 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 48
			recordDate: 2015-03-31 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 49
			recordDate: 2015-03-31 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 50
			recordDate: 2014-12-10 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 51
			recordDate: 2014-12-10 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 52
			recordDate: 2014-12-10 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 53
			recordDate: 2014-12-05 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 54
			recordDate: 2014-07-22 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 55
			recordDate: 2013-08-20 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 56
			recordDate: 2013-05-17 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 57
			recordDate: 2013-05-03 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 58
			recordDate: 2013-05-03 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 59
			recordDate: 2013-05-03 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 60
			recordDate: 2013-04-22 00:00:00
			code: ADDFLFEE
			description: Additional Application Filing Fees
		№ 61
			recordDate: 2013-02-21 00:00:00
			code: INCD
			description: Notice Mailed--Application Incomplete--Filing Date Assigned 
		№ 62
			recordDate: 2013-02-21 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 63
			recordDate: 2013-01-29 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 64
			recordDate: 2013-01-28 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 65
			recordDate: 2013-01-25 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 66
			recordDate: 2013-01-25 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 67
			recordDate: 2013-01-25 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9183049
	applIdStr: 13750965
	appl_id_txt: 13750965
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13750965
	appSubCls: 104000
	appLocation: ELECTRONIC
	appAttrDockNumber: 20346.042601
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 131836
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 7
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|7}
	appStatusDate: 2015-10-21T09:07:08Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-11-10T05:00:00Z
	APP_IND: 1
	appExamName: SWIFT, CHARLES M
	appExamNameFacet: SWIFT, CHARLES M
	firstInventorFile: No
	appClsSubCls: 718/104000
	appClsSubClsFacet: 718/104000
	applId: 13750965
	patentTitle: PROCESSING CONTENT USING PIPELINES
	appIntlPubNumber: 
	appConfrNumber: 4073
	appFilingDate: 2013-01-25T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Jonathan  Corley
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: David  Sayed
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Chris  Hawes
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Bradley  Marshall
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bainbridge Island, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Jims  Carrig
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 5
		№ 5
			nameLineOne: Jeff  Ramsden
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 6
	appCls: 718
	_version_: 1580433213626515456
	lastUpdatedTimestamp: 2017-10-05T15:49:24.642Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 043f6711-5f32-4b47-91a2-1be2571e614b
	responseHeader:
			status: 0
			QTime: 16
