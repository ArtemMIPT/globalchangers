DOCUMENT_BODY:
	appGrpArtNumber: 2441
	appGrpArtNumberFacet: 2441
	transactions:
		№ 0
			recordDate: 2017-01-24 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2017-01-24 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 2
			recordDate: 2017-01-24 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2017-01-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2017-01-04 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2016-12-16 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2016-12-16 00:00:00
			code: MM327-B
			description: Mail PUB Notice of non-compliant IDS
		№ 7
			recordDate: 2016-12-14 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 8
			recordDate: 2016-12-14 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 9
			recordDate: 2016-12-14 00:00:00
			code: M327-B
			description: PUB Notice of non-compliant IDS
		№ 10
			recordDate: 2016-12-12 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 11
			recordDate: 2016-12-12 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 12
			recordDate: 2016-12-12 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 13
			recordDate: 2016-12-12 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 14
			recordDate: 2016-12-12 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 15
			recordDate: 2016-09-26 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 16
			recordDate: 2016-09-26 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 17
			recordDate: 2016-09-26 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 18
			recordDate: 2016-09-21 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 19
			recordDate: 2016-09-18 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 20
			recordDate: 2016-09-18 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 21
			recordDate: 2016-09-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 22
			recordDate: 2016-09-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 23
			recordDate: 2016-08-15 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 24
			recordDate: 2016-08-15 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 25
			recordDate: 2016-08-08 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 26
			recordDate: 2016-08-08 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 27
			recordDate: 2016-08-08 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 28
			recordDate: 2016-08-08 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 29
			recordDate: 2016-08-08 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 30
			recordDate: 2016-08-08 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 31
			recordDate: 2016-07-25 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 32
			recordDate: 2016-07-25 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 33
			recordDate: 2016-07-20 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 34
			recordDate: 2016-07-20 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 35
			recordDate: 2016-07-19 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 36
			recordDate: 2016-06-16 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 37
			recordDate: 2016-06-07 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 38
			recordDate: 2016-06-07 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 39
			recordDate: 2016-06-07 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 40
			recordDate: 2016-06-01 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 41
			recordDate: 2016-05-24 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 42
			recordDate: 2016-02-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 43
			recordDate: 2016-02-09 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 44
			recordDate: 2016-02-09 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 45
			recordDate: 2016-02-03 00:00:00
			code: CTFR
			description: Final Rejection
		№ 46
			recordDate: 2016-01-12 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 47
			recordDate: 2016-01-12 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 48
			recordDate: 2015-11-10 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 49
			recordDate: 2015-11-06 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 50
			recordDate: 2015-11-06 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 51
			recordDate: 2015-11-06 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 52
			recordDate: 2015-11-06 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 53
			recordDate: 2015-09-30 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 54
			recordDate: 2015-09-22 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 55
			recordDate: 2015-08-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 56
			recordDate: 2015-08-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 57
			recordDate: 2015-08-06 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 58
			recordDate: 2015-08-03 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 59
			recordDate: 2015-07-29 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 60
			recordDate: 2015-07-29 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 61
			recordDate: 2015-07-29 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 62
			recordDate: 2015-07-29 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 63
			recordDate: 2015-07-29 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 64
			recordDate: 2015-07-29 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 65
			recordDate: 2015-07-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 66
			recordDate: 2015-07-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 67
			recordDate: 2015-07-29 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 68
			recordDate: 2015-07-29 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 69
			recordDate: 2015-07-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 70
			recordDate: 2015-07-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 71
			recordDate: 2015-07-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 72
			recordDate: 2015-07-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 73
			recordDate: 2015-07-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 74
			recordDate: 2015-07-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 75
			recordDate: 2015-07-27 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 76
			recordDate: 2015-07-27 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 77
			recordDate: 2015-07-27 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 78
			recordDate: 2015-07-27 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 79
			recordDate: 2015-07-27 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 80
			recordDate: 2015-07-27 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 81
			recordDate: 2015-07-27 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 82
			recordDate: 2015-06-08 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 83
			recordDate: 2015-06-08 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 84
			recordDate: 2015-06-08 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 85
			recordDate: 2015-05-07 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 86
			recordDate: 2015-05-07 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 87
			recordDate: 2015-05-07 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 88
			recordDate: 2015-03-26 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 89
			recordDate: 2015-03-26 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 90
			recordDate: 2015-03-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 91
			recordDate: 2014-11-24 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 92
			recordDate: 2014-09-19 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 93
			recordDate: 2014-09-19 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 94
			recordDate: 2014-09-19 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 95
			recordDate: 2014-07-23 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 96
			recordDate: 2014-07-23 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 97
			recordDate: 2014-07-23 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 98
			recordDate: 2014-04-21 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 99
			recordDate: 2014-04-21 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 100
			recordDate: 2014-04-21 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 101
			recordDate: 2014-02-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 102
			recordDate: 2013-11-26 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 103
			recordDate: 2013-11-26 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 104
			recordDate: 2013-11-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 105
			recordDate: 2013-07-23 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 106
			recordDate: 2013-07-03 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 107
			recordDate: 2013-07-03 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 108
			recordDate: 2013-06-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 109
			recordDate: 2013-06-06 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 110
			recordDate: 2013-06-06 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 111
			recordDate: 2013-06-05 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 112
			recordDate: 2013-06-05 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 113
			recordDate: 2013-05-06 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 114
			recordDate: 2013-04-29 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 115
			recordDate: 2013-04-29 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 116
			recordDate: 2013-04-29 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 117
			recordDate: 2013-04-29 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9553787
	applIdStr: 13873009
	appl_id_txt: 13873009
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13873009
	appSubCls: 224000
	appLocation: ELECTRONIC
	appAttrDockNumber: SEAZN.800A
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 79502
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2017-01-04T11:58:11Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2017-01-24T05:00:00Z
	APP_IND: 1
	appExamName: TSENG, LEON Y
	appExamNameFacet: TSENG, LEON Y
	firstInventorFile: Yes
	appClsSubCls: 709/224000
	appClsSubClsFacet: 709/224000
	applId: 13873009
	patentTitle: MONITORING HOSTED SERVICE USAGE
	appIntlPubNumber: 
	appConfrNumber: 4408
	appFilingDate: 2013-04-29T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Thomas Charles Stickle
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Saint James, 
			geoCode: NY
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: David Samuel Zipkin
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 709
	_version_: 1580433238817505282
	lastUpdatedTimestamp: 2017-10-05T15:49:48.66Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 735b84c0-d1c3-4f21-b74f-5f61b338de75
	responseHeader:
			status: 0
			QTime: 16
