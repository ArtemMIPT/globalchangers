DOCUMENT_BODY:
	appGrpArtNumber: 2488
	appGrpArtNumberFacet: 2488
	transactions:
		№ 0
			recordDate: 2016-07-15 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2015-07-07 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2015-07-07 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2015-06-18 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2015-06-17 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2015-06-04 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2015-06-04 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2015-06-03 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2015-06-03 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2015-03-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2015-03-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2015-03-17 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 12
			recordDate: 2015-03-13 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 13
			recordDate: 2015-03-11 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 14
			recordDate: 2014-12-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 15
			recordDate: 2014-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 16
			recordDate: 2014-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 17
			recordDate: 2013-12-14 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 18
			recordDate: 2013-05-08 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 19
			recordDate: 2013-04-11 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 20
			recordDate: 2013-04-11 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 21
			recordDate: 2013-04-11 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 22
			recordDate: 2013-04-11 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 23
			recordDate: 2013-04-11 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 24
			recordDate: 2013-03-13 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 25
			recordDate: 2013-03-06 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 26
			recordDate: 2013-03-06 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 27
			recordDate: 2013-03-06 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 28
			recordDate: 2013-03-06 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9077891
	applIdStr: 13787608
	appl_id_txt: 13787608
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13787608
	appSubCls: 046000
	appLocation: ELECTRONIC
	appAttrDockNumber: 085101-526923.401NPUS00
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136608
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2015-06-17T09:02:09Z
	LAST_MOD_TS: 2017-08-30T23:33:57Z
	patentIssueDate: 2015-07-07T04:00:00Z
	APP_IND: 1
	appExamName: DULEY, JANESE
	appExamNameFacet: DULEY, JANESE
	firstInventorFile: No
	appClsSubCls: 348/046000
	appClsSubClsFacet: 348/046000
	applId: 13787608
	patentTitle: DEPTH DETERMINATION USING CAMERA FOCUS
	appIntlPubNumber: 
	appConfrNumber: 2459
	appFilingDate: 2013-03-06T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Leo Benedict Baldwin
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Cupertino, 
			geoCode: CA
			country: (US)
			rankNo: 1
	appCls: 348
	_version_: 1580433221181505537
	lastUpdatedTimestamp: 2017-10-05T15:49:31.815Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 97c7c182-a293-42b3-a857-160cc92d5dd6
	responseHeader:
			status: 0
			QTime: 17
