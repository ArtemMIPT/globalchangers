DOCUMENT_BODY:
	appGrpArtNumber: 2144
	appGrpArtNumberFacet: 2144
	transactions:
		№ 0
			recordDate: 2017-01-10 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2017-01-10 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-12-21 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 3
			recordDate: 2016-12-08 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 4
			recordDate: 2016-12-08 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 5
			recordDate: 2016-12-06 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 6
			recordDate: 2016-12-06 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 7
			recordDate: 2016-09-20 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 8
			recordDate: 2016-09-19 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 9
			recordDate: 2016-06-08 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 10
			recordDate: 2016-06-08 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 11
			recordDate: 2016-06-07 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 12
			recordDate: 2016-06-07 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 13
			recordDate: 2016-04-27 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 14
			recordDate: 2016-04-21 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 15
			recordDate: 2016-03-08 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 16
			recordDate: 2016-03-07 00:00:00
			code: CTFR
			description: Final Rejection
		№ 17
			recordDate: 2015-12-17 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 18
			recordDate: 2015-12-08 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 19
			recordDate: 2015-12-02 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 20
			recordDate: 2015-11-17 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 21
			recordDate: 2015-09-09 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 22
			recordDate: 2015-09-08 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 23
			recordDate: 2015-07-24 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 24
			recordDate: 2015-07-24 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 25
			recordDate: 2015-07-23 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 26
			recordDate: 2015-07-23 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 27
			recordDate: 2015-07-23 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 28
			recordDate: 2015-07-01 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 29
			recordDate: 2015-06-12 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 30
			recordDate: 2015-06-12 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 31
			recordDate: 2015-06-12 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 32
			recordDate: 2015-06-12 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 33
			recordDate: 2015-05-28 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 34
			recordDate: 2015-05-26 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 35
			recordDate: 2015-05-26 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 36
			recordDate: 2015-05-19 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 37
			recordDate: 2015-05-11 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 38
			recordDate: 2015-05-11 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 39
			recordDate: 2015-03-24 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 40
			recordDate: 2015-03-23 00:00:00
			code: CTFR
			description: Final Rejection
		№ 41
			recordDate: 2015-03-09 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 42
			recordDate: 2015-02-25 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 43
			recordDate: 2015-02-19 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 44
			recordDate: 2015-02-19 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 45
			recordDate: 2015-02-19 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 46
			recordDate: 2015-02-19 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 47
			recordDate: 2015-02-05 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 48
			recordDate: 2015-01-27 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 49
			recordDate: 2015-01-27 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 50
			recordDate: 2014-11-19 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 51
			recordDate: 2014-11-17 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 52
			recordDate: 2014-10-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 53
			recordDate: 2013-01-23 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 54
			recordDate: 2013-01-23 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 55
			recordDate: 2012-12-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 56
			recordDate: 2012-11-13 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 57
			recordDate: 2012-10-19 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 58
			recordDate: 2012-10-19 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 59
			recordDate: 2012-10-19 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 60
			recordDate: 2012-09-24 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 61
			recordDate: 2012-09-19 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 62
			recordDate: 2012-09-19 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 63
			recordDate: 2012-09-19 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9542379
	applIdStr: 13623002
	appl_id_txt: 13623002
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13623002
	appSubCls: 235000
	appLocation: ELECTRONIC
	appAttrDockNumber: 08862.136 (L0136)
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 14432
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 7
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|7}
	appStatusDate: 2016-12-21T11:17:17Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2017-01-10T05:00:00Z
	APP_IND: 1
	appExamName: EDWARDS, JASON T
	appExamNameFacet: EDWARDS, JASON T
	firstInventorFile: No
	appClsSubCls: 715/235000
	appClsSubClsFacet: 715/235000
	applId: 13623002
	patentTitle: SYNCHRONIZING ELECTRONIC PUBLICATIONS BETWEEN USER DEVICES
	appIntlPubNumber: 
	appConfrNumber: 2340
	appFilingDate: 2012-09-19T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Kathirvel  Thannasi
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Chennai, 
			geoCode: 
			country: (IN)
			rankNo: 2
		№ 1
			nameLineOne: Ankur  Jain
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 2
			nameLineOne: Walter Manching Tseng
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 6
		№ 3
			nameLineOne: Lokesh  Joshi
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Mercer Island, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 4
			nameLineOne: Venkata Krishnan Ramamoorthy
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Chennai, 
			geoCode: 
			country: (IN)
			rankNo: 3
		№ 5
			nameLineOne: Palanidaran  Chidambaram
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Chennai, 
			geoCode: 
			country: (IN)
			rankNo: 5
	appCls: 715
	_version_: 1580433187124805634
	lastUpdatedTimestamp: 2017-10-05T15:48:59.366Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 2555c4ea-fcf8-4402-9cd6-c81e59fd1ab9
	responseHeader:
			status: 0
			QTime: 13
