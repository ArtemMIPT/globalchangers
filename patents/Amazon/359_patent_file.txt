DOCUMENT_BODY:
	appGrpArtNumber: 2845
	appGrpArtNumberFacet: 2845
	transactions:
		№ 0
			recordDate: 2015-09-08 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-09-08 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-08-19 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 3
			recordDate: 2015-08-04 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 4
			recordDate: 2015-08-04 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 5
			recordDate: 2015-08-03 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 6
			recordDate: 2015-08-03 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 7
			recordDate: 2015-05-15 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 8
			recordDate: 2015-05-14 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 9
			recordDate: 2015-04-21 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 10
			recordDate: 2015-04-17 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 11
			recordDate: 2015-01-22 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 12
			recordDate: 2015-01-12 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 13
			recordDate: 2014-10-27 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 14
			recordDate: 2013-10-03 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 15
			recordDate: 2013-05-10 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 16
			recordDate: 2013-05-07 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 17
			recordDate: 2013-04-09 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 18
			recordDate: 2013-04-09 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 19
			recordDate: 2013-04-09 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 20
			recordDate: 2013-04-09 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 21
			recordDate: 2013-04-09 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 22
			recordDate: 2013-03-13 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 23
			recordDate: 2013-03-08 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 24
			recordDate: 2013-03-07 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 25
			recordDate: 2013-03-07 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9130279
	applIdStr: 13789455
	appl_id_txt: 13789455
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13789455
	appSubCls: 853000
	appLocation: ELECTRONIC
	appAttrDockNumber: 33350.55 (L0055)
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 14432
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 5
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|5}
	appStatusDate: 2015-08-19T09:53:41Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-09-08T04:00:00Z
	APP_IND: 1
	appExamName: LE, HOANGANH T
	appExamNameFacet: LE, HOANGANH T
	firstInventorFile: No
	appClsSubCls: 343/853000
	appClsSubClsFacet: 343/853000
	applId: 13789455
	patentTitle: MULTI-FEED ANTENNA WITH INDEPENDENT TUNING CAPABILITY
	appIntlPubNumber: 
	appConfrNumber: 9336
	appFilingDate: 2013-03-07T05:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Tzung-I  Lee
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Jose, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: In Chul  Hyun
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Jose, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Cheol Su Kim
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Jose, 
			geoCode: CA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Jerry Weiming Kuo
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Jose, 
			geoCode: CA
			country: (US)
			rankNo: 4
	appCls: 343
	_version_: 1580433221527535619
	lastUpdatedTimestamp: 2017-10-05T15:49:32.161Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 97c7c182-a293-42b3-a857-160cc92d5dd6
	responseHeader:
			status: 0
			QTime: 17
