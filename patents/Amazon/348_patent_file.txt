DOCUMENT_BODY:
	appGrpArtNumber: 2467
	appGrpArtNumberFacet: 2467
	transactions:
		№ 0
			recordDate: 2015-06-16 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-06-16 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-05-28 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-05-27 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-05-22 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-05-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2015-05-14 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 7
			recordDate: 2015-05-12 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 8
			recordDate: 2015-02-06 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 9
			recordDate: 2015-02-06 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 10
			recordDate: 2015-01-26 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 11
			recordDate: 2015-01-26 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 12
			recordDate: 2015-01-23 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 13
			recordDate: 2014-12-10 00:00:00
			code: MM327-O
			description: Mail PUBS Notice Requiring Inventors Oath or Declaration
		№ 14
			recordDate: 2014-12-08 00:00:00
			code: M327-O
			description: PUBS Notice Requiring Inventors Oath or Declaration
		№ 15
			recordDate: 2014-11-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 16
			recordDate: 2014-11-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 17
			recordDate: 2014-11-06 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 18
			recordDate: 2014-11-03 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 19
			recordDate: 2014-11-01 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 20
			recordDate: 2014-07-31 00:00:00
			code: MEXAC
			description: Mail Interview Summary - Applicant Initiated - Conference
		№ 21
			recordDate: 2014-07-18 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 22
			recordDate: 2014-07-18 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 23
			recordDate: 2014-07-18 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 24
			recordDate: 2014-07-13 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 25
			recordDate: 2014-07-10 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 26
			recordDate: 2014-06-20 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 27
			recordDate: 2014-06-20 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 28
			recordDate: 2014-06-20 00:00:00
			code: EXAC
			description: Interview Summary - Applicant Initiated - Conference
		№ 29
			recordDate: 2014-04-10 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 30
			recordDate: 2014-04-10 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 31
			recordDate: 2014-04-10 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 32
			recordDate: 2014-04-05 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 33
			recordDate: 2014-04-05 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 34
			recordDate: 2013-07-16 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 35
			recordDate: 2013-06-07 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 36
			recordDate: 2013-04-19 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 37
			recordDate: 2013-04-09 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 38
			recordDate: 2013-04-09 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 39
			recordDate: 2013-04-08 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 40
			recordDate: 2013-04-08 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 41
			recordDate: 2013-03-13 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 42
			recordDate: 2013-03-06 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 43
			recordDate: 2013-03-06 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 44
			recordDate: 2013-03-06 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 45
			recordDate: 2013-03-06 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 46
			recordDate: 2013-03-06 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 47
			recordDate: 2013-03-06 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 48
			recordDate: 2013-03-06 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9059872
	applIdStr: 13787442
	appl_id_txt: 13787442
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13787442
	appSubCls: 401000
	appLocation: ELECTRONIC
	appAttrDockNumber: 20346.0068.CNUS00
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 131836
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 6
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|6}
	appStatusDate: 2015-05-27T11:13:04Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-06-16T04:00:00Z
	APP_IND: 1
	appExamName: CATTUNGAL, AJAY P
	appExamNameFacet: CATTUNGAL, AJAY P
	firstInventorFile: No
	appClsSubCls: 370/401000
	appClsSubClsFacet: 370/401000
	applId: 13787442
	patentTitle: EFFICIENT HIGHLY CONNECTED DEPLOYMENT UNITS
	appIntlPubNumber: 
	appConfrNumber: 4869
	appFilingDate: 2013-03-06T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Jagwinder Singh Brar
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Michael David Marr
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Monroe, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Tyson J. Lamoreaux
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Mark N. Kelly
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Dublin, 
			geoCode: 
			country: (IR)
			rankNo: 4
		№ 4
			nameLineOne: Justin O. Pietsch
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bothell, 
			geoCode: WA
			country: (US)
			rankNo: 5
	appCls: 370
	_version_: 1580433221173116931
	lastUpdatedTimestamp: 2017-10-05T15:49:31.809Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 97c7c182-a293-42b3-a857-160cc92d5dd6
	responseHeader:
			status: 0
			QTime: 17
