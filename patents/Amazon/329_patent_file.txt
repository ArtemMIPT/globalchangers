DOCUMENT_BODY:
	appGrpArtNumber: 2171
	appGrpArtNumberFacet: 2171
	transactions:
		№ 0
			recordDate: 2017-10-02 00:00:00
			code: M1551
			description: Payment of Maintenance Fee, 4th Year, Large Entity
		№ 1
			recordDate: 2014-04-01 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2014-04-01 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2014-03-12 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2014-03-05 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2014-03-04 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2014-02-12 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2014-02-12 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 8
			recordDate: 2014-02-12 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 9
			recordDate: 2014-02-12 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2014-01-27 00:00:00
			code: MM327-O
			description: Mail PUBS Notice Requiring Inventors Oath or Declaration
		№ 11
			recordDate: 2014-01-24 00:00:00
			code: M327-O
			description: PUBS Notice Requiring Inventors Oath or Declaration
		№ 12
			recordDate: 2013-11-14 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 13
			recordDate: 2013-09-26 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 14
			recordDate: 2013-09-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 15
			recordDate: 2013-06-13 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 16
			recordDate: 2013-06-05 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 17
			recordDate: 2013-05-21 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 18
			recordDate: 2013-05-21 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 19
			recordDate: 2013-05-21 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 20
			recordDate: 2013-05-20 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 21
			recordDate: 2013-05-20 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 22
			recordDate: 2013-05-09 00:00:00
			code: A.PE
			description: Preliminary Amendment
		№ 23
			recordDate: 2013-05-09 00:00:00
			code: FLFEE
			description: Payment of additional filing fee/Preexam
		№ 24
			recordDate: 2013-04-02 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 25
			recordDate: 2013-04-02 00:00:00
			code: INCD
			description: Notice Mailed--Application Incomplete--Filing Date Assigned 
		№ 26
			recordDate: 2013-02-28 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 27
			recordDate: 2013-02-26 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 28
			recordDate: 2013-02-26 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 29
			recordDate: 2013-02-26 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 30
			recordDate: 2013-02-26 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 31
			recordDate: 2013-02-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 32
			recordDate: 2013-02-26 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 33
			recordDate: 2013-02-26 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8689109
	applIdStr: 13777960
	appl_id_txt: 13777960
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13777960
	appSubCls: 741000
	appLocation: ELECTRONIC
	appAttrDockNumber: 120137.582C1
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 500
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 8
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|8}
	appStatusDate: 2014-03-12T10:02:39Z
	LAST_MOD_TS: 2017-10-02T10:04:58Z
	patentIssueDate: 2014-04-01T04:00:00Z
	APP_IND: 1
	appExamName: LEGGETT, ANDREA C.
	appExamNameFacet: LEGGETT, ANDREA C.
	firstInventorFile: No
	appClsSubCls: 715/741000
	appClsSubClsFacet: 715/741000
	applId: 13777960
	patentTitle: FACILITATING ACCESS TO FUNCTIONALITY VIA DISPLAYED INFORMATION
	appIntlPubNumber: 
	appConfrNumber: 6438
	appFilingDate: 2013-02-26T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Zheyin  Li
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Snoqualmie, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Suresh  Kumar
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Issaquah, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 2
			nameLineOne: Kamil  Jiwa
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 6
		№ 3
			nameLineOne: Patrick G. Franklin
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Ravensdale, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 4
			nameLineOne: James K. Keiger
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 5
			nameLineOne: Ramanathan  Palaniappan
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 5
		№ 6
			nameLineOne: Ares  Sakamoto
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 7
	appCls: 715
	_version_: 1580433219356983301
	lastUpdatedTimestamp: 2017-10-05T15:49:30.081Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 0a00969b-d87b-44b3-8fd5-1d76e5a8df75
	responseHeader:
			status: 0
			QTime: 18
