DOCUMENT_BODY:
	appGrpArtNumber: 2859
	appGrpArtNumberFacet: 2859
	transactions:
		№ 0
			recordDate: 2015-08-25 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-08-25 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-08-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-08-05 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-07-24 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-07-24 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-07-22 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-07-22 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-04-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2015-04-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2015-04-23 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2015-04-20 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2014-12-29 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 13
			recordDate: 2014-01-27 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 14
			recordDate: 2013-05-15 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 15
			recordDate: 2013-04-29 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 16
			recordDate: 2013-04-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 17
			recordDate: 2013-04-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 18
			recordDate: 2013-04-29 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 19
			recordDate: 2013-04-29 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 20
			recordDate: 2013-04-26 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 21
			recordDate: 2013-04-26 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 22
			recordDate: 2013-03-25 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 23
			recordDate: 2013-03-18 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 24
			recordDate: 2013-03-14 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 25
			recordDate: 2013-03-14 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 26
			recordDate: 2013-03-14 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9118187
	applIdStr: 13828740
	appl_id_txt: 13828740
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13828740
	appSubCls: 103000
	appLocation: ELECTRONIC
	appAttrDockNumber: AM2-0939US
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 29150
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2015-08-05T12:55:31Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-08-25T04:00:00Z
	APP_IND: 1
	appExamName: WILLIAMS, ARUN C
	appExamNameFacet: WILLIAMS, ARUN C
	firstInventorFile: No
	appClsSubCls: 320/103000
	appClsSubClsFacet: 320/103000
	applId: 13828740
	patentTitle: VIBRATIONAL ENERGY HARVESTER
	appIntlPubNumber: 
	appConfrNumber: 3804
	appFilingDate: 2013-03-14T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Rashed Adnan Islam
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Jose, 
			geoCode: CA
			country: (US)
			rankNo: 1
	appCls: 320
	_version_: 1580433229510344704
	lastUpdatedTimestamp: 2017-10-05T15:49:39.78Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 15a4ef0e-add3-4358-9cd3-7d1e69d9afa9
	responseHeader:
			status: 0
			QTime: 18
