DOCUMENT_BODY:
	appGrpArtNumber: 2195
	appGrpArtNumberFacet: 2195
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9276987
	applIdStr: 13794220
	appl_id_txt: 13794220
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13794220
	appSubCls: 102000
	appLocation: ELECTRONIC
	appAttrDockNumber: 120137.613C1
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 500
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|3}
	appStatusDate: 2016-02-10T12:26:51Z
	LAST_MOD_TS: 2017-10-22T09:30:15Z
	patentIssueDate: 2016-03-01T05:00:00Z
	APP_IND: 1
	appExamName: CHU JOY, JORGE A
	appExamNameFacet: CHU JOY, JORGE A
	firstInventorFile: No
	appClsSubCls: 718/102000
	appClsSubClsFacet: 718/102000
	applId: 13794220
	patentTitle: IDENTIFYING NODES ALREADY STORING INDICATED INPUT DATA TO PERFORM DISTRIBUTED EXECUTION OF AN INDICATED PROGRAM IN A NODE CLUSTER
	appIntlPubNumber: 
	transactions:
		№ 0
			recordDate: 2017-04-04 00:00:00
			code: N423
			description: Post Issue Communication - Certificate of Correction
		№ 1
			recordDate: 2016-03-01 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2016-03-01 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2016-02-10 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-02-02 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-02-01 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-01-22 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2016-01-22 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-11-09 00:00:00
			code: FLRCPT.C
			description: Filing Receipt - Corrected
		№ 9
			recordDate: 2015-11-06 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 10
			recordDate: 2015-11-02 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 11
			recordDate: 2015-11-02 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 12
			recordDate: 2015-10-17 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 13
			recordDate: 2015-10-16 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 14
			recordDate: 2015-09-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 15
			recordDate: 2015-09-28 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 16
			recordDate: 2015-09-28 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 17
			recordDate: 2015-07-02 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 18
			recordDate: 2015-06-28 00:00:00
			code: CTFR
			description: Final Rejection
		№ 19
			recordDate: 2015-03-25 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 20
			recordDate: 2015-03-23 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 21
			recordDate: 2015-03-11 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 22
			recordDate: 2015-03-04 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 23
			recordDate: 2015-03-04 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 24
			recordDate: 2014-12-23 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 25
			recordDate: 2014-12-20 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 26
			recordDate: 2014-11-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 27
			recordDate: 2014-10-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 28
			recordDate: 2013-11-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 29
			recordDate: 2013-11-02 00:00:00
			code: TI1050
			description: Transfer Inquiry to GAU
		№ 30
			recordDate: 2013-07-19 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 31
			recordDate: 2013-07-11 00:00:00
			code: A.PE
			description: Preliminary Amendment
		№ 32
			recordDate: 2013-04-22 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 33
			recordDate: 2013-04-22 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 34
			recordDate: 2013-04-22 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 35
			recordDate: 2013-04-22 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 36
			recordDate: 2013-03-16 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 37
			recordDate: 2013-03-12 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 38
			recordDate: 2013-03-11 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 39
			recordDate: 2013-03-11 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 40
			recordDate: 2013-03-11 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 41
			recordDate: 2013-03-11 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 42
			recordDate: 2013-03-11 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appConfrNumber: 6836
	appFilingDate: 2013-03-11T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Peter  Sirota
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Richendra  Khanna
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 718
	_version_: 1581964939158880257
	lastUpdatedTimestamp: 2017-10-22T13:35:31.927Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 6073073c-cbc6-49b0-b9b5-714ce578e5c8
	responseHeader:
			status: 0
			QTime: 17
