DOCUMENT_BODY:
	appGrpArtNumber: 2486
	appGrpArtNumberFacet: 2486
	transactions:
		№ 0
			recordDate: 2015-12-01 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-12-01 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-11-12 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-11-11 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-10-28 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2015-10-28 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 6
			recordDate: 2015-10-28 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 7
			recordDate: 2015-10-27 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 8
			recordDate: 2015-10-20 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 9
			recordDate: 2015-10-20 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 10
			recordDate: 2015-10-13 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 11
			recordDate: 2015-10-12 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 12
			recordDate: 2015-10-12 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 13
			recordDate: 2015-10-12 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 14
			recordDate: 2015-10-12 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 15
			recordDate: 2015-07-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 16
			recordDate: 2015-07-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 17
			recordDate: 2015-07-23 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 18
			recordDate: 2015-07-21 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 19
			recordDate: 2015-07-10 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 20
			recordDate: 2015-04-02 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 21
			recordDate: 2015-03-31 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 22
			recordDate: 2015-02-26 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 23
			recordDate: 2015-02-18 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 24
			recordDate: 2015-02-18 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 25
			recordDate: 2015-01-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 26
			recordDate: 2015-01-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 27
			recordDate: 2015-01-02 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 28
			recordDate: 2014-12-29 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 29
			recordDate: 2014-09-30 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 30
			recordDate: 2014-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 31
			recordDate: 2014-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 32
			recordDate: 2013-05-27 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 33
			recordDate: 2012-12-14 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 34
			recordDate: 2012-12-10 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 35
			recordDate: 2012-11-16 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 36
			recordDate: 2012-11-16 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 37
			recordDate: 2012-11-16 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 38
			recordDate: 2012-10-21 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 39
			recordDate: 2012-10-17 00:00:00
			code: A.PE
			description: Preliminary Amendment
		№ 40
			recordDate: 2012-10-17 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 41
			recordDate: 2012-10-17 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 42
			recordDate: 2012-10-17 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9202520
	applIdStr: 13654066
	appl_id_txt: 13654066
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13654066
	appSubCls: 077000
	appLocation: ELECTRONIC
	appAttrDockNumber: 25396-0224
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 29052
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|2}
	appStatusDate: 2015-11-11T10:36:10Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-12-01T05:00:00Z
	APP_IND: 1
	appExamName: BILLAH, MASUM
	appExamNameFacet: BILLAH, MASUM
	firstInventorFile: No
	appClsSubCls: 348/077000
	appClsSubClsFacet: 348/077000
	applId: 13654066
	patentTitle: SYSTEMS AND METHODS FOR DETERMINING CONTENT PREFERENCES BASED ON VOCAL UTTERANCES AND/OR MOVEMENT BY A USER
	appIntlPubNumber: 
	appConfrNumber: 3090
	appFilingDate: 2012-10-17T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Joshua K. Tang
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Luis Obispo, 
			geoCode: CA
			country: (US)
			rankNo: 1
	appCls: 348
	_version_: 1580433193746563072
	lastUpdatedTimestamp: 2017-10-05T15:49:05.657Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: cb75d65e-1d98-4317-bbe5-4e27ee422ded
	responseHeader:
			status: 0
			QTime: 16
