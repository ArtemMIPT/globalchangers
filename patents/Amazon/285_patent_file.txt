DOCUMENT_BODY:
	appGrpArtNumber: 2144
	appGrpArtNumberFacet: 2144
	transactions:
		№ 0
			recordDate: 2016-02-09 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-02-09 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-01-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-01-20 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-01-11 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-01-09 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-01-04 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2016-01-04 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-10-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 9
			recordDate: 2015-10-09 00:00:00
			code: FLRCPT.C
			description: Filing Receipt - Corrected
		№ 10
			recordDate: 2015-10-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 11
			recordDate: 2015-10-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 12
			recordDate: 2015-10-05 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 13
			recordDate: 2015-09-30 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 14
			recordDate: 2015-09-29 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 15
			recordDate: 2015-09-25 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 16
			recordDate: 2015-09-18 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 17
			recordDate: 2015-09-10 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 18
			recordDate: 2015-09-02 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 19
			recordDate: 2015-09-02 00:00:00
			code: MM327
			description: Mail Miscellaneous Communication to Applicant
		№ 20
			recordDate: 2015-08-17 00:00:00
			code: M327
			description: Miscellaneous Communication to Applicant - No Action Count
		№ 21
			recordDate: 2015-06-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 22
			recordDate: 2015-06-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 23
			recordDate: 2015-06-12 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 24
			recordDate: 2015-06-09 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 25
			recordDate: 2015-05-31 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 26
			recordDate: 2015-02-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 27
			recordDate: 2015-01-04 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 28
			recordDate: 2013-06-06 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 29
			recordDate: 2013-06-06 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 30
			recordDate: 2013-06-06 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 31
			recordDate: 2013-06-06 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 32
			recordDate: 2013-05-13 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 33
			recordDate: 2013-03-08 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 34
			recordDate: 2013-03-08 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 35
			recordDate: 2013-03-08 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 36
			recordDate: 2013-03-08 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 37
			recordDate: 2013-03-08 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 38
			recordDate: 2013-03-07 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 39
			recordDate: 2013-02-12 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 40
			recordDate: 2013-02-11 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 41
			recordDate: 2013-02-11 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 42
			recordDate: 2013-02-11 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9256340
	applIdStr: 13764494
	appl_id_txt: 13764494
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13764494
	appSubCls: 234000
	appLocation: ELECTRONIC
	appAttrDockNumber: 170103-1721
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|2}
	appStatusDate: 2016-01-20T12:32:16Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-02-09T05:00:00Z
	APP_IND: 1
	appExamName: KELLS, ASHER
	appExamNameFacet: KELLS, ASHER
	firstInventorFile: No
	appClsSubCls: 715/234000
	appClsSubClsFacet: 715/234000
	applId: 13764494
	patentTitle: PLACEMENT OF USER INTERFACE ELEMENTS BASED ON USER INPUT INDICATING A HIGH VALUE AREA
	appIntlPubNumber: 
	appConfrNumber: 4626
	appFilingDate: 2013-02-11T05:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: William Alexander Strand
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Issaquah, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 715
	_version_: 1580433216444039168
	lastUpdatedTimestamp: 2017-10-05T15:49:27.281Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 4619a0f3-746d-490b-bd96-48bc8c9d71bb
	responseHeader:
			status: 0
			QTime: 16
