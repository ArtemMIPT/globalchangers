DOCUMENT_BODY:
	appGrpArtNumber: 3684
	appGrpArtNumberFacet: 3684
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8706566
	applIdStr: 13720816
	appl_id_txt: 13720816
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13720816
	appSubCls: 026100
	appLocation: ELECTRONIC
	appAttrDockNumber: MIPS.393C1
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 20995
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2014-04-02T09:44:15Z
	LAST_MOD_TS: 2017-10-23T10:33:50Z
	patentIssueDate: 2014-04-22T04:00:00Z
	APP_IND: 1
	appExamName: ZIMMERMAN, MATTHEW E
	appExamNameFacet: ZIMMERMAN, MATTHEW E
	firstInventorFile: No
	appClsSubCls: 705/026100
	appClsSubClsFacet: 705/026100
	applId: 13720816
	patentTitle: METHOD, MEDIUM, AND SYSTEM FOR ADJUSTING A SELECTABLE ELEMENT BASED ON SOCIAL NETWORKING USAGE
	appIntlPubNumber: 
	transactions:
		№ 0
			recordDate: 2014-04-22 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2014-04-22 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2014-04-03 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2014-04-02 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2014-03-20 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2014-03-19 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2014-02-25 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 7
			recordDate: 2014-02-25 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 8
			recordDate: 2014-02-25 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2014-02-25 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2013-12-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 11
			recordDate: 2013-12-18 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 12
			recordDate: 2013-12-18 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 13
			recordDate: 2013-12-14 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 14
			recordDate: 2013-12-10 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 15
			recordDate: 2013-12-10 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 16
			recordDate: 2013-12-10 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 17
			recordDate: 2013-12-10 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 18
			recordDate: 2013-12-06 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 19
			recordDate: 2013-11-22 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 20
			recordDate: 2013-11-22 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 21
			recordDate: 2013-11-22 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 22
			recordDate: 2013-02-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 23
			recordDate: 2013-02-14 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 24
			recordDate: 2013-01-23 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 25
			recordDate: 2013-01-23 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 26
			recordDate: 2013-01-23 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 27
			recordDate: 2013-01-23 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 28
			recordDate: 2013-01-23 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 29
			recordDate: 2013-01-22 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 30
			recordDate: 2012-12-26 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 31
			recordDate: 2012-12-21 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 32
			recordDate: 2012-12-21 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 33
			recordDate: 2012-12-19 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 34
			recordDate: 2012-12-19 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 35
			recordDate: 2012-12-19 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appConfrNumber: 1260
	appFilingDate: 2012-12-19T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Jeetendra  Mirchandani
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Sumit Kumar Sultania
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Vinayak R. Hegde
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 3
	appCls: 705
	_version_: 1582059455598559234
	lastUpdatedTimestamp: 2017-10-23T14:37:49.825Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 6127cd1c-1e1f-490f-8308-652c641ee1b3
	responseHeader:
			status: 0
			QTime: 15
