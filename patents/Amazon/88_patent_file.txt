DOCUMENT_BODY:
	appGrpArtNumber: 3682
	appGrpArtNumberFacet: 3682
	transactions:
		№ 0
			recordDate: 2017-05-09 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9648056
	applIdStr: 13676753
	appl_id_txt: 13676753
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13676753
	appSubCls: 014350
	appLocation: ELECTRONIC
	appAttrDockNumber: 170114-1050
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|4}
	appStatusDate: 2017-04-19T10:28:59Z
	LAST_MOD_TS: 2017-05-09T14:46:46Z
	patentIssueDate: 2017-05-09T04:00:00Z
	APP_IND: 1
	appExamName: HAMILTON, MATTHEW L
	appExamNameFacet: HAMILTON, MATTHEW L
	firstInventorFile: No
	appClsSubCls: 705/014350
	appClsSubClsFacet: 705/014350
	applId: 13676753
	patentTitle: GEOGRAPHIC CONTENT DISCOVERY
	appIntlPubNumber: 
	appConfrNumber: 1364
	appFilingDate: 2012-11-14T05:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Mitchell  Kim
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Irvine, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Dave  Mohla
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Irvine, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Devesh  Khare
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Tustin, 
			geoCode: CA
			country: (US)
			rankNo: 3
	appCls: 705
	_version_: 1580433198368686083
	lastUpdatedTimestamp: 2017-10-05T15:49:10.049Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 683ba366-d23a-40da-948e-a872a584e98a
	responseHeader:
			status: 0
			QTime: 15
