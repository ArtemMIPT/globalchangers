DOCUMENT_BODY:
	appGrpArtNumber: 2661
	appGrpArtNumberFacet: 2661
	transactions:
		№ 0
			recordDate: 2015-10-13 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-10-13 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-09-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-09-23 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-09-15 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-09-15 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-09-11 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-09-11 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-07-22 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 9
			recordDate: 2015-07-22 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 10
			recordDate: 2015-07-20 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 11
			recordDate: 2015-06-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 12
			recordDate: 2015-06-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 13
			recordDate: 2015-06-24 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 14
			recordDate: 2015-06-15 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 15
			recordDate: 2015-06-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 16
			recordDate: 2015-06-15 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 17
			recordDate: 2015-06-15 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 18
			recordDate: 2015-06-02 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 19
			recordDate: 2015-06-01 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 20
			recordDate: 2015-06-01 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 21
			recordDate: 2015-03-25 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 22
			recordDate: 2015-03-25 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 23
			recordDate: 2015-03-25 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 24
			recordDate: 2015-03-20 00:00:00
			code: CTFR
			description: Final Rejection
		№ 25
			recordDate: 2014-12-08 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 26
			recordDate: 2014-12-05 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 27
			recordDate: 2014-09-11 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 28
			recordDate: 2014-09-11 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 29
			recordDate: 2014-09-11 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 30
			recordDate: 2014-09-03 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 31
			recordDate: 2013-12-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 32
			recordDate: 2013-01-22 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 33
			recordDate: 2012-12-17 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 34
			recordDate: 2012-11-29 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 35
			recordDate: 2012-11-29 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 36
			recordDate: 2012-11-29 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 37
			recordDate: 2012-11-29 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 38
			recordDate: 2012-11-06 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 39
			recordDate: 2012-11-02 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 40
			recordDate: 2012-11-02 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 41
			recordDate: 2012-11-02 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9160835
	applIdStr: 13667785
	appl_id_txt: 13667785
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13667785
	appSubCls: 211120
	appLocation: ELECTRONIC
	appAttrDockNumber: 25396-0217
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 29052
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|2}
	appStatusDate: 2015-09-23T12:12:10Z
	LAST_MOD_TS: 2017-04-26T16:31:16Z
	patentIssueDate: 2015-10-13T04:00:00Z
	APP_IND: 1
	appExamName: GEBRIEL, SELAM T
	appExamNameFacet: GEBRIEL, SELAM T
	firstInventorFile: No
	appClsSubCls: 348/211120
	appClsSubClsFacet: 348/211120
	applId: 13667785
	patentTitle: Systems and Methods for Communicating Between Devices Using Quick Response Images
	appIntlPubNumber: 
	appConfrNumber: 2849
	appFilingDate: 2012-11-02T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Peter C. Beckman
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Falls Church, 
			geoCode: VA
			country: (US)
			rankNo: 1
	appCls: 348
	_version_: 1580433196435111936
	lastUpdatedTimestamp: 2017-10-05T15:49:08.212Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: a90b2916-4a41-40a9-9c00-a319afeb849f
	responseHeader:
			status: 0
			QTime: 15
