DOCUMENT_BODY:
	appGrpArtNumber: 2194
	appGrpArtNumberFacet: 2194
	transactions:
		№ 0
			recordDate: 2014-11-11 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2014-11-11 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2014-10-22 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 3
			recordDate: 2014-09-29 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 4
			recordDate: 2014-09-26 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 5
			recordDate: 2014-09-26 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2014-09-26 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 7
			recordDate: 2014-09-02 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 8
			recordDate: 2014-09-02 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 9
			recordDate: 2014-08-27 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 10
			recordDate: 2014-08-26 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 11
			recordDate: 2014-08-05 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 12
			recordDate: 2014-07-17 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 13
			recordDate: 2014-07-15 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 14
			recordDate: 2014-06-30 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 15
			recordDate: 2014-06-27 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 16
			recordDate: 2014-05-29 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 17
			recordDate: 2014-05-27 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 18
			recordDate: 2014-05-27 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 19
			recordDate: 2014-03-27 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 20
			recordDate: 2014-03-24 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 21
			recordDate: 2013-05-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 22
			recordDate: 2013-01-22 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 23
			recordDate: 2013-01-22 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 24
			recordDate: 2012-12-10 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 25
			recordDate: 2012-11-13 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 26
			recordDate: 2012-10-23 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 27
			recordDate: 2012-10-23 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 28
			recordDate: 2012-10-23 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 29
			recordDate: 2012-09-26 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 30
			recordDate: 2012-09-25 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 31
			recordDate: 2012-09-25 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 32
			recordDate: 2012-09-25 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8887181
	applIdStr: 13626712
	appl_id_txt: 13626712
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13626712
	appSubCls: 328000
	appLocation: ELECTRONIC
	appAttrDockNumber: 08862.150 (L0150)
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 14432
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2014-10-22T08:51:47Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2014-11-11T05:00:00Z
	APP_IND: 1
	appExamName: HO, ANDY
	appExamNameFacet: HO, ANDY
	firstInventorFile: No
	appClsSubCls: 719/328000
	appClsSubClsFacet: 719/328000
	applId: 13626712
	patentTitle: APPLICATION ADD-ON PLATFORM
	appIntlPubNumber: 
	appConfrNumber: 1732
	appFilingDate: 2012-09-25T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Luhui  Hu
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bothell, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Samuel S. Gigliotti
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 719
	_version_: 1580433187987783683
	lastUpdatedTimestamp: 2017-10-05T15:49:00.142Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 27a4098c-12d4-49df-b6f6-358e3142263c
	responseHeader:
			status: 0
			QTime: 15
