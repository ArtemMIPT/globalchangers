DOCUMENT_BODY:
	appGrpArtNumber: 3696
	appGrpArtNumberFacet: 3696
	transactions:
		№ 0
			recordDate: 2015-03-10 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-03-10 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-02-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-02-18 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-02-06 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-01-30 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-01-07 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-01-07 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 8
			recordDate: 2015-01-07 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 9
			recordDate: 2015-01-07 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2014-11-21 00:00:00
			code: MM327-O
			description: Mail PUBS Notice Requiring Inventors Oath or Declaration
		№ 11
			recordDate: 2014-11-19 00:00:00
			code: M327-O
			description: PUBS Notice Requiring Inventors Oath or Declaration
		№ 12
			recordDate: 2014-10-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 13
			recordDate: 2014-10-08 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 14
			recordDate: 2014-10-08 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 15
			recordDate: 2014-10-04 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 16
			recordDate: 2014-10-03 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 17
			recordDate: 2014-09-16 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 18
			recordDate: 2014-09-11 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 19
			recordDate: 2014-08-04 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 20
			recordDate: 2014-07-28 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 21
			recordDate: 2014-07-28 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 22
			recordDate: 2014-06-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 23
			recordDate: 2014-06-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 24
			recordDate: 2014-06-12 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 25
			recordDate: 2014-06-03 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 26
			recordDate: 2014-06-02 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 27
			recordDate: 2013-04-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 28
			recordDate: 2013-04-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 29
			recordDate: 2013-04-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 30
			recordDate: 2013-04-22 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 31
			recordDate: 2013-04-22 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 32
			recordDate: 2013-04-22 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 33
			recordDate: 2013-04-19 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 34
			recordDate: 2013-04-19 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 35
			recordDate: 2013-03-19 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 36
			recordDate: 2013-03-15 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 37
			recordDate: 2013-03-13 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 38
			recordDate: 2013-03-13 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 39
			recordDate: 2013-03-13 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 40
			recordDate: 2013-03-13 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 41
			recordDate: 2013-03-13 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8977568
	applIdStr: 13801706
	appl_id_txt: 13801706
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13801706
	appSubCls: 039000
	appLocation: ELECTRONIC
	appAttrDockNumber: AM2-0200USC1
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 29150
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 6
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|6}
	appStatusDate: 2015-02-18T12:39:11Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-03-10T04:00:00Z
	APP_IND: 1
	appExamName: NIQUETTE, ROBERT R
	appExamNameFacet: NIQUETTE, ROBERT R
	firstInventorFile: No
	appClsSubCls: 705/039000
	appClsSubClsFacet: 705/039000
	applId: 13801706
	patentTitle: ANONYMOUS MOBILE PAYMENTS
	appIntlPubNumber: 
	appConfrNumber: 1390
	appFilingDate: 2013-03-13T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Derek Edward Wegner
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Vashon Island, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Chih-Jen  Huang
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Issaquah, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 2
			nameLineOne: Paul C. Schattauer
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 3
			nameLineOne: Diwakar  Gupta
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 4
			nameLineOne: Philip  Yuen
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 5
	appCls: 705
	_version_: 1580433223978057728
	lastUpdatedTimestamp: 2017-10-05T15:49:34.502Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: cc915edb-47a7-45a5-937d-e5065d213ba1
	responseHeader:
			status: 0
			QTime: 16
