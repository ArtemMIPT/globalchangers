DOCUMENT_BODY:
	appGrpArtNumber: 2158
	appGrpArtNumberFacet: 2158
	transactions:
		№ 0
			recordDate: 2016-05-03 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-05-03 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-04-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-04-13 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-04-04 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-04-04 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-04-01 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2016-04-01 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2016-01-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2016-01-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2016-01-06 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2015-12-27 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2015-12-17 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 13
			recordDate: 2015-12-17 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 14
			recordDate: 2015-12-17 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 15
			recordDate: 2015-12-16 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 16
			recordDate: 2015-11-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 17
			recordDate: 2015-11-16 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 18
			recordDate: 2015-11-16 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 19
			recordDate: 2015-11-16 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 20
			recordDate: 2015-10-01 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 21
			recordDate: 2015-09-24 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 22
			recordDate: 2015-07-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 23
			recordDate: 2015-07-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 24
			recordDate: 2015-07-17 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 25
			recordDate: 2015-07-13 00:00:00
			code: CTFR
			description: Final Rejection
		№ 26
			recordDate: 2015-03-25 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 27
			recordDate: 2015-03-20 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 28
			recordDate: 2015-03-11 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 29
			recordDate: 2015-03-03 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 30
			recordDate: 2015-03-03 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 31
			recordDate: 2014-12-22 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 32
			recordDate: 2014-12-22 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 33
			recordDate: 2014-12-22 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 34
			recordDate: 2014-12-15 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 35
			recordDate: 2014-09-02 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 36
			recordDate: 2014-08-21 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 37
			recordDate: 2014-07-08 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 38
			recordDate: 2014-07-01 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 39
			recordDate: 2014-07-01 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 40
			recordDate: 2014-05-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 41
			recordDate: 2014-05-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 42
			recordDate: 2014-05-23 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 43
			recordDate: 2014-05-19 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 44
			recordDate: 2014-05-19 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 45
			recordDate: 2014-04-03 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 46
			recordDate: 2014-04-03 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 47
			recordDate: 2013-12-31 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 48
			recordDate: 2013-01-07 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 49
			recordDate: 2013-01-06 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 50
			recordDate: 2012-12-17 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 51
			recordDate: 2012-12-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 52
			recordDate: 2012-12-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 53
			recordDate: 2012-12-17 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 54
			recordDate: 2012-12-17 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 55
			recordDate: 2012-12-14 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 56
			recordDate: 2012-11-20 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 57
			recordDate: 2012-11-16 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 58
			recordDate: 2012-11-16 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 59
			recordDate: 2012-11-16 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9330198
	applIdStr: 13679254
	appl_id_txt: 13679254
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13679254
	appSubCls: 756000
	appLocation: ELECTRONIC
	appAttrDockNumber: 170108-1990
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2016-04-13T10:30:48Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-05-03T04:00:00Z
	APP_IND: 1
	appExamName: RUIZ, ANGELICA
	appExamNameFacet: RUIZ, ANGELICA
	firstInventorFile: No
	appClsSubCls: 707/756000
	appClsSubClsFacet: 707/756000
	applId: 13679254
	patentTitle: MAPPING STORED CLIENT DATA TO REQUESTED DATA USING METADATA
	appIntlPubNumber: 
	appConfrNumber: 9888
	appFilingDate: 2012-11-16T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Daniel W. Hitchcock
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Brad Lee Campbell
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 2
			nameLineOne: Owen G. Griffiths
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
	appCls: 707
	_version_: 1580433198851031041
	lastUpdatedTimestamp: 2017-10-05T15:49:10.539Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 683ba366-d23a-40da-948e-a872a584e98a
	responseHeader:
			status: 0
			QTime: 15
