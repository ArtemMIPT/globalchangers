DOCUMENT_BODY:
	appGrpArtNumber: 2168
	appGrpArtNumberFacet: 2168
	transactions:
		№ 0
			recordDate: 2016-09-20 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-09-20 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-09-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-08-31 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-08-08 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-08-08 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-08-05 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2016-08-05 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2016-06-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 9
			recordDate: 2016-06-10 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 10
			recordDate: 2016-06-10 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 11
			recordDate: 2016-06-07 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 12
			recordDate: 2016-06-07 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 13
			recordDate: 2016-06-06 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 14
			recordDate: 2016-06-01 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 15
			recordDate: 2016-06-01 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 16
			recordDate: 2016-05-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 17
			recordDate: 2016-05-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 18
			recordDate: 2016-05-05 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 19
			recordDate: 2016-05-02 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 20
			recordDate: 2016-04-29 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 21
			recordDate: 2016-04-27 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 22
			recordDate: 2016-04-27 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 23
			recordDate: 2016-04-27 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 24
			recordDate: 2016-04-14 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 25
			recordDate: 2016-04-14 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 26
			recordDate: 2016-01-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 27
			recordDate: 2016-01-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 28
			recordDate: 2016-01-14 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 29
			recordDate: 2016-01-08 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 30
			recordDate: 2016-01-04 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 31
			recordDate: 2016-01-04 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 32
			recordDate: 2015-12-21 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 33
			recordDate: 2015-12-21 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 34
			recordDate: 2015-12-21 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 35
			recordDate: 2015-12-03 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 36
			recordDate: 2015-12-03 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 37
			recordDate: 2015-11-30 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 38
			recordDate: 2015-11-30 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 39
			recordDate: 2015-11-25 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 40
			recordDate: 2015-11-23 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 41
			recordDate: 2015-11-23 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 42
			recordDate: 2015-08-21 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 43
			recordDate: 2015-08-21 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 44
			recordDate: 2015-08-21 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 45
			recordDate: 2015-08-18 00:00:00
			code: CTFR
			description: Final Rejection
		№ 46
			recordDate: 2015-08-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 47
			recordDate: 2015-07-02 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 48
			recordDate: 2015-06-12 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 49
			recordDate: 2015-06-12 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 50
			recordDate: 2015-04-14 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 51
			recordDate: 2015-04-14 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 52
			recordDate: 2015-02-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 53
			recordDate: 2015-02-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 54
			recordDate: 2015-02-12 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 55
			recordDate: 2015-02-05 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 56
			recordDate: 2015-02-04 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 57
			recordDate: 2015-02-04 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 58
			recordDate: 2014-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 59
			recordDate: 2014-06-25 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 60
			recordDate: 2014-06-25 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 61
			recordDate: 2014-05-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 62
			recordDate: 2014-05-29 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 63
			recordDate: 2013-12-03 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 64
			recordDate: 2013-12-03 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 65
			recordDate: 2013-12-03 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 66
			recordDate: 2013-11-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 67
			recordDate: 2013-11-29 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 68
			recordDate: 2013-11-29 00:00:00
			code: PG-PB-DT
			description: PG-Pub Notice of new or Revised projected publication date
		№ 69
			recordDate: 2013-11-25 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 70
			recordDate: 2013-05-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 71
			recordDate: 2013-05-03 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 72
			recordDate: 2013-04-23 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 73
			recordDate: 2013-04-23 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 74
			recordDate: 2013-04-23 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 75
			recordDate: 2013-04-22 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 76
			recordDate: 2013-04-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 77
			recordDate: 2013-04-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 78
			recordDate: 2013-04-17 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 79
			recordDate: 2013-04-17 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 80
			recordDate: 2013-04-16 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 81
			recordDate: 2013-03-22 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 82
			recordDate: 2013-03-22 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 83
			recordDate: 2013-03-15 00:00:00
			code: A.PE
			description: Preliminary Amendment
		№ 84
			recordDate: 2013-03-14 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 85
			recordDate: 2013-03-11 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 86
			recordDate: 2013-03-11 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 87
			recordDate: 2013-03-11 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 88
			recordDate: 2013-03-11 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9449038
	applIdStr: 13792643
	appl_id_txt: 13792643
	appEarlyPubNumber: US20140149355A1
	appEntityStatus: UNDISCOUNTED
	id: 13792643
	appSubCls: 652000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-46901
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 8
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|8}
	appStatusDate: 2016-08-31T11:11:45Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-05-29T04:00:00Z
	patentIssueDate: 2016-09-20T04:00:00Z
	APP_IND: 1
	appExamName: TRAN, ANHTAI V
	appExamNameFacet: TRAN, ANHTAI V
	firstInventorFile: No
	appClsSubCls: 707/652000
	appClsSubClsFacet: 707/652000
	applId: 13792643
	patentTitle: STREAMING RESTORE OF A DATABASE FROM A BACKUP SYSTEM
	appIntlPubNumber: 
	appConfrNumber: 2744
	appFilingDate: 2013-03-11T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: ANURAG WINDLASS GUPTA
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: ATHERTON, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: JAKUB  KULESZA
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: BELLEVUE, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: DEEPAK  AGARWAL
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: REDMOND, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: ALEKSANDRAS  SURNA
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: REDMOND, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: TUSHAR  JAIN
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SAN FRANCISCO, 
			geoCode: CA
			country: (US)
			rankNo: 5
		№ 5
			nameLineOne: ZELAINE  FONG
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SAN CARLOS, 
			geoCode: CA
			country: (US)
			rankNo: 6
		№ 6
			nameLineOne: STEFANO  STEFANI
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: ISSAQUAH, 
			geoCode: WA
			country: (US)
			rankNo: 7
	appCls: 707
	_version_: 1580433222068600832
	lastUpdatedTimestamp: 2017-10-05T15:49:32.656Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: d2fc8545-85f2-44cb-95ac-21a830edee81
	responseHeader:
			status: 0
			QTime: 17
