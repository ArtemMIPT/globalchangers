DOCUMENT_BODY:
	appGrpArtNumber: 2155
	appGrpArtNumberFacet: 2155
	transactions:
		№ 0
			recordDate: 2015-12-15 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-12-15 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-11-25 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-11-24 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-11-11 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-11-11 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-11-09 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-11-09 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 9
			recordDate: 2015-08-21 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2015-08-21 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2015-08-21 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 12
			recordDate: 2015-08-18 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 13
			recordDate: 2015-08-13 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 14
			recordDate: 2015-08-05 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 15
			recordDate: 2015-08-05 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 16
			recordDate: 2015-03-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 17
			recordDate: 2015-03-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 18
			recordDate: 2015-03-05 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 19
			recordDate: 2015-03-02 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 20
			recordDate: 2015-02-25 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 21
			recordDate: 2015-02-25 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 22
			recordDate: 2015-02-23 00:00:00
			code: ELC.
			description: Response to Election / Restriction Filed
		№ 23
			recordDate: 2014-12-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 24
			recordDate: 2014-12-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 25
			recordDate: 2014-12-23 00:00:00
			code: MCTRS
			description: Mail Restriction Requirement
		№ 26
			recordDate: 2014-12-17 00:00:00
			code: CTRS
			description: Restriction/Election Requirement
		№ 27
			recordDate: 2014-11-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 28
			recordDate: 2014-10-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 29
			recordDate: 2014-10-16 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 30
			recordDate: 2014-10-01 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 31
			recordDate: 2014-10-01 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 32
			recordDate: 2014-10-01 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 33
			recordDate: 2014-04-11 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 34
			recordDate: 2014-04-11 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 35
			recordDate: 2014-04-11 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 36
			recordDate: 2014-04-11 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 37
			recordDate: 2014-04-10 00:00:00
			code: PG-PB-DT
			description: PG-Pub Notice of new or Revised projected publication date
		№ 38
			recordDate: 2014-04-08 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 39
			recordDate: 2013-06-14 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 40
			recordDate: 2013-06-13 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 41
			recordDate: 2013-05-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 42
			recordDate: 2013-05-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 43
			recordDate: 2013-05-22 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 44
			recordDate: 2013-05-22 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 45
			recordDate: 2013-05-22 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 46
			recordDate: 2013-05-21 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 47
			recordDate: 2013-05-21 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 48
			recordDate: 2013-04-16 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 49
			recordDate: 2013-04-15 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 50
			recordDate: 2013-04-15 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 51
			recordDate: 2013-04-15 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 52
			recordDate: 2013-04-15 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9213726
	applIdStr: 13862934
	appl_id_txt: 13862934
	appEarlyPubNumber: US20140310249A1
	appEntityStatus: UNDISCOUNTED
	id: 13862934
	appSubCls: 688000
	appLocation: ELECTRONIC
	appAttrDockNumber: 101058.000054
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 23377
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2015-11-24T10:22:57Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-10-16T04:00:00Z
	patentIssueDate: 2015-12-15T05:00:00Z
	APP_IND: 1
	appExamName: WOO, ISAAC M
	appExamNameFacet: WOO, ISAAC M
	firstInventorFile: Yes
	appClsSubCls: 707/688000
	appClsSubClsFacet: 707/688000
	applId: 13862934
	patentTitle: DATABASE COST TRACING AND ANALYSIS
	appIntlPubNumber: 
	appConfrNumber: 1499
	appFilingDate: 2013-04-15T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Marcin Piotr Kowalski
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Capetown, 
			geoCode: 
			country: (ZA)
			rankNo: 1
	appCls: 707
	_version_: 1580433236809482241
	lastUpdatedTimestamp: 2017-10-05T15:49:46.738Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: d87e7f50-ef6f-4911-a2f1-f087fb34a43a
	responseHeader:
			status: 0
			QTime: 17
