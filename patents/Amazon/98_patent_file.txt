DOCUMENT_BODY:
	appGrpArtNumber: 2195
	appGrpArtNumberFacet: 2195
	transactions:
		№ 0
			recordDate: 2016-09-20 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-09-20 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-09-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-08-31 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-08-19 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-08-19 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-08-18 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 7
			recordDate: 2016-08-18 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2016-08-18 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2016-06-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2016-06-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2016-06-01 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 12
			recordDate: 2016-05-27 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 13
			recordDate: 2016-05-23 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 14
			recordDate: 2016-05-18 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 15
			recordDate: 2016-05-12 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 16
			recordDate: 2016-04-25 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 17
			recordDate: 2016-04-25 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 18
			recordDate: 2015-11-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 19
			recordDate: 2015-11-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 20
			recordDate: 2015-11-23 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 21
			recordDate: 2015-11-16 00:00:00
			code: CTFR
			description: Final Rejection
		№ 22
			recordDate: 2015-09-14 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 23
			recordDate: 2015-09-08 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 24
			recordDate: 2015-09-08 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 25
			recordDate: 2015-04-08 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 26
			recordDate: 2015-04-08 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 27
			recordDate: 2015-04-08 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 28
			recordDate: 2015-04-05 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 29
			recordDate: 2015-03-11 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 30
			recordDate: 2015-02-13 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 31
			recordDate: 2013-02-04 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 32
			recordDate: 2013-01-09 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 33
			recordDate: 2012-12-18 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 34
			recordDate: 2012-12-18 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 35
			recordDate: 2012-12-18 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 36
			recordDate: 2012-12-18 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 37
			recordDate: 2012-12-18 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 38
			recordDate: 2012-12-17 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 39
			recordDate: 2012-11-20 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 40
			recordDate: 2012-11-16 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 41
			recordDate: 2012-11-16 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 42
			recordDate: 2012-11-16 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 43
			recordDate: 2012-11-16 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9448819
	applIdStr: 13679451
	appl_id_txt: 13679451
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13679451
	appSubCls: 001000
	appLocation: ELECTRONIC
	appAttrDockNumber: 8904-89691-01
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 24197
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2016-08-31T10:10:17Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-09-20T04:00:00Z
	APP_IND: 1
	appExamName: WAI, ERIC CHARLES
	appExamNameFacet: WAI, ERIC CHARLES
	firstInventorFile: No
	appClsSubCls: 718/001000
	appClsSubClsFacet: 718/001000
	applId: 13679451
	patentTitle: USER-INFLUENCED PLACEMENT OF VIRTUAL MACHINES
	appIntlPubNumber: 
	appConfrNumber: 8459
	appFilingDate: 2012-11-16T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Eden Grail Adogla
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 718
	_version_: 1580433198866759680
	lastUpdatedTimestamp: 2017-10-05T15:49:10.541Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 683ba366-d23a-40da-948e-a872a584e98a
	responseHeader:
			status: 0
			QTime: 15
