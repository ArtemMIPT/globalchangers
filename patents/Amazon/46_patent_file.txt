DOCUMENT_BODY:
	appGrpArtNumber: 2486
	appGrpArtNumberFacet: 2486
	transactions:
		№ 0
			recordDate: 2017-02-07 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2017-02-07 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2017-01-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2017-01-18 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2017-01-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2017-01-09 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 6
			recordDate: 2017-01-09 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 7
			recordDate: 2017-01-06 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 8
			recordDate: 2017-01-04 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 9
			recordDate: 2016-12-15 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 10
			recordDate: 2016-12-13 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 11
			recordDate: 2016-12-12 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 12
			recordDate: 2016-12-12 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 13
			recordDate: 2016-12-12 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 14
			recordDate: 2016-12-12 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 15
			recordDate: 2016-12-12 00:00:00
			code: DRWF
			description: Workflow - Drawings Finished
		№ 16
			recordDate: 2016-12-12 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 17
			recordDate: 2016-09-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 18
			recordDate: 2016-09-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 19
			recordDate: 2016-09-14 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 20
			recordDate: 2016-09-09 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 21
			recordDate: 2016-09-05 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 22
			recordDate: 2016-08-30 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 23
			recordDate: 2016-07-05 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 24
			recordDate: 2016-07-05 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 25
			recordDate: 2016-06-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 26
			recordDate: 2016-06-27 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 27
			recordDate: 2016-03-25 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 28
			recordDate: 2016-03-25 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 29
			recordDate: 2016-03-25 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 30
			recordDate: 2016-03-20 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 31
			recordDate: 2016-02-26 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 32
			recordDate: 2016-02-26 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 33
			recordDate: 2016-02-12 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 34
			recordDate: 2016-02-12 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 35
			recordDate: 2016-02-12 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 36
			recordDate: 2015-10-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 37
			recordDate: 2015-10-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 38
			recordDate: 2015-10-13 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 39
			recordDate: 2015-10-05 00:00:00
			code: CTFR
			description: Final Rejection
		№ 40
			recordDate: 2015-06-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 41
			recordDate: 2015-06-26 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 42
			recordDate: 2015-06-26 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 43
			recordDate: 2015-01-30 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 44
			recordDate: 2015-01-30 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 45
			recordDate: 2015-01-30 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 46
			recordDate: 2015-01-26 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 47
			recordDate: 2015-01-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 48
			recordDate: 2014-09-30 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 49
			recordDate: 2014-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 50
			recordDate: 2014-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 51
			recordDate: 2014-07-25 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 52
			recordDate: 2014-07-25 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 53
			recordDate: 2014-04-25 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 54
			recordDate: 2014-04-24 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 55
			recordDate: 2013-10-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 56
			recordDate: 2013-10-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 57
			recordDate: 2013-10-24 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 58
			recordDate: 2013-10-17 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 59
			recordDate: 2013-03-09 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 60
			recordDate: 2012-11-29 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 61
			recordDate: 2012-11-08 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 62
			recordDate: 2012-11-08 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 63
			recordDate: 2012-11-08 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 64
			recordDate: 2012-11-08 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 65
			recordDate: 2012-11-08 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 66
			recordDate: 2012-11-07 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 67
			recordDate: 2012-10-22 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 68
			recordDate: 2012-10-18 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 69
			recordDate: 2012-10-18 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 70
			recordDate: 2012-10-18 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9562762
	applIdStr: 13654879
	appl_id_txt: 13654879
	appEarlyPubNumber: US20140111615A1
	appEntityStatus: UNDISCOUNTED
	id: 13654879
	appSubCls: 046000
	appLocation: ELECTRONIC
	appAttrDockNumber: 170108-1840
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 6
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|6}
	appStatusDate: 2017-01-18T12:14:22Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-04-24T04:00:00Z
	patentIssueDate: 2017-02-07T05:00:00Z
	APP_IND: 1
	appExamName: BILLAH, MASUM
	appExamNameFacet: BILLAH, MASUM
	firstInventorFile: No
	appClsSubCls: 348/046000
	appClsSubClsFacet: 348/046000
	applId: 13654879
	patentTitle: AUTOMATED OPTICAL DIMENSIONING AND IMAGING
	appIntlPubNumber: 
	appConfrNumber: 2957
	appFilingDate: 2012-10-18T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Sarah D. Benjamin
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Jonathan G. McGuire
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 2
			nameLineOne: Sunil  Ramesh
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Jose, 
			geoCode: CA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Peter D. Rowley
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Sammamish, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Matthew Warren Amacker
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Santa Clara, 
			geoCode: CA
			country: (US)
			rankNo: 5
	appCls: 348
	_version_: 1580433193878683651
	lastUpdatedTimestamp: 2017-10-05T15:49:05.769Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: cb75d65e-1d98-4317-bbe5-4e27ee422ded
	responseHeader:
			status: 0
			QTime: 16
