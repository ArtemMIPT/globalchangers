DOCUMENT_BODY:
	appGrpArtNumber: 2454
	appGrpArtNumberFacet: 2454
	transactions:
		№ 0
			recordDate: 2016-09-19 00:00:00
			code: N423
			description: Post Issue Communication - Certificate of Correction
		№ 1
			recordDate: 2016-08-02 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2016-08-02 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2016-07-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2016-07-13 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2016-06-29 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2016-06-29 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 7
			recordDate: 2016-06-29 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 8
			recordDate: 2016-06-28 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2016-06-28 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2016-04-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 11
			recordDate: 2016-04-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 12
			recordDate: 2016-04-12 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 13
			recordDate: 2016-04-06 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 14
			recordDate: 2016-04-01 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 15
			recordDate: 2016-03-03 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 16
			recordDate: 2016-02-23 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 17
			recordDate: 2016-02-23 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 18
			recordDate: 2016-02-05 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 19
			recordDate: 2016-02-05 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 20
			recordDate: 2016-01-04 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 21
			recordDate: 2015-12-31 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 22
			recordDate: 2015-12-17 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 23
			recordDate: 2015-11-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 24
			recordDate: 2015-11-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 25
			recordDate: 2015-11-05 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 26
			recordDate: 2015-11-02 00:00:00
			code: CTFR
			description: Final Rejection
		№ 27
			recordDate: 2015-09-11 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 28
			recordDate: 2015-09-04 00:00:00
			code: LTDR
			description: Incoming Letter Pertaining to the Drawings
		№ 29
			recordDate: 2015-09-04 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 30
			recordDate: 2015-09-04 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 31
			recordDate: 2015-08-04 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 32
			recordDate: 2015-07-28 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 33
			recordDate: 2015-07-28 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 34
			recordDate: 2015-05-04 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 35
			recordDate: 2015-05-04 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 36
			recordDate: 2015-05-04 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 37
			recordDate: 2015-04-29 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 38
			recordDate: 2015-04-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 39
			recordDate: 2014-10-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 40
			recordDate: 2014-09-05 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 41
			recordDate: 2014-09-04 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 42
			recordDate: 2014-08-22 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 43
			recordDate: 2014-08-22 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 44
			recordDate: 2014-08-22 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 45
			recordDate: 2014-03-07 00:00:00
			code: PST_CRD
			description: Mail Post Card
		№ 46
			recordDate: 2014-02-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 47
			recordDate: 2014-02-27 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 48
			recordDate: 2014-02-21 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 49
			recordDate: 2014-02-21 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 50
			recordDate: 2014-02-20 00:00:00
			code: PG-PB-DT
			description: PG-Pub Notice of new or Revised projected publication date
		№ 51
			recordDate: 2014-02-12 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 52
			recordDate: 2013-11-02 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 53
			recordDate: 2013-05-20 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 54
			recordDate: 2013-04-04 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 55
			recordDate: 2013-04-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 56
			recordDate: 2013-04-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 57
			recordDate: 2013-04-04 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 58
			recordDate: 2013-04-04 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 59
			recordDate: 2013-04-03 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 60
			recordDate: 2013-04-03 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 61
			recordDate: 2013-03-09 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 62
			recordDate: 2013-03-04 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 63
			recordDate: 2013-03-04 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 64
			recordDate: 2013-03-04 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9407505
	applIdStr: 13784276
	appl_id_txt: 13784276
	appEarlyPubNumber: US20140250215A1
	appEntityStatus: UNDISCOUNTED
	id: 13784276
	appSubCls: 223000
	appLocation: ELECTRONIC
	appAttrDockNumber: 101058.000027
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 23377
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2016-07-13T09:19:50Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-09-04T04:00:00Z
	patentIssueDate: 2016-08-02T04:00:00Z
	APP_IND: 1
	appExamName: DONAGHUE, LARRY D
	appExamNameFacet: DONAGHUE, LARRY D
	firstInventorFile: No
	appClsSubCls: 709/223000
	appClsSubClsFacet: 709/223000
	applId: 13784276
	patentTitle: CONFIGURATION AND VERIFICATION BY TRUSTED PROVIDER
	appIntlPubNumber: 
	appConfrNumber: 3143
	appFilingDate: 2013-03-04T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Peter Zachary Bowen
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bainbridge Island, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 709
	_version_: 1580433220575428610
	lastUpdatedTimestamp: 2017-10-05T15:49:31.231Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 97c7c182-a293-42b3-a857-160cc92d5dd6
	responseHeader:
			status: 0
			QTime: 17
