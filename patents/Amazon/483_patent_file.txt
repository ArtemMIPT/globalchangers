DOCUMENT_BODY:
	appGrpArtNumber: 2159
	appGrpArtNumberFacet: 2159
	transactions:
		№ 0
			recordDate: 2016-10-04 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-10-04 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 2
			recordDate: 2016-10-04 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2016-09-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2016-09-14 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2016-09-01 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2016-09-01 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2016-08-31 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 8
			recordDate: 2016-08-31 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2016-08-31 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2016-06-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 11
			recordDate: 2016-06-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 12
			recordDate: 2016-06-14 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 13
			recordDate: 2016-06-10 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 14
			recordDate: 2016-06-09 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 15
			recordDate: 2016-06-09 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 16
			recordDate: 2016-06-07 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 17
			recordDate: 2016-02-22 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 18
			recordDate: 2016-02-22 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 19
			recordDate: 2016-02-18 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 20
			recordDate: 2016-02-16 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 21
			recordDate: 2016-02-16 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 22
			recordDate: 2016-02-16 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 23
			recordDate: 2016-02-11 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 24
			recordDate: 2015-10-15 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 25
			recordDate: 2015-10-15 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 26
			recordDate: 2015-10-15 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 27
			recordDate: 2015-10-06 00:00:00
			code: CTFR
			description: Final Rejection
		№ 28
			recordDate: 2015-08-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 29
			recordDate: 2015-08-20 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 30
			recordDate: 2015-08-20 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 31
			recordDate: 2015-06-02 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 32
			recordDate: 2015-05-28 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 33
			recordDate: 2015-05-28 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 34
			recordDate: 2015-02-25 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 35
			recordDate: 2015-02-25 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 36
			recordDate: 2015-02-25 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 37
			recordDate: 2015-02-18 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 38
			recordDate: 2014-11-05 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 39
			recordDate: 2013-05-28 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 40
			recordDate: 2013-05-24 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 41
			recordDate: 2013-05-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 42
			recordDate: 2013-05-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 43
			recordDate: 2013-05-09 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 44
			recordDate: 2013-05-09 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 45
			recordDate: 2013-05-09 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 46
			recordDate: 2013-05-08 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 47
			recordDate: 2013-05-08 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 48
			recordDate: 2013-04-02 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 49
			recordDate: 2013-03-29 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 50
			recordDate: 2013-03-27 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 51
			recordDate: 2013-03-27 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 52
			recordDate: 2013-03-27 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9460163
	applIdStr: 13851576
	appl_id_txt: 13851576
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13851576
	appSubCls: 722000
	appLocation: ELECTRONIC
	appAttrDockNumber: 170114-1980
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 7
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|7}
	appStatusDate: 2016-09-14T12:36:18Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-10-04T04:00:00Z
	APP_IND: 1
	appExamName: SOMERS, MARC S
	appExamNameFacet: SOMERS, MARC S
	firstInventorFile: Yes
	appClsSubCls: 707/722000
	appClsSubClsFacet: 707/722000
	applId: 13851576
	patentTitle: CONFIGURABLE EXTRACTIONS IN SOCIAL MEDIA
	appIntlPubNumber: 
	appConfrNumber: 5445
	appFilingDate: 2013-03-27T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Catherine Ann Champion
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Algona, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Olivier  Suritz
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Toronto, 
			geoCode: 
			country: (CA)
			rankNo: 4
		№ 2
			nameLineOne: Daniel Seiji Kuwahara
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Waterloo, 
			geoCode: 
			country: (CA)
			rankNo: 6
		№ 3
			nameLineOne: Colin Kieron Toal
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: St. Thomas, 
			geoCode: 
			country: (CA)
			rankNo: 1
		№ 4
			nameLineOne: Jacqueline Joanne van der Net
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Burien, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 5
			nameLineOne: William Alfred Nagel
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 5
	appCls: 707
	_version_: 1580433234422923265
	lastUpdatedTimestamp: 2017-10-05T15:49:44.465Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 8d2a2ffd-bf72-4810-a487-8f9a738c994e
	responseHeader:
			status: 0
			QTime: 17
