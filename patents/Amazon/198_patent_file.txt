DOCUMENT_BODY:
	appGrpArtNumber: 2641
	appGrpArtNumberFacet: 2641
	transactions:
		№ 0
			recordDate: 2015-01-13 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-01-13 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2014-12-23 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 3
			recordDate: 2014-12-16 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 4
			recordDate: 2014-12-08 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 5
			recordDate: 2014-12-08 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2014-12-08 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 7
			recordDate: 2014-11-21 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 8
			recordDate: 2014-11-21 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 9
			recordDate: 2014-11-18 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 10
			recordDate: 2014-11-10 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 11
			recordDate: 2014-11-05 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 12
			recordDate: 2014-09-19 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 13
			recordDate: 2014-09-18 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 14
			recordDate: 2014-09-17 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 15
			recordDate: 2014-09-17 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 16
			recordDate: 2014-09-16 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 17
			recordDate: 2014-09-12 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 18
			recordDate: 2014-09-12 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 19
			recordDate: 2014-07-28 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 20
			recordDate: 2014-07-25 00:00:00
			code: CTFR
			description: Final Rejection
		№ 21
			recordDate: 2014-07-25 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 22
			recordDate: 2014-06-19 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 23
			recordDate: 2014-06-13 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 24
			recordDate: 2014-06-13 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 25
			recordDate: 2014-06-12 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 26
			recordDate: 2014-06-06 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 27
			recordDate: 2014-03-10 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 28
			recordDate: 2014-03-06 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 29
			recordDate: 2014-01-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 30
			recordDate: 2013-12-23 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 31
			recordDate: 2013-12-18 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 32
			recordDate: 2013-03-05 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 33
			recordDate: 2013-02-26 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 34
			recordDate: 2013-02-26 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 35
			recordDate: 2013-02-13 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 36
			recordDate: 2013-01-23 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 37
			recordDate: 2013-01-23 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 38
			recordDate: 2013-01-23 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 39
			recordDate: 2012-12-26 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 40
			recordDate: 2012-12-19 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 41
			recordDate: 2012-12-19 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 42
			recordDate: 2012-12-19 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8934869
	applIdStr: 13720673
	appl_id_txt: 13720673
	appEarlyPubNumber: US20140171088A1
	appEntityStatus: UNDISCOUNTED
	id: 13720673
	appSubCls: 440000
	appLocation: ELECTRONIC
	appAttrDockNumber: 08862-178 (L0178)
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 14432
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2014-12-23T11:10:06Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-06-19T04:00:00Z
	patentIssueDate: 2015-01-13T05:00:00Z
	APP_IND: 1
	appExamName: NGUYEN, KHAI MINH
	appExamNameFacet: NGUYEN, KHAI MINH
	firstInventorFile: No
	appClsSubCls: 455/440000
	appClsSubClsFacet: 455/440000
	applId: 13720673
	patentTitle: DETERMINING MOBILITY STATES FOR A USER DEVICE
	appIntlPubNumber: 
	appConfrNumber: 6384
	appFilingDate: 2012-12-19T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Kiran K. Edara
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Cupertino, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Varada  Gopalakrishnan
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Cupertino, 
			geoCode: CA
			country: (US)
			rankNo: 2
	appCls: 455
	_version_: 1580433207337156613
	lastUpdatedTimestamp: 2017-10-05T15:49:18.616Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 6127cd1c-1e1f-490f-8308-652c641ee1b3
	responseHeader:
			status: 0
			QTime: 15
