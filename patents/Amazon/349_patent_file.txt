DOCUMENT_BODY:
	appGrpArtNumber: 2454
	appGrpArtNumberFacet: 2454
	transactions:
		№ 0
			recordDate: 2016-07-19 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-07-19 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-06-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-06-29 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-06-17 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-06-17 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-06-16 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2016-06-16 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2016-06-16 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 9
			recordDate: 2016-06-16 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 10
			recordDate: 2016-06-16 00:00:00
			code: MM327
			description: Mail Miscellaneous Communication to Applicant
		№ 11
			recordDate: 2016-06-13 00:00:00
			code: M327
			description: Miscellaneous Communication to Applicant - No Action Count
		№ 12
			recordDate: 2016-06-13 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 13
			recordDate: 2016-06-10 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 14
			recordDate: 2016-06-08 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 15
			recordDate: 2016-06-08 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 16
			recordDate: 2016-06-08 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 17
			recordDate: 2016-03-31 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 18
			recordDate: 2016-03-31 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 19
			recordDate: 2016-03-31 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 20
			recordDate: 2016-03-22 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 21
			recordDate: 2016-03-11 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 22
			recordDate: 2016-03-04 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 23
			recordDate: 2016-01-26 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 24
			recordDate: 2016-01-20 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 25
			recordDate: 2016-01-12 00:00:00
			code: M865E
			description: Electronic request for Examiner Interview
		№ 26
			recordDate: 2015-12-04 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 27
			recordDate: 2015-12-04 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 28
			recordDate: 2015-12-04 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 29
			recordDate: 2015-12-02 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 30
			recordDate: 2015-09-17 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 31
			recordDate: 2015-09-17 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 32
			recordDate: 2015-09-12 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 33
			recordDate: 2015-09-08 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 34
			recordDate: 2015-09-08 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 35
			recordDate: 2015-08-13 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 36
			recordDate: 2015-08-07 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 37
			recordDate: 2015-06-19 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 38
			recordDate: 2015-06-19 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 39
			recordDate: 2015-06-19 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 40
			recordDate: 2015-05-07 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 41
			recordDate: 2015-05-07 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 42
			recordDate: 2015-05-07 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 43
			recordDate: 2015-05-04 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 44
			recordDate: 2015-05-04 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 45
			recordDate: 2015-05-04 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 46
			recordDate: 2015-05-04 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 47
			recordDate: 2015-04-07 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 48
			recordDate: 2014-10-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 49
			recordDate: 2013-11-02 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 50
			recordDate: 2013-06-11 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 51
			recordDate: 2013-04-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 52
			recordDate: 2013-04-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 53
			recordDate: 2013-04-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 54
			recordDate: 2013-04-24 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 55
			recordDate: 2013-04-24 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 56
			recordDate: 2013-04-24 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 57
			recordDate: 2013-04-23 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 58
			recordDate: 2013-04-23 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 59
			recordDate: 2013-03-18 00:00:00
			code: L128
			description: Cleared by L&R (LARS)
		№ 60
			recordDate: 2013-03-13 00:00:00
			code: L198
			description: Referred to Level 2 (LARS) by OIPE CSR
		№ 61
			recordDate: 2013-03-06 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 62
			recordDate: 2013-03-06 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 63
			recordDate: 2013-03-06 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9398066
	applIdStr: 13787553
	appl_id_txt: 13787553
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13787553
	appSubCls: 213000
	appLocation: ELECTRONIC
	appAttrDockNumber: 101058.000028
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 23377
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2016-06-29T11:20:25Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-07-19T04:00:00Z
	APP_IND: 1
	appExamName: DONAGHUE, LARRY D
	appExamNameFacet: DONAGHUE, LARRY D
	firstInventorFile: No
	appClsSubCls: 709/213000
	appClsSubClsFacet: 709/213000
	applId: 13787553
	patentTitle: SERVER DEFENSES AGAINST USE OF TAINTED CACHE
	appIntlPubNumber: 
	appConfrNumber: 5099
	appFilingDate: 2013-03-06T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Gregory Branchek Roth
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Nicholas Howard Brown
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 709
	_version_: 1580433221176262658
	lastUpdatedTimestamp: 2017-10-05T15:49:31.815Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 97c7c182-a293-42b3-a857-160cc92d5dd6
	responseHeader:
			status: 0
			QTime: 17
