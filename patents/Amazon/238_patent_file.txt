DOCUMENT_BODY:
	appGrpArtNumber: 2143
	appGrpArtNumberFacet: 2143
	transactions:
		№ 0
			recordDate: 2015-12-22 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-12-22 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-12-03 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-12-02 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-11-20 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2015-11-20 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 6
			recordDate: 2015-11-20 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 7
			recordDate: 2015-11-19 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 8
			recordDate: 2015-11-16 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 9
			recordDate: 2015-11-16 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 10
			recordDate: 2015-11-06 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 11
			recordDate: 2015-11-05 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 12
			recordDate: 2015-11-05 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 13
			recordDate: 2015-11-05 00:00:00
			code: DRWF
			description: Workflow - Drawings Finished
		№ 14
			recordDate: 2015-11-05 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 15
			recordDate: 2015-11-05 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 16
			recordDate: 2015-08-19 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 17
			recordDate: 2015-08-19 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 18
			recordDate: 2015-08-19 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 19
			recordDate: 2015-08-16 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 20
			recordDate: 2015-08-13 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 21
			recordDate: 2015-08-13 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 22
			recordDate: 2015-08-12 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 23
			recordDate: 2015-04-14 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 24
			recordDate: 2015-04-13 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 25
			recordDate: 2015-04-09 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 26
			recordDate: 2015-03-11 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 27
			recordDate: 2015-03-11 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 28
			recordDate: 2015-01-16 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 29
			recordDate: 2015-01-16 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 30
			recordDate: 2015-01-16 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 31
			recordDate: 2015-01-12 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 32
			recordDate: 2015-01-08 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 33
			recordDate: 2015-01-05 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 34
			recordDate: 2014-12-23 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 35
			recordDate: 2014-12-23 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 36
			recordDate: 2014-12-12 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 37
			recordDate: 2014-12-12 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 38
			recordDate: 2014-10-08 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 39
			recordDate: 2014-10-08 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 40
			recordDate: 2014-10-08 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 41
			recordDate: 2014-09-30 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 42
			recordDate: 2014-08-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 43
			recordDate: 2013-05-13 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 44
			recordDate: 2013-04-08 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 45
			recordDate: 2013-02-14 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 46
			recordDate: 2013-02-14 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 47
			recordDate: 2013-02-14 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 48
			recordDate: 2013-02-14 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 49
			recordDate: 2013-01-22 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 50
			recordDate: 2013-01-17 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 51
			recordDate: 2013-01-17 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 52
			recordDate: 2013-01-17 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9218108
	applIdStr: 13743867
	appl_id_txt: 13743867
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13743867
	appSubCls: 771000
	appLocation: ELECTRONIC
	appAttrDockNumber: 25396-0265
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 29052
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|2}
	appStatusDate: 2015-12-02T11:44:43Z
	LAST_MOD_TS: 2017-09-04T08:30:19Z
	patentIssueDate: 2015-12-22T05:00:00Z
	APP_IND: 1
	appExamName: FORTINO, ASHLEY
	appExamNameFacet: FORTINO, ASHLEY
	firstInventorFile: No
	appClsSubCls: 715/771000
	appClsSubClsFacet: 715/771000
	applId: 13743867
	patentTitle: Battery Charging Metrics and Representations
	appIntlPubNumber: 
	appConfrNumber: 4619
	appFilingDate: 2013-01-17T05:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Rangaprabhu  Parthasarathy
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Jose, 
			geoCode: CA
			country: (US)
			rankNo: 1
	appCls: 715
	_version_: 1580433212159557634
	lastUpdatedTimestamp: 2017-10-05T15:49:23.238Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 38365122-7fb9-41d5-b2d7-e4dacdf0b560
	responseHeader:
			status: 0
			QTime: 16
