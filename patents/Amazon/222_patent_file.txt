DOCUMENT_BODY:
	appGrpArtNumber: 2457
	appGrpArtNumberFacet: 2457
	transactions:
		№ 0
			recordDate: 2016-03-29 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-03-29 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-03-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-03-09 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-02-22 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-02-19 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-02-17 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2016-02-17 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-11-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2015-11-18 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2015-11-18 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2015-11-03 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2015-08-28 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 13
			recordDate: 2015-08-26 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 14
			recordDate: 2015-08-26 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 15
			recordDate: 2015-03-26 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 16
			recordDate: 2015-03-26 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 17
			recordDate: 2015-03-26 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 18
			recordDate: 2015-03-22 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 19
			recordDate: 2014-12-02 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 20
			recordDate: 2014-08-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 21
			recordDate: 2014-08-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 22
			recordDate: 2013-08-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 23
			recordDate: 2013-03-27 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 24
			recordDate: 2013-02-05 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 25
			recordDate: 2013-02-05 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 26
			recordDate: 2013-02-05 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 27
			recordDate: 2013-02-05 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 28
			recordDate: 2013-02-05 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 29
			recordDate: 2013-02-04 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 30
			recordDate: 2013-01-06 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 31
			recordDate: 2013-01-03 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 32
			recordDate: 2013-01-03 00:00:00
			code: IEXX
			description: Initial Exam Team nn
		№ 33
			recordDate: 2013-01-03 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9300759
	applIdStr: 13733705
	appl_id_txt: 13733705
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13733705
	appSubCls: 203000
	appLocation: ELECTRONIC
	appAttrDockNumber: 101058.000018
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 23377
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|2}
	appStatusDate: 2016-03-09T13:07:07Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-03-29T04:00:00Z
	APP_IND: 1
	appExamName: DALENCOURT, YVES
	appExamNameFacet: DALENCOURT, YVES
	firstInventorFile: No
	appClsSubCls: 709/203000
	appClsSubClsFacet: 709/203000
	applId: 13733705
	patentTitle: API CALLS WITH DEPENDENCIES
	appIntlPubNumber: 
	appConfrNumber: 5432
	appFilingDate: 2013-01-03T05:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Andrew James Jorgensen
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Lynnwood, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 709
	_version_: 1580433209986908160
	lastUpdatedTimestamp: 2017-10-05T15:49:21.159Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 38365122-7fb9-41d5-b2d7-e4dacdf0b560
	responseHeader:
			status: 0
			QTime: 16
