DOCUMENT_BODY:
	appGrpArtNumber: 2163
	appGrpArtNumberFacet: 2163
	transactions:
		№ 0
			recordDate: 2016-01-05 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-01-05 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-12-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-12-16 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-12-02 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-12-01 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-11-25 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-11-25 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-08-26 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2015-08-26 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2015-08-26 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2015-08-23 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2015-08-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 13
			recordDate: 2015-08-19 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 14
			recordDate: 2015-08-19 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 15
			recordDate: 2015-06-19 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 16
			recordDate: 2015-06-18 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 17
			recordDate: 2015-06-12 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 18
			recordDate: 2015-06-09 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 19
			recordDate: 2015-06-09 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 20
			recordDate: 2015-03-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 21
			recordDate: 2015-03-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 22
			recordDate: 2015-03-12 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 23
			recordDate: 2015-03-06 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 24
			recordDate: 2014-12-12 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 25
			recordDate: 2014-12-10 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 26
			recordDate: 2014-12-10 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 27
			recordDate: 2014-12-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 28
			recordDate: 2014-12-01 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 29
			recordDate: 2014-11-25 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 30
			recordDate: 2014-11-20 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 31
			recordDate: 2014-11-17 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 32
			recordDate: 2014-11-10 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 33
			recordDate: 2014-11-10 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 34
			recordDate: 2014-11-04 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 35
			recordDate: 2014-11-04 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 36
			recordDate: 2014-09-10 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 37
			recordDate: 2014-09-10 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 38
			recordDate: 2014-09-10 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 39
			recordDate: 2014-09-05 00:00:00
			code: CTFR
			description: Final Rejection
		№ 40
			recordDate: 2014-07-01 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 41
			recordDate: 2014-06-26 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 42
			recordDate: 2014-03-26 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 43
			recordDate: 2014-03-26 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 44
			recordDate: 2014-03-26 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 45
			recordDate: 2014-03-21 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 46
			recordDate: 2014-03-21 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 47
			recordDate: 2014-01-14 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 48
			recordDate: 2013-01-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 49
			recordDate: 2013-01-30 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 50
			recordDate: 2013-01-16 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 51
			recordDate: 2013-01-14 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 52
			recordDate: 2013-01-04 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 53
			recordDate: 2013-01-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 54
			recordDate: 2013-01-04 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 55
			recordDate: 2013-01-03 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 56
			recordDate: 2012-12-03 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 57
			recordDate: 2012-12-03 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 58
			recordDate: 2012-12-02 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 59
			recordDate: 2012-11-30 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 60
			recordDate: 2012-11-30 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 61
			recordDate: 2012-11-30 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 62
			recordDate: 2012-11-30 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9230011
	applIdStr: 13689919
	appl_id_txt: 13689919
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13689919
	appSubCls: 706000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-46600
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2015-12-16T13:42:06Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-01-05T05:00:00Z
	APP_IND: 1
	appExamName: NGUYEN, KIM T
	appExamNameFacet: NGUYEN, KIM T
	firstInventorFile: No
	appClsSubCls: 707/706000
	appClsSubClsFacet: 707/706000
	applId: 13689919
	patentTitle: INDEX-BASED QUERYING OF ARCHIVED DATA SETS
	appIntlPubNumber: 
	appConfrNumber: 1069
	appFilingDate: 2012-11-30T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: KARTHIK  TAMILMANI
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Redmond, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 707
	_version_: 1580433200959717378
	lastUpdatedTimestamp: 2017-10-05T15:49:12.53Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 397b5564-109f-402b-a2a8-4568b73be568
	responseHeader:
			status: 0
			QTime: 15
