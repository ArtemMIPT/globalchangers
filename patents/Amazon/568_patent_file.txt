DOCUMENT_BODY:
	appGrpArtNumber: 3627
	appGrpArtNumberFacet: 3627
	transactions:
		№ 0
			recordDate: 2016-09-13 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-09-13 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 2
			recordDate: 2016-09-13 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2016-08-25 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2016-08-24 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2016-08-17 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2016-08-17 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2016-08-16 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 8
			recordDate: 2016-08-16 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2016-08-16 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2016-05-24 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 11
			recordDate: 2016-05-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 12
			recordDate: 2016-05-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 13
			recordDate: 2016-05-17 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 14
			recordDate: 2016-05-12 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 15
			recordDate: 2016-05-05 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 16
			recordDate: 2016-05-05 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 17
			recordDate: 2016-05-03 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 18
			recordDate: 2016-05-02 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 19
			recordDate: 2016-05-02 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 20
			recordDate: 2016-03-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 21
			recordDate: 2016-03-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 22
			recordDate: 2016-03-01 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 23
			recordDate: 2016-02-24 00:00:00
			code: CTFR
			description: Final Rejection
		№ 24
			recordDate: 2016-02-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 25
			recordDate: 2016-02-10 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 26
			recordDate: 2015-11-10 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 27
			recordDate: 2015-11-10 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 28
			recordDate: 2015-11-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 29
			recordDate: 2015-11-04 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 30
			recordDate: 2015-10-30 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 31
			recordDate: 2015-10-30 00:00:00
			code: W/N=
			description: Withdrawal of Notice of Allowance
		№ 32
			recordDate: 2015-09-25 00:00:00
			code: MM327-O
			description: Mail PUBS Notice Requiring Inventors Oath or Declaration
		№ 33
			recordDate: 2015-09-23 00:00:00
			code: M327-O
			description: PUBS Notice Requiring Inventors Oath or Declaration
		№ 34
			recordDate: 2015-09-16 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 35
			recordDate: 2015-09-16 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 36
			recordDate: 2015-09-16 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 37
			recordDate: 2015-09-11 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 38
			recordDate: 2015-09-11 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 39
			recordDate: 2015-09-09 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 40
			recordDate: 2015-07-10 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 41
			recordDate: 2015-06-30 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 42
			recordDate: 2015-04-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 43
			recordDate: 2015-04-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 44
			recordDate: 2015-04-02 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 45
			recordDate: 2015-03-30 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 46
			recordDate: 2013-06-28 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 47
			recordDate: 2013-06-22 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 48
			recordDate: 2013-06-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 49
			recordDate: 2013-06-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 50
			recordDate: 2013-06-11 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 51
			recordDate: 2013-06-11 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 52
			recordDate: 2013-06-11 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 53
			recordDate: 2013-06-10 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 54
			recordDate: 2013-06-10 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 55
			recordDate: 2013-05-13 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 56
			recordDate: 2013-05-08 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 57
			recordDate: 2013-05-08 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 58
			recordDate: 2013-05-08 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 59
			recordDate: 2013-05-08 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9443219
	applIdStr: 13889606
	appl_id_txt: 13889606
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13889606
	appSubCls: 340000
	appLocation: ELECTRONIC
	appAttrDockNumber: 170105-1051
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 5
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|5}
	appStatusDate: 2016-08-24T08:52:11Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-09-13T04:00:00Z
	APP_IND: 1
	appExamName: WILDER, ANDREW H
	appExamNameFacet: WILDER, ANDREW H
	firstInventorFile: No
	appClsSubCls: 705/340000
	appClsSubClsFacet: 705/340000
	applId: 13889606
	patentTitle: COURIER MANAGEMENT
	appIntlPubNumber: 
	appConfrNumber: 8619
	appFilingDate: 2013-05-08T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Charles M. Griffith
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bainbridge Island, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Jinqian  Li
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (CA)
			rankNo: 2
		№ 2
			nameLineOne: Jonathan J. Shakes
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Mercer Island, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Jin  Lai
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Beijing, 
			geoCode: 
			country: (CN)
			rankNo: 4
	appCls: 705
	_version_: 1580433242167705600
	lastUpdatedTimestamp: 2017-10-05T15:49:51.822Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 8a5d78fb-c4aa-4695-8abb-3e524687bf44
	responseHeader:
			status: 0
			QTime: 18
