DOCUMENT_BODY:
	appGrpArtNumber: 2645
	appGrpArtNumberFacet: 2645
	transactions:
		№ 0
			recordDate: 2016-05-10 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-05-10 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-04-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-04-20 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-04-07 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-04-07 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-04-06 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 7
			recordDate: 2016-04-06 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2016-04-06 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2016-01-07 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2016-01-07 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2016-01-07 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 12
			recordDate: 2015-12-17 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 13
			recordDate: 2015-12-15 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 14
			recordDate: 2015-12-10 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 15
			recordDate: 2015-12-08 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 16
			recordDate: 2015-12-08 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 17
			recordDate: 2015-12-03 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 18
			recordDate: 2015-11-24 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 19
			recordDate: 2015-09-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 20
			recordDate: 2015-09-09 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 21
			recordDate: 2015-09-09 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 22
			recordDate: 2015-09-02 00:00:00
			code: CTFR
			description: Final Rejection
		№ 23
			recordDate: 2015-05-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 24
			recordDate: 2015-05-15 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 25
			recordDate: 2015-05-15 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 26
			recordDate: 2014-11-20 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 27
			recordDate: 2014-11-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 28
			recordDate: 2014-11-20 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 29
			recordDate: 2014-11-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 30
			recordDate: 2014-11-15 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 31
			recordDate: 2013-11-02 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 32
			recordDate: 2013-10-07 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 33
			recordDate: 2013-10-04 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 34
			recordDate: 2013-10-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 35
			recordDate: 2013-10-04 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 36
			recordDate: 2013-10-03 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 37
			recordDate: 2013-03-15 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 38
			recordDate: 2013-03-15 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 39
			recordDate: 2013-03-15 00:00:00
			code: ADDFLFEE
			description: Additional Application Filing Fees
		№ 40
			recordDate: 2013-03-15 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 41
			recordDate: 2013-01-31 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 42
			recordDate: 2013-01-31 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 43
			recordDate: 2013-01-31 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 44
			recordDate: 2013-01-31 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 45
			recordDate: 2013-01-31 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 46
			recordDate: 2013-01-31 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 47
			recordDate: 2013-01-31 00:00:00
			code: INCD
			description: Notice Mailed--Application Incomplete--Filing Date Assigned 
		№ 48
			recordDate: 2012-12-11 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 49
			recordDate: 2012-12-07 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 50
			recordDate: 2012-12-07 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 51
			recordDate: 2012-12-07 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9338053
	applIdStr: 13708068
	appl_id_txt: 13708068
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13708068
	appSubCls: 220000
	appLocation: ELECTRONIC
	appAttrDockNumber: 170104-1471
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|3}
	appStatusDate: 2016-04-20T11:53:58Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-05-10T04:00:00Z
	APP_IND: 1
	appExamName: BILGRAMI, ASGHAR H
	appExamNameFacet: BILGRAMI, ASGHAR H
	firstInventorFile: No
	appClsSubCls: 709/220000
	appClsSubClsFacet: 709/220000
	applId: 13708068
	patentTitle: AUTOMATICALLY CONFIGURING VIRTUAL PRIVATE NETWORKS
	appIntlPubNumber: 
	appConfrNumber: 6381
	appFilingDate: 2012-12-07T05:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Aparna  Nagargadde
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Herndon, 
			geoCode: VA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Kevin C. Miller
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Herndon, 
			geoCode: VA
			country: (US)
			rankNo: 1
	appCls: 709
	_version_: 1580433204776534021
	lastUpdatedTimestamp: 2017-10-05T15:49:16.2Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 580c0491-4c13-4537-b2b0-749b4885eba3
	responseHeader:
			status: 0
			QTime: 15
