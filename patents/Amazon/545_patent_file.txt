DOCUMENT_BODY:
	appGrpArtNumber: 2454
	appGrpArtNumberFacet: 2454
	transactions:
		№ 0
			recordDate: 2017-09-07 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2017-08-11 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 2
			recordDate: 2017-08-11 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 3
			recordDate: 2017-08-11 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 4
			recordDate: 2017-07-24 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 5
			recordDate: 2017-05-04 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 6
			recordDate: 2017-05-04 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 7
			recordDate: 2017-05-04 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 8
			recordDate: 2017-02-08 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 9
			recordDate: 2017-02-08 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 10
			recordDate: 2017-02-03 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 11
			recordDate: 2017-02-03 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 12
			recordDate: 2017-02-03 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 13
			recordDate: 2017-02-03 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 14
			recordDate: 2017-02-03 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 15
			recordDate: 2017-02-03 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 16
			recordDate: 2017-02-03 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 17
			recordDate: 2017-01-25 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 18
			recordDate: 2017-01-25 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 19
			recordDate: 2017-01-19 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 20
			recordDate: 2017-01-19 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 21
			recordDate: 2017-01-18 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 22
			recordDate: 2017-01-10 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 23
			recordDate: 2017-01-06 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 24
			recordDate: 2017-01-06 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 25
			recordDate: 2016-10-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 26
			recordDate: 2016-10-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 27
			recordDate: 2016-10-06 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 28
			recordDate: 2016-09-29 00:00:00
			code: CTFR
			description: Final Rejection
		№ 29
			recordDate: 2016-09-23 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 30
			recordDate: 2016-09-23 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 31
			recordDate: 2016-09-23 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 32
			recordDate: 2016-08-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 33
			recordDate: 2016-08-01 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 34
			recordDate: 2016-07-29 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 35
			recordDate: 2016-07-28 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 36
			recordDate: 2016-07-25 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 37
			recordDate: 2016-07-25 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 38
			recordDate: 2016-07-13 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 39
			recordDate: 2016-07-07 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 40
			recordDate: 2016-04-25 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 41
			recordDate: 2016-04-25 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 42
			recordDate: 2016-04-25 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 43
			recordDate: 2016-04-15 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 44
			recordDate: 2016-01-15 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 45
			recordDate: 2016-01-15 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 46
			recordDate: 2015-12-28 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 47
			recordDate: 2015-12-28 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 48
			recordDate: 2015-10-20 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 49
			recordDate: 2015-10-20 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 50
			recordDate: 2015-10-20 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 51
			recordDate: 2015-10-16 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 52
			recordDate: 2015-09-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 53
			recordDate: 2015-09-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 54
			recordDate: 2015-09-23 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 55
			recordDate: 2015-09-18 00:00:00
			code: CTFR
			description: Final Rejection
		№ 56
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 57
			recordDate: 2015-07-22 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 58
			recordDate: 2015-07-16 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 59
			recordDate: 2015-07-16 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 60
			recordDate: 2015-03-16 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 61
			recordDate: 2015-03-16 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 62
			recordDate: 2015-03-16 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 63
			recordDate: 2015-02-22 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 64
			recordDate: 2015-01-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 65
			recordDate: 2015-01-02 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 66
			recordDate: 2015-01-02 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 67
			recordDate: 2015-01-02 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 68
			recordDate: 2014-10-30 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 69
			recordDate: 2014-05-01 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 70
			recordDate: 2014-05-01 00:00:00
			code: PG-PB-DT
			description: PG-Pub Notice of new or Revised projected publication date
		№ 71
			recordDate: 2014-04-28 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 72
			recordDate: 2014-02-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 73
			recordDate: 2013-07-23 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 74
			recordDate: 2013-06-07 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 75
			recordDate: 2013-06-07 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 76
			recordDate: 2013-06-07 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 77
			recordDate: 2013-06-07 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 78
			recordDate: 2013-06-07 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 79
			recordDate: 2013-05-06 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 80
			recordDate: 2013-04-29 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 81
			recordDate: 2013-04-29 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 82
			recordDate: 2013-04-29 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 83
			recordDate: 2013-04-29 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Non Final Action Mailed
	appStatus_txt: Non Final Action Mailed
	appStatusFacet: Non Final Action Mailed
	patentNumber: 
	applIdStr: 13872725
	appl_id_txt: 13872725
	appEarlyPubNumber: US20140325037A1
	appEntityStatus: UNDISCOUNTED
	id: 13872725
	appSubCls: 220000
	appLocation: ELECTRONIC
	appAttrDockNumber: AM2-1702US
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136607
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Seattle|WA|US|2}
	appStatusDate: 2017-08-09T17:18:59Z
	LAST_MOD_TS: 2017-09-07T10:52:43Z
	appEarlyPubDate: 2014-10-30T04:00:00Z
	APP_IND: 1
	appExamName: MCLEOD, MARSHALL M
	appExamNameFacet: MCLEOD, MARSHALL M
	firstInventorFile: Yes
	appClsSubCls: 709/220000
	appClsSubClsFacet: 709/220000
	applId: 13872725
	patentTitle: Automated Creation of Private Virtual Networks in a Service Provider Network
	appIntlPubNumber: 
	appConfrNumber: 9028
	appFilingDate: 2013-04-29T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Simon Jeremy Elisha
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 709
	_version_: 1580433238749347843
	lastUpdatedTimestamp: 2017-10-05T15:49:48.6Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 735b84c0-d1c3-4f21-b74f-5f61b338de75
	responseHeader:
			status: 0
			QTime: 16
