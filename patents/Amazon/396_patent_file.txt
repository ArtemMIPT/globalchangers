DOCUMENT_BODY:
	appGrpArtNumber: 2156
	appGrpArtNumberFacet: 2156
	transactions:
		№ 0
			recordDate: 2015-05-19 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-05-19 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-04-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-04-29 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-04-23 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-04-22 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-04-21 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-04-21 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-01-21 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2015-01-21 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2015-01-21 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2015-01-15 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2015-01-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 13
			recordDate: 2015-01-15 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 14
			recordDate: 2014-12-23 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 15
			recordDate: 2014-12-17 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 16
			recordDate: 2014-09-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 17
			recordDate: 2014-09-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 18
			recordDate: 2014-09-17 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 19
			recordDate: 2014-09-11 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 20
			recordDate: 2014-09-10 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 21
			recordDate: 2014-08-29 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 22
			recordDate: 2013-05-07 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 23
			recordDate: 2013-05-06 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 24
			recordDate: 2013-04-26 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 25
			recordDate: 2013-04-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 26
			recordDate: 2013-04-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 27
			recordDate: 2013-04-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 28
			recordDate: 2013-04-19 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 29
			recordDate: 2013-04-19 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 30
			recordDate: 2013-04-19 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 31
			recordDate: 2013-04-18 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 32
			recordDate: 2013-04-18 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 33
			recordDate: 2013-03-15 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 34
			recordDate: 2013-03-12 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 35
			recordDate: 2013-03-12 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 36
			recordDate: 2013-03-12 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 37
			recordDate: 2013-03-12 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9037571
	applIdStr: 13795508
	appl_id_txt: 13795508
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13795508
	appSubCls: 716000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-50000
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2015-04-29T09:12:00Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-05-19T04:00:00Z
	APP_IND: 1
	appExamName: VO, TRUONG V
	appExamNameFacet: VO, TRUONG V
	firstInventorFile: No
	appClsSubCls: 707/716000
	appClsSubClsFacet: 707/716000
	applId: 13795508
	patentTitle: TOPOLOGY SERVICE USING CLOSURE TABLES AND METAGRAPHS
	appIntlPubNumber: 
	appConfrNumber: 6189
	appFilingDate: 2013-03-12T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: JACOB ADAM GABRIELSON
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: MATTHEW PAUL BARANOWSKI
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 2
			nameLineOne: VIJAYARAGHAVAN  APPARSUNDARAM
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: AMHERST, 
			geoCode: MA
			country: (US)
			rankNo: 3
	appCls: 707
	_version_: 1580433222574014468
	lastUpdatedTimestamp: 2017-10-05T15:49:33.111Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 6073073c-cbc6-49b0-b9b5-714ce578e5c8
	responseHeader:
			status: 0
			QTime: 17
