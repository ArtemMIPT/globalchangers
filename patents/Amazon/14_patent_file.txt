DOCUMENT_BODY:
	appGrpArtNumber: 2622
	appGrpArtNumberFacet: 2622
	transactions:
		№ 0
			recordDate: 2016-07-19 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2015-06-23 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2015-06-23 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2015-06-05 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2015-06-03 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2015-05-26 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2015-05-21 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2015-05-20 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2015-05-20 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2015-03-16 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 10
			recordDate: 2015-03-16 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 11
			recordDate: 2015-03-03 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 12
			recordDate: 2015-03-03 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 13
			recordDate: 2015-03-03 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 14
			recordDate: 2015-02-27 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 15
			recordDate: 2015-02-06 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 16
			recordDate: 2015-02-06 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 17
			recordDate: 2015-02-04 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 18
			recordDate: 2015-02-04 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 19
			recordDate: 2015-02-04 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 20
			recordDate: 2014-11-13 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 21
			recordDate: 2014-11-06 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 22
			recordDate: 2014-11-06 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 23
			recordDate: 2014-09-04 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 24
			recordDate: 2014-09-04 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 25
			recordDate: 2014-09-04 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 26
			recordDate: 2014-08-27 00:00:00
			code: CTFR
			description: Final Rejection
		№ 27
			recordDate: 2014-07-24 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 28
			recordDate: 2014-07-22 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 29
			recordDate: 2014-07-18 00:00:00
			code: C614
			description: New or Additional Drawing Filed
		№ 30
			recordDate: 2014-07-18 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 31
			recordDate: 2014-07-16 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 32
			recordDate: 2014-07-16 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 33
			recordDate: 2014-04-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 34
			recordDate: 2014-04-18 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 35
			recordDate: 2014-04-18 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 36
			recordDate: 2014-04-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 37
			recordDate: 2014-04-14 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 38
			recordDate: 2014-02-25 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 39
			recordDate: 2013-11-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 40
			recordDate: 2013-05-16 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 41
			recordDate: 2013-05-16 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 42
			recordDate: 2013-05-16 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 43
			recordDate: 2013-03-03 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 44
			recordDate: 2013-01-16 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 45
			recordDate: 2012-12-27 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 46
			recordDate: 2012-12-27 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 47
			recordDate: 2012-12-26 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 48
			recordDate: 2012-12-14 00:00:00
			code: OATHDECL
			description: A statement by one or more inventors satisfying the requirement under 35 USC 115, Oath of the Applic
		№ 49
			recordDate: 2012-12-14 00:00:00
			code: ADDFLFEE
			description: Additional Application Filing Fees
		№ 50
			recordDate: 2012-10-15 00:00:00
			code: INCD
			description: Notice Mailed--Application Incomplete--Filing Date Assigned 
		№ 51
			recordDate: 2012-10-15 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 52
			recordDate: 2012-09-26 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 53
			recordDate: 2012-09-25 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 54
			recordDate: 2012-09-25 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 55
			recordDate: 2012-09-25 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 56
			recordDate: 2012-09-25 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9063563
	applIdStr: 13626475
	appl_id_txt: 13626475
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13626475
	appSubCls: 156000
	appLocation: ELECTRONIC
	appAttrDockNumber: 085101-526826.296NPUS00
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136608
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NY
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NY|US|4}
	appStatusDate: 2015-06-03T10:49:48Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-06-23T04:00:00Z
	APP_IND: 1
	appExamName: HICKS, CHARLES V
	appExamNameFacet: HICKS, CHARLES V
	firstInventorFile: No
	appClsSubCls: 345/156000
	appClsSubClsFacet: 345/156000
	applId: 13626475
	patentTitle: GESTURE ACTIONS FOR INTERFACE ELEMENTS
	appIntlPubNumber: 
	appConfrNumber: 9957
	appFilingDate: 2012-09-25T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Timothy T. Gray
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Aaron Michael Donsbach
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Mark R. Privett
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
	appCls: 345
	_version_: 1580433187967860738
	lastUpdatedTimestamp: 2017-10-05T15:49:00.142Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 2555c4ea-fcf8-4402-9cd6-c81e59fd1ab9
	responseHeader:
			status: 0
			QTime: 13
