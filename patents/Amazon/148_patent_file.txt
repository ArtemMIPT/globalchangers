DOCUMENT_BODY:
	appGrpArtNumber: 2431
	appGrpArtNumberFacet: 2431
	transactions:
		№ 0
			recordDate: 2017-06-20 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2017-06-20 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2017-06-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2017-05-31 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2017-05-22 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2017-01-19 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2017-01-19 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2017-01-12 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 8
			recordDate: 2017-01-12 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2017-01-12 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2016-10-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 11
			recordDate: 2016-10-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 12
			recordDate: 2016-10-13 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 13
			recordDate: 2016-10-07 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 14
			recordDate: 2016-09-28 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 15
			recordDate: 2016-08-18 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 16
			recordDate: 2016-08-15 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 17
			recordDate: 2016-08-15 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 18
			recordDate: 2016-05-26 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 19
			recordDate: 2016-05-23 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 20
			recordDate: 2016-04-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 21
			recordDate: 2016-04-18 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 22
			recordDate: 2016-04-18 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 23
			recordDate: 2016-04-12 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 24
			recordDate: 2016-04-05 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 25
			recordDate: 2016-03-02 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 26
			recordDate: 2016-03-02 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 27
			recordDate: 2016-02-17 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 28
			recordDate: 2016-02-17 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 29
			recordDate: 2016-02-17 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 30
			recordDate: 2016-02-17 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 31
			recordDate: 2016-02-17 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 32
			recordDate: 2016-01-13 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 33
			recordDate: 2016-01-08 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 34
			recordDate: 2015-11-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 35
			recordDate: 2015-11-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 36
			recordDate: 2015-11-13 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 37
			recordDate: 2015-11-06 00:00:00
			code: CTFR
			description: Final Rejection
		№ 38
			recordDate: 2015-08-11 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 39
			recordDate: 2015-07-18 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 40
			recordDate: 2015-07-18 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 41
			recordDate: 2015-06-08 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 42
			recordDate: 2015-06-02 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 43
			recordDate: 2015-06-02 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 44
			recordDate: 2015-03-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 45
			recordDate: 2015-03-18 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 46
			recordDate: 2015-03-18 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 47
			recordDate: 2015-03-12 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 48
			recordDate: 2015-01-14 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 49
			recordDate: 2015-01-14 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 50
			recordDate: 2014-12-22 00:00:00
			code: TI1050
			description: Transfer Inquiry to GAU
		№ 51
			recordDate: 2014-09-22 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 52
			recordDate: 2013-05-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 53
			recordDate: 2013-03-08 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 54
			recordDate: 2013-03-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 55
			recordDate: 2013-03-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 56
			recordDate: 2013-03-01 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 57
			recordDate: 2013-03-01 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 58
			recordDate: 2013-03-01 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 59
			recordDate: 2013-02-28 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 60
			recordDate: 2013-01-07 00:00:00
			code: L128
			description: Cleared by L&R (LARS)
		№ 61
			recordDate: 2012-12-07 00:00:00
			code: L198
			description: Referred to Level 2 (LARS) by OIPE CSR
		№ 62
			recordDate: 2012-12-05 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 63
			recordDate: 2012-12-05 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 64
			recordDate: 2012-12-05 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9684630
	applIdStr: 13706024
	appl_id_txt: 13706024
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13706024
	appSubCls: 021000
	appLocation: ELECTRONIC
	appAttrDockNumber: 170114-1080
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: P.O. Box 8102
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
		№ 1
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||P.O. Box 8102||Reno|NV|US|4},{|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2017-05-31T07:48:20Z
	LAST_MOD_TS: 2017-06-20T15:29:05Z
	patentIssueDate: 2017-06-20T04:00:00Z
	APP_IND: 1
	appExamName: DOAN, TRANG T
	appExamNameFacet: DOAN, TRANG T
	firstInventorFile: No
	appClsSubCls: 726/021000
	appClsSubClsFacet: 726/021000
	applId: 13706024
	patentTitle: PROVISIONING OF CRYPTOGRAPHIC MODULES
	appIntlPubNumber: 
	appConfrNumber: 4621
	appFilingDate: 2012-12-05T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Michael David Marr
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Monroe, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Nachiketh Rao Potlapally
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Arlington, 
			geoCode: VA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Matthew David Klein
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
	appCls: 726
	_version_: 1580433204307820550
	lastUpdatedTimestamp: 2017-10-05T15:49:15.764Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 580c0491-4c13-4537-b2b0-749b4885eba3
	responseHeader:
			status: 0
			QTime: 15
