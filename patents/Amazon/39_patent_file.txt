DOCUMENT_BODY:
	appGrpArtNumber: 2695
	appGrpArtNumberFacet: 2695
	transactions:
		№ 0
			recordDate: 2015-05-26 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-05-26 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-05-07 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-05-06 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-04-27 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-04-27 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-04-22 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-04-22 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-03-16 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 9
			recordDate: 2015-03-16 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 10
			recordDate: 2015-03-16 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 11
			recordDate: 2015-03-11 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 12
			recordDate: 2015-03-10 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 13
			recordDate: 2015-01-30 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 14
			recordDate: 2015-01-30 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 15
			recordDate: 2015-01-30 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 16
			recordDate: 2015-01-22 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 17
			recordDate: 2015-01-21 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 18
			recordDate: 2015-01-21 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 19
			recordDate: 2015-01-20 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 20
			recordDate: 2015-01-20 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 21
			recordDate: 2014-10-28 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 22
			recordDate: 2014-10-28 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 23
			recordDate: 2014-10-28 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 24
			recordDate: 2014-10-21 00:00:00
			code: CTFR
			description: Final Rejection
		№ 25
			recordDate: 2014-08-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 26
			recordDate: 2014-08-27 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 27
			recordDate: 2014-07-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 28
			recordDate: 2014-07-29 00:00:00
			code: MEXIA
			description: Mail Applicant Initiated Interview Summary
		№ 29
			recordDate: 2014-07-24 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 30
			recordDate: 2014-07-24 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 31
			recordDate: 2014-05-27 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 32
			recordDate: 2014-05-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 33
			recordDate: 2014-05-27 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 34
			recordDate: 2014-05-19 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 35
			recordDate: 2014-05-19 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 36
			recordDate: 2014-04-10 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 37
			recordDate: 2014-04-10 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 38
			recordDate: 2014-04-10 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 39
			recordDate: 2014-03-11 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 40
			recordDate: 2014-03-07 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 41
			recordDate: 2014-03-06 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 42
			recordDate: 2013-11-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 43
			recordDate: 2013-09-10 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 44
			recordDate: 2013-09-10 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 45
			recordDate: 2013-09-10 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 46
			recordDate: 2013-08-27 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 47
			recordDate: 2013-03-04 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 48
			recordDate: 2012-11-27 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 49
			recordDate: 2012-11-05 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 50
			recordDate: 2012-11-05 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 51
			recordDate: 2012-11-05 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 52
			recordDate: 2012-11-05 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 53
			recordDate: 2012-11-05 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 54
			recordDate: 2012-11-02 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 55
			recordDate: 2012-10-11 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 56
			recordDate: 2012-10-09 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 57
			recordDate: 2012-10-09 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 58
			recordDate: 2012-10-09 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 59
			recordDate: 2012-10-09 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9041686
	applIdStr: 13647985
	appl_id_txt: 13647985
	appEarlyPubNumber: US20140062939A1
	appEntityStatus: UNDISCOUNTED
	id: 13647985
	appSubCls: 174000
	appLocation: ELECTRONIC
	appAttrDockNumber: AM2-0892US
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 29150
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 6
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|6}
	appStatusDate: 2015-05-06T10:03:44Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-03-06T05:00:00Z
	patentIssueDate: 2015-05-26T04:00:00Z
	APP_IND: 1
	appExamName: HALEY, JOSEPH R
	appExamNameFacet: HALEY, JOSEPH R
	firstInventorFile: No
	appClsSubCls: 345/174000
	appClsSubClsFacet: 345/174000
	applId: 13647985
	patentTitle: Electronic Device Component Stack
	appIntlPubNumber: 
	appConfrNumber: 3911
	appFilingDate: 2012-10-09T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Robert Waverly Zehner
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Los Gatos, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Anoop  Menon
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Capitola, 
			geoCode: CA
			country: (US)
			rankNo: 3
		№ 2
			nameLineOne: Kari J. Rinko
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Helsinki, 
			geoCode: 
			country: (FI)
			rankNo: 5
		№ 3
			nameLineOne: Hannah Rebecca Lewbel
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Campbell, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 4
			nameLineOne: Scott M. Dylewski
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Francisco, 
			geoCode: CA
			country: (US)
			rankNo: 4
	appCls: 345
	_version_: 1580433192493514756
	lastUpdatedTimestamp: 2017-10-05T15:49:04.476Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 27a4098c-12d4-49df-b6f6-358e3142263c
	responseHeader:
			status: 0
			QTime: 15
