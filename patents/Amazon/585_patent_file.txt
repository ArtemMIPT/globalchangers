DOCUMENT_BODY:
	appGrpArtNumber: 2914
	appGrpArtNumberFacet: 2914
	transactions:
		№ 0
			recordDate: 2014-05-20 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2014-05-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 2
			recordDate: 2014-04-30 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 3
			recordDate: 2014-04-18 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 4
			recordDate: 2014-04-18 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 5
			recordDate: 2014-03-25 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 6
			recordDate: 2014-03-25 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2014-03-25 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2014-03-06 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 9
			recordDate: 2014-03-06 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 10
			recordDate: 2014-02-07 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 11
			recordDate: 2014-01-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 12
			recordDate: 2014-01-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 13
			recordDate: 2014-01-06 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 14
			recordDate: 2013-12-30 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 15
			recordDate: 2013-12-28 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 16
			recordDate: 2013-01-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 17
			recordDate: 2012-12-31 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 18
			recordDate: 2012-12-31 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 19
			recordDate: 2012-12-31 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 20
			recordDate: 2012-12-31 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 21
			recordDate: 2012-12-31 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 22
			recordDate: 2012-12-28 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 23
			recordDate: 2012-12-15 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 24
			recordDate: 2012-12-13 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 25
			recordDate: 2012-12-13 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: D705420
	applIdStr: 29439596
	appl_id_txt: 29439596
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 29439596
	appSubCls: 387000
	appLocation: ELECTRONIC
	appAttrDockNumber: 170114-1180
	appType: Design
	appTypeFacet: Design
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2014-04-30T13:21:46Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2014-05-20T04:00:00Z
	APP_IND: 1
	appExamName: MULLER, DAVID G
	appExamNameFacet: MULLER, DAVID G
	firstInventorFile: No
	appClsSubCls: D23/387000
	appClsSubClsFacet: D23/387000
	applId: 29439596
	patentTitle: AIRFLOW ADAPTER ASSEMBLY
	appIntlPubNumber: 
	appConfrNumber: 2810
	appFilingDate: 2012-12-13T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: John William Eichelberg
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Spokane, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: D23
	_version_: 1580433805852803075
	lastUpdatedTimestamp: 2017-10-05T15:58:49.424Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: dd2929a0-c346-4dea-bfb2-0f0118a13a85
	responseHeader:
			status: 0
			QTime: 16
