DOCUMENT_BODY:
	appGrpArtNumber: 2441
	appGrpArtNumberFacet: 2441
	transactions:
		№ 0
			recordDate: 2017-05-09 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2017-05-09 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2017-04-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2017-04-19 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2017-03-30 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2017-03-30 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2017-03-29 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2017-03-29 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2017-01-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2017-01-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2017-01-12 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2016-12-19 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2016-12-15 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 13
			recordDate: 2016-12-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 14
			recordDate: 2016-12-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 15
			recordDate: 2016-12-13 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 16
			recordDate: 2016-12-08 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 17
			recordDate: 2016-12-08 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 18
			recordDate: 2016-10-25 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 19
			recordDate: 2016-10-25 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 20
			recordDate: 2016-09-30 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 21
			recordDate: 2016-09-30 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 22
			recordDate: 2016-08-08 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 23
			recordDate: 2016-08-08 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 24
			recordDate: 2016-08-08 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 25
			recordDate: 2016-08-03 00:00:00
			code: CTFR
			description: Final Rejection
		№ 26
			recordDate: 2016-06-06 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 27
			recordDate: 2016-05-18 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 28
			recordDate: 2016-02-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 29
			recordDate: 2016-02-18 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 30
			recordDate: 2016-02-18 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 31
			recordDate: 2016-02-09 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 32
			recordDate: 2016-02-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 33
			recordDate: 2015-12-04 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 34
			recordDate: 2015-12-04 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 35
			recordDate: 2015-12-03 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 36
			recordDate: 2015-12-03 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 37
			recordDate: 2015-12-03 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 38
			recordDate: 2015-11-23 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 39
			recordDate: 2015-11-23 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 40
			recordDate: 2015-11-18 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 41
			recordDate: 2015-11-18 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 42
			recordDate: 2015-11-13 00:00:00
			code: AFNE
			description: After Final Consideration Program Amendment too Extensive
		№ 43
			recordDate: 2015-11-13 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 44
			recordDate: 2015-11-10 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 45
			recordDate: 2015-11-09 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 46
			recordDate: 2015-11-09 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 47
			recordDate: 2015-10-26 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 48
			recordDate: 2015-10-19 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 49
			recordDate: 2015-09-08 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 50
			recordDate: 2015-09-08 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 51
			recordDate: 2015-09-08 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 52
			recordDate: 2015-09-01 00:00:00
			code: CTFR
			description: Final Rejection
		№ 53
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 54
			recordDate: 2015-08-27 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 55
			recordDate: 2015-07-31 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 56
			recordDate: 2015-07-24 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 57
			recordDate: 2015-07-17 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 58
			recordDate: 2015-07-13 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 59
			recordDate: 2015-07-13 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 60
			recordDate: 2015-04-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 61
			recordDate: 2015-04-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 62
			recordDate: 2015-04-24 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 63
			recordDate: 2015-04-20 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 64
			recordDate: 2015-04-16 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 65
			recordDate: 2015-04-16 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 66
			recordDate: 2015-04-16 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 67
			recordDate: 2015-04-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 68
			recordDate: 2015-04-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 69
			recordDate: 2015-04-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 70
			recordDate: 2015-04-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 71
			recordDate: 2015-03-30 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 72
			recordDate: 2015-01-14 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 73
			recordDate: 2014-12-15 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 74
			recordDate: 2014-12-15 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 75
			recordDate: 2014-12-15 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 76
			recordDate: 2014-10-03 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 77
			recordDate: 2014-10-02 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 78
			recordDate: 2014-09-19 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 79
			recordDate: 2014-09-19 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 80
			recordDate: 2014-09-19 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 81
			recordDate: 2014-07-22 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 82
			recordDate: 2014-07-22 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 83
			recordDate: 2014-07-22 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 84
			recordDate: 2014-04-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 85
			recordDate: 2014-04-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 86
			recordDate: 2014-04-01 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 87
			recordDate: 2014-03-27 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 88
			recordDate: 2014-02-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 89
			recordDate: 2013-06-28 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 90
			recordDate: 2013-05-20 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 91
			recordDate: 2013-05-20 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 92
			recordDate: 2013-05-20 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 93
			recordDate: 2013-05-20 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 94
			recordDate: 2013-05-20 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 95
			recordDate: 2013-05-17 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 96
			recordDate: 2013-05-17 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 97
			recordDate: 2013-05-07 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 98
			recordDate: 2013-04-09 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 99
			recordDate: 2013-04-03 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 100
			recordDate: 2013-04-02 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 101
			recordDate: 2013-04-02 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 102
			recordDate: 2013-04-02 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9645840
	applIdStr: 13855449
	appl_id_txt: 13855449
	appEarlyPubNumber: US20140297866A1
	appEntityStatus: UNDISCOUNTED
	id: 13855449
	appSubCls: 226000
	appLocation: ELECTRONIC
	appAttrDockNumber: 101058.000056
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 23377
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 8
	applicantsFacet: {|Amazon Technologies, Inc||||Reno|NV|US|8}
	appStatusDate: 2017-04-19T08:53:01Z
	LAST_MOD_TS: 2017-05-09T13:32:36Z
	appEarlyPubDate: 2014-10-02T04:00:00Z
	patentIssueDate: 2017-05-09T04:00:00Z
	APP_IND: 1
	appExamName: MANIWANG, JOSEPH R
	appExamNameFacet: MANIWANG, JOSEPH R
	firstInventorFile: Yes
	appClsSubCls: 709/226000
	appClsSubClsFacet: 709/226000
	applId: 13855449
	patentTitle: USER-DEFINED POOLS
	appIntlPubNumber: 
	appConfrNumber: 5860
	appFilingDate: 2013-04-02T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc
	firstNamedApplicantFacet:  Amazon Technologies, Inc
	inventors:
		№ 0
			nameLineOne: Rachid  Ennaji
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Jin Seop Kim
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Brian  Helfrich
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: David John Ward JR.
			nameLineTwo:  
			suffix: JR.
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Stephen Alden Elliott
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 5
		№ 5
			nameLineOne: Peng  Zhai
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 6
		№ 6
			nameLineOne: Dhanvi Harsha Kapila
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Redmond, 
			geoCode: WA
			country: (US)
			rankNo: 7
	appCls: 709
	_version_: 1580433235161120771
	lastUpdatedTimestamp: 2017-10-05T15:49:45.153Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 8d2a2ffd-bf72-4810-a487-8f9a738c994e
	responseHeader:
			status: 0
			QTime: 17
