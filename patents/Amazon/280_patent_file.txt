DOCUMENT_BODY:
	appGrpArtNumber: 2138
	appGrpArtNumberFacet: 2138
	transactions:
		№ 0
			recordDate: 2015-02-03 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-02-03 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-01-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-01-14 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2014-12-30 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2014-12-30 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2014-12-24 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2014-12-24 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2014-10-07 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2014-10-07 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2014-10-07 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2014-10-02 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2014-09-29 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 13
			recordDate: 2014-09-25 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 14
			recordDate: 2014-09-20 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 15
			recordDate: 2014-08-14 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 16
			recordDate: 2013-06-07 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 17
			recordDate: 2013-04-12 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 18
			recordDate: 2013-03-29 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 19
			recordDate: 2013-03-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 20
			recordDate: 2013-03-29 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 21
			recordDate: 2013-03-28 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 22
			recordDate: 2013-03-05 00:00:00
			code: ADDFLFEE
			description: Additional Application Filing Fees
		№ 23
			recordDate: 2013-03-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 24
			recordDate: 2013-03-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 25
			recordDate: 2013-03-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 26
			recordDate: 2013-03-01 00:00:00
			code: INCD
			description: Notice Mailed--Application Incomplete--Filing Date Assigned 
		№ 27
			recordDate: 2013-03-01 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 28
			recordDate: 2013-02-06 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 29
			recordDate: 2013-02-04 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 30
			recordDate: 2013-02-04 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 31
			recordDate: 2013-02-04 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8949535
	applIdStr: 13758920
	appl_id_txt: 13758920
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13758920
	appSubCls: 119000
	appLocation: ELECTRONIC
	appAttrDockNumber: 3516-P7456
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 20551
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2015-01-14T10:08:14Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-02-03T05:00:00Z
	APP_IND: 1
	appExamName: ELMORE, STEPHEN C
	appExamNameFacet: ELMORE, STEPHEN C
	firstInventorFile: No
	appClsSubCls: 711/119000
	appClsSubClsFacet: 711/119000
	applId: 13758920
	patentTitle: Cache Updating
	appIntlPubNumber: 
	appConfrNumber: 2784
	appFilingDate: 2013-02-04T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Jamie  Hunter
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Kirkland, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 711
	_version_: 1580433215370297345
	lastUpdatedTimestamp: 2017-10-05T15:49:26.3Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 4619a0f3-746d-490b-bd96-48bc8c9d71bb
	responseHeader:
			status: 0
			QTime: 16
