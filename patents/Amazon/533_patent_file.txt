DOCUMENT_BODY:
	appGrpArtNumber: 2452
	appGrpArtNumberFacet: 2452
	transactions:
		№ 0
			recordDate: 2017-03-21 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2017-03-21 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 2
			recordDate: 2017-03-21 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2017-03-02 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2017-03-01 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2017-02-08 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2017-02-07 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2017-02-06 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2017-02-06 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2016-12-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2016-12-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2016-12-05 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 12
			recordDate: 2016-12-01 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 13
			recordDate: 2016-11-21 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 14
			recordDate: 2016-11-21 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 15
			recordDate: 2016-11-17 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 16
			recordDate: 2016-11-11 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 17
			recordDate: 2016-11-11 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 18
			recordDate: 2016-11-09 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 19
			recordDate: 2016-11-01 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 20
			recordDate: 2016-08-15 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 21
			recordDate: 2016-08-15 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 22
			recordDate: 2016-08-15 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 23
			recordDate: 2016-08-07 00:00:00
			code: CTFR
			description: Final Rejection
		№ 24
			recordDate: 2016-05-14 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 25
			recordDate: 2016-05-04 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 26
			recordDate: 2016-05-04 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 27
			recordDate: 2016-05-04 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 28
			recordDate: 2015-11-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 29
			recordDate: 2015-11-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 30
			recordDate: 2015-11-06 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 31
			recordDate: 2015-10-29 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 32
			recordDate: 2015-09-10 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 33
			recordDate: 2015-05-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 34
			recordDate: 2015-01-13 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 35
			recordDate: 2014-02-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 36
			recordDate: 2013-08-07 00:00:00
			code: C602
			description: Oath or Declaration Filed (Including Supplemental)
		№ 37
			recordDate: 2013-07-23 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 38
			recordDate: 2013-06-06 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 39
			recordDate: 2013-06-06 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 40
			recordDate: 2013-06-06 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 41
			recordDate: 2013-06-06 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 42
			recordDate: 2013-06-06 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 43
			recordDate: 2013-04-29 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 44
			recordDate: 2013-04-26 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 45
			recordDate: 2013-04-24 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 46
			recordDate: 2013-04-24 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 47
			recordDate: 2013-04-24 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9602381
	applIdStr: 13869437
	appl_id_txt: 13869437
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13869437
	appSubCls: 224000
	appLocation: ELECTRONIC
	appAttrDockNumber: 110.0067
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 103182
	applicants:
		№ 0
			nameLineOne: Amazon Technologies,Inc
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 7
	applicantsFacet: {|Amazon Technologies,Inc||||Reno|NV|US|7}
	appStatusDate: 2017-03-01T11:21:35Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2017-03-21T04:00:00Z
	APP_IND: 1
	appExamName: DAILEY, THOMAS J
	appExamNameFacet: DAILEY, THOMAS J
	firstInventorFile: Yes
	appClsSubCls: 709/224000
	appClsSubClsFacet: 709/224000
	applId: 13869437
	patentTitle: Real Time Adaptive Monitoring
	appIntlPubNumber: 
	appConfrNumber: 6351
	appFilingDate: 2013-04-24T04:00:00Z
	firstNamedApplicant:  Amazon Technologies,Inc
	firstNamedApplicantFacet:  Amazon Technologies,Inc
	inventors:
		№ 0
			nameLineOne: Josiah William Jordan
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Kenneth Hale Montanez
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Bhupinder Singh Sidana
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Kenmore, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Maksym  Kovalenko
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Newcastle, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Harshal Dilip Wanjari
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Issaquah, 
			geoCode: WA
			country: (US)
			rankNo: 5
		№ 5
			nameLineOne: Ajay  Mohan
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 6
	appCls: 709
	_version_: 1580433238079307779
	lastUpdatedTimestamp: 2017-10-05T15:49:47.955Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: a2cc947d-62d2-4cea-96b3-f2488704cc44
	responseHeader:
			status: 0
			QTime: 16
