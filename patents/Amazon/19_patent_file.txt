DOCUMENT_BODY:
	appGrpArtNumber: 2156
	appGrpArtNumberFacet: 2156
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9495551
	applIdStr: 13626678
	appl_id_txt: 13626678
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13626678
	appSubCls: 784000
	appLocation: ELECTRONIC
	appAttrDockNumber: AM2-0828US
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136607
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2016-10-26T13:24:40Z
	LAST_MOD_TS: 2017-10-18T15:37:12Z
	patentIssueDate: 2016-11-15T05:00:00Z
	APP_IND: 1
	appExamName: AL HASHEMI, SANA A
	appExamNameFacet: AL HASHEMI, SANA A
	firstInventorFile: No
	appClsSubCls: 707/784000
	appClsSubClsFacet: 707/784000
	applId: 13626678
	patentTitle: SHARING DIGITAL LIBRARIES
	appIntlPubNumber: 
	transactions:
		№ 0
			recordDate: 2017-09-07 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2016-11-15 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2016-11-15 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2016-10-27 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2016-10-26 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2016-10-11 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2016-10-10 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2016-10-07 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2016-10-07 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2016-07-26 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 10
			recordDate: 2016-07-26 00:00:00
			code: R48ACLT
			description: Letter Accepting Correction of Inventorship Under Rule 1.48
		№ 11
			recordDate: 2016-07-26 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 12
			recordDate: 2016-07-07 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 13
			recordDate: 2016-07-07 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 14
			recordDate: 2016-07-07 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 15
			recordDate: 2016-07-02 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 16
			recordDate: 2016-06-28 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 17
			recordDate: 2016-06-18 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 18
			recordDate: 2016-06-13 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 19
			recordDate: 2016-06-13 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 20
			recordDate: 2016-04-26 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 21
			recordDate: 2016-04-26 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 22
			recordDate: 2016-03-24 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 23
			recordDate: 2016-03-24 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 24
			recordDate: 2016-03-18 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 25
			recordDate: 2016-03-10 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 26
			recordDate: 2016-03-04 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 27
			recordDate: 2016-03-04 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 28
			recordDate: 2016-01-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 29
			recordDate: 2016-01-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 30
			recordDate: 2016-01-12 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 31
			recordDate: 2016-01-04 00:00:00
			code: CTFR
			description: Final Rejection
		№ 32
			recordDate: 2015-11-07 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 33
			recordDate: 2015-11-05 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 34
			recordDate: 2015-08-11 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 35
			recordDate: 2015-08-11 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 36
			recordDate: 2015-08-11 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 37
			recordDate: 2015-08-06 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 38
			recordDate: 2015-07-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 39
			recordDate: 2015-07-29 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 40
			recordDate: 2015-07-20 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 41
			recordDate: 2015-07-20 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 42
			recordDate: 2015-07-20 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 43
			recordDate: 2015-02-20 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 44
			recordDate: 2015-02-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 45
			recordDate: 2015-02-20 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 46
			recordDate: 2015-02-10 00:00:00
			code: CTFR
			description: Final Rejection
		№ 47
			recordDate: 2014-12-16 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 48
			recordDate: 2014-12-11 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 49
			recordDate: 2014-12-11 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 50
			recordDate: 2014-12-11 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 51
			recordDate: 2014-08-28 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 52
			recordDate: 2014-08-28 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 53
			recordDate: 2014-08-28 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 54
			recordDate: 2014-08-25 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 55
			recordDate: 2013-08-08 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 56
			recordDate: 2013-05-21 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 57
			recordDate: 2013-05-20 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 58
			recordDate: 2013-05-20 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 59
			recordDate: 2013-05-20 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 60
			recordDate: 2013-05-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 61
			recordDate: 2013-05-20 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 62
			recordDate: 2013-05-20 00:00:00
			code: MPEN
			description: Mail Pre-Exam Notice
		№ 63
			recordDate: 2013-05-06 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 64
			recordDate: 2013-05-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 65
			recordDate: 2013-05-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 66
			recordDate: 2013-05-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 67
			recordDate: 2013-05-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 68
			recordDate: 2013-05-06 00:00:00
			code: MPEN
			description: Mail Pre-Exam Notice
		№ 69
			recordDate: 2013-05-06 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 70
			recordDate: 2013-05-06 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 71
			recordDate: 2013-05-03 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 72
			recordDate: 2013-05-03 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 73
			recordDate: 2013-04-17 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 74
			recordDate: 2013-04-17 00:00:00
			code: SMAL
			description: Applicant Has Filed a Verified Statement of Small Entity Status in Compliance with 37 CFR 1.27
		№ 75
			recordDate: 2013-01-08 00:00:00
			code: C602
			description: Oath or Declaration Filed (Including Supplemental)
		№ 76
			recordDate: 2012-10-06 00:00:00
			code: L128
			description: Cleared by L&R (LARS)
		№ 77
			recordDate: 2012-09-26 00:00:00
			code: L198
			description: Referred to Level 2 (LARS) by OIPE CSR
		№ 78
			recordDate: 2012-09-25 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 79
			recordDate: 2012-09-25 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 80
			recordDate: 2012-09-25 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 81
			recordDate: 2012-09-25 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appConfrNumber: 3206
	appFilingDate: 2012-09-25T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Peter Thomas Killalea
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Joshua M. Goodspeed
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: London, 
			geoCode: 
			country: (GB)
			rankNo: 1
	appCls: 707
	_version_: 1581626311196016640
	lastUpdatedTimestamp: 2017-10-18T19:53:11.122Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 2555c4ea-fcf8-4402-9cd6-c81e59fd1ab9
	responseHeader:
			status: 0
			QTime: 13
