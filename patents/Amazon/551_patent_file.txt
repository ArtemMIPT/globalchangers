DOCUMENT_BODY:
	appGrpArtNumber: 2431
	appGrpArtNumberFacet: 2431
	transactions:
		№ 0
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 1
			recordDate: 2015-06-30 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2015-06-30 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2015-06-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2015-06-10 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2015-05-27 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2015-05-26 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2015-05-22 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2015-05-22 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2015-05-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 10
			recordDate: 2015-05-22 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 11
			recordDate: 2015-05-21 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 12
			recordDate: 2015-03-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 13
			recordDate: 2015-03-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 14
			recordDate: 2015-03-06 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 15
			recordDate: 2015-03-03 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 16
			recordDate: 2015-03-03 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 17
			recordDate: 2015-02-28 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 18
			recordDate: 2015-02-26 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 19
			recordDate: 2015-01-10 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 20
			recordDate: 2014-12-29 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 21
			recordDate: 2014-12-29 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 22
			recordDate: 2014-12-29 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 23
			recordDate: 2014-12-29 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 24
			recordDate: 2014-12-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 25
			recordDate: 2014-08-28 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 26
			recordDate: 2014-08-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 27
			recordDate: 2014-08-27 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 28
			recordDate: 2014-08-23 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 29
			recordDate: 2014-06-30 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 30
			recordDate: 2014-02-11 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 31
			recordDate: 2013-12-18 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 32
			recordDate: 2013-12-18 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 33
			recordDate: 2013-06-17 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 34
			recordDate: 2013-06-13 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 35
			recordDate: 2013-06-13 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 36
			recordDate: 2013-06-13 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 37
			recordDate: 2013-06-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 38
			recordDate: 2013-06-10 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 39
			recordDate: 2013-06-07 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 40
			recordDate: 2013-06-07 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 41
			recordDate: 2013-05-06 00:00:00
			code: L128
			description: Cleared by L&R (LARS)
		№ 42
			recordDate: 2013-05-06 00:00:00
			code: L198
			description: Referred to Level 2 (LARS) by OIPE CSR
		№ 43
			recordDate: 2013-04-30 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 44
			recordDate: 2013-04-29 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 45
			recordDate: 2013-04-29 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 46
			recordDate: 2013-04-29 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 47
			recordDate: 2013-04-29 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9071429
	applIdStr: 13873083
	appl_id_txt: 13873083
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13873083
	appSubCls: 286000
	appLocation: ELECTRONIC
	appAttrDockNumber: 020346.043801
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 131836
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2015-06-10T11:37:48Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-06-30T04:00:00Z
	APP_IND: 1
	appExamName: SAVENKOV, VADIM
	appExamNameFacet: SAVENKOV, VADIM
	firstInventorFile: Yes
	appClsSubCls: 380/286000
	appClsSubClsFacet: 380/286000
	applId: 13873083
	patentTitle: REVOCABLE SHREDDING OF SECURITY CREDENTIALS
	appIntlPubNumber: 
	appConfrNumber: 5648
	appFilingDate: 2013-04-29T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Gregory Branchek Roth
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Matthew James Wren
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Eric Jason Brandwine
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Haymarket, 
			geoCode: VA
			country: (US)
			rankNo: 3
	appCls: 380
	_version_: 1580433238795485184
	lastUpdatedTimestamp: 2017-10-05T15:49:48.633Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 735b84c0-d1c3-4f21-b74f-5f61b338de75
	responseHeader:
			status: 0
			QTime: 16
