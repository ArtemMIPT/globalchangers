DOCUMENT_BODY:
	appGrpArtNumber: 2141
	appGrpArtNumberFacet: 2141
	transactions:
		№ 0
			recordDate: 2017-02-21 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2017-02-21 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2017-02-02 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2017-02-01 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2017-01-20 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-11-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2016-11-30 00:00:00
			code: MM327-B
			description: Mail PUB Notice of non-compliant IDS
		№ 7
			recordDate: 2016-11-28 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 8
			recordDate: 2016-11-28 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 9
			recordDate: 2016-11-28 00:00:00
			code: M327-B
			description: PUB Notice of non-compliant IDS
		№ 10
			recordDate: 2016-11-18 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 11
			recordDate: 2016-11-18 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 12
			recordDate: 2016-11-18 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 13
			recordDate: 2016-11-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 14
			recordDate: 2016-11-18 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 15
			recordDate: 2016-09-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 16
			recordDate: 2016-09-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 17
			recordDate: 2016-09-02 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 18
			recordDate: 2016-08-31 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 19
			recordDate: 2016-08-30 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 20
			recordDate: 2016-08-30 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 21
			recordDate: 2016-06-08 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 22
			recordDate: 2016-05-31 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 23
			recordDate: 2016-05-31 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 24
			recordDate: 2016-05-31 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 25
			recordDate: 2016-05-31 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 26
			recordDate: 2016-05-31 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 27
			recordDate: 2016-01-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 28
			recordDate: 2016-01-29 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 29
			recordDate: 2016-01-29 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 30
			recordDate: 2016-01-22 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 31
			recordDate: 2016-01-17 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 32
			recordDate: 2015-10-07 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 33
			recordDate: 2015-10-07 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 34
			recordDate: 2015-10-02 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 35
			recordDate: 2015-10-02 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 36
			recordDate: 2015-10-02 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 37
			recordDate: 2015-10-02 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 38
			recordDate: 2015-10-02 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 39
			recordDate: 2015-08-27 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 40
			recordDate: 2015-08-21 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 41
			recordDate: 2015-07-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 42
			recordDate: 2015-07-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 43
			recordDate: 2015-07-02 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 44
			recordDate: 2015-06-26 00:00:00
			code: CTFR
			description: Final Rejection
		№ 45
			recordDate: 2015-06-26 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 46
			recordDate: 2015-05-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 47
			recordDate: 2015-05-28 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 48
			recordDate: 2015-05-27 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 49
			recordDate: 2015-05-27 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 50
			recordDate: 2015-05-27 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 51
			recordDate: 2015-05-27 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 52
			recordDate: 2015-04-07 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 53
			recordDate: 2015-04-07 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 54
			recordDate: 2015-02-27 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 55
			recordDate: 2015-02-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 56
			recordDate: 2015-02-27 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 57
			recordDate: 2015-02-20 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 58
			recordDate: 2015-02-20 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 59
			recordDate: 2014-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 60
			recordDate: 2013-11-07 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 61
			recordDate: 2013-11-07 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 62
			recordDate: 2013-11-07 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 63
			recordDate: 2013-06-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 64
			recordDate: 2013-06-24 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 65
			recordDate: 2013-06-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 66
			recordDate: 2013-05-13 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 67
			recordDate: 2013-04-08 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 68
			recordDate: 2013-02-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 69
			recordDate: 2013-02-22 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 70
			recordDate: 2013-02-22 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 71
			recordDate: 2013-02-21 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 72
			recordDate: 2013-02-01 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 73
			recordDate: 2013-01-30 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 74
			recordDate: 2013-01-29 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 75
			recordDate: 2013-01-29 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9577889
	applIdStr: 13753500
	appl_id_txt: 13753500
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13753500
	appSubCls: 738000
	appLocation: ELECTRONIC
	appAttrDockNumber: SEAZN.764A4
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 79502
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 5
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|5}
	appStatusDate: 2017-02-01T11:47:15Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2017-02-21T05:00:00Z
	APP_IND: 1
	appExamName: XIAO, DI
	appExamNameFacet: XIAO, DI
	firstInventorFile: No
	appClsSubCls: 715/738000
	appClsSubClsFacet: 715/738000
	applId: 13753500
	patentTitle: MANAGING PAGE-LEVEL USAGE DATA
	appIntlPubNumber: 
	appConfrNumber: 2201
	appFilingDate: 2013-01-29T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: AUREL  DUMITRASCU
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SUCEAVA, 
			geoCode: 
			country: (RO)
			rankNo: 1
		№ 1
			nameLineOne: ALEXANDRU  BURCIU
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: IASI, 
			geoCode: 
			country: (RO)
			rankNo: 2
		№ 2
			nameLineOne: SEBASTIAN  KOHLMEIER
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: MOUNTLAKE TERRACE, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: ALEXANDRU  TRONCIU
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: ROMAN, 
			geoCode: 
			country: (RO)
			rankNo: 4
	appCls: 715
	_version_: 1580433214083694596
	lastUpdatedTimestamp: 2017-10-05T15:49:25.049Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 043f6711-5f32-4b47-91a2-1be2571e614b
	responseHeader:
			status: 0
			QTime: 16
