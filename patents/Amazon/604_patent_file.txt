DOCUMENT_BODY:
	appGrpArtNumber: 1614
	appGrpArtNumberFacet: 1614
	transactions:
		№ 0
			recordDate: 2009-11-03 00:00:00
			code: P105
			description: Notification of Intntl. Appl. Number and Intntl. Filing Date
		№ 1
			recordDate: 2009-11-03 00:00:00
			code: P102
			description: Notification Concerning Payment of Fees
		№ 2
			recordDate: 2009-11-03 00:00:00
			code: MLIB
			description: Record Copy Mailed
		№ 3
			recordDate: 2009-10-29 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 4
			recordDate: 2009-10-26 00:00:00
			code: RCDT
			description: Receipt Date
		№ 5
			recordDate: 2009-10-26 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 6
			recordDate: 2009-10-26 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Application Undergoing Preexam Processing
	appStatus_txt: Application Undergoing Preexam Processing
	appStatusFacet: Application Undergoing Preexam Processing
	wipoEarlyPubDate: 2010-06-03T04:00:00Z
	patentNumber: 
	applIdStr: PCT/US09/62012
	appl_id_txt: PCT/US09/62012
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: PCT/US09/62012
	appSubCls: 
	wipoEarlyPubNumber: 2010062536
	appLocation: ELECTRONIC
	appAttrDockNumber: IVGN 1024 PCT
	appType: PCT
	appTypeFacet: PCT
	appCustNumber: 52059
	applicants:
		№ 0
			nameLineOne: 4854 WEST AMAZON DRIVE
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: EUGENE, 
			geoCode: OR
			country: (US)
			rankNo: 0
		№ 1
			nameLineOne: 2610 WINDSOR CIRCLE EAST
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: EUGENE, 
			geoCode: OR
			country: (US)
			rankNo: 0
		№ 2
			nameLineOne: 2940 W. 19TH AVENUE
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: EUGENE, 
			geoCode: OR
			country: (US)
			rankNo: 0
	applicantsFacet: {|4854 WEST AMAZON DRIVE||||EUGENE|OR|US|0},{|2610 WINDSOR CIRCLE EAST||||EUGENE|OR|US|0},{|2940 W. 19TH AVENUE||||EUGENE|OR|US|0}
	appStatusDate: 2009-10-26T17:32:58Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	APP_IND: 2
	firstInventorFile: Other
	appClsSubCls: 514/
	appClsSubClsFacet: 514/
	applId: PCT/US09/62012
	patentTitle: MODIFIED ACTINOMYCIN-BASED NUCLEIC ACID STAINS AND METHODS OF THEIR USE
	appIntlPubNumber: 
	appConfrNumber: 1982
	appFilingDate: 2009-10-26T04:00:00Z
	firstNamedApplicant:  4854 WEST AMAZON DRIVE
	firstNamedApplicantFacet:  4854 WEST AMAZON DRIVE
	appCls: 514
	_version_: 1580434328978980866
	lastUpdatedTimestamp: 2017-10-05T16:07:08.312Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 79caee4f-944c-430b-a48c-e91782b84924
	responseHeader:
			status: 0
			QTime: 15
