DOCUMENT_BODY:
	appGrpArtNumber: 2196
	appGrpArtNumberFacet: 2196
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9430280
	applIdStr: 13764705
	appl_id_txt: 13764705
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13764705
	appSubCls: 104000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-48500
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 6
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|6}
	appStatusDate: 2016-08-10T10:33:27Z
	LAST_MOD_TS: 2017-10-27T11:31:28Z
	patentIssueDate: 2016-08-30T04:00:00Z
	APP_IND: 1
	appExamName: LABUD, JONATHAN R
	appExamNameFacet: LABUD, JONATHAN R
	firstInventorFile: No
	appClsSubCls: 718/104000
	appClsSubClsFacet: 718/104000
	applId: 13764705
	patentTitle: TASK TIMEOUTS BASED ON INPUT DATA CHARACTERISTICS
	appIntlPubNumber: 
	transactions:
		№ 0
			recordDate: 2016-08-30 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-08-30 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-08-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-08-10 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-07-25 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-07-25 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-07-22 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2016-07-22 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2016-04-22 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2016-04-22 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2016-04-22 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2016-04-06 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2016-04-01 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 13
			recordDate: 2016-03-05 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 14
			recordDate: 2016-02-29 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 15
			recordDate: 2016-02-29 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 16
			recordDate: 2015-12-29 00:00:00
			code: PST_CRD
			description: Mail Post Card
		№ 17
			recordDate: 2015-12-21 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 18
			recordDate: 2015-12-21 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 19
			recordDate: 2015-12-14 00:00:00
			code: CTFR
			description: Final Rejection
		№ 20
			recordDate: 2015-06-18 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 21
			recordDate: 2015-06-12 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 22
			recordDate: 2015-03-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 23
			recordDate: 2015-03-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 24
			recordDate: 2015-03-12 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 25
			recordDate: 2015-02-23 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 26
			recordDate: 2015-02-23 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 27
			recordDate: 2014-07-22 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 28
			recordDate: 2013-08-20 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 29
			recordDate: 2013-04-08 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 30
			recordDate: 2013-03-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 31
			recordDate: 2013-03-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 32
			recordDate: 2013-03-06 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 33
			recordDate: 2013-03-06 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 34
			recordDate: 2013-03-06 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 35
			recordDate: 2013-03-05 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 36
			recordDate: 2013-02-13 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 37
			recordDate: 2013-02-12 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 38
			recordDate: 2013-02-12 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 39
			recordDate: 2013-02-11 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 40
			recordDate: 2013-02-11 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 41
			recordDate: 2013-02-11 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 42
			recordDate: 2013-02-11 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appConfrNumber: 3843
	appFilingDate: 2013-02-11T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: KATHRYN MARIE SHIH
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: CARL LOUIS CHRISTOFFERSON
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: RICHARD JEFFREY COLE
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: PETER  SIROTA
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: VAIBHAV  AGGARWAL
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: BELLEVUE, 
			geoCode: WA
			country: (US)
			rankNo: 5
	appCls: 718
	_version_: 1582425552250208256
	lastUpdatedTimestamp: 2017-10-27T15:36:46.8Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 4619a0f3-746d-490b-bd96-48bc8c9d71bb
	responseHeader:
			status: 0
			QTime: 16
