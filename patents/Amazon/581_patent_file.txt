DOCUMENT_BODY:
	appGrpArtNumber: 2641
	appGrpArtNumberFacet: 2641
	transactions:
		№ 0
			recordDate: 2017-09-07 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2017-07-11 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2017-07-11 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 3
			recordDate: 2017-07-11 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 4
			recordDate: 2017-06-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2017-06-21 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 6
			recordDate: 2017-06-06 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 7
			recordDate: 2017-06-06 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 8
			recordDate: 2017-06-05 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2017-06-05 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2017-05-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 11
			recordDate: 2017-05-04 00:00:00
			code: R48ACLT
			description: Letter Accepting Correction of Inventorship Under Rule 1.48
		№ 12
			recordDate: 2017-05-04 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 13
			recordDate: 2017-05-01 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 14
			recordDate: 2017-03-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 15
			recordDate: 2017-03-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 16
			recordDate: 2017-03-06 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 17
			recordDate: 2017-02-23 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 18
			recordDate: 2017-02-15 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 19
			recordDate: 2017-02-13 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 20
			recordDate: 2016-12-06 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 21
			recordDate: 2016-11-23 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 22
			recordDate: 2016-09-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 23
			recordDate: 2016-08-26 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 24
			recordDate: 2016-08-26 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 25
			recordDate: 2016-06-24 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 26
			recordDate: 2016-03-30 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 27
			recordDate: 2016-01-04 00:00:00
			code: APBR
			description: Appeal Brief Review Complete
		№ 28
			recordDate: 2015-12-22 00:00:00
			code: T1OFF
			description: track 1 OFF
		№ 29
			recordDate: 2015-12-22 00:00:00
			code: AP.B
			description: Appeal Brief Filed
		№ 30
			recordDate: 2015-10-26 00:00:00
			code: N/AP
			description: Notice of Appeal Filed
		№ 31
			recordDate: 2015-10-26 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 32
			recordDate: 2015-10-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 33
			recordDate: 2015-10-21 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 34
			recordDate: 2015-10-15 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 35
			recordDate: 2015-09-10 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 36
			recordDate: 2015-09-08 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 37
			recordDate: 2015-07-07 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 38
			recordDate: 2015-07-07 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 39
			recordDate: 2015-07-07 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 40
			recordDate: 2015-06-29 00:00:00
			code: CTFR
			description: Final Rejection
		№ 41
			recordDate: 2015-03-17 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 42
			recordDate: 2015-03-10 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 43
			recordDate: 2015-02-23 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 44
			recordDate: 2015-02-12 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 45
			recordDate: 2015-02-12 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 46
			recordDate: 2014-12-10 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 47
			recordDate: 2014-12-10 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 48
			recordDate: 2014-12-10 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 49
			recordDate: 2014-12-05 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 50
			recordDate: 2014-11-26 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 51
			recordDate: 2013-08-28 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 52
			recordDate: 2013-08-27 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 53
			recordDate: 2013-08-26 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 54
			recordDate: 2013-08-26 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 55
			recordDate: 2013-08-26 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 56
			recordDate: 2013-08-23 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 57
			recordDate: 2013-08-23 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 58
			recordDate: 2013-08-14 00:00:00
			code: A.PE
			description: Preliminary Amendment
		№ 59
			recordDate: 2013-08-14 00:00:00
			code: FLFEE
			description: Payment of additional filing fee/Preexam
		№ 60
			recordDate: 2013-07-26 00:00:00
			code: C602
			description: Oath or Declaration Filed (Including Supplemental)
		№ 61
			recordDate: 2013-07-26 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 62
			recordDate: 2013-07-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 63
			recordDate: 2013-06-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 64
			recordDate: 2013-06-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 65
			recordDate: 2013-06-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 66
			recordDate: 2013-06-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 67
			recordDate: 2013-06-14 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 68
			recordDate: 2013-06-14 00:00:00
			code: INCD
			description: Notice Mailed--Application Incomplete--Filing Date Assigned 
		№ 69
			recordDate: 2013-06-14 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 70
			recordDate: 2013-05-16 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 71
			recordDate: 2013-05-13 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 72
			recordDate: 2013-05-13 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 73
			recordDate: 2013-05-13 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 74
			recordDate: 2013-05-13 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 75
			recordDate: 2013-05-13 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9706601
	applIdStr: 13892950
	appl_id_txt: 13892950
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13892950
	appSubCls: 426100
	appLocation: ELECTRONIC
	appAttrDockNumber: AM2-0217USC1
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136607
	applicants:
		№ 0
			nameLineOne: Amazon Tecnnologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Tecnnologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2017-06-21T12:49:32Z
	LAST_MOD_TS: 2017-09-07T10:52:43Z
	patentIssueDate: 2017-07-11T04:00:00Z
	APP_IND: 1
	appExamName: ZHANG, XIANG
	appExamNameFacet: ZHANG, XIANG
	firstInventorFile: No
	appClsSubCls: 455/426100
	appClsSubClsFacet: 455/426100
	applId: 13892950
	patentTitle: MODE SWITCHING USER DEVICE
	appIntlPubNumber: 
	appConfrNumber: 3065
	appFilingDate: 2013-05-13T04:00:00Z
	firstNamedApplicant:  Amazon Tecnnologies, Inc.
	firstNamedApplicantFacet:  Amazon Tecnnologies, Inc.
	inventors:
		№ 0
			nameLineOne: Kenneth Paul Kiraly
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Menlo Park, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Subram  Narasimhan
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Saratoga, 
			geoCode: CA
			country: (US)
			rankNo: 2
	appCls: 455
	_version_: 1580433242932117507
	lastUpdatedTimestamp: 2017-10-05T15:49:52.585Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: dd2929a0-c346-4dea-bfb2-0f0118a13a85
	responseHeader:
			status: 0
			QTime: 16
