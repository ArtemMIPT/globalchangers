DOCUMENT_BODY:
	appGrpArtNumber: 2665
	appGrpArtNumberFacet: 2665
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8867817
	applIdStr: 13663329
	appl_id_txt: 13663329
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13663329
	appSubCls: 141000
	appLocation: ELECTRONIC
	appAttrDockNumber: 08862.155 (L0155)
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 14432
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2014-10-01T10:02:24Z
	LAST_MOD_TS: 2017-11-01T19:24:21Z
	patentIssueDate: 2014-10-21T04:00:00Z
	APP_IND: 1
	appExamName: SHAH, UTPAL D
	appExamNameFacet: SHAH, UTPAL D
	firstInventorFile: No
	appClsSubCls: 382/141000
	appClsSubClsFacet: 382/141000
	applId: 13663329
	patentTitle: DISPLAY ANALYSIS USING SCANNED IMAGES
	appIntlPubNumber: 
	transactions:
		№ 0
			recordDate: 2014-10-21 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2014-10-21 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2014-10-01 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 3
			recordDate: 2014-09-19 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 4
			recordDate: 2014-09-19 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 5
			recordDate: 2014-09-18 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 6
			recordDate: 2014-09-18 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 7
			recordDate: 2014-07-08 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 8
			recordDate: 2014-07-07 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 9
			recordDate: 2014-07-01 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 10
			recordDate: 2014-06-25 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 11
			recordDate: 2014-06-23 00:00:00
			code: ELC.
			description: Response to Election / Restriction Filed
		№ 12
			recordDate: 2014-05-05 00:00:00
			code: MCTRS
			description: Mail Restriction Requirement
		№ 13
			recordDate: 2014-04-24 00:00:00
			code: CTRS
			description: Restriction/Election Requirement
		№ 14
			recordDate: 2013-12-16 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 15
			recordDate: 2013-01-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 16
			recordDate: 2012-12-06 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 17
			recordDate: 2012-11-16 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 18
			recordDate: 2012-11-16 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 19
			recordDate: 2012-11-16 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 20
			recordDate: 2012-10-31 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 21
			recordDate: 2012-10-29 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 22
			recordDate: 2012-10-29 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 23
			recordDate: 2012-10-29 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appConfrNumber: 7905
	appFilingDate: 2012-10-29T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: TED JOHN COOPER
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Sunnyvale, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: OMAIR ABDUL RAHMAN
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Santa Clara, 
			geoCode: CA
			country: (US)
			rankNo: 2
	appCls: 382
	_version_: 1582909811431833602
	lastUpdatedTimestamp: 2017-11-01T23:53:52.348Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: a90b2916-4a41-40a9-9c00-a319afeb849f
	responseHeader:
			status: 0
			QTime: 15
