DOCUMENT_BODY:
	appGrpArtNumber: 2831
	appGrpArtNumberFacet: 2831
	transactions:
		№ 0
			recordDate: 2015-10-06 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-10-06 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-09-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-09-16 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-08-31 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-08-31 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-08-28 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-08-28 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-06-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2015-06-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2015-06-12 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2015-06-09 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2015-06-05 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 13
			recordDate: 2015-06-04 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 14
			recordDate: 2015-06-04 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 15
			recordDate: 2015-03-26 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 16
			recordDate: 2015-03-20 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 17
			recordDate: 2015-03-20 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 18
			recordDate: 2015-03-13 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 19
			recordDate: 2015-03-03 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 20
			recordDate: 2015-03-03 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 21
			recordDate: 2014-11-26 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 22
			recordDate: 2014-11-26 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 23
			recordDate: 2014-11-26 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 24
			recordDate: 2014-11-18 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 25
			recordDate: 2014-05-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 26
			recordDate: 2013-06-16 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 27
			recordDate: 2013-06-05 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 28
			recordDate: 2013-05-08 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 29
			recordDate: 2013-04-29 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 30
			recordDate: 2013-04-29 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 31
			recordDate: 2013-04-27 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 32
			recordDate: 2013-04-27 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 33
			recordDate: 2013-03-25 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 34
			recordDate: 2013-03-18 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 35
			recordDate: 2013-03-14 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 36
			recordDate: 2013-03-14 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9153902
	applIdStr: 13828834
	appl_id_txt: 13828834
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13828834
	appSubCls: 449000
	appLocation: ELECTRONIC
	appAttrDockNumber: 25396-0264
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 29052
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 5
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|5}
	appStatusDate: 2015-09-16T12:30:49Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-10-06T04:00:00Z
	APP_IND: 1
	appExamName: VU, HIEN D
	appExamNameFacet: VU, HIEN D
	firstInventorFile: No
	appClsSubCls: 439/449000
	appClsSubClsFacet: 439/449000
	applId: 13828834
	patentTitle: Impact Load Transfer Mount for Connectors
	appIntlPubNumber: 
	appConfrNumber: 8287
	appFilingDate: 2013-03-14T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Joshua Paul Davies
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Fremont, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Angel Wilfredo Martinez
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Cupertino, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Andrew  McIntyre
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Francisco, 
			geoCode: CA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Brandon Michael Potens
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Campbell, 
			geoCode: CA
			country: (US)
			rankNo: 4
	appCls: 439
	_version_: 1580433229541801988
	lastUpdatedTimestamp: 2017-10-05T15:49:39.81Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 15a4ef0e-add3-4358-9cd3-7d1e69d9afa9
	responseHeader:
			status: 0
			QTime: 18
