DOCUMENT_BODY:
	appGrpArtNumber: 2623
	appGrpArtNumberFacet: 2623
	transactions:
		№ 0
			recordDate: 2016-12-20 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-12-20 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-12-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-11-30 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-11-14 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-11-11 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-11-10 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2016-11-10 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2016-08-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2016-08-18 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2016-08-18 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2016-08-11 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2016-08-03 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 13
			recordDate: 2016-08-03 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 14
			recordDate: 2016-07-25 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 15
			recordDate: 2016-07-25 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 16
			recordDate: 2016-07-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 17
			recordDate: 2016-07-11 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 18
			recordDate: 2016-07-05 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 19
			recordDate: 2016-07-05 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 20
			recordDate: 2016-06-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 21
			recordDate: 2016-06-24 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 22
			recordDate: 2016-06-24 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 23
			recordDate: 2016-04-26 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 24
			recordDate: 2016-04-26 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 25
			recordDate: 2016-04-25 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 26
			recordDate: 2016-03-30 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 27
			recordDate: 2016-03-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 28
			recordDate: 2016-03-24 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 29
			recordDate: 2016-03-18 00:00:00
			code: CTFR
			description: Final Rejection
		№ 30
			recordDate: 2015-11-25 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 31
			recordDate: 2015-11-20 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 32
			recordDate: 2015-11-19 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 33
			recordDate: 2015-11-06 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 34
			recordDate: 2015-08-22 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 35
			recordDate: 2015-08-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 36
			recordDate: 2015-08-20 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 37
			recordDate: 2015-08-17 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 38
			recordDate: 2014-07-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 39
			recordDate: 2014-03-31 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 40
			recordDate: 2014-02-04 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 41
			recordDate: 2013-07-25 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 42
			recordDate: 2013-07-25 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 43
			recordDate: 2013-05-08 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 44
			recordDate: 2013-05-02 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 45
			recordDate: 2013-04-15 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 46
			recordDate: 2013-04-15 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 47
			recordDate: 2013-04-15 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 48
			recordDate: 2013-04-15 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 49
			recordDate: 2013-03-15 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 50
			recordDate: 2013-03-12 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 51
			recordDate: 2013-03-11 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 52
			recordDate: 2013-03-11 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 53
			recordDate: 2013-03-11 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9524036
	applIdStr: 13794573
	appl_id_txt: 13794573
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13794573
	appSubCls: 158000
	appLocation: ELECTRONIC
	appAttrDockNumber: 085101-526925.403NPUS00
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136608
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2016-11-30T14:33:02Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-12-20T05:00:00Z
	APP_IND: 1
	appExamName: ABDULSELAM, ABBAS I
	appExamNameFacet: ABDULSELAM, ABBAS I
	firstInventorFile: No
	appClsSubCls: 345/158000
	appClsSubClsFacet: 345/158000
	applId: 13794573
	patentTitle: MOTIONS FOR DISPLAYING ADDITIONAL CONTENT
	appIntlPubNumber: 
	appConfrNumber: 1013
	appFilingDate: 2013-03-11T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: RYAN HASTINGS CASSIDY
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Robert King Myers
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Santa Cruz, 
			geoCode: CA
			country: (US)
			rankNo: 2
	appCls: 345
	_version_: 1580433222408339459
	lastUpdatedTimestamp: 2017-10-05T15:49:33.025Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 6073073c-cbc6-49b0-b9b5-714ce578e5c8
	responseHeader:
			status: 0
			QTime: 17
