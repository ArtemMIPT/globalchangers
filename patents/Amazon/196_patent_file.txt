DOCUMENT_BODY:
	appGrpArtNumber: 2659
	appGrpArtNumberFacet: 2659
	transactions:
		№ 0
			recordDate: 2016-06-21 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-06-21 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-06-01 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 3
			recordDate: 2016-05-27 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 4
			recordDate: 2016-05-26 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 5
			recordDate: 2016-05-24 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2016-05-17 00:00:00
			code: MM327
			description: Mail Miscellaneous Communication to Applicant
		№ 7
			recordDate: 2016-05-16 00:00:00
			code: M327
			description: Miscellaneous Communication to Applicant - No Action Count
		№ 8
			recordDate: 2016-05-16 00:00:00
			code: WABN
			description: Withdraw Publication/Pre-Exam Abandon
		№ 9
			recordDate: 2016-05-12 00:00:00
			code: MM327
			description: Mail Miscellaneous Communication to Applicant
		№ 10
			recordDate: 2016-05-11 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 11
			recordDate: 2016-05-11 00:00:00
			code: M327
			description: Miscellaneous Communication to Applicant - No Action Count
		№ 12
			recordDate: 2016-05-02 00:00:00
			code: MABN7
			description: Mail Abandonment for Failure to Correct Drawings/Oath
		№ 13
			recordDate: 2016-04-18 00:00:00
			code: ABN7
			description: Abandonment for Failure to Correct Drawings/Oath/NonPub Request
		№ 14
			recordDate: 2016-04-08 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 15
			recordDate: 2016-04-07 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 16
			recordDate: 2016-04-07 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 17
			recordDate: 2016-01-15 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 18
			recordDate: 2016-01-13 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 19
			recordDate: 2015-12-15 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 20
			recordDate: 2015-12-15 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 21
			recordDate: 2015-12-14 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 22
			recordDate: 2015-12-14 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 23
			recordDate: 2015-12-10 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 24
			recordDate: 2015-12-03 00:00:00
			code: AFNE
			description: After Final Consideration Program Amendment too Extensive
		№ 25
			recordDate: 2015-12-03 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 26
			recordDate: 2015-11-19 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 27
			recordDate: 2015-11-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 28
			recordDate: 2015-11-17 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 29
			recordDate: 2015-11-17 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 30
			recordDate: 2015-08-18 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 31
			recordDate: 2015-08-13 00:00:00
			code: CTFR
			description: Final Rejection
		№ 32
			recordDate: 2015-07-06 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 33
			recordDate: 2015-06-30 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 34
			recordDate: 2015-03-30 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 35
			recordDate: 2015-03-25 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 36
			recordDate: 2014-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 37
			recordDate: 2014-08-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 38
			recordDate: 2014-07-01 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 39
			recordDate: 2013-09-23 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 40
			recordDate: 2013-08-12 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 41
			recordDate: 2013-03-11 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 42
			recordDate: 2013-01-24 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 43
			recordDate: 2013-01-24 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 44
			recordDate: 2013-01-24 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 45
			recordDate: 2012-12-26 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 46
			recordDate: 2012-12-19 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 47
			recordDate: 2012-12-19 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 48
			recordDate: 2012-12-19 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 49
			recordDate: 2012-12-19 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9372850
	applIdStr: 13720195
	appl_id_txt: 13720195
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13720195
	appSubCls: 009000
	appLocation: ELECTRONIC
	appAttrDockNumber: P6949
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136714
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 5
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|5}
	appStatusDate: 2016-06-01T10:52:01Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-06-21T04:00:00Z
	APP_IND: 1
	appExamName: GUERRA-ERAZO, EDGAR X
	appExamNameFacet: GUERRA-ERAZO, EDGAR X
	firstInventorFile: No
	appClsSubCls: 704/009000
	appClsSubClsFacet: 704/009000
	applId: 13720195
	patentTitle: MACHINED BOOK DETECTION
	appIntlPubNumber: 
	appConfrNumber: 3693
	appFilingDate: 2012-12-19T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Mitsuo  Takaki
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Divya  Mahalingam
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: David Gordon Leatham
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Kirkland, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: David Rezazadeh Azari
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 4
	appCls: 704
	_version_: 1580433207248027648
	lastUpdatedTimestamp: 2017-10-05T15:49:18.542Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 6127cd1c-1e1f-490f-8308-652c641ee1b3
	responseHeader:
			status: 0
			QTime: 15
