DOCUMENT_BODY:
	transactions:
		№ 0
			recordDate: 2014-03-02 00:00:00
			code: EXPRO
			description: EXPIRED PROVISIONAL
		№ 1
			recordDate: 2013-03-21 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 2
			recordDate: 2013-03-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2013-03-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2013-03-21 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 5
			recordDate: 2013-03-21 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 6
			recordDate: 2013-03-20 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 7
			recordDate: 2013-03-20 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 8
			recordDate: 2013-03-04 00:00:00
			code: L128
			description: Cleared by L&R (LARS)
		№ 9
			recordDate: 2013-03-01 00:00:00
			code: L198
			description: Referred to Level 2 (LARS) by OIPE CSR
		№ 10
			recordDate: 2013-02-27 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 11
			recordDate: 2013-02-27 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Provisional Application Expired
	appStatus_txt: Provisional Application Expired
	appStatusFacet: Provisional Application Expired
	patentNumber: 
	applIdStr: 61770220
	appl_id_txt: 61770220
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 61770220
	appSubCls: 
	appLocation: ELECTRONIC
	appAttrDockNumber: 579-6035
	appType: Provisional
	appTypeFacet: Provisional
	appCustNumber: 109263
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: RENO, 
			geoCode: NV
			country: (US)
			rankNo: 6
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC||||RENO|NV|US|6}
	appStatusDate: 2014-03-03T01:35:19Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	APP_IND: 1
	firstInventorFile: Other
	applId: 61770220
	patentTitle: POWER CELL EMBEDDED IN ENCLOSURE
	appIntlPubNumber: 
	appConfrNumber: 4316
	appFilingDate: 2013-02-27T05:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC
	inventors:
		№ 0
			nameLineOne: ERIK AVY VAKNINE
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SAN JOSE, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: YUTING  YEH
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SUNNYVALE, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: LIFENG  CUI
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: PALO ALTO, 
			geoCode: CA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: ROBERT NASRY HASBUN
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: FALL CITY, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: POON-KEONG  ANG
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: CUPERTINO, 
			geoCode: CA
			country: (US)
			rankNo: 5
	appCls: 
	_version_: 1580434069531918340
	lastUpdatedTimestamp: 2017-10-05T16:03:00.885Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: dd2929a0-c346-4dea-bfb2-0f0118a13a85
	responseHeader:
			status: 0
			QTime: 16
