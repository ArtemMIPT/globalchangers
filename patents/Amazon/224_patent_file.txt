DOCUMENT_BODY:
	appGrpArtNumber: 2196
	appGrpArtNumberFacet: 2196
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9448820
	applIdStr: 13733780
	appl_id_txt: 13733780
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13733780
	appSubCls: 001000
	appLocation: ELECTRONIC
	appAttrDockNumber: 101058.000034
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 23377
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2016-08-31T10:10:20Z
	LAST_MOD_TS: 2017-10-27T11:31:28Z
	patentIssueDate: 2016-09-20T04:00:00Z
	APP_IND: 1
	appExamName: LABUD, JONATHAN R
	appExamNameFacet: LABUD, JONATHAN R
	firstInventorFile: No
	appClsSubCls: 718/001000
	appClsSubClsFacet: 718/001000
	applId: 13733780
	patentTitle: CONSTRAINT VERIFICATION FOR DISTRIBUTED APPLICATIONS
	appIntlPubNumber: 
	transactions:
		№ 0
			recordDate: 2016-09-20 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-09-20 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-09-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-08-31 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-08-23 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-08-18 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2016-08-18 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 7
			recordDate: 2016-08-15 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 8
			recordDate: 2016-08-10 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 9
			recordDate: 2016-07-14 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 10
			recordDate: 2016-07-07 00:00:00
			code: FRCE
			description: Workflow - Request for RCE - Finish
		№ 11
			recordDate: 2016-07-07 00:00:00
			code: FRCE
			description: Workflow - Request for RCE - Finish
		№ 12
			recordDate: 2016-07-07 00:00:00
			code: QPREQ
			description: Quick Path IDS Request
		№ 13
			recordDate: 2016-07-07 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 14
			recordDate: 2016-07-07 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 15
			recordDate: 2016-07-07 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 16
			recordDate: 2016-07-07 00:00:00
			code: MP015
			description: Mail-Record Petition Decision of Granted to Withdraw from Issue - with assigned Patent NO.
		№ 17
			recordDate: 2016-07-07 00:00:00
			code: P015
			description: Record Petition Decision of Granted to Withdraw from Issue - with assigned Patent NO.
		№ 18
			recordDate: 2016-07-07 00:00:00
			code: WFIS
			description: Withdrawal Patent Case from Issue
		№ 19
			recordDate: 2016-07-07 00:00:00
			code: PET.
			description: Petition Entered
		№ 20
			recordDate: 2016-07-07 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 21
			recordDate: 2016-06-23 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 22
			recordDate: 2016-06-22 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 23
			recordDate: 2016-06-15 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 24
			recordDate: 2016-06-15 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 25
			recordDate: 2016-06-14 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 26
			recordDate: 2016-06-14 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 27
			recordDate: 2016-06-14 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 28
			recordDate: 2016-04-12 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 29
			recordDate: 2016-04-12 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 30
			recordDate: 2016-04-12 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 31
			recordDate: 2016-04-06 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 32
			recordDate: 2016-04-06 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 33
			recordDate: 2016-04-05 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 34
			recordDate: 2016-04-04 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 35
			recordDate: 2016-04-04 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 36
			recordDate: 2016-04-04 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 37
			recordDate: 2016-03-28 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 38
			recordDate: 2016-03-28 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 39
			recordDate: 2016-03-28 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 40
			recordDate: 2016-03-22 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 41
			recordDate: 2016-03-18 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 42
			recordDate: 2016-03-18 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 43
			recordDate: 2016-02-11 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 44
			recordDate: 2016-01-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 45
			recordDate: 2016-01-13 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 46
			recordDate: 2016-01-13 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 47
			recordDate: 2015-12-17 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 48
			recordDate: 2015-10-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 49
			recordDate: 2015-10-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 50
			recordDate: 2015-10-13 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 51
			recordDate: 2015-09-30 00:00:00
			code: CTFR
			description: Final Rejection
		№ 52
			recordDate: 2015-09-26 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 53
			recordDate: 2015-09-26 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 54
			recordDate: 2015-09-26 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 55
			recordDate: 2015-06-17 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 56
			recordDate: 2015-06-16 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 57
			recordDate: 2015-06-16 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 58
			recordDate: 2015-06-16 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 59
			recordDate: 2015-06-16 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 60
			recordDate: 2015-06-16 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 61
			recordDate: 2015-03-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 62
			recordDate: 2015-03-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 63
			recordDate: 2015-03-12 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 64
			recordDate: 2015-02-23 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 65
			recordDate: 2015-02-23 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 66
			recordDate: 2015-02-23 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 67
			recordDate: 2015-01-26 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 68
			recordDate: 2015-01-26 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 69
			recordDate: 2015-01-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 70
			recordDate: 2015-01-26 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 71
			recordDate: 2015-01-26 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 72
			recordDate: 2015-01-26 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 73
			recordDate: 2014-09-25 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 74
			recordDate: 2014-09-25 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 75
			recordDate: 2014-09-25 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 76
			recordDate: 2014-09-11 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 77
			recordDate: 2013-05-10 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 78
			recordDate: 2013-04-26 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 79
			recordDate: 2013-04-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 80
			recordDate: 2013-02-14 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 81
			recordDate: 2013-02-04 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 82
			recordDate: 2013-02-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 83
			recordDate: 2013-02-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 84
			recordDate: 2013-02-04 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 85
			recordDate: 2013-02-04 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 86
			recordDate: 2013-02-01 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 87
			recordDate: 2013-01-06 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 88
			recordDate: 2013-01-03 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 89
			recordDate: 2013-01-03 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 90
			recordDate: 2013-01-03 00:00:00
			code: IEXX
			description: Initial Exam Team nn
		№ 91
			recordDate: 2013-01-03 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
	appConfrNumber: 4565
	appFilingDate: 2013-01-03T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Nicholas Alexander Allen
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 718
	_version_: 1582425552248111106
	lastUpdatedTimestamp: 2017-10-27T15:36:46.805Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 38365122-7fb9-41d5-b2d7-e4dacdf0b560
	responseHeader:
			status: 0
			QTime: 16
