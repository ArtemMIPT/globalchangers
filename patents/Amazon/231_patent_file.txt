DOCUMENT_BODY:
	appGrpArtNumber: 3625
	appGrpArtNumberFacet: 3625
	transactions:
		№ 0
			recordDate: 2017-01-24 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2017-01-24 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2017-01-05 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2017-01-04 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-12-12 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2016-12-12 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 6
			recordDate: 2016-12-12 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 7
			recordDate: 2016-12-09 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 8
			recordDate: 2016-12-06 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 9
			recordDate: 2016-12-05 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 10
			recordDate: 2016-12-03 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 11
			recordDate: 2016-11-30 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 12
			recordDate: 2016-11-30 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 13
			recordDate: 2016-11-30 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 14
			recordDate: 2016-10-07 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 15
			recordDate: 2016-10-07 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 16
			recordDate: 2016-10-07 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 17
			recordDate: 2016-10-05 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 18
			recordDate: 2016-10-04 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 19
			recordDate: 2016-08-12 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 20
			recordDate: 2016-08-08 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 21
			recordDate: 2016-08-08 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 22
			recordDate: 2016-06-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 23
			recordDate: 2016-06-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 24
			recordDate: 2016-06-02 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 25
			recordDate: 2016-05-30 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 26
			recordDate: 2016-01-31 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 27
			recordDate: 2016-01-31 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 28
			recordDate: 2016-01-19 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 29
			recordDate: 2016-01-19 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 30
			recordDate: 2015-10-21 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 31
			recordDate: 2015-10-21 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 32
			recordDate: 2015-10-21 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 33
			recordDate: 2015-10-16 00:00:00
			code: CTFR
			description: Final Rejection
		№ 34
			recordDate: 2015-07-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 35
			recordDate: 2015-07-21 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 36
			recordDate: 2015-07-21 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 37
			recordDate: 2015-07-07 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 38
			recordDate: 2015-06-30 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 39
			recordDate: 2015-06-30 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 40
			recordDate: 2015-04-03 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 41
			recordDate: 2015-04-03 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 42
			recordDate: 2015-04-03 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 43
			recordDate: 2015-03-30 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 44
			recordDate: 2015-03-28 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 45
			recordDate: 2014-02-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 46
			recordDate: 2014-02-10 00:00:00
			code: FLRCPT.C
			description: Filing Receipt - Corrected
		№ 47
			recordDate: 2013-02-16 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 48
			recordDate: 2013-02-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 49
			recordDate: 2013-02-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 50
			recordDate: 2013-02-11 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 51
			recordDate: 2013-02-11 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 52
			recordDate: 2013-02-11 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 53
			recordDate: 2013-02-09 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 54
			recordDate: 2013-01-16 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 55
			recordDate: 2013-01-11 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 56
			recordDate: 2013-01-11 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 57
			recordDate: 2013-01-11 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 58
			recordDate: 2013-01-11 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 59
			recordDate: 2013-01-11 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9552600
	applIdStr: 13739427
	appl_id_txt: 13739427
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13739427
	appSubCls: 026100
	appLocation: ELECTRONIC
	appAttrDockNumber: 170103-1901
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2017-01-04T12:13:46Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2017-01-24T05:00:00Z
	APP_IND: 1
	appExamName: DESAI, RESHA
	appExamNameFacet: DESAI, RESHA
	firstInventorFile: No
	appClsSubCls: 705/026100
	appClsSubClsFacet: 705/026100
	applId: 13739427
	patentTitle: GENERATING AND UPDATING RECOMMENDATIONS FOR MERCHANTS
	appIntlPubNumber: 
	appConfrNumber: 8547
	appFilingDate: 2013-01-11T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Anton V. Goldberg
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Robert H. Mahfoud
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Mercer Island, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 2
			nameLineOne: Trevor Alexander Popiel
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Lynnwood, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 705
	_version_: 1580433211400388611
	lastUpdatedTimestamp: 2017-10-05T15:49:22.52Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 38365122-7fb9-41d5-b2d7-e4dacdf0b560
	responseHeader:
			status: 0
			QTime: 16
