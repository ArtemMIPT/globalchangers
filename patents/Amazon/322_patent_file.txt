DOCUMENT_BODY:
	appGrpArtNumber: 2451
	appGrpArtNumberFacet: 2451
	transactions:
		№ 0
			recordDate: 2015-03-25 00:00:00
			code: N423
			description: Post Issue Communication - Certificate of Correction
		№ 1
			recordDate: 2014-08-12 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2014-08-12 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2014-07-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2014-07-23 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2014-07-10 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2014-07-03 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2014-06-27 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 8
			recordDate: 2014-06-27 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2014-06-27 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2014-04-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 11
			recordDate: 2014-04-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 12
			recordDate: 2014-04-01 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 13
			recordDate: 2014-03-27 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 14
			recordDate: 2014-03-19 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 15
			recordDate: 2014-03-08 00:00:00
			code: P575
			description: Paralegal TD Not accepted
		№ 16
			recordDate: 2014-03-07 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 17
			recordDate: 2014-03-05 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 18
			recordDate: 2014-03-05 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 19
			recordDate: 2014-03-05 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 20
			recordDate: 2013-11-08 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 21
			recordDate: 2013-11-08 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 22
			recordDate: 2013-11-08 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 23
			recordDate: 2013-11-06 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 24
			recordDate: 2013-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 25
			recordDate: 2013-05-23 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 26
			recordDate: 2013-05-23 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 27
			recordDate: 2013-04-19 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 28
			recordDate: 2013-04-19 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 29
			recordDate: 2013-04-19 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 30
			recordDate: 2013-04-19 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 31
			recordDate: 2013-03-25 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 32
			recordDate: 2013-03-19 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 33
			recordDate: 2013-03-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 34
			recordDate: 2013-03-15 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 35
			recordDate: 2013-03-15 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 36
			recordDate: 2013-03-14 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 37
			recordDate: 2013-02-21 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 38
			recordDate: 2013-02-20 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 39
			recordDate: 2013-02-19 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 40
			recordDate: 2013-02-19 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8805991
	applIdStr: 13770818
	appl_id_txt: 13770818
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13770818
	appSubCls: 223000
	appLocation: ELECTRONIC
	appAttrDockNumber: SEAZN.335C3
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 79502
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2014-07-23T10:55:12Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2014-08-12T04:00:00Z
	APP_IND: 1
	appExamName: MAUNG, ZARNI
	appExamNameFacet: MAUNG, ZARNI
	firstInventorFile: No
	appClsSubCls: 709/223000
	appClsSubClsFacet: 709/223000
	applId: 13770818
	patentTitle: SYSTEM AND METHOD FOR ROUTING SERVICE REQUESTS
	appIntlPubNumber: 
	appConfrNumber: 4633
	appFilingDate: 2013-02-19T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Swaminathan  Sivasubramanian
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Dan Mihai Dumitriu
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 709
	_version_: 1580433217783070720
	lastUpdatedTimestamp: 2017-10-05T15:49:28.591Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 0a00969b-d87b-44b3-8fd5-1d76e5a8df75
	responseHeader:
			status: 0
			QTime: 18
