DOCUMENT_BODY:
	appGrpArtNumber: 3685
	appGrpArtNumberFacet: 3685
	appStatus: Non Final Action Mailed
	appStatus_txt: Non Final Action Mailed
	appStatusFacet: Non Final Action Mailed
	patentNumber: 
	applIdStr: 13794600
	appl_id_txt: 13794600
	appEarlyPubNumber: US20140258155A1
	appEntityStatus: UNDISCOUNTED
	id: 13794600
	appSubCls: 059000
	appLocation: ELECTRONIC
	appAttrDockNumber: ZNET.775A
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 20995
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 6
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|6}
	appStatusDate: 2017-08-17T17:31:15Z
	LAST_MOD_TS: 2017-11-13T11:07:38Z
	appEarlyPubDate: 2014-09-11T04:00:00Z
	APP_IND: 1
	appExamName: CHOO, JOHANN Y
	appExamNameFacet: CHOO, JOHANN Y
	firstInventorFile: No
	appClsSubCls: 705/059000
	appClsSubClsFacet: 705/059000
	applId: 13794600
	patentTitle: APPLICATION MARKETPLACE FOR VIRTUAL DESKTOPS
	appIntlPubNumber: 
	transactions:
		№ 0
			recordDate: 2017-08-21 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 1
			recordDate: 2017-08-21 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 2
			recordDate: 2017-08-21 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 3
			recordDate: 2017-08-09 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 4
			recordDate: 2017-08-09 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 5
			recordDate: 2017-08-09 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 6
			recordDate: 2017-05-19 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 7
			recordDate: 2017-05-19 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 8
			recordDate: 2017-02-10 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 9
			recordDate: 2017-02-10 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 10
			recordDate: 2017-02-09 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 11
			recordDate: 2017-02-09 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 12
			recordDate: 2017-02-07 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 13
			recordDate: 2017-02-07 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 14
			recordDate: 2017-02-07 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 15
			recordDate: 2017-02-07 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 16
			recordDate: 2017-02-07 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 17
			recordDate: 2017-02-01 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 18
			recordDate: 2016-09-28 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 19
			recordDate: 2016-09-28 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 20
			recordDate: 2016-09-28 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 21
			recordDate: 2016-09-12 00:00:00
			code: CTFR
			description: Final Rejection
		№ 22
			recordDate: 2016-09-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 23
			recordDate: 2016-09-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 24
			recordDate: 2016-09-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 25
			recordDate: 2016-08-29 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 26
			recordDate: 2016-08-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 27
			recordDate: 2016-07-26 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 28
			recordDate: 2016-07-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 29
			recordDate: 2016-07-02 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 30
			recordDate: 2016-06-29 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 31
			recordDate: 2016-06-29 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 32
			recordDate: 2016-06-02 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 33
			recordDate: 2016-05-24 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 34
			recordDate: 2016-04-08 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 35
			recordDate: 2016-02-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 36
			recordDate: 2016-02-29 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 37
			recordDate: 2016-02-29 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 38
			recordDate: 2016-02-19 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 39
			recordDate: 2016-02-18 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 40
			recordDate: 2016-02-18 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 41
			recordDate: 2016-02-18 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 42
			recordDate: 2015-07-28 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 43
			recordDate: 2015-01-07 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 44
			recordDate: 2015-01-07 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 45
			recordDate: 2015-01-07 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 46
			recordDate: 2014-09-12 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 47
			recordDate: 2014-09-11 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 48
			recordDate: 2014-08-15 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 49
			recordDate: 2014-08-15 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 50
			recordDate: 2014-08-15 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 51
			recordDate: 2014-03-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 52
			recordDate: 2014-03-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 53
			recordDate: 2014-03-06 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 54
			recordDate: 2014-02-28 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 55
			recordDate: 2014-02-28 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 56
			recordDate: 2014-02-27 00:00:00
			code: PG-PB-DT
			description: PG-Pub Notice of new or Revised projected publication date
		№ 57
			recordDate: 2014-02-18 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 58
			recordDate: 2013-10-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 59
			recordDate: 2013-10-09 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 60
			recordDate: 2013-07-23 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 61
			recordDate: 2013-07-23 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 62
			recordDate: 2013-07-13 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 63
			recordDate: 2013-07-13 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 64
			recordDate: 2013-07-13 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 65
			recordDate: 2013-06-11 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 66
			recordDate: 2013-04-26 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 67
			recordDate: 2013-04-26 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 68
			recordDate: 2013-04-26 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 69
			recordDate: 2013-04-25 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 70
			recordDate: 2013-04-25 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 71
			recordDate: 2013-03-25 00:00:00
			code: L128
			description: Cleared by L&R (LARS)
		№ 72
			recordDate: 2013-03-15 00:00:00
			code: L198
			description: Referred to Level 2 (LARS) by OIPE CSR
		№ 73
			recordDate: 2013-03-12 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 74
			recordDate: 2013-03-11 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 75
			recordDate: 2013-03-11 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appConfrNumber: 1039
	appFilingDate: 2013-03-11T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Deepak  Suryanarayanan
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Eugene Michael Farrell
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Sammamish, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: David Everard Brown
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Cape Town, Western Cape, 
			geoCode: 
			country: (ZA)
			rankNo: 3
		№ 3
			nameLineOne: Stephen William Luszcz
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Ajit Nagendra Padukone
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 5
	appCls: 705
	_version_: 1583968034608906243
	lastUpdatedTimestamp: 2017-11-13T16:13:52.624Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 6073073c-cbc6-49b0-b9b5-714ce578e5c8
	responseHeader:
			status: 0
			QTime: 17
