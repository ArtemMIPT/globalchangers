DOCUMENT_BODY:
	appGrpArtNumber: 2833
	appGrpArtNumberFacet: 2833
	transactions:
		№ 0
			recordDate: 2015-07-21 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-07-21 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-07-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-06-30 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-06-12 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2015-06-12 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 6
			recordDate: 2015-06-12 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 7
			recordDate: 2015-06-11 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 8
			recordDate: 2015-06-09 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 9
			recordDate: 2015-06-09 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 10
			recordDate: 2015-06-04 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 11
			recordDate: 2015-06-03 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 12
			recordDate: 2015-06-03 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 13
			recordDate: 2015-06-03 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 14
			recordDate: 2015-03-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 15
			recordDate: 2015-03-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 16
			recordDate: 2015-03-17 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 17
			recordDate: 2015-03-11 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 18
			recordDate: 2015-02-02 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 19
			recordDate: 2015-01-27 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 20
			recordDate: 2014-12-24 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 21
			recordDate: 2014-12-19 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 22
			recordDate: 2014-12-19 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 23
			recordDate: 2014-11-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 24
			recordDate: 2014-11-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 25
			recordDate: 2014-11-06 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 26
			recordDate: 2014-10-30 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 27
			recordDate: 2014-04-10 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 28
			recordDate: 2013-12-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 29
			recordDate: 2013-11-19 00:00:00
			code: TI1050
			description: Transfer Inquiry to GAU
		№ 30
			recordDate: 2013-11-02 00:00:00
			code: TI1050
			description: Transfer Inquiry to GAU
		№ 31
			recordDate: 2013-05-08 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 32
			recordDate: 2013-05-07 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 33
			recordDate: 2013-05-06 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 34
			recordDate: 2013-04-16 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 35
			recordDate: 2013-04-16 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 36
			recordDate: 2013-04-15 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 37
			recordDate: 2013-04-15 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 38
			recordDate: 2013-03-14 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 39
			recordDate: 2013-03-11 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 40
			recordDate: 2013-03-11 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 41
			recordDate: 2013-03-11 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9088112
	applIdStr: 13793057
	appl_id_txt: 13793057
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13793057
	appSubCls: 079000
	appLocation: ELECTRONIC
	appAttrDockNumber: 25396-0268
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 29052
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 6
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|6}
	appStatusDate: 2015-06-30T09:58:07Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-07-21T04:00:00Z
	APP_IND: 1
	appExamName: NGUYEN, TRUC T
	appExamNameFacet: NGUYEN, TRUC T
	firstInventorFile: No
	appClsSubCls: 439/079000
	appClsSubClsFacet: 439/079000
	applId: 13793057
	patentTitle: COUPLING ELECTRONIC RECEPTACLES TO DEVICES
	appIntlPubNumber: 
	appConfrNumber: 6027
	appFilingDate: 2013-03-11T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Joshua Paul Davies
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Fremont, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Brandon Michael Potens
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Campbell, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Andrew  McIntyre
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Francisco, 
			geoCode: CA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Angel Wilfredo Martinez
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Cupertino, 
			geoCode: CA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Kelly Erin Johnson
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Menlo Park, 
			geoCode: CA
			country: (US)
			rankNo: 5
	appCls: 439
	_version_: 1580433222156681217
	lastUpdatedTimestamp: 2017-10-05T15:49:32.774Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: d2fc8545-85f2-44cb-95ac-21a830edee81
	responseHeader:
			status: 0
			QTime: 17
