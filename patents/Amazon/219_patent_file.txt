DOCUMENT_BODY:
	appGrpArtNumber: 2457
	appGrpArtNumberFacet: 2457
	transactions:
		№ 0
			recordDate: 2016-03-29 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-03-29 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-03-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-03-09 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-02-23 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-02-23 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-02-18 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2016-02-18 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-12-03 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2015-12-03 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2015-12-03 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2015-12-01 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2015-09-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 13
			recordDate: 2015-09-17 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 14
			recordDate: 2015-09-01 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 15
			recordDate: 2015-08-25 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 16
			recordDate: 2015-06-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 17
			recordDate: 2015-06-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 18
			recordDate: 2015-06-17 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 19
			recordDate: 2015-06-13 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 20
			recordDate: 2015-04-17 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 21
			recordDate: 2015-04-15 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 22
			recordDate: 2015-03-11 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 23
			recordDate: 2015-03-03 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 24
			recordDate: 2015-03-03 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 25
			recordDate: 2015-01-15 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 26
			recordDate: 2015-01-15 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 27
			recordDate: 2015-01-15 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 28
			recordDate: 2015-01-10 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 29
			recordDate: 2014-08-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 30
			recordDate: 2014-08-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 31
			recordDate: 2013-08-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 32
			recordDate: 2013-03-27 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 33
			recordDate: 2013-02-25 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 34
			recordDate: 2013-02-25 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 35
			recordDate: 2013-02-25 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 36
			recordDate: 2013-02-25 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 37
			recordDate: 2013-02-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 38
			recordDate: 2013-02-01 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 39
			recordDate: 2013-02-01 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 40
			recordDate: 2013-01-31 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 41
			recordDate: 2013-01-05 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 42
			recordDate: 2013-01-02 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 43
			recordDate: 2013-01-02 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 44
			recordDate: 2013-01-02 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 45
			recordDate: 2013-01-02 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9300625
	applIdStr: 13733019
	appl_id_txt: 13733019
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13733019
	appSubCls: 217000
	appLocation: ELECTRONIC
	appAttrDockNumber: 090204-0853913 (060200US)
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 107508
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2016-03-09T12:27:09Z
	LAST_MOD_TS: 2017-08-30T14:31:45Z
	patentIssueDate: 2016-03-29T04:00:00Z
	APP_IND: 1
	appExamName: JACOBS, LASHONDA T
	appExamNameFacet: JACOBS, LASHONDA T
	firstInventorFile: No
	appClsSubCls: 709/217000
	appClsSubClsFacet: 709/217000
	applId: 13733019
	patentTitle: NETWORK ADDRESS VERIFICATION
	appIntlPubNumber: 
	appConfrNumber: 1062
	appFilingDate: 2013-01-02T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Nachiketh Rao Potlapally
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Arlington, 
			geoCode: VA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Eric Jason Brandwine
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Haymarket, 
			geoCode: VA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Patrick Brigham Cullen
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Herndon, 
			geoCode: VA
			country: (US)
			rankNo: 3
	appCls: 709
	_version_: 1580433209718472706
	lastUpdatedTimestamp: 2017-10-05T15:49:20.911Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 08b71c7b-e732-4071-856a-34b5a913df41
	responseHeader:
			status: 0
			QTime: 17
