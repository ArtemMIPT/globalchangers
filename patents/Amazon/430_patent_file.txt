DOCUMENT_BODY:
	appGrpArtNumber: 2456
	appGrpArtNumberFacet: 2456
	transactions:
		№ 0
			recordDate: 2017-07-31 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 1
			recordDate: 2017-06-26 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 2
			recordDate: 2017-06-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 3
			recordDate: 2017-05-22 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 4
			recordDate: 2017-05-22 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 5
			recordDate: 2017-05-22 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 6
			recordDate: 2017-05-11 00:00:00
			code: CTFR
			description: Final Rejection
		№ 7
			recordDate: 2017-05-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 8
			recordDate: 2017-05-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 9
			recordDate: 2017-03-03 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 10
			recordDate: 2017-03-02 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 11
			recordDate: 2017-03-02 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 12
			recordDate: 2017-01-18 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 13
			recordDate: 2017-01-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 14
			recordDate: 2016-12-27 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 15
			recordDate: 2016-12-27 00:00:00
			code: MEXET
			description: Mail Interview Summary - Examiner Initiated - Telephonic
		№ 16
			recordDate: 2016-12-21 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 17
			recordDate: 2016-12-08 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 18
			recordDate: 2016-12-08 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 19
			recordDate: 2016-11-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 20
			recordDate: 2016-11-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 21
			recordDate: 2016-11-01 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 22
			recordDate: 2016-10-26 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 23
			recordDate: 2016-10-24 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 24
			recordDate: 2016-10-24 00:00:00
			code: MAPCR
			description: Mail Appeals conf. Reopen Prosec.
		№ 25
			recordDate: 2016-10-18 00:00:00
			code: APCR
			description: Pre-Appeals Conference Decision - Reopen Prosecution
		№ 26
			recordDate: 2016-03-16 00:00:00
			code: AP.C
			description: Request for Pre-Appeal Conference Filed
		№ 27
			recordDate: 2016-03-16 00:00:00
			code: N/AP
			description: Notice of Appeal Filed
		№ 28
			recordDate: 2016-01-15 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 29
			recordDate: 2016-01-15 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 30
			recordDate: 2016-01-15 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 31
			recordDate: 2016-01-08 00:00:00
			code: CTFR
			description: Final Rejection
		№ 32
			recordDate: 2015-10-26 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 33
			recordDate: 2015-10-21 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 34
			recordDate: 2015-10-21 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 35
			recordDate: 2015-05-22 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 36
			recordDate: 2015-05-22 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 37
			recordDate: 2015-05-22 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 38
			recordDate: 2015-05-19 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 39
			recordDate: 2015-04-27 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 40
			recordDate: 2015-04-24 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 41
			recordDate: 2015-04-24 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 42
			recordDate: 2015-03-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 43
			recordDate: 2015-03-10 00:00:00
			code: MEXIA
			description: Mail Applicant Initiated Interview Summary
		№ 44
			recordDate: 2015-02-27 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 45
			recordDate: 2015-02-27 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 46
			recordDate: 2015-01-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 47
			recordDate: 2015-01-09 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 48
			recordDate: 2015-01-09 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 49
			recordDate: 2015-01-06 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 50
			recordDate: 2015-01-03 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 51
			recordDate: 2014-11-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 52
			recordDate: 2014-09-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 53
			recordDate: 2014-09-19 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 54
			recordDate: 2014-09-05 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 55
			recordDate: 2014-09-05 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 56
			recordDate: 2014-03-20 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 57
			recordDate: 2014-03-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 58
			recordDate: 2014-03-20 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 59
			recordDate: 2014-03-12 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 60
			recordDate: 2014-02-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 61
			recordDate: 2013-06-11 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 62
			recordDate: 2013-04-26 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 63
			recordDate: 2013-04-26 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 64
			recordDate: 2013-04-26 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 65
			recordDate: 2013-04-26 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 66
			recordDate: 2013-04-26 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 67
			recordDate: 2013-04-25 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 68
			recordDate: 2013-04-25 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 69
			recordDate: 2013-03-24 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 70
			recordDate: 2013-03-17 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 71
			recordDate: 2013-03-14 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 72
			recordDate: 2013-03-14 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Final Rejection Mailed
	appStatus_txt: Final Rejection Mailed
	appStatusFacet: Final Rejection Mailed
	patentNumber: 
	applIdStr: 13827693
	appl_id_txt: 13827693
	appEarlyPubNumber: US20140280872A1
	appEntityStatus: UNDISCOUNTED
	id: 13827693
	appSubCls: 224000
	appLocation: ELECTRONIC
	appAttrDockNumber: 170114-1690
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|2}
	appStatusDate: 2017-05-18T13:20:05Z
	LAST_MOD_TS: 2017-08-08T08:31:15Z
	appEarlyPubDate: 2014-09-18T04:00:00Z
	APP_IND: 1
	appExamName: CHEN, WUJI
	appExamNameFacet: CHEN, WUJI
	firstInventorFile: No
	appClsSubCls: 709/224000
	appClsSubClsFacet: 709/224000
	applId: 13827693
	patentTitle: INVENTORY SERVICE FOR DISTRIBUTED INFRASTRUCTURE
	appIntlPubNumber: 
	appConfrNumber: 2464
	appFilingDate: 2013-03-14T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Thomas Charles Stickle
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Saint James, 
			geoCode: NY
			country: (US)
			rankNo: 1
	appCls: 709
	_version_: 1580433229319503874
	lastUpdatedTimestamp: 2017-10-05T15:49:39.61Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 15a4ef0e-add3-4358-9cd3-7d1e69d9afa9
	responseHeader:
			status: 0
			QTime: 18
