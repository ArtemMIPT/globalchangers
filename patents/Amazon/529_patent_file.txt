DOCUMENT_BODY:
	appGrpArtNumber: 2641
	appGrpArtNumberFacet: 2641
	transactions:
		№ 0
			recordDate: 2015-09-22 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-09-22 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 2
			recordDate: 2015-09-22 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2015-09-03 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2015-09-02 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2015-08-25 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2015-08-25 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 7
			recordDate: 2015-08-25 00:00:00
			code: MM327
			description: Mail Miscellaneous Communication to Applicant
		№ 8
			recordDate: 2015-08-24 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 9
			recordDate: 2015-08-17 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 10
			recordDate: 2015-08-17 00:00:00
			code: M327
			description: Miscellaneous Communication to Applicant - No Action Count
		№ 11
			recordDate: 2015-07-27 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 12
			recordDate: 2015-06-22 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 13
			recordDate: 2015-06-19 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 14
			recordDate: 2015-06-17 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 15
			recordDate: 2015-06-16 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 16
			recordDate: 2015-06-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 17
			recordDate: 2015-06-15 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 18
			recordDate: 2015-06-15 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 19
			recordDate: 2015-06-15 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 20
			recordDate: 2015-06-15 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 21
			recordDate: 2015-05-28 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 22
			recordDate: 2015-05-28 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 23
			recordDate: 2015-05-12 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 24
			recordDate: 2015-05-12 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 25
			recordDate: 2015-05-12 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 26
			recordDate: 2015-05-07 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 27
			recordDate: 2015-05-07 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 28
			recordDate: 2015-05-07 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 29
			recordDate: 2015-04-23 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 30
			recordDate: 2015-04-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 31
			recordDate: 2015-04-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 32
			recordDate: 2015-04-01 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 33
			recordDate: 2015-03-28 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 34
			recordDate: 2015-03-10 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 35
			recordDate: 2015-03-10 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 36
			recordDate: 2015-03-10 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 37
			recordDate: 2015-03-10 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 38
			recordDate: 2015-03-10 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 39
			recordDate: 2015-03-10 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 40
			recordDate: 2014-12-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 41
			recordDate: 2014-12-12 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 42
			recordDate: 2014-12-12 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 43
			recordDate: 2014-12-12 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 44
			recordDate: 2014-12-12 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 45
			recordDate: 2014-09-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 46
			recordDate: 2014-09-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 47
			recordDate: 2014-09-12 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 48
			recordDate: 2014-09-08 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 49
			recordDate: 2014-06-05 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 50
			recordDate: 2013-06-27 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 51
			recordDate: 2013-06-17 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 52
			recordDate: 2013-06-04 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 53
			recordDate: 2013-06-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 54
			recordDate: 2013-06-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 55
			recordDate: 2013-06-04 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 56
			recordDate: 2013-06-04 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 57
			recordDate: 2013-06-03 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 58
			recordDate: 2013-06-03 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 59
			recordDate: 2013-04-25 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 60
			recordDate: 2013-04-22 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 61
			recordDate: 2013-04-22 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 62
			recordDate: 2013-04-22 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 63
			recordDate: 2013-04-22 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 64
			recordDate: 2013-04-22 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 65
			recordDate: 2013-04-22 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 66
			recordDate: 2013-04-22 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9143898
	applIdStr: 13867462
	appl_id_txt: 13867462
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13867462
	appSubCls: 456300
	appLocation: ELECTRONIC
	appAttrDockNumber: 170115-1040
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2015-09-02T15:41:58Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-09-22T04:00:00Z
	APP_IND: 1
	appExamName: SHAHEED, KHALID W
	appExamNameFacet: SHAHEED, KHALID W
	firstInventorFile: Yes
	appClsSubCls: 455/456300
	appClsSubClsFacet: 455/456300
	applId: 13867462
	patentTitle: AUTOMATICALLY SELECTING ALERT MODES BASED ON LOCATION
	appIntlPubNumber: 
	appConfrNumber: 4250
	appFilingDate: 2013-04-22T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: JEFFREY SCOTT BARR
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Sammamish, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 455
	_version_: 1580433237700771842
	lastUpdatedTimestamp: 2017-10-05T15:49:47.592Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: a2cc947d-62d2-4cea-96b3-f2488704cc44
	responseHeader:
			status: 0
			QTime: 16
