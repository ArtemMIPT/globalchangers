DOCUMENT_BODY:
	appGrpArtNumber: 2159
	appGrpArtNumberFacet: 2159
	transactions:
		№ 0
			recordDate: 2016-10-04 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-10-04 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 2
			recordDate: 2016-10-04 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2016-09-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2016-09-14 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2016-09-02 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2016-09-02 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 7
			recordDate: 2016-09-02 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 8
			recordDate: 2016-09-01 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 9
			recordDate: 2016-08-31 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 10
			recordDate: 2016-08-31 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 11
			recordDate: 2016-08-30 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 12
			recordDate: 2016-08-29 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 13
			recordDate: 2016-08-29 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 14
			recordDate: 2016-08-29 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 15
			recordDate: 2016-08-29 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 16
			recordDate: 2016-06-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 17
			recordDate: 2016-06-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 18
			recordDate: 2016-06-14 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 19
			recordDate: 2016-06-10 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 20
			recordDate: 2016-06-06 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 21
			recordDate: 2016-06-03 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 22
			recordDate: 2016-05-27 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 23
			recordDate: 2016-04-07 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 24
			recordDate: 2016-04-07 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 25
			recordDate: 2016-04-07 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 26
			recordDate: 2016-04-02 00:00:00
			code: CTFR
			description: Final Rejection
		№ 27
			recordDate: 2016-02-13 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 28
			recordDate: 2016-01-27 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 29
			recordDate: 2015-12-14 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 30
			recordDate: 2015-12-07 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 31
			recordDate: 2015-11-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 32
			recordDate: 2015-11-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 33
			recordDate: 2015-11-02 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 34
			recordDate: 2015-10-27 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 35
			recordDate: 2015-08-10 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 36
			recordDate: 2015-08-05 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 37
			recordDate: 2015-08-05 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 38
			recordDate: 2015-08-05 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 39
			recordDate: 2015-07-30 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 40
			recordDate: 2015-07-30 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 41
			recordDate: 2015-03-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 42
			recordDate: 2015-03-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 43
			recordDate: 2015-03-24 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 44
			recordDate: 2015-03-19 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 45
			recordDate: 2014-11-05 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 46
			recordDate: 2013-06-05 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 47
			recordDate: 2013-06-04 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 48
			recordDate: 2013-05-20 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 49
			recordDate: 2013-05-20 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 50
			recordDate: 2013-05-20 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 51
			recordDate: 2013-05-17 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 52
			recordDate: 2013-05-13 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 53
			recordDate: 2013-05-13 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 54
			recordDate: 2013-05-13 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 55
			recordDate: 2013-05-13 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 56
			recordDate: 2013-05-11 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 57
			recordDate: 2013-04-02 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 58
			recordDate: 2013-03-29 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 59
			recordDate: 2013-03-28 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 60
			recordDate: 2013-03-28 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 61
			recordDate: 2013-03-28 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9460190
	applIdStr: 13852154
	appl_id_txt: 13852154
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13852154
	appSubCls: 737000
	appLocation: ELECTRONIC
	appAttrDockNumber: 170114-1850
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2016-09-14T12:54:48Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-10-04T04:00:00Z
	APP_IND: 1
	appExamName: BURKE, JEFF A
	appExamNameFacet: BURKE, JEFF A
	firstInventorFile: Yes
	appClsSubCls: 707/737000
	appClsSubClsFacet: 707/737000
	applId: 13852154
	patentTitle: ATTRIBUTE VALUE DEPENDENT CLASSIFICATION OF ITEMS
	appIntlPubNumber: 
	appConfrNumber: 3852
	appFilingDate: 2013-03-28T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Jason Peter Hein
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Brian Robert Root-Bernstein
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 707
	_version_: 1580433234528829440
	lastUpdatedTimestamp: 2017-10-05T15:49:44.567Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 8d2a2ffd-bf72-4810-a487-8f9a738c994e
	responseHeader:
			status: 0
			QTime: 17
