DOCUMENT_BODY:
	appGrpArtNumber: 2683
	appGrpArtNumberFacet: 2683
	transactions:
		№ 0
			recordDate: 2015-05-19 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-05-19 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-04-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-04-29 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-04-14 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-04-14 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-04-14 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 7
			recordDate: 2015-04-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 8
			recordDate: 2015-04-14 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 9
			recordDate: 2015-04-14 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 10
			recordDate: 2015-04-09 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 11
			recordDate: 2015-04-09 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 12
			recordDate: 2015-04-09 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 13
			recordDate: 2015-04-07 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 14
			recordDate: 2015-04-02 00:00:00
			code: DRWF
			description: Workflow - Drawings Finished
		№ 15
			recordDate: 2015-04-02 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 16
			recordDate: 2015-02-23 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 17
			recordDate: 2015-02-23 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 18
			recordDate: 2015-02-18 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 19
			recordDate: 2015-01-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 20
			recordDate: 2015-01-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 21
			recordDate: 2015-01-23 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 22
			recordDate: 2015-01-21 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 23
			recordDate: 2014-12-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 24
			recordDate: 2014-12-23 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 25
			recordDate: 2014-12-23 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 26
			recordDate: 2014-09-26 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 27
			recordDate: 2014-09-26 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 28
			recordDate: 2014-09-26 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 29
			recordDate: 2014-09-23 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 30
			recordDate: 2014-09-22 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 31
			recordDate: 2014-09-15 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 32
			recordDate: 2014-09-12 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 33
			recordDate: 2014-09-12 00:00:00
			code: ELC.
			description: Response to Election / Restriction Filed
		№ 34
			recordDate: 2014-09-12 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 35
			recordDate: 2014-09-12 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 36
			recordDate: 2014-09-11 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 37
			recordDate: 2014-07-15 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 38
			recordDate: 2014-07-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 39
			recordDate: 2014-07-14 00:00:00
			code: MCTRS
			description: Mail Restriction Requirement
		№ 40
			recordDate: 2014-07-08 00:00:00
			code: CTRS
			description: Restriction/Election Requirement
		№ 41
			recordDate: 2014-04-05 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 42
			recordDate: 2014-03-19 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 43
			recordDate: 2014-03-19 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 44
			recordDate: 2014-03-19 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 45
			recordDate: 2014-03-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 46
			recordDate: 2014-03-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 47
			recordDate: 2014-03-13 00:00:00
			code: PG-PB-DT
			description: PG-Pub Notice of new or Revised projected publication date
		№ 48
			recordDate: 2014-03-06 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 49
			recordDate: 2013-06-11 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 50
			recordDate: 2013-05-14 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 51
			recordDate: 2013-04-19 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 52
			recordDate: 2013-04-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 53
			recordDate: 2013-04-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 54
			recordDate: 2013-04-19 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 55
			recordDate: 2013-04-19 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 56
			recordDate: 2013-04-18 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 57
			recordDate: 2013-04-18 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 58
			recordDate: 2013-03-14 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 59
			recordDate: 2013-03-11 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 60
			recordDate: 2013-03-11 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 61
			recordDate: 2013-03-11 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9035752
	applIdStr: 13792481
	appl_id_txt: 13792481
	appEarlyPubNumber: US20140253305A1
	appEntityStatus: UNDISCOUNTED
	id: 13792481
	appSubCls: 407200
	appLocation: ELECTRONIC
	appAttrDockNumber: 579-7010
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 109263
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: RENO, 
			geoCode: NV
			country: (US)
			rankNo: 7
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||RENO|NV|US|7}
	appStatusDate: 2015-04-29T10:36:58Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-09-11T04:00:00Z
	patentIssueDate: 2015-05-19T04:00:00Z
	APP_IND: 1
	appExamName: HOFSASS, JEFFERY A
	appExamNameFacet: HOFSASS, JEFFERY A
	firstInventorFile: No
	appClsSubCls: 340/407200
	appClsSubClsFacet: 340/407200
	applId: 13792481
	patentTitle: FORCE SENSING INPUT DEVICE UNDER AN UNBROKEN EXTERIOR PORTION OF A DEVICE
	appIntlPubNumber: 
	appConfrNumber: 4812
	appFilingDate: 2013-03-11T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: ILYA DANIEL ROSENBERG
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: MOUNTAIN VIEW, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: LAKSHMAN  RATHNAM
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: MOUNTAIN VIEW, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: NADIM  AWAD
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SAN FRANCISCO, 
			geoCode: CA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: JOHN AARON ZARRAGA
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SAN FRANCISCO, 
			geoCode: CA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: DAVID CHARLES BUUCK
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: PRUNEDALE, 
			geoCode: CA
			country: (US)
			rankNo: 5
		№ 5
			nameLineOne: JULIEN GEORGE BEGUIN
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SAN FRANCISCO, 
			geoCode: CA
			country: (US)
			rankNo: 6
	appCls: 340
	_version_: 1580433222048677891
	lastUpdatedTimestamp: 2017-10-05T15:49:32.675Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: d2fc8545-85f2-44cb-95ac-21a830edee81
	responseHeader:
			status: 0
			QTime: 17
