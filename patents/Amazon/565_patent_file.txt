DOCUMENT_BODY:
	appGrpArtNumber: 2665
	appGrpArtNumberFacet: 2665
	transactions:
		№ 0
			recordDate: 2016-07-07 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2015-12-08 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2015-12-08 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 3
			recordDate: 2015-12-08 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 4
			recordDate: 2015-11-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2015-11-18 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 6
			recordDate: 2015-11-06 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 7
			recordDate: 2015-11-06 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 8
			recordDate: 2015-11-05 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2015-11-05 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2015-08-19 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 11
			recordDate: 2015-08-19 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 12
			recordDate: 2015-08-19 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 13
			recordDate: 2015-08-13 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 14
			recordDate: 2015-08-04 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 15
			recordDate: 2015-08-04 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 16
			recordDate: 2015-08-03 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 17
			recordDate: 2015-08-03 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 18
			recordDate: 2015-07-16 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 19
			recordDate: 2015-07-16 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 20
			recordDate: 2015-07-13 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 21
			recordDate: 2015-07-12 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 22
			recordDate: 2015-07-10 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 23
			recordDate: 2015-05-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 24
			recordDate: 2015-05-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 25
			recordDate: 2015-05-12 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 26
			recordDate: 2015-05-05 00:00:00
			code: CTFR
			description: Final Rejection
		№ 27
			recordDate: 2015-04-22 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 28
			recordDate: 2015-04-15 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 29
			recordDate: 2015-01-21 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 30
			recordDate: 2015-01-21 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 31
			recordDate: 2015-01-21 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 32
			recordDate: 2015-01-13 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 33
			recordDate: 2014-07-16 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 34
			recordDate: 2014-06-18 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 35
			recordDate: 2014-06-18 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 36
			recordDate: 2013-08-28 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 37
			recordDate: 2013-06-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 38
			recordDate: 2013-06-21 00:00:00
			code: FLRCPT.C
			description: Filing Receipt - Corrected
		№ 39
			recordDate: 2013-06-18 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 40
			recordDate: 2013-06-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 41
			recordDate: 2013-06-10 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 42
			recordDate: 2013-06-10 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 43
			recordDate: 2013-06-08 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 44
			recordDate: 2013-06-08 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 45
			recordDate: 2013-05-09 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 46
			recordDate: 2013-05-07 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 47
			recordDate: 2013-05-06 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 48
			recordDate: 2013-05-06 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 49
			recordDate: 2013-05-06 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 50
			recordDate: 2013-05-06 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9208548
	applIdStr: 13888247
	appl_id_txt: 13888247
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13888247
	appSubCls: 165000
	appLocation: ELECTRONIC
	appAttrDockNumber: 085101-527022.502NPUS00
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136608
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2015-11-18T11:18:06Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-12-08T05:00:00Z
	APP_IND: 1
	appExamName: LIU, LI
	appExamNameFacet: LIU, LI
	firstInventorFile: Yes
	appClsSubCls: 382/165000
	appClsSubClsFacet: 382/165000
	applId: 13888247
	patentTitle: AUTOMATIC IMAGE ENHANCEMENT
	appIntlPubNumber: 
	appConfrNumber: 6912
	appFilingDate: 2013-05-06T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Isaac Scott Noble
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Soquel, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: David W. Stafford
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Cupertino, 
			geoCode: CA
			country: (US)
			rankNo: 2
	appCls: 382
	_version_: 1580433241950650369
	lastUpdatedTimestamp: 2017-10-05T15:49:51.653Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 8a5d78fb-c4aa-4695-8abb-3e524687bf44
	responseHeader:
			status: 0
			QTime: 18
