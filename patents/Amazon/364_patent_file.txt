DOCUMENT_BODY:
	appGrpArtNumber: 2454
	appGrpArtNumberFacet: 2454
	transactions:
		№ 0
			recordDate: 2017-09-07 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2017-03-07 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2017-03-07 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2017-02-16 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2017-02-15 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2017-01-31 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2017-01-31 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2017-01-25 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2017-01-25 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2016-11-11 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2016-11-10 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2016-11-10 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 12
			recordDate: 2016-11-08 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 13
			recordDate: 2016-11-02 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 14
			recordDate: 2016-11-02 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 15
			recordDate: 2016-10-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 16
			recordDate: 2016-10-18 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 17
			recordDate: 2016-10-17 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 18
			recordDate: 2016-10-14 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 19
			recordDate: 2016-10-11 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 20
			recordDate: 2016-07-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 21
			recordDate: 2016-07-11 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 22
			recordDate: 2016-07-11 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 23
			recordDate: 2016-07-01 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 24
			recordDate: 2016-06-23 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 25
			recordDate: 2016-05-23 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 26
			recordDate: 2016-05-23 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 27
			recordDate: 2016-04-23 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 28
			recordDate: 2016-04-23 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 29
			recordDate: 2016-04-20 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 30
			recordDate: 2016-04-20 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 31
			recordDate: 2016-04-20 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 32
			recordDate: 2016-02-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 33
			recordDate: 2016-02-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 34
			recordDate: 2016-02-24 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 35
			recordDate: 2016-02-18 00:00:00
			code: CTFR
			description: Final Rejection
		№ 36
			recordDate: 2016-02-10 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 37
			recordDate: 2015-12-30 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 38
			recordDate: 2015-12-18 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 39
			recordDate: 2015-12-18 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 40
			recordDate: 2015-12-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 41
			recordDate: 2015-09-21 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 42
			recordDate: 2015-09-21 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 43
			recordDate: 2015-09-21 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 44
			recordDate: 2015-09-16 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 45
			recordDate: 2015-08-20 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 46
			recordDate: 2015-08-20 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 47
			recordDate: 2015-08-20 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 48
			recordDate: 2015-08-19 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 49
			recordDate: 2015-08-18 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 50
			recordDate: 2015-08-18 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 51
			recordDate: 2015-08-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 52
			recordDate: 2015-05-22 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 53
			recordDate: 2015-05-22 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 54
			recordDate: 2015-05-22 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 55
			recordDate: 2015-05-14 00:00:00
			code: CTFR
			description: Final Rejection
		№ 56
			recordDate: 2015-04-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 57
			recordDate: 2015-04-16 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 58
			recordDate: 2015-04-16 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 59
			recordDate: 2015-04-09 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 60
			recordDate: 2015-04-09 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 61
			recordDate: 2015-02-27 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 62
			recordDate: 2015-02-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 63
			recordDate: 2015-02-27 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 64
			recordDate: 2015-02-23 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 65
			recordDate: 2015-02-23 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 66
			recordDate: 2015-02-10 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 67
			recordDate: 2015-02-10 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 68
			recordDate: 2014-10-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 69
			recordDate: 2013-11-02 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 70
			recordDate: 2013-04-16 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 71
			recordDate: 2013-04-16 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 72
			recordDate: 2013-04-16 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 73
			recordDate: 2013-04-16 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 74
			recordDate: 2013-04-16 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 75
			recordDate: 2013-03-14 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 76
			recordDate: 2013-03-08 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 77
			recordDate: 2013-03-08 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 78
			recordDate: 2013-03-08 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9588788
	applIdStr: 13791193
	appl_id_txt: 13791193
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13791193
	appSubCls: 201000
	appLocation: ELECTRONIC
	appAttrDockNumber: AM2-1625US
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136607
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 6
	applicantsFacet: {|Amazon Technologies, Inc.||||Seattle|WA|US|6}
	appStatusDate: 2017-02-15T10:03:37Z
	LAST_MOD_TS: 2017-09-07T10:52:43Z
	patentIssueDate: 2017-03-07T05:00:00Z
	APP_IND: 1
	appExamName: WHIPPLE, BRIAN P
	appExamNameFacet: WHIPPLE, BRIAN P
	firstInventorFile: No
	appClsSubCls: 709/201000
	appClsSubClsFacet: 709/201000
	applId: 13791193
	patentTitle: Optimized Communication Between Program Components Executing in Virtual Machines
	appIntlPubNumber: 
	appConfrNumber: 1049
	appFilingDate: 2013-03-08T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Harsha  Ramalingam
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Kirkland, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Bhavnish H. Lathia
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Redmond, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Michael James McInerny
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Kyle Bradley Peterson
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Leon Robert Warman
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Kirkland, 
			geoCode: WA
			country: (US)
			rankNo: 5
	appCls: 709
	_version_: 1580433221812748288
	lastUpdatedTimestamp: 2017-10-05T15:49:32.382Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: d2fc8545-85f2-44cb-95ac-21a830edee81
	responseHeader:
			status: 0
			QTime: 17
