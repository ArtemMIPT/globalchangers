DOCUMENT_BODY:
	appGrpArtNumber: 2457
	appGrpArtNumberFacet: 2457
	transactions:
		№ 0
			recordDate: 2015-04-28 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-04-28 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-04-08 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 3
			recordDate: 2015-03-26 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 4
			recordDate: 2015-03-26 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-03-25 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-03-24 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-03-24 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-01-07 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 9
			recordDate: 2015-01-06 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 10
			recordDate: 2014-12-23 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 11
			recordDate: 2014-08-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 12
			recordDate: 2014-08-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 13
			recordDate: 2014-08-05 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 14
			recordDate: 2014-07-07 00:00:00
			code: TI1050
			description: Transfer Inquiry to GAU
		№ 15
			recordDate: 2014-03-27 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 16
			recordDate: 2013-05-17 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 17
			recordDate: 2013-02-02 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 18
			recordDate: 2013-01-30 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 19
			recordDate: 2013-01-10 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 20
			recordDate: 2013-01-10 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 21
			recordDate: 2013-01-09 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 22
			recordDate: 2012-12-11 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 23
			recordDate: 2012-12-06 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 24
			recordDate: 2012-12-06 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 25
			recordDate: 2012-12-06 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 26
			recordDate: 2012-12-06 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9021020
	applIdStr: 13707329
	appl_id_txt: 13707329
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13707329
	appSubCls: 203000
	appLocation: ELECTRONIC
	appAttrDockNumber: 33350.49 (L0049)
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 14432
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2015-04-08T09:21:22Z
	LAST_MOD_TS: 2017-08-30T14:31:45Z
	patentIssueDate: 2015-04-28T04:00:00Z
	APP_IND: 1
	appExamName: JACOBS, LASHONDA T
	appExamNameFacet: JACOBS, LASHONDA T
	firstInventorFile: No
	appClsSubCls: 709/203000
	appClsSubClsFacet: 709/203000
	applId: 13707329
	patentTitle: APPLICATION RECOGNITION BASED ON MEDIA ANALYSIS
	appIntlPubNumber: 
	appConfrNumber: 8397
	appFilingDate: 2012-12-06T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: SHARADH  RAMASWAMY
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Sunnyvale, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: KENNETH MARK KARAKOTSIOS
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Jose, 
			geoCode: CA
			country: (US)
			rankNo: 2
	appCls: 709
	_version_: 1580433204632879107
	lastUpdatedTimestamp: 2017-10-05T15:49:16.06Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 580c0491-4c13-4537-b2b0-749b4885eba3
	responseHeader:
			status: 0
			QTime: 15
