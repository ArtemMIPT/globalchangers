DOCUMENT_BODY:
	appGrpArtNumber: 2445
	appGrpArtNumberFacet: 2445
	appStatus: Non Final Action Mailed
	appStatus_txt: Non Final Action Mailed
	appStatusFacet: Non Final Action Mailed
	patentNumber: 
	applIdStr: 13833945
	appl_id_txt: 13833945
	appEarlyPubNumber: US20140280884A1
	appEntityStatus: UNDISCOUNTED
	id: 13833945
	appSubCls: 224000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-49900
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|3}
	appStatusDate: 2017-11-16T14:09:27Z
	LAST_MOD_TS: 2017-11-21T08:55:50Z
	appEarlyPubDate: 2014-09-18T04:00:00Z
	APP_IND: 1
	appExamName: TON, DA T
	appExamNameFacet: TON, DA T
	firstInventorFile: No
	appClsSubCls: 709/224000
	appClsSubClsFacet: 709/224000
	applId: 13833945
	patentTitle: NETWORK TRAFFIC MAPPING AND PERFORMANCE ANALYSIS
	appIntlPubNumber: 
	transactions:
		№ 0
			recordDate: 2017-11-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 1
			recordDate: 2017-11-20 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 2
			recordDate: 2017-11-15 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 3
			recordDate: 2017-08-03 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 4
			recordDate: 2017-07-14 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 5
			recordDate: 2017-04-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 6
			recordDate: 2017-04-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 7
			recordDate: 2017-04-14 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 8
			recordDate: 2017-04-10 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 9
			recordDate: 2017-04-06 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 10
			recordDate: 2017-04-06 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 11
			recordDate: 2017-03-01 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 12
			recordDate: 2017-02-14 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 13
			recordDate: 2017-01-06 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 14
			recordDate: 2017-01-06 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 15
			recordDate: 2016-11-17 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 16
			recordDate: 2016-11-17 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 17
			recordDate: 2016-11-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 18
			recordDate: 2016-11-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 19
			recordDate: 2016-11-14 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 20
			recordDate: 2016-11-07 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 21
			recordDate: 2016-04-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 22
			recordDate: 2016-04-29 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 23
			recordDate: 2016-04-25 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 24
			recordDate: 2016-04-25 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 25
			recordDate: 2016-04-25 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 26
			recordDate: 2016-04-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 27
			recordDate: 2016-04-14 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 28
			recordDate: 2016-04-06 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 29
			recordDate: 2016-04-06 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 30
			recordDate: 2016-03-30 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 31
			recordDate: 2016-03-23 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 32
			recordDate: 2016-03-23 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 33
			recordDate: 2015-12-28 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 34
			recordDate: 2015-12-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 35
			recordDate: 2015-12-23 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 36
			recordDate: 2015-12-18 00:00:00
			code: CTFR
			description: Final Rejection
		№ 37
			recordDate: 2015-12-07 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 38
			recordDate: 2015-12-01 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 39
			recordDate: 2015-09-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 40
			recordDate: 2015-09-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 41
			recordDate: 2015-09-01 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 42
			recordDate: 2015-08-25 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 43
			recordDate: 2015-08-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 44
			recordDate: 2015-08-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 45
			recordDate: 2014-12-02 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 46
			recordDate: 2014-12-02 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 47
			recordDate: 2014-11-20 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 48
			recordDate: 2014-09-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 49
			recordDate: 2014-09-19 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 50
			recordDate: 2014-03-25 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 51
			recordDate: 2014-03-25 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 52
			recordDate: 2014-03-25 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 53
			recordDate: 2014-03-14 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 54
			recordDate: 2014-02-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 55
			recordDate: 2013-06-18 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 56
			recordDate: 2013-05-02 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 57
			recordDate: 2013-05-02 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 58
			recordDate: 2013-05-02 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 59
			recordDate: 2013-05-02 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 60
			recordDate: 2013-05-02 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 61
			recordDate: 2013-05-01 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 62
			recordDate: 2013-05-01 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 63
			recordDate: 2013-03-27 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 64
			recordDate: 2013-03-20 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 65
			recordDate: 2013-03-15 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 66
			recordDate: 2013-03-15 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 67
			recordDate: 2013-03-15 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 68
			recordDate: 2013-03-15 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appConfrNumber: 2172
	appFilingDate: 2013-03-15T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: IAN ROGER SEARLE
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: BELLEVUE, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: PETER NICHOLAS DESANTIS
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 709
	_version_: 1584685198465826816
	lastUpdatedTimestamp: 2017-11-21T14:12:53.367Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 791bb353-5cc2-4f42-8948-5645f9c7ff30
	responseHeader:
			status: 0
			QTime: 19
