DOCUMENT_BODY:
	appGrpArtNumber: 2666
	appGrpArtNumberFacet: 2666
	transactions:
		№ 0
			recordDate: 2016-08-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 1
			recordDate: 2016-08-17 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 2
			recordDate: 2016-08-16 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 3
			recordDate: 2016-01-05 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 4
			recordDate: 2016-01-05 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 5
			recordDate: 2015-12-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2015-12-16 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 7
			recordDate: 2015-12-01 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 8
			recordDate: 2015-12-01 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 9
			recordDate: 2015-11-30 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 10
			recordDate: 2015-11-30 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 11
			recordDate: 2015-11-30 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 12
			recordDate: 2015-10-16 00:00:00
			code: MM327-O
			description: Mail PUBS Notice Requiring Inventors Oath or Declaration
		№ 13
			recordDate: 2015-10-14 00:00:00
			code: M327-O
			description: PUBS Notice Requiring Inventors Oath or Declaration
		№ 14
			recordDate: 2015-08-31 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 15
			recordDate: 2015-08-31 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 16
			recordDate: 2015-08-31 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 17
			recordDate: 2015-08-24 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 18
			recordDate: 2015-08-20 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 19
			recordDate: 2015-08-20 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 20
			recordDate: 2015-08-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 21
			recordDate: 2015-08-17 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 22
			recordDate: 2015-06-23 00:00:00
			code: PST_CRD
			description: Mail Post Card
		№ 23
			recordDate: 2015-06-15 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 24
			recordDate: 2015-06-15 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 25
			recordDate: 2015-06-10 00:00:00
			code: CTFR
			description: Final Rejection
		№ 26
			recordDate: 2015-04-16 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 27
			recordDate: 2015-04-15 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 28
			recordDate: 2015-04-09 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 29
			recordDate: 2015-04-03 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 30
			recordDate: 2015-04-03 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 31
			recordDate: 2015-01-16 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 32
			recordDate: 2015-01-16 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 33
			recordDate: 2015-01-16 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 34
			recordDate: 2015-01-09 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 35
			recordDate: 2015-01-09 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 36
			recordDate: 2015-01-09 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 37
			recordDate: 2015-01-09 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 38
			recordDate: 2014-12-05 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 39
			recordDate: 2014-12-05 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 40
			recordDate: 2014-10-02 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 41
			recordDate: 2014-09-05 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 42
			recordDate: 2014-09-04 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 43
			recordDate: 2014-05-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 44
			recordDate: 2014-05-29 00:00:00
			code: FLRCPT.C
			description: Filing Receipt - Corrected
		№ 45
			recordDate: 2014-05-20 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 46
			recordDate: 2014-05-20 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 47
			recordDate: 2014-03-28 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 48
			recordDate: 2014-03-28 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 49
			recordDate: 2014-03-28 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 50
			recordDate: 2014-03-28 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 51
			recordDate: 2014-03-27 00:00:00
			code: PG-PB-DT
			description: PG-Pub Notice of new or Revised projected publication date
		№ 52
			recordDate: 2014-03-17 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 53
			recordDate: 2013-12-14 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 54
			recordDate: 2013-05-17 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 55
			recordDate: 2013-05-09 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 56
			recordDate: 2013-05-09 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 57
			recordDate: 2013-05-09 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 58
			recordDate: 2013-05-08 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 59
			recordDate: 2013-05-08 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 60
			recordDate: 2013-05-07 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 61
			recordDate: 2013-05-07 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 62
			recordDate: 2013-04-03 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 63
			recordDate: 2013-03-29 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 64
			recordDate: 2013-03-15 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 65
			recordDate: 2013-03-15 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 66
			recordDate: 2013-03-15 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9232353
	applIdStr: 13842546
	appl_id_txt: 13842546
	appEarlyPubNumber: US20140247346A1
	appEntityStatus: UNDISCOUNTED
	id: 13842546
	appSubCls: 103000
	appLocation: ELECTRONIC
	appAttrDockNumber: 085101-527292.83CNUS00
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136608
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 6
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|6}
	appStatusDate: 2015-12-16T15:03:14Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-09-04T04:00:00Z
	patentIssueDate: 2016-01-05T05:00:00Z
	APP_IND: 1
	appExamName: NAKHJAVAN, SHERVIN K
	appExamNameFacet: NAKHJAVAN, SHERVIN K
	firstInventorFile: No
	appClsSubCls: 382/103000
	appClsSubClsFacet: 382/103000
	applId: 13842546
	patentTitle: APPROACHES FOR DEVICE LOCATION AND COMMUNICATION
	appIntlPubNumber: 
	appConfrNumber: 9435
	appFilingDate: 2013-03-15T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: BRADLEY J. BOZARTH
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Sunnyvale, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: KENNETH M. KARAKOTSIOS
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Jose, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: GREGORY M. HART
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Mercer Island, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: IAN W. FREED
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: JEFFREY P. BEZOS
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Greater Seattle Area, 
			geoCode: WA
			country: (US)
			rankNo: 5
	appCls: 382
	_version_: 1580433232690675715
	lastUpdatedTimestamp: 2017-10-05T15:49:42.814Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 46476086-7610-45b6-b7a3-ac4922c8865a
	responseHeader:
			status: 0
			QTime: 16
