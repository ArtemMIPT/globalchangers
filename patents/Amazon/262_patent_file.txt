DOCUMENT_BODY:
	appGrpArtNumber: 3625
	appGrpArtNumberFacet: 3625
	transactions:
		№ 0
			recordDate: 2016-03-01 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-03-01 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-02-12 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-02-10 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-02-04 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-02-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2016-02-04 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 7
			recordDate: 2016-02-04 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 8
			recordDate: 2016-01-29 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 9
			recordDate: 2016-01-29 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 10
			recordDate: 2016-01-27 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 11
			recordDate: 2016-01-20 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 12
			recordDate: 2016-01-20 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 13
			recordDate: 2016-01-20 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 14
			recordDate: 2015-11-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 15
			recordDate: 2015-11-09 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 16
			recordDate: 2015-11-09 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 17
			recordDate: 2015-11-04 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 18
			recordDate: 2015-11-04 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 19
			recordDate: 2015-11-04 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 20
			recordDate: 2015-11-02 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 21
			recordDate: 2015-11-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 22
			recordDate: 2015-11-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 23
			recordDate: 2015-11-02 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 24
			recordDate: 2015-10-28 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 25
			recordDate: 2015-10-21 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 26
			recordDate: 2015-10-21 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 27
			recordDate: 2015-10-16 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 28
			recordDate: 2015-09-07 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 29
			recordDate: 2015-09-02 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 30
			recordDate: 2015-09-02 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 31
			recordDate: 2015-08-19 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 32
			recordDate: 2015-08-11 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 33
			recordDate: 2015-08-11 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 34
			recordDate: 2015-06-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 35
			recordDate: 2015-06-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 36
			recordDate: 2015-06-02 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 37
			recordDate: 2015-05-28 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 38
			recordDate: 2015-05-08 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 39
			recordDate: 2015-05-04 00:00:00
			code: ELC.
			description: Response to Election / Restriction Filed
		№ 40
			recordDate: 2015-05-04 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 41
			recordDate: 2015-02-11 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 42
			recordDate: 2015-02-11 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 43
			recordDate: 2015-02-11 00:00:00
			code: MCTRS
			description: Mail Restriction Requirement
		№ 44
			recordDate: 2015-02-05 00:00:00
			code: CTRS
			description: Restriction/Election Requirement
		№ 45
			recordDate: 2013-06-22 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 46
			recordDate: 2013-06-18 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 47
			recordDate: 2013-04-30 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 48
			recordDate: 2013-04-30 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 49
			recordDate: 2013-04-30 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 50
			recordDate: 2013-04-22 00:00:00
			code: ADDFLFEE
			description: Additional Application Filing Fees
		№ 51
			recordDate: 2013-04-22 00:00:00
			code: OATHDECL
			description: A statement by one or more inventors satisfying the requirement under 35 USC 115, Oath of the Applic
		№ 52
			recordDate: 2013-02-20 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 53
			recordDate: 2013-02-20 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 54
			recordDate: 2013-02-20 00:00:00
			code: INCD
			description: Notice Mailed--Application Incomplete--Filing Date Assigned 
		№ 55
			recordDate: 2013-01-30 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 56
			recordDate: 2013-01-28 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 57
			recordDate: 2013-01-25 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 58
			recordDate: 2013-01-25 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9275408
	applIdStr: 13750574
	appl_id_txt: 13750574
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13750574
	appSubCls: 026100
	appLocation: ELECTRONIC
	appAttrDockNumber: P5907/AM2-1680US
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 53377
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Seattle|WA|US|2}
	appStatusDate: 2016-02-10T11:10:18Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-03-01T05:00:00Z
	APP_IND: 1
	appExamName: ALLEN, WILLIAM J
	appExamNameFacet: ALLEN, WILLIAM J
	firstInventorFile: No
	appClsSubCls: 705/026100
	appClsSubClsFacet: 705/026100
	applId: 13750574
	patentTitle: Transferring Ownership of Computing Resources
	appIntlPubNumber: 
	appConfrNumber: 6462
	appFilingDate: 2013-01-25T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Wesley Gavin King
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Cape Town, 
			geoCode: 
			country: (ZA)
			rankNo: 1
	appCls: 705
	_version_: 1580433213566746626
	lastUpdatedTimestamp: 2017-10-05T15:49:24.565Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 043f6711-5f32-4b47-91a2-1be2571e614b
	responseHeader:
			status: 0
			QTime: 16
