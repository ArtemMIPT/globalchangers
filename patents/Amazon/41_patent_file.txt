DOCUMENT_BODY:
	appGrpArtNumber: 2667
	appGrpArtNumberFacet: 2667
	transactions:
		№ 0
			recordDate: 2014-04-25 00:00:00
			code: N423
			description: Post Issue Communication - Certificate of Correction
		№ 1
			recordDate: 2014-04-09 00:00:00
			code: N423
			description: Post Issue Communication - Certificate of Correction
		№ 2
			recordDate: 2013-10-08 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 3
			recordDate: 2013-10-08 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 4
			recordDate: 2013-09-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2013-09-18 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 6
			recordDate: 2013-09-12 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 7
			recordDate: 2013-09-11 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 8
			recordDate: 2013-09-09 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2013-09-09 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2013-07-17 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 11
			recordDate: 2013-06-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 12
			recordDate: 2013-06-18 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 13
			recordDate: 2013-06-18 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 14
			recordDate: 2013-06-13 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 15
			recordDate: 2013-06-08 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 16
			recordDate: 2013-06-07 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 17
			recordDate: 2013-06-04 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 18
			recordDate: 2013-06-04 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 19
			recordDate: 2013-03-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 20
			recordDate: 2013-03-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 21
			recordDate: 2013-03-06 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 22
			recordDate: 2013-02-27 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 23
			recordDate: 2013-01-07 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 24
			recordDate: 2013-01-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 25
			recordDate: 2013-01-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 26
			recordDate: 2013-01-04 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 27
			recordDate: 2013-01-04 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 28
			recordDate: 2012-12-03 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 29
			recordDate: 2012-11-08 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 30
			recordDate: 2012-11-08 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 31
			recordDate: 2012-11-08 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 32
			recordDate: 2012-11-07 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 33
			recordDate: 2012-10-17 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 34
			recordDate: 2012-10-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 35
			recordDate: 2012-10-15 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 36
			recordDate: 2012-10-15 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 37
			recordDate: 2012-10-15 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 38
			recordDate: 2012-10-15 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 39
			recordDate: 2012-10-15 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8553930
	applIdStr: 13652355
	appl_id_txt: 13652355
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13652355
	appSubCls: 100000
	appLocation: ELECTRONIC
	appAttrDockNumber: AM2-0321USC1
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 29150
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|2}
	appStatusDate: 2013-09-18T08:47:12Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2013-10-08T04:00:00Z
	APP_IND: 1
	appExamName: AZARIAN, SEYED H
	appExamNameFacet: AZARIAN, SEYED H
	firstInventorFile: No
	appClsSubCls: 382/100000
	appClsSubClsFacet: 382/100000
	applId: 13652355
	patentTitle: CROWD SOURCE CONTENT EDITING
	appIntlPubNumber: 
	appConfrNumber: 3019
	appFilingDate: 2012-10-15T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: James David Meyers
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Santa Clara, 
			geoCode: CA
			country: (US)
			rankNo: 1
	appCls: 382
	_version_: 1580433193406824451
	lastUpdatedTimestamp: 2017-10-05T15:49:05.324Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: cb75d65e-1d98-4317-bbe5-4e27ee422ded
	responseHeader:
			status: 0
			QTime: 16
