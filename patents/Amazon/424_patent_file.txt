DOCUMENT_BODY:
	appGrpArtNumber: 2192
	appGrpArtNumberFacet: 2192
	transactions:
		№ 0
			recordDate: 2015-08-18 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-08-18 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-07-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-07-29 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-07-20 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-07-14 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-07-13 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-07-13 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-04-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2015-04-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2015-04-13 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2015-04-07 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2015-04-04 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 13
			recordDate: 2015-04-04 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 14
			recordDate: 2015-03-26 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 15
			recordDate: 2015-03-26 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 16
			recordDate: 2014-12-22 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 17
			recordDate: 2014-12-17 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 18
			recordDate: 2014-09-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 19
			recordDate: 2014-09-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 20
			recordDate: 2014-09-17 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 21
			recordDate: 2014-08-25 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 22
			recordDate: 2014-08-18 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 23
			recordDate: 2014-05-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 24
			recordDate: 2014-01-28 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 25
			recordDate: 2013-06-11 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 26
			recordDate: 2013-04-23 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 27
			recordDate: 2013-04-23 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 28
			recordDate: 2013-04-23 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 29
			recordDate: 2013-04-23 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 30
			recordDate: 2013-04-23 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 31
			recordDate: 2013-04-22 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 32
			recordDate: 2013-04-22 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 33
			recordDate: 2013-03-28 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 34
			recordDate: 2013-03-20 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 35
			recordDate: 2013-03-16 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 36
			recordDate: 2013-03-14 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 37
			recordDate: 2013-03-14 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 38
			recordDate: 2013-03-14 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9110680
	applIdStr: 13804047
	appl_id_txt: 13804047
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13804047
	appSubCls: 159000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-52900
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|2}
	appStatusDate: 2015-07-29T11:34:29Z
	LAST_MOD_TS: 2017-08-20T18:30:16Z
	patentIssueDate: 2015-08-18T04:00:00Z
	APP_IND: 1
	appExamName: CHOWDHURY, ZIAUL A.
	appExamNameFacet: CHOWDHURY, ZIAUL A.
	firstInventorFile: No
	appClsSubCls: 717/159000
	appClsSubClsFacet: 717/159000
	applId: 13804047
	patentTitle: AVOIDING OR DEFERRING DATA COPIES
	appIntlPubNumber: 
	appConfrNumber: 1088
	appFilingDate: 2013-03-14T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: JEREMY  BOYNES
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: MERCER ISLAND, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 717
	_version_: 1580433224416362496
	lastUpdatedTimestamp: 2017-10-05T15:49:34.924Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 15a4ef0e-add3-4358-9cd3-7d1e69d9afa9
	responseHeader:
			status: 0
			QTime: 18
