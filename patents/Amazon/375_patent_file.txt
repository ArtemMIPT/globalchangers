DOCUMENT_BODY:
	appGrpArtNumber: 2168
	appGrpArtNumberFacet: 2168
	transactions:
		№ 0
			recordDate: 2016-09-20 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-09-20 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-09-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-08-31 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-08-19 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-08-19 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-08-18 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2016-08-18 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2016-06-13 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 9
			recordDate: 2016-06-13 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 10
			recordDate: 2016-06-13 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 11
			recordDate: 2016-06-08 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 12
			recordDate: 2016-06-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 13
			recordDate: 2016-06-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 14
			recordDate: 2016-06-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 15
			recordDate: 2016-06-07 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 16
			recordDate: 2016-06-06 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 17
			recordDate: 2016-06-06 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 18
			recordDate: 2016-06-06 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 19
			recordDate: 2016-06-06 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 20
			recordDate: 2016-06-06 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 21
			recordDate: 2016-06-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 22
			recordDate: 2016-06-06 00:00:00
			code: MM327-B
			description: Mail PUB Notice of non-compliant IDS
		№ 23
			recordDate: 2016-06-02 00:00:00
			code: M327-B
			description: PUB Notice of non-compliant IDS
		№ 24
			recordDate: 2016-06-01 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 25
			recordDate: 2016-06-01 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 26
			recordDate: 2016-05-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 27
			recordDate: 2016-05-18 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 28
			recordDate: 2016-05-18 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 29
			recordDate: 2016-05-14 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 30
			recordDate: 2016-04-27 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 31
			recordDate: 2016-04-15 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 32
			recordDate: 2016-04-15 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 33
			recordDate: 2016-01-15 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 34
			recordDate: 2016-01-15 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 35
			recordDate: 2016-01-15 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 36
			recordDate: 2016-01-11 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 37
			recordDate: 2016-01-11 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 38
			recordDate: 2016-01-11 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 39
			recordDate: 2016-01-06 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 40
			recordDate: 2016-01-06 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 41
			recordDate: 2016-01-06 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 42
			recordDate: 2015-12-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 43
			recordDate: 2015-12-22 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 44
			recordDate: 2015-12-16 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 45
			recordDate: 2015-12-16 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 46
			recordDate: 2015-12-15 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 47
			recordDate: 2015-12-08 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 48
			recordDate: 2015-09-08 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 49
			recordDate: 2015-09-08 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 50
			recordDate: 2015-09-08 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 51
			recordDate: 2015-08-27 00:00:00
			code: CTFR
			description: Final Rejection
		№ 52
			recordDate: 2015-08-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 53
			recordDate: 2015-07-01 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 54
			recordDate: 2015-06-12 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 55
			recordDate: 2015-06-12 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 56
			recordDate: 2015-04-14 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 57
			recordDate: 2015-04-14 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 58
			recordDate: 2015-02-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 59
			recordDate: 2015-02-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 60
			recordDate: 2015-02-12 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 61
			recordDate: 2015-02-07 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 62
			recordDate: 2015-02-05 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 63
			recordDate: 2014-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 64
			recordDate: 2014-05-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 65
			recordDate: 2014-05-29 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 66
			recordDate: 2013-12-03 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 67
			recordDate: 2013-12-03 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 68
			recordDate: 2013-12-03 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 69
			recordDate: 2013-11-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 70
			recordDate: 2013-11-29 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 71
			recordDate: 2013-11-29 00:00:00
			code: PG-PB-DT
			description: PG-Pub Notice of new or Revised projected publication date
		№ 72
			recordDate: 2013-11-25 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 73
			recordDate: 2013-05-08 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 74
			recordDate: 2013-05-07 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 75
			recordDate: 2013-04-25 00:00:00
			code: C602
			description: Oath or Declaration Filed (Including Supplemental)
		№ 76
			recordDate: 2013-04-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 77
			recordDate: 2013-04-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 78
			recordDate: 2013-04-15 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 79
			recordDate: 2013-04-15 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 80
			recordDate: 2013-04-15 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 81
			recordDate: 2013-04-12 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 82
			recordDate: 2013-04-12 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 83
			recordDate: 2013-03-22 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 84
			recordDate: 2013-03-22 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 85
			recordDate: 2013-03-15 00:00:00
			code: A.PE
			description: Preliminary Amendment
		№ 86
			recordDate: 2013-03-14 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 87
			recordDate: 2013-03-11 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 88
			recordDate: 2013-03-11 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 89
			recordDate: 2013-03-11 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 90
			recordDate: 2013-03-11 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9449040
	applIdStr: 13792914
	appl_id_txt: 13792914
	appEarlyPubNumber: US20140149357A1
	appEntityStatus: UNDISCOUNTED
	id: 13792914
	appSubCls: 652000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-47000
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2016-08-31T11:11:52Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-05-29T04:00:00Z
	patentIssueDate: 2016-09-20T04:00:00Z
	APP_IND: 1
	appExamName: TRAN, ANHTAI V
	appExamNameFacet: TRAN, ANHTAI V
	firstInventorFile: No
	appClsSubCls: 707/652000
	appClsSubClsFacet: 707/652000
	applId: 13792914
	patentTitle: BLOCK RESTORE ORDERING IN A STREAMING RESTORE SYSTEM
	appIntlPubNumber: 
	appConfrNumber: 1096
	appFilingDate: 2013-03-11T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: ANURAG WINDLASS GUPTA
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: ATHERTON, 
			geoCode: CA
			country: (US)
			rankNo: 1
	appCls: 707
	_version_: 1580433222137806848
	lastUpdatedTimestamp: 2017-10-05T15:49:32.717Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: d2fc8545-85f2-44cb-95ac-21a830edee81
	responseHeader:
			status: 0
			QTime: 17
