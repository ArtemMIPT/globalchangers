DOCUMENT_BODY:
	appGrpArtNumber: 2172
	appGrpArtNumberFacet: 2172
	transactions:
		№ 0
			recordDate: 2016-12-06 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-12-06 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-11-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-11-16 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-11-07 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-11-05 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-11-01 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 7
			recordDate: 2016-11-01 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2016-11-01 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2016-08-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2016-08-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2016-08-12 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 12
			recordDate: 2016-08-08 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 13
			recordDate: 2016-08-05 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 14
			recordDate: 2016-04-27 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 15
			recordDate: 2016-04-27 00:00:00
			code: MAPCA
			description: Mail Appeals conf. Rej. withdrawn
		№ 16
			recordDate: 2016-04-25 00:00:00
			code: APCA
			description: Pre-Appeals Conference Decision - Rejection Withdrawn
		№ 17
			recordDate: 2016-02-23 00:00:00
			code: AP.C
			description: Request for Pre-Appeal Conference Filed
		№ 18
			recordDate: 2016-02-23 00:00:00
			code: N/AP
			description: Notice of Appeal Filed
		№ 19
			recordDate: 2015-11-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 20
			recordDate: 2015-11-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 21
			recordDate: 2015-11-23 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 22
			recordDate: 2015-11-17 00:00:00
			code: CTFR
			description: Final Rejection
		№ 23
			recordDate: 2015-05-12 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 24
			recordDate: 2015-05-11 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 25
			recordDate: 2015-02-11 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 26
			recordDate: 2015-02-11 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 27
			recordDate: 2015-02-11 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 28
			recordDate: 2015-02-06 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 29
			recordDate: 2014-07-16 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 30
			recordDate: 2013-01-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 31
			recordDate: 2013-01-14 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 32
			recordDate: 2013-01-07 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 33
			recordDate: 2013-01-07 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 34
			recordDate: 2013-01-07 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 35
			recordDate: 2013-01-07 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 36
			recordDate: 2013-01-07 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 37
			recordDate: 2013-01-04 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 38
			recordDate: 2012-12-10 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 39
			recordDate: 2012-12-06 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 40
			recordDate: 2012-12-06 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 41
			recordDate: 2012-12-06 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9513762
	applIdStr: 13706547
	appl_id_txt: 13706547
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13706547
	appSubCls: 762000
	appLocation: ELECTRONIC
	appAttrDockNumber: 170114-1040
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 7
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|7}
	appStatusDate: 2016-11-16T14:43:48Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-12-06T05:00:00Z
	APP_IND: 1
	appExamName: GREENE, SABRINA LETICIA
	appExamNameFacet: GREENE, SABRINA LETICIA
	firstInventorFile: No
	appClsSubCls: 715/762000
	appClsSubClsFacet: 715/762000
	applId: 13706547
	patentTitle: STATIC CONTENT UPDATES
	appIntlPubNumber: 
	appConfrNumber: 1068
	appFilingDate: 2012-12-06T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Patrick Lee Baumann
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Costa Mesa, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Abdullah Mohammed Jibaly
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Foothill Ranch, 
			geoCode: CA
			country: (US)
			rankNo: 3
		№ 2
			nameLineOne: Kenley Bryan Capps
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Rancho Santa Margarita, 
			geoCode: CA
			country: (US)
			rankNo: 5
		№ 3
			nameLineOne: Mustafa  Hakim
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Mission Viejo, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 4
			nameLineOne: Jordan Petrov Marinov
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Corona, 
			geoCode: CA
			country: (US)
			rankNo: 4
		№ 5
			nameLineOne: Brigham Mark Brown
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Lake Forest, 
			geoCode: CA
			country: (US)
			rankNo: 6
	appCls: 715
	_version_: 1580433204385415169
	lastUpdatedTimestamp: 2017-10-05T15:49:15.831Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 580c0491-4c13-4537-b2b0-749b4885eba3
	responseHeader:
			status: 0
			QTime: 15
