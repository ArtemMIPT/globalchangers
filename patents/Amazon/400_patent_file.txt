DOCUMENT_BODY:
	appGrpArtNumber: 2192
	appGrpArtNumberFacet: 2192
	transactions:
		№ 0
			recordDate: 2015-05-05 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-05-05 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-04-16 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-04-15 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-04-10 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-04-07 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-04-06 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 7
			recordDate: 2015-04-06 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2015-04-06 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2015-01-20 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2015-01-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2015-01-20 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 12
			recordDate: 2015-01-13 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 13
			recordDate: 2015-01-08 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 14
			recordDate: 2015-01-08 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 15
			recordDate: 2015-01-04 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 16
			recordDate: 2014-12-23 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 17
			recordDate: 2014-09-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 18
			recordDate: 2014-09-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 19
			recordDate: 2014-09-24 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 20
			recordDate: 2014-09-18 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 21
			recordDate: 2014-05-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 22
			recordDate: 2014-01-22 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 23
			recordDate: 2013-06-04 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 24
			recordDate: 2013-04-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 25
			recordDate: 2013-04-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 26
			recordDate: 2013-04-17 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 27
			recordDate: 2013-04-17 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 28
			recordDate: 2013-04-17 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 29
			recordDate: 2013-04-16 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 30
			recordDate: 2013-04-16 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 31
			recordDate: 2013-03-16 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 32
			recordDate: 2013-03-13 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 33
			recordDate: 2013-03-12 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 34
			recordDate: 2013-03-12 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9027004
	applIdStr: 13797233
	appl_id_txt: 13797233
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13797233
	appSubCls: 140000
	appLocation: ELECTRONIC
	appAttrDockNumber: 101058.000040
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 23377
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2015-04-15T11:25:40Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-05-05T04:00:00Z
	APP_IND: 1
	appExamName: KENDALL, CHUCK O
	appExamNameFacet: KENDALL, CHUCK O
	firstInventorFile: No
	appClsSubCls: 717/140000
	appClsSubClsFacet: 717/140000
	applId: 13797233
	patentTitle: INJECTION OF SUPPLEMENTAL COMPUTER INSTRUCTIONS
	appIntlPubNumber: 
	appConfrNumber: 3751
	appFilingDate: 2013-03-12T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Michael R. Siwapinyoyos
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Cerritos, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Stephen C. Johnson
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Fullerton, 
			geoCode: CA
			country: (US)
			rankNo: 1
	appCls: 717
	_version_: 1580433222900121604
	lastUpdatedTimestamp: 2017-10-05T15:49:33.476Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: cc915edb-47a7-45a5-937d-e5065d213ba1
	responseHeader:
			status: 0
			QTime: 16
