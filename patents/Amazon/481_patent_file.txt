DOCUMENT_BODY:
	appGrpArtNumber: 2197
	appGrpArtNumberFacet: 2197
	transactions:
		№ 0
			recordDate: 2016-10-18 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-10-18 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 2
			recordDate: 2016-10-18 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2016-09-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2016-09-28 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2016-09-20 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2016-09-20 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2016-09-19 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 8
			recordDate: 2016-09-19 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2016-09-19 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2016-09-12 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 11
			recordDate: 2016-09-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 12
			recordDate: 2016-09-01 00:00:00
			code: R48ACLT
			description: Letter Accepting Correction of Inventorship Under Rule 1.48
		№ 13
			recordDate: 2016-09-01 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 14
			recordDate: 2016-06-21 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 15
			recordDate: 2016-06-21 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 16
			recordDate: 2016-06-21 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 17
			recordDate: 2016-06-16 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 18
			recordDate: 2016-06-10 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 19
			recordDate: 2016-06-10 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 20
			recordDate: 2016-06-09 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 21
			recordDate: 2016-06-09 00:00:00
			code: MAPCR
			description: Mail Appeals conf. Reopen Prosec.
		№ 22
			recordDate: 2016-06-08 00:00:00
			code: APCR
			description: Pre-Appeals Conference Decision - Reopen Prosecution
		№ 23
			recordDate: 2016-05-12 00:00:00
			code: AP.C
			description: Request for Pre-Appeal Conference Filed
		№ 24
			recordDate: 2016-05-12 00:00:00
			code: N/AP
			description: Notice of Appeal Filed
		№ 25
			recordDate: 2016-03-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 26
			recordDate: 2016-03-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 27
			recordDate: 2016-03-02 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 28
			recordDate: 2016-02-25 00:00:00
			code: CTFR
			description: Final Rejection
		№ 29
			recordDate: 2015-12-07 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 30
			recordDate: 2015-11-27 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 31
			recordDate: 2015-11-26 00:00:00
			code: LTDR
			description: Incoming Letter Pertaining to the Drawings
		№ 32
			recordDate: 2015-11-03 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 33
			recordDate: 2015-10-29 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 34
			recordDate: 2015-08-27 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 35
			recordDate: 2015-08-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 36
			recordDate: 2015-08-27 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 37
			recordDate: 2015-08-23 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 38
			recordDate: 2015-07-26 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 39
			recordDate: 2015-07-26 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 40
			recordDate: 2015-07-24 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 41
			recordDate: 2015-07-24 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 42
			recordDate: 2015-07-24 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 43
			recordDate: 2015-05-20 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 44
			recordDate: 2015-05-14 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 45
			recordDate: 2015-05-14 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 46
			recordDate: 2015-04-30 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 47
			recordDate: 2015-04-30 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 48
			recordDate: 2015-04-30 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 49
			recordDate: 2015-04-24 00:00:00
			code: CTFR
			description: Final Rejection
		№ 50
			recordDate: 2015-02-27 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 51
			recordDate: 2015-02-24 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 52
			recordDate: 2015-02-24 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 53
			recordDate: 2014-11-24 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 54
			recordDate: 2014-11-18 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 55
			recordDate: 2014-11-18 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 56
			recordDate: 2014-11-18 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 57
			recordDate: 2014-10-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 58
			recordDate: 2014-10-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 59
			recordDate: 2014-10-24 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 60
			recordDate: 2014-10-20 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 61
			recordDate: 2014-06-03 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 62
			recordDate: 2014-01-28 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 63
			recordDate: 2013-06-28 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 64
			recordDate: 2013-05-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 65
			recordDate: 2013-05-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 66
			recordDate: 2013-05-15 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 67
			recordDate: 2013-05-15 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 68
			recordDate: 2013-05-15 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 69
			recordDate: 2013-05-14 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 70
			recordDate: 2013-05-14 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 71
			recordDate: 2013-04-08 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 72
			recordDate: 2013-03-31 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 73
			recordDate: 2013-03-25 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 74
			recordDate: 2013-03-25 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 75
			recordDate: 2013-03-25 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9471299
	applIdStr: 13850112
	appl_id_txt: 13850112
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13850112
	appSubCls: 170000
	appLocation: ELECTRONIC
	appAttrDockNumber: 170115-1320
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2016-09-28T09:30:16Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-10-18T04:00:00Z
	APP_IND: 1
	appExamName: RUTTEN, JAMES D
	appExamNameFacet: RUTTEN, JAMES D
	firstInventorFile: Yes
	appClsSubCls: 717/170000
	appClsSubClsFacet: 717/170000
	applId: 13850112
	patentTitle: UPDATING CODE WITHIN AN APPLICATION
	appIntlPubNumber: 
	appConfrNumber: 6401
	appFilingDate: 2013-03-25T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Michael Thor Nelson
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Tukwila, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Mobeen  Fikree
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 2
			nameLineOne: Steven Eric Schiesser
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 3
			nameLineOne: Christopher Lawrence Lavin
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Anthony Warren Sajec
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bothell, 
			geoCode: WA
			country: (US)
			rankNo: 5
		№ 5
			nameLineOne: Tuan Duy Le
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Redmond, 
			geoCode: WA
			country: (US)
			rankNo: 6
	appCls: 717
	_version_: 1580433234054873088
	lastUpdatedTimestamp: 2017-10-05T15:49:44.114Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 8d2a2ffd-bf72-4810-a487-8f9a738c994e
	responseHeader:
			status: 0
			QTime: 17
