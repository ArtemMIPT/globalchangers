DOCUMENT_BODY:
	appGrpArtNumber: 3651
	appGrpArtNumberFacet: 3651
	transactions:
		№ 0
			recordDate: 2016-03-18 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2015-05-27 00:00:00
			code: N423
			description: Post Issue Communication - Certificate of Correction
		№ 2
			recordDate: 2015-03-17 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 3
			recordDate: 2015-03-17 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 4
			recordDate: 2015-02-26 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2015-02-25 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 6
			recordDate: 2015-02-12 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 7
			recordDate: 2015-02-12 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 8
			recordDate: 2015-02-06 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2015-02-06 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2014-12-10 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 11
			recordDate: 2014-12-10 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 12
			recordDate: 2014-12-10 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 13
			recordDate: 2014-12-07 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 14
			recordDate: 2014-11-30 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 15
			recordDate: 2014-11-25 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 16
			recordDate: 2014-11-21 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 17
			recordDate: 2014-11-21 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 18
			recordDate: 2014-11-21 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 19
			recordDate: 2014-11-21 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 20
			recordDate: 2014-11-21 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 21
			recordDate: 2014-10-28 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 22
			recordDate: 2014-10-28 00:00:00
			code: MM327-B
			description: Mail PUB Notice of non-compliant IDS
		№ 23
			recordDate: 2014-10-25 00:00:00
			code: M327-B
			description: PUB Notice of non-compliant IDS
		№ 24
			recordDate: 2014-10-22 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 25
			recordDate: 2014-10-22 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 26
			recordDate: 2014-10-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 27
			recordDate: 2014-10-09 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 28
			recordDate: 2014-10-09 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 29
			recordDate: 2014-10-06 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 30
			recordDate: 2014-09-10 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 31
			recordDate: 2014-09-03 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 32
			recordDate: 2014-06-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 33
			recordDate: 2014-06-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 34
			recordDate: 2014-06-05 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 35
			recordDate: 2014-06-02 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 36
			recordDate: 2014-06-01 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 37
			recordDate: 2014-01-23 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 38
			recordDate: 2014-01-23 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 39
			recordDate: 2014-01-23 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 40
			recordDate: 2013-05-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 41
			recordDate: 2013-05-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 42
			recordDate: 2013-05-10 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 43
			recordDate: 2013-05-10 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 44
			recordDate: 2013-04-25 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 45
			recordDate: 2013-03-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 46
			recordDate: 2013-02-08 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 47
			recordDate: 2013-01-16 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 48
			recordDate: 2013-01-16 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 49
			recordDate: 2013-01-16 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 50
			recordDate: 2013-01-15 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 51
			recordDate: 2012-12-18 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 52
			recordDate: 2012-12-13 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 53
			recordDate: 2012-12-13 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 54
			recordDate: 2012-12-13 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8983647
	applIdStr: 13713898
	appl_id_txt: 13713898
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13713898
	appSubCls: 216000
	appLocation: ELECTRONIC
	appAttrDockNumber: 080663.0254
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136595
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 5
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|5}
	appStatusDate: 2015-02-25T10:20:14Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-03-17T04:00:00Z
	APP_IND: 1
	appExamName: CUMBESS, YOLANDA RENEE
	appExamNameFacet: CUMBESS, YOLANDA RENEE
	firstInventorFile: No
	appClsSubCls: 700/216000
	appClsSubClsFacet: 700/216000
	applId: 13713898
	patentTitle: INVENTORY SYSTEM WITH CLIMATE-CONTROLLED INVENTORY
	appIntlPubNumber: 
	appConfrNumber: 1805
	appFilingDate: 2012-12-13T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Samvid H. Dwarakanath
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Sean W. Blakey
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Bryant R. Casteel
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Kirkland, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Bruce J. Cooper
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 4
	appCls: 700
	_version_: 1580433206012805123
	lastUpdatedTimestamp: 2017-10-05T15:49:17.377Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 095e6b89-a18e-4c8a-b2d2-0eda3c7e70b6
	responseHeader:
			status: 0
			QTime: 16
