DOCUMENT_BODY:
	appGrpArtNumber: 2195
	appGrpArtNumberFacet: 2195
	appStatus: Response to Non-Final Office Action Entered and Forwarded to Examiner
	appStatus_txt: Response to Non-Final Office Action Entered and Forwarded to Examiner
	appStatusFacet: Response to Non-Final Office Action Entered and Forwarded to Examiner
	patentNumber: 
	applIdStr: 13889914
	appl_id_txt: 13889914
	appEarlyPubNumber: US20140337833A1
	appEntityStatus: UNDISCOUNTED
	id: 13889914
	appSubCls: 001000
	appLocation: ELECTRONIC
	appAttrDockNumber: PM5151-US/AM2-1712US
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136607
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Seattle|WA|US|2}
	appStatusDate: 2017-09-15T18:15:12Z
	LAST_MOD_TS: 2017-10-22T09:30:15Z
	appEarlyPubDate: 2014-11-13T05:00:00Z
	APP_IND: 1
	appExamName: CHU JOY, JORGE A
	appExamNameFacet: CHU JOY, JORGE A
	firstInventorFile: Yes
	appClsSubCls: 718/001000
	appClsSubClsFacet: 718/001000
	applId: 13889914
	patentTitle: User-Influenced Placement of Virtual Machine Instances
	appIntlPubNumber: 
	transactions:
		№ 0
			recordDate: 2017-09-15 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 1
			recordDate: 2017-09-07 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 2
			recordDate: 2017-08-31 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 3
			recordDate: 2017-08-31 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 4
			recordDate: 2017-08-31 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 5
			recordDate: 2017-08-31 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 6
			recordDate: 2017-04-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 7
			recordDate: 2017-04-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 8
			recordDate: 2017-04-14 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 9
			recordDate: 2017-04-03 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 10
			recordDate: 2017-03-23 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 11
			recordDate: 2016-12-11 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 12
			recordDate: 2016-12-11 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 13
			recordDate: 2016-12-09 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 14
			recordDate: 2016-12-09 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 15
			recordDate: 2016-12-09 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 16
			recordDate: 2016-12-09 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 17
			recordDate: 2016-12-09 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 18
			recordDate: 2016-11-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 19
			recordDate: 2016-11-10 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 20
			recordDate: 2016-11-05 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 21
			recordDate: 2016-11-04 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 22
			recordDate: 2016-10-31 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 23
			recordDate: 2016-10-31 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 24
			recordDate: 2016-10-28 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 25
			recordDate: 2016-10-27 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 26
			recordDate: 2016-10-24 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 27
			recordDate: 2016-10-24 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 28
			recordDate: 2016-06-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 29
			recordDate: 2016-06-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 30
			recordDate: 2016-06-24 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 31
			recordDate: 2016-06-21 00:00:00
			code: CTFR
			description: Final Rejection
		№ 32
			recordDate: 2016-06-20 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 33
			recordDate: 2016-06-20 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 34
			recordDate: 2016-06-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 35
			recordDate: 2016-06-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 36
			recordDate: 2016-04-15 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 37
			recordDate: 2016-04-04 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 38
			recordDate: 2016-03-31 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 39
			recordDate: 2016-03-31 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 40
			recordDate: 2016-03-10 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 41
			recordDate: 2016-03-10 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 42
			recordDate: 2016-01-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 43
			recordDate: 2016-01-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 44
			recordDate: 2016-01-05 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 45
			recordDate: 2015-12-30 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 46
			recordDate: 2015-12-28 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 47
			recordDate: 2015-11-19 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 48
			recordDate: 2015-11-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 49
			recordDate: 2015-11-16 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 50
			recordDate: 2015-11-16 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 51
			recordDate: 2015-11-16 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 52
			recordDate: 2015-11-16 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 53
			recordDate: 2015-10-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 54
			recordDate: 2015-10-29 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 55
			recordDate: 2015-10-19 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 56
			recordDate: 2015-10-19 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 57
			recordDate: 2015-10-13 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 58
			recordDate: 2015-10-13 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 59
			recordDate: 2015-09-17 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 60
			recordDate: 2015-09-16 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 61
			recordDate: 2015-09-16 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 62
			recordDate: 2015-09-16 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 63
			recordDate: 2015-09-16 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 64
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 65
			recordDate: 2015-07-16 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 66
			recordDate: 2015-07-16 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 67
			recordDate: 2015-07-16 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 68
			recordDate: 2015-07-08 00:00:00
			code: CTFR
			description: Final Rejection
		№ 69
			recordDate: 2015-07-01 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 70
			recordDate: 2015-06-02 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 71
			recordDate: 2015-06-01 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 72
			recordDate: 2015-05-29 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 73
			recordDate: 2015-05-29 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 74
			recordDate: 2015-05-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 75
			recordDate: 2015-05-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 76
			recordDate: 2015-05-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 77
			recordDate: 2015-05-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 78
			recordDate: 2015-05-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 79
			recordDate: 2015-05-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 80
			recordDate: 2015-04-30 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 81
			recordDate: 2015-04-21 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 82
			recordDate: 2015-04-21 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 83
			recordDate: 2015-03-04 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 84
			recordDate: 2015-03-04 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 85
			recordDate: 2015-03-04 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 86
			recordDate: 2015-02-23 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 87
			recordDate: 2015-02-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 88
			recordDate: 2014-12-16 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 89
			recordDate: 2014-11-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 90
			recordDate: 2014-11-13 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 91
			recordDate: 2014-05-14 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 92
			recordDate: 2014-05-08 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 93
			recordDate: 2014-04-28 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 94
			recordDate: 2014-04-28 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 95
			recordDate: 2014-01-31 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 96
			recordDate: 2013-06-19 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 97
			recordDate: 2013-06-11 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 98
			recordDate: 2013-06-11 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 99
			recordDate: 2013-06-11 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 100
			recordDate: 2013-06-11 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 101
			recordDate: 2013-06-11 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 102
			recordDate: 2013-05-14 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 103
			recordDate: 2013-05-08 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 104
			recordDate: 2013-05-08 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 105
			recordDate: 2013-05-08 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 106
			recordDate: 2013-05-08 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appConfrNumber: 8161
	appFilingDate: 2013-05-08T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Eden Grail Adogla
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 718
	_version_: 1581964939160977408
	lastUpdatedTimestamp: 2017-10-22T13:35:31.936Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 8a5d78fb-c4aa-4695-8abb-3e524687bf44
	responseHeader:
			status: 0
			QTime: 18
