DOCUMENT_BODY:
	appGrpArtNumber: 3632
	appGrpArtNumberFacet: 3632
	transactions:
		№ 0
			recordDate: 2017-07-04 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9698529
	applIdStr: 13780495
	appl_id_txt: 13780495
	appEarlyPubNumber: 
	appEntityStatus:  
	id: 13780495
	appSubCls: 065000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-49800
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|3}
	appStatusDate: 2017-06-14T07:18:10Z
	LAST_MOD_TS: 2017-07-04T16:30:34Z
	patentIssueDate: 2017-07-04T04:00:00Z
	APP_IND: 1
	appExamName: MCNICHOLS, ERET C
	appExamNameFacet: MCNICHOLS, ERET C
	firstInventorFile: No
	appClsSubCls: 248/065000
	appClsSubClsFacet: 248/065000
	applId: 13780495
	patentTitle: CABLE HOLDER FOR SYSTEM SERVICEABILTY
	appIntlPubNumber: 
	appConfrNumber: 1110
	appFilingDate: 2013-02-28T05:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: KENNETH MICHAEL SCHOW
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: PETER GEORGE ROSS
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: OLYMPIA, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 248
	_version_: 1580433219808919557
	lastUpdatedTimestamp: 2017-10-05T15:49:30.508Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 0a00969b-d87b-44b3-8fd5-1d76e5a8df75
	responseHeader:
			status: 0
			QTime: 18
