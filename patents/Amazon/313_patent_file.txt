DOCUMENT_BODY:
	appGrpArtNumber: 2116
	appGrpArtNumberFacet: 2116
	transactions:
		№ 0
			recordDate: 2016-07-19 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2015-06-16 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2015-06-16 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2015-05-28 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2015-05-27 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2015-05-14 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2015-05-13 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2015-05-12 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2015-05-12 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2015-03-03 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 10
			recordDate: 2015-02-26 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 11
			recordDate: 2015-02-26 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 12
			recordDate: 2015-02-26 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 13
			recordDate: 2015-02-23 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 14
			recordDate: 2015-02-22 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 15
			recordDate: 2015-02-18 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 16
			recordDate: 2014-07-01 00:00:00
			code: A.QU
			description: Response after Ex Parte Quayle Action
		№ 17
			recordDate: 2014-07-01 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 18
			recordDate: 2014-06-27 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 19
			recordDate: 2014-06-27 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 20
			recordDate: 2014-06-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 21
			recordDate: 2014-06-09 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 22
			recordDate: 2014-06-09 00:00:00
			code: MCTEQ
			description: Mail Ex Parte Quayle Action (PTOL - 326)
		№ 23
			recordDate: 2014-06-02 00:00:00
			code: CTEQ
			description: Quayle action
		№ 24
			recordDate: 2014-05-28 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 25
			recordDate: 2014-05-28 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 26
			recordDate: 2014-05-27 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 27
			recordDate: 2014-05-27 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 28
			recordDate: 2014-05-05 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 29
			recordDate: 2014-05-05 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 30
			recordDate: 2014-04-30 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 31
			recordDate: 2014-04-30 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 32
			recordDate: 2014-04-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 33
			recordDate: 2014-04-28 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 34
			recordDate: 2014-02-28 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 35
			recordDate: 2014-02-28 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 36
			recordDate: 2014-02-28 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 37
			recordDate: 2014-02-23 00:00:00
			code: CTFR
			description: Final Rejection
		№ 38
			recordDate: 2014-02-05 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 39
			recordDate: 2014-02-03 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 40
			recordDate: 2013-11-21 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 41
			recordDate: 2013-11-21 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 42
			recordDate: 2013-11-21 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 43
			recordDate: 2013-11-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 44
			recordDate: 2013-11-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 45
			recordDate: 2013-11-01 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 46
			recordDate: 2013-10-29 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 47
			recordDate: 2013-08-04 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 48
			recordDate: 2013-08-01 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 49
			recordDate: 2013-05-01 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 50
			recordDate: 2013-04-30 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 51
			recordDate: 2013-04-02 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 52
			recordDate: 2013-03-11 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 53
			recordDate: 2013-03-11 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 54
			recordDate: 2013-03-11 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 55
			recordDate: 2013-02-20 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 56
			recordDate: 2013-02-18 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 57
			recordDate: 2013-02-18 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 58
			recordDate: 2013-02-18 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 59
			recordDate: 2013-02-18 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9058128
	applIdStr: 13769746
	appl_id_txt: 13769746
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13769746
	appSubCls: 300000
	appLocation: ELECTRONIC
	appAttrDockNumber: 085101-527124.103CNUS00
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136608
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2015-05-27T08:56:38Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-06-16T04:00:00Z
	APP_IND: 1
	appExamName: ELAMIN, ABDELMONIEM I
	appExamNameFacet: ELAMIN, ABDELMONIEM I
	firstInventorFile: No
	appClsSubCls: 713/300000
	appClsSubClsFacet: 713/300000
	applId: 13769746
	patentTitle: POWER MANAGEMENT FOR ELECTRONIC DEVICES
	appIntlPubNumber: 
	appConfrNumber: 4170
	appFilingDate: 2013-02-18T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: KEELA N. ROBISON
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Ian W. Freed
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 713
	_version_: 1580433217367834628
	lastUpdatedTimestamp: 2017-10-05T15:49:28.17Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 4e446174-b94c-4fff-9a7f-c35223ddaa58
	responseHeader:
			status: 0
			QTime: 18
