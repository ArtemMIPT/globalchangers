DOCUMENT_BODY:
	appGrpArtNumber: 2168
	appGrpArtNumberFacet: 2168
	appStatus: Non Final Action Mailed
	appStatus_txt: Non Final Action Mailed
	appStatusFacet: Non Final Action Mailed
	patentNumber: 
	applIdStr: 13780077
	appl_id_txt: 13780077
	appEarlyPubNumber: US20140244585A1
	appEntityStatus: UNDISCOUNTED
	id: 13780077
	appSubCls: 639000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-45700
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|3}
	appStatusDate: 2017-10-16T13:53:52Z
	LAST_MOD_TS: 2017-11-01T19:24:21Z
	appEarlyPubDate: 2014-08-28T04:00:00Z
	APP_IND: 1
	appExamName: JOHNSON, JOHNESE T
	appExamNameFacet: JOHNSON, JOHNESE T
	firstInventorFile: No
	appClsSubCls: 707/639000
	appClsSubClsFacet: 707/639000
	applId: 13780077
	patentTitle: DATABASE SYSTEM PROVIDING SINGLE-TENANT AND MULTI-TENANT ENVIRONMENTS
	appIntlPubNumber: 
	transactions:
		№ 0
			recordDate: 2017-10-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 1
			recordDate: 2017-10-18 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 2
			recordDate: 2017-10-18 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 3
			recordDate: 2017-08-21 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 4
			recordDate: 2017-08-21 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 5
			recordDate: 2017-05-16 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 6
			recordDate: 2017-05-09 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 7
			recordDate: 2017-02-27 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 8
			recordDate: 2017-02-27 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 9
			recordDate: 2017-02-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2017-02-09 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2017-02-09 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 12
			recordDate: 2017-01-31 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 13
			recordDate: 2016-12-30 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 14
			recordDate: 2016-10-07 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 15
			recordDate: 2016-09-30 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 16
			recordDate: 2016-09-30 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 17
			recordDate: 2016-09-30 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 18
			recordDate: 2016-09-30 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 19
			recordDate: 2016-05-31 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 20
			recordDate: 2016-05-31 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 21
			recordDate: 2016-05-31 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 22
			recordDate: 2015-08-21 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 23
			recordDate: 2015-05-08 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 24
			recordDate: 2015-05-06 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 25
			recordDate: 2015-05-06 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 26
			recordDate: 2015-01-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 27
			recordDate: 2015-01-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 28
			recordDate: 2015-01-06 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 29
			recordDate: 2014-12-29 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 30
			recordDate: 2014-12-29 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 31
			recordDate: 2014-12-29 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 32
			recordDate: 2014-10-09 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 33
			recordDate: 2014-10-09 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 34
			recordDate: 2014-09-05 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 35
			recordDate: 2014-09-04 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 36
			recordDate: 2014-08-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 37
			recordDate: 2014-08-28 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 38
			recordDate: 2014-08-05 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 39
			recordDate: 2014-03-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 40
			recordDate: 2014-03-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 41
			recordDate: 2014-03-13 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 42
			recordDate: 2014-02-28 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 43
			recordDate: 2013-05-02 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 44
			recordDate: 2013-04-26 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 45
			recordDate: 2013-04-02 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 46
			recordDate: 2013-04-02 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 47
			recordDate: 2013-04-02 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 48
			recordDate: 2013-04-02 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 49
			recordDate: 2013-04-02 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 50
			recordDate: 2013-04-01 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 51
			recordDate: 2013-04-01 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 52
			recordDate: 2013-03-22 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 53
			recordDate: 2013-03-22 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 54
			recordDate: 2013-03-04 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 55
			recordDate: 2013-02-28 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 56
			recordDate: 2013-02-28 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 57
			recordDate: 2013-02-28 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 58
			recordDate: 2013-02-28 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appConfrNumber: 1976
	appFilingDate: 2013-02-28T05:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: SWAMINATHAN  SIVASUBRAMANIAN
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: STEFANO  STEFANI
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: ISSAQUAH, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 707
	_version_: 1582909811441270787
	lastUpdatedTimestamp: 2017-11-01T23:53:52.348Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 0a00969b-d87b-44b3-8fd5-1d76e5a8df75
	responseHeader:
			status: 0
			QTime: 18
