DOCUMENT_BODY:
	appGrpArtNumber: 2441
	appGrpArtNumberFacet: 2441
	transactions:
		№ 0
			recordDate: 2017-09-07 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2016-06-14 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2016-06-14 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2016-05-26 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2016-05-25 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2016-05-12 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2016-05-12 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2016-05-11 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2016-05-11 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2016-02-26 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2016-02-26 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2016-02-26 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 12
			recordDate: 2016-02-22 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 13
			recordDate: 2016-02-03 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 14
			recordDate: 2015-12-28 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 15
			recordDate: 2015-09-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 16
			recordDate: 2015-09-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 17
			recordDate: 2015-09-24 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 18
			recordDate: 2015-09-20 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 19
			recordDate: 2015-08-27 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 20
			recordDate: 2015-08-27 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 21
			recordDate: 2015-08-24 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 22
			recordDate: 2015-08-24 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 23
			recordDate: 2015-06-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 24
			recordDate: 2015-06-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 25
			recordDate: 2015-06-01 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 26
			recordDate: 2015-05-26 00:00:00
			code: CTFR
			description: Final Rejection
		№ 27
			recordDate: 2015-04-09 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 28
			recordDate: 2015-04-02 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 29
			recordDate: 2015-02-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 30
			recordDate: 2015-02-24 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 31
			recordDate: 2015-02-23 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 32
			recordDate: 2015-01-07 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 33
			recordDate: 2015-01-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 34
			recordDate: 2015-01-05 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 35
			recordDate: 2014-12-26 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 36
			recordDate: 2014-12-22 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 37
			recordDate: 2014-09-25 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 38
			recordDate: 2013-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 39
			recordDate: 2013-04-25 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 40
			recordDate: 2013-03-25 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 41
			recordDate: 2013-03-25 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 42
			recordDate: 2013-03-25 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 43
			recordDate: 2013-03-25 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 44
			recordDate: 2013-03-25 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 45
			recordDate: 2013-03-01 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 46
			recordDate: 2013-02-27 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 47
			recordDate: 2013-02-27 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 48
			recordDate: 2013-02-27 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 49
			recordDate: 2013-02-27 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 50
			recordDate: 2013-02-27 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9369332
	applIdStr: 13778395
	appl_id_txt: 13778395
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13778395
	appSubCls: 214000
	appLocation: ELECTRONIC
	appAttrDockNumber: AM2-1688US
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136607
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Seattle|WA|US|3}
	appStatusDate: 2016-05-25T10:16:49Z
	LAST_MOD_TS: 2017-09-07T10:52:43Z
	patentIssueDate: 2016-06-14T04:00:00Z
	APP_IND: 1
	appExamName: ZONG, RUOLEI
	appExamNameFacet: ZONG, RUOLEI
	firstInventorFile: No
	appClsSubCls: 709/214000
	appClsSubClsFacet: 709/214000
	applId: 13778395
	patentTitle: In-Memory Distributed Cache
	appIntlPubNumber: 
	appConfrNumber: 5410
	appFilingDate: 2013-02-27T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Patrick Devere Smith
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Zachary Ganwise Fewtrell
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Redmond, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 709
	_version_: 1580433219447160833
	lastUpdatedTimestamp: 2017-10-05T15:49:30.146Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 0a00969b-d87b-44b3-8fd5-1d76e5a8df75
	responseHeader:
			status: 0
			QTime: 18
