DOCUMENT_BODY:
	appGrpArtNumber: 2622
	appGrpArtNumberFacet: 2622
	transactions:
		№ 0
			recordDate: 2016-07-07 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2016-02-16 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2016-02-16 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2016-01-28 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2016-01-27 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2016-01-08 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2016-01-08 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2015-12-30 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2015-12-30 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2015-12-16 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 10
			recordDate: 2015-12-16 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 11
			recordDate: 2015-12-16 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 12
			recordDate: 2015-12-16 00:00:00
			code: MM327
			description: Mail Miscellaneous Communication to Applicant
		№ 13
			recordDate: 2015-12-10 00:00:00
			code: M327
			description: Miscellaneous Communication to Applicant - No Action Count
		№ 14
			recordDate: 2015-12-08 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 15
			recordDate: 2015-12-07 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 16
			recordDate: 2015-10-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 17
			recordDate: 2015-10-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 18
			recordDate: 2015-10-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 19
			recordDate: 2015-10-13 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 20
			recordDate: 2015-10-08 00:00:00
			code: MNRAB
			description: Mail Notice of Rescinded Abandonment
		№ 21
			recordDate: 2015-10-07 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 22
			recordDate: 2015-10-06 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 23
			recordDate: 2015-10-06 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 24
			recordDate: 2015-10-06 00:00:00
			code: NRAB
			description: Notice of Rescinded Abandonment in TCs
		№ 25
			recordDate: 2015-09-11 00:00:00
			code: MOPPT
			description: Mail O.P. Petition Decision
		№ 26
			recordDate: 2015-09-09 00:00:00
			code: MPREV
			description: Mail-Petition to Revive Application - Granted
		№ 27
			recordDate: 2015-09-08 00:00:00
			code: PREV
			description: Petition to Revive Application - Granted
		№ 28
			recordDate: 2015-09-08 00:00:00
			code: OPPT
			description: O.P. Petition Decision
		№ 29
			recordDate: 2015-06-12 00:00:00
			code: PET.
			description: Petition Entered
		№ 30
			recordDate: 2015-02-03 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 31
			recordDate: 2015-02-03 00:00:00
			code: MABN2
			description: Mail Abandonment for Failure to Respond to Office Action
		№ 32
			recordDate: 2015-01-27 00:00:00
			code: ABN2
			description: Aband. for Failure to Respond to O. A.
		№ 33
			recordDate: 2014-10-31 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 34
			recordDate: 2014-10-30 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 35
			recordDate: 2014-10-30 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 36
			recordDate: 2014-10-30 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 37
			recordDate: 2014-10-27 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 38
			recordDate: 2014-10-27 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 39
			recordDate: 2014-06-30 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 40
			recordDate: 2014-06-30 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 41
			recordDate: 2014-06-30 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 42
			recordDate: 2014-06-24 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 43
			recordDate: 2014-05-05 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 44
			recordDate: 2014-03-31 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 45
			recordDate: 2014-03-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 46
			recordDate: 2014-03-10 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 47
			recordDate: 2014-02-04 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 48
			recordDate: 2013-04-25 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 49
			recordDate: 2013-04-18 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 50
			recordDate: 2013-04-04 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 51
			recordDate: 2013-04-04 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 52
			recordDate: 2013-04-03 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 53
			recordDate: 2013-03-18 00:00:00
			code: ADDFLFEE
			description: Additional Application Filing Fees
		№ 54
			recordDate: 2013-01-18 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 55
			recordDate: 2013-01-18 00:00:00
			code: INCD
			description: Notice Mailed--Application Incomplete--Filing Date Assigned 
		№ 56
			recordDate: 2012-12-12 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 57
			recordDate: 2012-12-10 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 58
			recordDate: 2012-12-10 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 59
			recordDate: 2012-12-10 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 60
			recordDate: 2012-12-10 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9262067
	applIdStr: 13709643
	appl_id_txt: 13709643
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13709643
	appSubCls: 173000
	appLocation: ELECTRONIC
	appAttrDockNumber: 085101-526920.398NPUS00
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136608
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 8
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|8}
	appStatusDate: 2016-01-27T12:49:36Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-02-16T05:00:00Z
	APP_IND: 1
	appExamName: HICKS, CHARLES V
	appExamNameFacet: HICKS, CHARLES V
	firstInventorFile: No
	appClsSubCls: 345/173000
	appClsSubClsFacet: 345/173000
	applId: 13709643
	patentTitle: APPROACHES FOR DISPLAYING ALTERNATE VIEWS OF INFORMATION
	appIntlPubNumber: 
	appConfrNumber: 8561
	appFilingDate: 2012-12-10T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Matthew Paul Bell
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Sunol, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Dong  Zhou
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Jose, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Guenael Thomas Strutt
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Jose, 
			geoCode: CA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Isaac Scott Noble
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Soquel, 
			geoCode: CA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Stephen Michael Polansky
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Santa Clara, 
			geoCode: CA
			country: (US)
			rankNo: 5
		№ 5
			nameLineOne: Jason Robert Weber
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Mountain View, 
			geoCode: CA
			country: (US)
			rankNo: 6
	appCls: 345
	_version_: 1580433205031337988
	lastUpdatedTimestamp: 2017-10-05T15:49:16.434Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 580c0491-4c13-4537-b2b0-749b4885eba3
	responseHeader:
			status: 0
			QTime: 15
