DOCUMENT_BODY:
	appGrpArtNumber: 2667
	appGrpArtNumberFacet: 2667
	transactions:
		№ 0
			recordDate: 2015-11-19 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2015-09-15 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2015-09-15 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2015-08-26 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-08-11 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-08-11 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-08-10 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-08-10 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-06-10 00:00:00
			code: FLRCPT.C
			description: Filing Receipt - Corrected
		№ 9
			recordDate: 2015-05-20 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 10
			recordDate: 2015-05-19 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 11
			recordDate: 2015-05-19 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 12
			recordDate: 2015-05-19 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 13
			recordDate: 2015-05-18 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 14
			recordDate: 2015-05-18 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 15
			recordDate: 2015-01-28 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 16
			recordDate: 2015-01-26 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 17
			recordDate: 2015-01-26 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 18
			recordDate: 2014-09-24 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 19
			recordDate: 2014-09-22 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 20
			recordDate: 2014-02-14 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 21
			recordDate: 2013-09-24 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 22
			recordDate: 2013-01-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 23
			recordDate: 2013-01-22 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 24
			recordDate: 2013-01-03 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 25
			recordDate: 2013-01-03 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 26
			recordDate: 2013-01-02 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 27
			recordDate: 2012-12-02 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 28
			recordDate: 2012-11-29 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 29
			recordDate: 2012-11-29 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 30
			recordDate: 2012-11-29 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 31
			recordDate: 2012-11-29 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9135517
	applIdStr: 13689151
	appl_id_txt: 13689151
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13689151
	appSubCls: 195000
	appLocation: ELECTRONIC
	appAttrDockNumber: P6541
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136714
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2015-08-26T09:38:41Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-09-15T04:00:00Z
	APP_IND: 1
	appExamName: CONWAY, THOMAS A
	appExamNameFacet: CONWAY, THOMAS A
	firstInventorFile: No
	appClsSubCls: 382/195000
	appClsSubClsFacet: 382/195000
	applId: 13689151
	patentTitle: IMAGE BASED DOCUMENT IDENTIFICATION BASED ON OBTAINED AND STORED DOCUMENT CHARACTERISTICS
	appIntlPubNumber: 
	appConfrNumber: 2830
	appFilingDate: 2012-11-29T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Jeffrey Penrod Adams
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Tyngsborough, 
			geoCode: MA
			country: (US)
			rankNo: 1
	appCls: 382
	_version_: 1580433200846471172
	lastUpdatedTimestamp: 2017-10-05T15:49:12.439Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 397b5564-109f-402b-a2a8-4568b73be568
	responseHeader:
			status: 0
			QTime: 15
