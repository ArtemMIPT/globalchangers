DOCUMENT_BODY:
	appGrpArtNumber: 2191
	appGrpArtNumberFacet: 2191
	transactions:
		№ 0
			recordDate: 2016-09-08 00:00:00
			code: N423
			description: Post Issue Communication - Certificate of Correction
		№ 1
			recordDate: 2016-05-17 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2016-05-17 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2016-04-28 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2016-04-27 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2016-04-19 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2016-04-18 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2016-04-15 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 8
			recordDate: 2016-04-15 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2016-04-15 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2016-02-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 11
			recordDate: 2016-02-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 12
			recordDate: 2016-02-01 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 13
			recordDate: 2016-01-25 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 14
			recordDate: 2016-01-22 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 15
			recordDate: 2016-01-18 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 16
			recordDate: 2015-12-22 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 17
			recordDate: 2015-12-10 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 18
			recordDate: 2015-12-10 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 19
			recordDate: 2015-12-10 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 20
			recordDate: 2015-12-10 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 21
			recordDate: 2015-11-16 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 22
			recordDate: 2015-11-10 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 23
			recordDate: 2015-09-10 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 24
			recordDate: 2015-09-10 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 25
			recordDate: 2015-09-10 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 26
			recordDate: 2015-09-03 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 27
			recordDate: 2015-08-25 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 28
			recordDate: 2015-07-22 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 29
			recordDate: 2015-07-22 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 30
			recordDate: 2015-07-10 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 31
			recordDate: 2015-07-10 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 32
			recordDate: 2015-07-10 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 33
			recordDate: 2015-07-10 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 34
			recordDate: 2015-07-10 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 35
			recordDate: 2015-07-10 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 36
			recordDate: 2015-05-19 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 37
			recordDate: 2015-05-13 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 38
			recordDate: 2015-05-13 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 39
			recordDate: 2015-04-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 40
			recordDate: 2015-04-09 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 41
			recordDate: 2015-04-09 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 42
			recordDate: 2015-04-01 00:00:00
			code: CTFR
			description: Final Rejection
		№ 43
			recordDate: 2015-03-10 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 44
			recordDate: 2015-02-02 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 45
			recordDate: 2015-01-29 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 46
			recordDate: 2015-01-29 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 47
			recordDate: 2014-11-20 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 48
			recordDate: 2014-11-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 49
			recordDate: 2014-11-13 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 50
			recordDate: 2014-11-13 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 51
			recordDate: 2014-08-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 52
			recordDate: 2014-08-29 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 53
			recordDate: 2014-08-29 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 54
			recordDate: 2014-08-25 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 55
			recordDate: 2014-08-22 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 56
			recordDate: 2014-08-22 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 57
			recordDate: 2014-02-05 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 58
			recordDate: 2014-02-05 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 59
			recordDate: 2014-02-05 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 60
			recordDate: 2013-12-17 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 61
			recordDate: 2013-05-16 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 62
			recordDate: 2013-05-16 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 63
			recordDate: 2013-02-21 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 64
			recordDate: 2012-12-31 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 65
			recordDate: 2012-12-31 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 66
			recordDate: 2012-12-31 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 67
			recordDate: 2012-12-31 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 68
			recordDate: 2012-12-27 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 69
			recordDate: 2012-12-13 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 70
			recordDate: 2012-12-13 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 71
			recordDate: 2012-12-13 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 72
			recordDate: 2012-12-12 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 73
			recordDate: 2012-11-15 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 74
			recordDate: 2012-11-14 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 75
			recordDate: 2012-11-14 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 76
			recordDate: 2012-11-14 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9342291
	applIdStr: 13677081
	appl_id_txt: 13677081
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13677081
	appSubCls: 170000
	appLocation: ELECTRONIC
	appAttrDockNumber: SEAZN.746A
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 79502
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: ()
			rankNo: 5
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV||5}
	appStatusDate: 2016-04-27T12:46:15Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-05-17T04:00:00Z
	APP_IND: 1
	appExamName: THATCHER, CLINT A
	appExamNameFacet: THATCHER, CLINT A
	firstInventorFile: No
	appClsSubCls: 717/170000
	appClsSubClsFacet: 717/170000
	applId: 13677081
	patentTitle: DISTRIBUTED UPDATE SERVICE
	appIntlPubNumber: 
	appConfrNumber: 5121
	appFilingDate: 2012-11-14T05:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Jiaqi  Guo
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Matthew D. Klein
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Gang  Li
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Baogang  Song
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 4
	appCls: 717
	_version_: 1580433198447329284
	lastUpdatedTimestamp: 2017-10-05T15:49:10.125Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 683ba366-d23a-40da-948e-a872a584e98a
	responseHeader:
			status: 0
			QTime: 15
