DOCUMENT_BODY:
	appGrpArtNumber: 2695
	appGrpArtNumberFacet: 2695
	transactions:
		№ 0
			recordDate: 2014-11-04 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2014-11-04 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2014-10-16 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2014-10-15 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2014-10-03 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2014-10-02 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2014-10-01 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2014-10-01 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2014-09-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 9
			recordDate: 2014-09-30 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 10
			recordDate: 2014-09-25 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 11
			recordDate: 2014-07-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 12
			recordDate: 2014-07-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 13
			recordDate: 2014-07-02 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 14
			recordDate: 2014-06-27 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 15
			recordDate: 2014-06-26 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 16
			recordDate: 2014-06-18 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 17
			recordDate: 2014-06-12 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 18
			recordDate: 2014-06-12 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 19
			recordDate: 2014-06-12 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 20
			recordDate: 2014-06-12 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 21
			recordDate: 2014-05-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 22
			recordDate: 2014-05-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 23
			recordDate: 2014-05-23 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 24
			recordDate: 2014-05-19 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 25
			recordDate: 2014-05-15 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 26
			recordDate: 2014-05-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 27
			recordDate: 2014-05-12 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 28
			recordDate: 2014-05-12 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 29
			recordDate: 2014-05-12 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 30
			recordDate: 2014-05-08 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 31
			recordDate: 2014-05-07 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 32
			recordDate: 2014-05-07 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 33
			recordDate: 2014-05-07 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 34
			recordDate: 2014-05-07 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 35
			recordDate: 2014-05-07 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 36
			recordDate: 2014-05-07 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 37
			recordDate: 2014-04-15 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 38
			recordDate: 2014-04-10 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 39
			recordDate: 2014-04-10 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 40
			recordDate: 2014-03-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 41
			recordDate: 2014-03-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 42
			recordDate: 2014-03-12 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 43
			recordDate: 2014-03-07 00:00:00
			code: CTFR
			description: Final Rejection
		№ 44
			recordDate: 2014-03-03 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 45
			recordDate: 2014-01-21 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 46
			recordDate: 2014-01-08 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 47
			recordDate: 2014-01-08 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 48
			recordDate: 2014-01-08 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 49
			recordDate: 2014-01-08 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 50
			recordDate: 2013-11-26 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 51
			recordDate: 2013-11-21 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 52
			recordDate: 2013-11-21 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 53
			recordDate: 2013-10-03 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 54
			recordDate: 2013-10-03 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 55
			recordDate: 2013-10-03 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 56
			recordDate: 2013-09-29 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 57
			recordDate: 2013-07-01 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 58
			recordDate: 2013-07-01 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 59
			recordDate: 2013-07-01 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 60
			recordDate: 2013-07-01 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 61
			recordDate: 2013-05-24 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 62
			recordDate: 2013-05-17 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 63
			recordDate: 2013-05-13 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 64
			recordDate: 2013-05-13 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 65
			recordDate: 2013-05-13 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 66
			recordDate: 2013-05-13 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 67
			recordDate: 2013-05-13 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 68
			recordDate: 2013-05-10 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 69
			recordDate: 2013-05-10 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 70
			recordDate: 2013-04-03 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 71
			recordDate: 2013-03-29 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 72
			recordDate: 2013-03-15 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 73
			recordDate: 2013-03-15 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 74
			recordDate: 2013-03-15 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8878809
	applIdStr: 13843741
	appl_id_txt: 13843741
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13843741
	appSubCls: 173000
	appLocation: ELECTRONIC
	appAttrDockNumber: AM2-0236USC1
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 29150
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 5
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|5}
	appStatusDate: 2014-10-15T09:50:09Z
	LAST_MOD_TS: 2017-08-02T10:38:05Z
	patentIssueDate: 2014-11-04T05:00:00Z
	APP_IND: 1
	appExamName: GIESY, ADAM
	appExamNameFacet: GIESY, ADAM
	firstInventorFile: No
	appClsSubCls: 345/173000
	appClsSubClsFacet: 345/173000
	applId: 13843741
	patentTitle: TOUCH-SCREEN USER INTERFACE
	appIntlPubNumber: 
	appConfrNumber: 1810
	appFilingDate: 2013-03-15T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: John T. Kim
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: La Canada, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Joseph J. Hebenstreit
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Francisco, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Christopher  Green
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Francisco, 
			geoCode: CA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Kevin E. Keller
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Sunnyvale, 
			geoCode: CA
			country: (US)
			rankNo: 4
	appCls: 345
	_version_: 1580433232866836481
	lastUpdatedTimestamp: 2017-10-05T15:49:42.98Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 46476086-7610-45b6-b7a3-ac4922c8865a
	responseHeader:
			status: 0
			QTime: 16
