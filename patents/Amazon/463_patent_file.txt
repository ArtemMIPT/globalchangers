DOCUMENT_BODY:
	appGrpArtNumber: 2155
	appGrpArtNumberFacet: 2155
	transactions:
		№ 0
			recordDate: 2017-03-28 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2017-03-28 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2017-03-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2017-03-08 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2017-02-21 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2017-02-17 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2017-02-16 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2017-02-16 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2016-12-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2016-12-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2016-12-02 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2016-11-29 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2016-11-15 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 13
			recordDate: 2016-09-23 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 14
			recordDate: 2016-09-16 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 15
			recordDate: 2016-09-16 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 16
			recordDate: 2016-05-16 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 17
			recordDate: 2016-05-16 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 18
			recordDate: 2016-05-16 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 19
			recordDate: 2016-05-09 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 20
			recordDate: 2016-03-16 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 21
			recordDate: 2016-03-16 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 22
			recordDate: 2016-03-14 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 23
			recordDate: 2016-03-14 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 24
			recordDate: 2016-03-14 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 25
			recordDate: 2016-02-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 26
			recordDate: 2016-02-29 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 27
			recordDate: 2016-02-24 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 28
			recordDate: 2016-02-24 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 29
			recordDate: 2016-02-23 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 30
			recordDate: 2016-02-18 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 31
			recordDate: 2016-02-12 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 32
			recordDate: 2016-02-12 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 33
			recordDate: 2015-11-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 34
			recordDate: 2015-11-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 35
			recordDate: 2015-11-12 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 36
			recordDate: 2015-11-06 00:00:00
			code: CTFR
			description: Final Rejection
		№ 37
			recordDate: 2015-09-08 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 38
			recordDate: 2015-09-02 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 39
			recordDate: 2015-06-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 40
			recordDate: 2015-06-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 41
			recordDate: 2015-06-02 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 42
			recordDate: 2015-05-28 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 43
			recordDate: 2014-10-07 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 44
			recordDate: 2013-05-21 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 45
			recordDate: 2013-05-20 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 46
			recordDate: 2013-05-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 47
			recordDate: 2013-05-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 48
			recordDate: 2013-05-06 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 49
			recordDate: 2013-05-06 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 50
			recordDate: 2013-05-06 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 51
			recordDate: 2013-05-03 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 52
			recordDate: 2013-05-03 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 53
			recordDate: 2013-03-28 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 54
			recordDate: 2013-03-23 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 55
			recordDate: 2013-03-15 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 56
			recordDate: 2013-03-15 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9607019
	applIdStr: 13838298
	appl_id_txt: 13838298
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13838298
	appSubCls: 652000
	appLocation: ELECTRONIC
	appAttrDockNumber: 101058.000079
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 23377
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2017-03-08T11:00:37Z
	LAST_MOD_TS: 2017-09-05T13:35:09Z
	patentIssueDate: 2017-03-28T04:00:00Z
	APP_IND: 1
	appExamName: TRAN, LOC
	appExamNameFacet: TRAN, LOC
	firstInventorFile: No
	appClsSubCls: 707/652000
	appClsSubClsFacet: 707/652000
	applId: 13838298
	patentTitle: SPLITTING DATABASE PARTITIONS
	appIntlPubNumber: 
	appConfrNumber: 3713
	appFilingDate: 2013-03-15T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Bjorn Patrick Swift
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Maximiliano  Maccanti
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Stefano  Stefani
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Issaquah, 
			geoCode: WA
			country: (US)
			rankNo: 3
	appCls: 707
	_version_: 1580433231643148291
	lastUpdatedTimestamp: 2017-10-05T15:49:41.818Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 46476086-7610-45b6-b7a3-ac4922c8865a
	responseHeader:
			status: 0
			QTime: 16
