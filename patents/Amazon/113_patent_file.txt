DOCUMENT_BODY:
	appGrpArtNumber: 2458
	appGrpArtNumberFacet: 2458
	transactions:
		№ 0
			recordDate: 2017-02-14 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2017-02-14 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2017-01-26 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2017-01-25 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2017-01-04 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2017-01-03 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-12-28 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 7
			recordDate: 2016-12-28 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2016-12-28 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2016-11-03 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 10
			recordDate: 2016-10-27 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 11
			recordDate: 2016-10-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 12
			recordDate: 2016-10-21 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 13
			recordDate: 2016-10-21 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 14
			recordDate: 2016-10-18 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 15
			recordDate: 2016-10-18 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 16
			recordDate: 2016-10-18 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 17
			recordDate: 2016-10-11 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 18
			recordDate: 2016-09-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 19
			recordDate: 2016-09-29 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 20
			recordDate: 2016-09-29 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 21
			recordDate: 2016-09-25 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 22
			recordDate: 2016-09-25 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 23
			recordDate: 2016-09-19 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 24
			recordDate: 2016-09-19 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 25
			recordDate: 2016-06-21 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 26
			recordDate: 2016-03-15 00:00:00
			code: APBR
			description: Appeal Brief Review Complete
		№ 27
			recordDate: 2016-03-09 00:00:00
			code: T1OFF
			description: track 1 OFF
		№ 28
			recordDate: 2016-03-09 00:00:00
			code: AP.B
			description: Appeal Brief Filed
		№ 29
			recordDate: 2016-03-09 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 30
			recordDate: 2016-02-08 00:00:00
			code: MAPCP
			description: Mail Appeals conf. Proceed to BPAI
		№ 31
			recordDate: 2016-02-03 00:00:00
			code: APCP
			description: Pre-Appeals Conference Decision - Proceed to BPAI
		№ 32
			recordDate: 2015-12-15 00:00:00
			code: AP.C
			description: Request for Pre-Appeal Conference Filed
		№ 33
			recordDate: 2015-12-15 00:00:00
			code: N/AP
			description: Notice of Appeal Filed
		№ 34
			recordDate: 2015-10-15 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 35
			recordDate: 2015-10-15 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 36
			recordDate: 2015-10-15 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 37
			recordDate: 2015-10-08 00:00:00
			code: CTFR
			description: Final Rejection
		№ 38
			recordDate: 2015-07-20 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 39
			recordDate: 2015-07-13 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 40
			recordDate: 2015-07-13 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 41
			recordDate: 2015-04-27 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 42
			recordDate: 2015-04-24 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 43
			recordDate: 2015-02-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 44
			recordDate: 2015-02-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 45
			recordDate: 2015-02-13 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 46
			recordDate: 2015-02-08 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 47
			recordDate: 2014-12-30 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 48
			recordDate: 2014-12-22 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 49
			recordDate: 2014-08-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 50
			recordDate: 2013-02-04 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 51
			recordDate: 2013-01-07 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 52
			recordDate: 2012-12-21 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 53
			recordDate: 2012-12-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 54
			recordDate: 2012-12-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 55
			recordDate: 2012-12-21 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 56
			recordDate: 2012-12-21 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 57
			recordDate: 2012-12-20 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 58
			recordDate: 2012-11-29 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 59
			recordDate: 2012-11-21 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 60
			recordDate: 2012-11-21 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 61
			recordDate: 2012-11-21 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9571331
	applIdStr: 13683658
	appl_id_txt: 13683658
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13683658
	appSubCls: 220000
	appLocation: ELECTRONIC
	appAttrDockNumber: 170114-1190
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|2}
	appStatusDate: 2017-01-25T10:27:38Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2017-02-14T05:00:00Z
	APP_IND: 1
	appExamName: DINH, KHANH Q
	appExamNameFacet: DINH, KHANH Q
	firstInventorFile: No
	appClsSubCls: 709/220000
	appClsSubClsFacet: 709/220000
	applId: 13683658
	patentTitle: TECHNIQUES FOR ACCESSING LOCAL NETWORKS VIA A VIRTUALIZED GATEWAY
	appIntlPubNumber: 
	appConfrNumber: 3919
	appFilingDate: 2012-11-21T05:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Ahmed Fuad Siddiqui
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Everett, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 709
	_version_: 1580433199622782979
	lastUpdatedTimestamp: 2017-10-05T15:49:11.277Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 387aa367-becb-4d53-bb4d-b05b5e55e7d5
	responseHeader:
			status: 0
			QTime: 17
