DOCUMENT_BODY:
	appGrpArtNumber: 2456
	appGrpArtNumberFacet: 2456
	transactions:
		№ 0
			recordDate: 2016-09-13 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-09-13 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-08-25 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-08-24 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-08-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2016-08-17 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 6
			recordDate: 2016-08-17 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 7
			recordDate: 2016-08-16 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 8
			recordDate: 2016-08-12 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 9
			recordDate: 2016-08-12 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 10
			recordDate: 2016-08-12 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 11
			recordDate: 2016-08-11 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 12
			recordDate: 2016-08-11 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 13
			recordDate: 2016-08-10 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 14
			recordDate: 2016-08-09 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 15
			recordDate: 2016-08-09 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 16
			recordDate: 2016-08-09 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 17
			recordDate: 2016-05-25 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 18
			recordDate: 2016-05-25 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 19
			recordDate: 2016-05-25 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 20
			recordDate: 2016-05-23 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 21
			recordDate: 2016-05-17 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 22
			recordDate: 2016-04-04 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 23
			recordDate: 2016-03-24 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 24
			recordDate: 2016-03-24 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 25
			recordDate: 2016-03-02 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 26
			recordDate: 2016-03-02 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 27
			recordDate: 2016-02-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 28
			recordDate: 2016-02-19 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 29
			recordDate: 2016-02-12 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 30
			recordDate: 2016-02-10 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 31
			recordDate: 2016-02-02 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 32
			recordDate: 2016-01-07 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 33
			recordDate: 2016-01-04 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 34
			recordDate: 2015-12-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 35
			recordDate: 2015-12-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 36
			recordDate: 2015-12-02 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 37
			recordDate: 2015-11-27 00:00:00
			code: CTFR
			description: Final Rejection
		№ 38
			recordDate: 2015-10-27 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 39
			recordDate: 2015-10-21 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 40
			recordDate: 2015-10-20 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 41
			recordDate: 2015-10-14 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 42
			recordDate: 2015-07-21 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 43
			recordDate: 2015-07-21 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 44
			recordDate: 2015-07-21 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 45
			recordDate: 2015-07-15 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 46
			recordDate: 2015-07-13 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 47
			recordDate: 2015-03-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 48
			recordDate: 2015-03-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 49
			recordDate: 2015-03-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 50
			recordDate: 2014-03-27 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 51
			recordDate: 2013-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 52
			recordDate: 2013-05-20 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 53
			recordDate: 2013-04-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 54
			recordDate: 2013-04-02 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 55
			recordDate: 2013-04-02 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 56
			recordDate: 2013-04-02 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 57
			recordDate: 2013-04-02 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 58
			recordDate: 2013-04-02 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 59
			recordDate: 2013-04-01 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 60
			recordDate: 2013-04-01 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 61
			recordDate: 2013-03-04 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 62
			recordDate: 2013-03-01 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 63
			recordDate: 2013-02-28 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 64
			recordDate: 2013-02-28 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9444717
	applIdStr: 13781347
	appl_id_txt: 13781347
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13781347
	appSubCls: 224000
	appLocation: ELECTRONIC
	appAttrDockNumber: 101058.000024
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 23377
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|3}
	appStatusDate: 2016-08-24T10:30:21Z
	LAST_MOD_TS: 2017-04-11T15:01:47Z
	patentIssueDate: 2016-09-13T04:00:00Z
	APP_IND: 1
	appExamName: CHEN, WUJI
	appExamNameFacet: CHEN, WUJI
	firstInventorFile: No
	appClsSubCls: 709/224000
	appClsSubClsFacet: 709/224000
	applId: 13781347
	patentTitle: TEST GENERATION SERVICE
	appIntlPubNumber: 
	appConfrNumber: 1932
	appFilingDate: 2013-02-28T05:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Anirudh Balachandra Aithal
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Michael David Marr
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Monroe, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 709
	_version_: 1580433219996614659
	lastUpdatedTimestamp: 2017-10-05T15:49:30.693Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 0a00969b-d87b-44b3-8fd5-1d76e5a8df75
	responseHeader:
			status: 0
			QTime: 18
