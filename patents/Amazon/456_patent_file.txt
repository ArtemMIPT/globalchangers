DOCUMENT_BODY:
	appGrpArtNumber: 3628
	appGrpArtNumberFacet: 3628
	transactions:
		№ 0
			recordDate: 2016-10-11 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-10-11 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-09-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-09-21 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-09-09 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-09-08 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-09-07 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2016-09-07 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2016-06-07 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2016-06-07 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2016-06-07 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2016-06-03 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2016-05-20 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 13
			recordDate: 2016-05-20 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 14
			recordDate: 2016-05-17 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 15
			recordDate: 2016-05-16 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 16
			recordDate: 2016-05-16 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 17
			recordDate: 2016-05-16 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 18
			recordDate: 2016-03-16 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 19
			recordDate: 2016-03-16 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 20
			recordDate: 2016-03-16 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 21
			recordDate: 2016-03-09 00:00:00
			code: CTFR
			description: Final Rejection
		№ 22
			recordDate: 2016-01-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 23
			recordDate: 2016-01-04 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 24
			recordDate: 2015-10-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 25
			recordDate: 2015-10-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 26
			recordDate: 2015-10-02 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 27
			recordDate: 2015-09-28 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 28
			recordDate: 2015-07-17 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 29
			recordDate: 2015-07-17 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 30
			recordDate: 2015-07-16 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 31
			recordDate: 2015-07-16 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 32
			recordDate: 2015-07-02 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 33
			recordDate: 2015-07-02 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 34
			recordDate: 2015-06-26 00:00:00
			code: AFIR
			description: After Final Consideration Program Improper Request
		№ 35
			recordDate: 2015-06-26 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 36
			recordDate: 2015-06-22 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 37
			recordDate: 2015-06-17 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 38
			recordDate: 2015-06-17 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 39
			recordDate: 2015-04-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 40
			recordDate: 2015-04-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 41
			recordDate: 2015-04-17 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 42
			recordDate: 2015-04-13 00:00:00
			code: CTFR
			description: Final Rejection
		№ 43
			recordDate: 2015-02-20 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 44
			recordDate: 2015-02-16 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 45
			recordDate: 2014-11-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 46
			recordDate: 2014-11-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 47
			recordDate: 2014-11-14 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 48
			recordDate: 2014-11-10 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 49
			recordDate: 2014-11-10 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 50
			recordDate: 2013-08-20 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 51
			recordDate: 2013-05-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 52
			recordDate: 2013-05-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 53
			recordDate: 2013-05-01 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 54
			recordDate: 2013-05-01 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 55
			recordDate: 2013-05-01 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 56
			recordDate: 2013-04-30 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 57
			recordDate: 2013-04-30 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 58
			recordDate: 2013-03-28 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 59
			recordDate: 2013-03-20 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 60
			recordDate: 2013-03-15 00:00:00
			code: A.PE
			description: Preliminary Amendment
		№ 61
			recordDate: 2013-03-15 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 62
			recordDate: 2013-03-15 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 63
			recordDate: 2013-03-15 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 64
			recordDate: 2013-03-15 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 65
			recordDate: 2013-03-15 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 66
			recordDate: 2013-03-15 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9466043
	applIdStr: 13833970
	appl_id_txt: 13833970
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13833970
	appSubCls: 330000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-18001
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 8
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|8}
	appStatusDate: 2016-09-21T10:53:58Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-10-11T04:00:00Z
	APP_IND: 1
	appExamName: CHEN, GEORGE YUNG CHIEH
	appExamNameFacet: CHEN, GEORGE YUNG CHIEH
	firstInventorFile: No
	appClsSubCls: 705/330000
	appClsSubClsFacet: 705/330000
	applId: 13833970
	patentTitle: SYSTEM AND METHOD FOR GENERATING SHIPMENT FORECASTS FOR MATERIALS HANDLING FACILITIES
	appIntlPubNumber: 
	appConfrNumber: 9456
	appFilingDate: 2013-03-15T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: RICHA  AGARWAL
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: ISSAQUAH, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: THOMAS YVES PAUL HELLEBOID
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: SHYAM  MISHRA
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: BELLEVUE, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: LIN  WAN
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: BELLEVUE, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: SIMON M. PATRICK
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 5
		№ 5
			nameLineOne: JIN  LAI
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: BEIJING, 
			geoCode: 
			country: (CN)
			rankNo: 6
		№ 6
			nameLineOne: MICHAEL MAHESH BHASKARAN
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: BELLEVUE, 
			geoCode: WA
			country: (US)
			rankNo: 7
	appCls: 705
	_version_: 1580433230761295877
	lastUpdatedTimestamp: 2017-10-05T15:49:40.987Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 791bb353-5cc2-4f42-8948-5645f9c7ff30
	responseHeader:
			status: 0
			QTime: 19
