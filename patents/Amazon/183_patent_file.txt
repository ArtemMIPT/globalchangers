DOCUMENT_BODY:
	appGrpArtNumber: 2196
	appGrpArtNumberFacet: 2196
	transactions:
		№ 0
			recordDate: 2015-06-09 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-06-09 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-05-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-05-20 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-05-05 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-05-05 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-05-04 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-05-04 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-02-03 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2015-02-03 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2015-02-03 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2015-01-29 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2015-01-27 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 13
			recordDate: 2015-01-27 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 14
			recordDate: 2015-01-27 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 15
			recordDate: 2015-01-27 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 16
			recordDate: 2014-12-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 17
			recordDate: 2014-12-17 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 18
			recordDate: 2014-09-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 19
			recordDate: 2014-09-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 20
			recordDate: 2014-09-17 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 21
			recordDate: 2014-09-09 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 22
			recordDate: 2014-06-17 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 23
			recordDate: 2014-05-09 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 24
			recordDate: 2013-05-10 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 25
			recordDate: 2013-02-12 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 26
			recordDate: 2013-01-28 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 27
			recordDate: 2013-01-28 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 28
			recordDate: 2013-01-18 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 29
			recordDate: 2013-01-18 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 30
			recordDate: 2013-01-18 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 31
			recordDate: 2013-01-17 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 32
			recordDate: 2012-12-19 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 33
			recordDate: 2012-12-19 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 34
			recordDate: 2012-12-19 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 35
			recordDate: 2012-12-14 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 36
			recordDate: 2012-12-14 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 37
			recordDate: 2012-12-14 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 38
			recordDate: 2012-12-14 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9052942
	applIdStr: 13715883
	appl_id_txt: 13715883
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13715883
	appSubCls: 102000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-48900
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 6
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|6}
	appStatusDate: 2015-05-20T11:27:00Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-06-09T04:00:00Z
	APP_IND: 1
	appExamName: LEE, ADAM
	appExamNameFacet: LEE, ADAM
	firstInventorFile: No
	appClsSubCls: 718/102000
	appClsSubClsFacet: 718/102000
	applId: 13715883
	patentTitle: STORAGE OBJECT DELETION JOB MANAGEMENT
	appIntlPubNumber: 
	appConfrNumber: 9193
	appFilingDate: 2012-12-14T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: JEFFREY MICHAEL BARBER
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Praveen Kumar Gattu
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Redmond, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Derek Ernest Denny-Brown II
			nameLineTwo:  
			suffix: II
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Carl Yates Perry
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Christopher Henning Elving
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Sunnyvale, 
			geoCode: CA
			country: (US)
			rankNo: 5
	appCls: 718
	_version_: 1580433206370369536
	lastUpdatedTimestamp: 2017-10-05T15:49:17.657Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 6127cd1c-1e1f-490f-8308-652c641ee1b3
	responseHeader:
			status: 0
			QTime: 15
