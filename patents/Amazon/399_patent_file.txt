DOCUMENT_BODY:
	appGrpArtNumber: 2154
	appGrpArtNumberFacet: 2154
	transactions:
		№ 0
			recordDate: 2015-11-05 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 1
			recordDate: 2015-11-05 00:00:00
			code: FLRCPT.C
			description: Filing Receipt - Corrected
		№ 2
			recordDate: 2015-10-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 3
			recordDate: 2015-10-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 4
			recordDate: 2015-10-14 00:00:00
			code: MPEN
			description: Mail Pre-Exam Notice
		№ 5
			recordDate: 2015-10-13 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 6
			recordDate: 2015-10-13 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 7
			recordDate: 2015-09-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 8
			recordDate: 2015-09-23 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 9
			recordDate: 2015-09-09 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 10
			recordDate: 2015-09-03 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 11
			recordDate: 2015-09-03 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 12
			recordDate: 2015-09-03 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 13
			recordDate: 2015-08-13 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 14
			recordDate: 2015-08-13 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 15
			recordDate: 2015-08-13 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 16
			recordDate: 2015-08-08 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 17
			recordDate: 2015-07-30 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 18
			recordDate: 2015-07-28 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 19
			recordDate: 2015-07-28 00:00:00
			code: MR327
			description: RX - Mail Miscellaneous Communication to Applicant
		№ 20
			recordDate: 2015-07-16 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 21
			recordDate: 2015-07-06 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 22
			recordDate: 2015-07-06 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 23
			recordDate: 2015-07-01 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 24
			recordDate: 2015-06-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 25
			recordDate: 2015-06-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 26
			recordDate: 2015-06-17 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 27
			recordDate: 2015-06-01 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 28
			recordDate: 2015-06-01 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 29
			recordDate: 2015-06-01 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 30
			recordDate: 2015-05-19 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 31
			recordDate: 2015-05-19 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 32
			recordDate: 2015-05-13 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 33
			recordDate: 2015-05-11 00:00:00
			code: C614
			description: New or Additional Drawing Filed
		№ 34
			recordDate: 2015-05-11 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 35
			recordDate: 2015-03-11 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 36
			recordDate: 2015-03-03 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 37
			recordDate: 2015-03-03 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 38
			recordDate: 2015-02-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 39
			recordDate: 2015-02-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 40
			recordDate: 2015-02-12 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 41
			recordDate: 2015-02-07 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 42
			recordDate: 2015-02-07 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 43
			recordDate: 2014-10-06 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 44
			recordDate: 2014-10-06 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 45
			recordDate: 2014-09-03 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 46
			recordDate: 2013-05-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 47
			recordDate: 2013-05-03 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 48
			recordDate: 2013-04-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 49
			recordDate: 2013-04-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 50
			recordDate: 2013-04-19 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 51
			recordDate: 2013-04-19 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 52
			recordDate: 2013-04-19 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 53
			recordDate: 2013-04-18 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 54
			recordDate: 2013-04-18 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 55
			recordDate: 2013-03-16 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 56
			recordDate: 2013-03-12 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 57
			recordDate: 2013-03-12 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 58
			recordDate: 2013-03-12 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9158805
	applIdStr: 13796361
	appl_id_txt: 13796361
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13796361
	appSubCls: 694000
	appLocation: ELECTRONIC
	appAttrDockNumber: 579-7021
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 109263
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2015-09-23T12:54:20Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-10-13T04:00:00Z
	APP_IND: 1
	appExamName: ABRAHAM, AHMED M
	appExamNameFacet: ABRAHAM, AHMED M
	firstInventorFile: No
	appClsSubCls: 707/694000
	appClsSubClsFacet: 707/694000
	applId: 13796361
	patentTitle: STATISTICAL DATA QUALITY DETERMINATION FOR STORAGE SYSTEMS
	appIntlPubNumber: 
	appConfrNumber: 5486
	appFilingDate: 2013-03-12T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: ADAM STEPHEN DUNCAN
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: SANTOSH  KALKI
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SAMMAMISH, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 2
			nameLineOne: JENNY BANDY FRESHWATER
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 3
	appCls: 707
	_version_: 1580433222722912257
	lastUpdatedTimestamp: 2017-10-05T15:49:33.324Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 6073073c-cbc6-49b0-b9b5-714ce578e5c8
	responseHeader:
			status: 0
			QTime: 17
