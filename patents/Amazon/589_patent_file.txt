DOCUMENT_BODY:
	transactions:
		№ 0
			recordDate: 2013-12-01 00:00:00
			code: EXPRO
			description: EXPIRED PROVISIONAL
		№ 1
			recordDate: 2012-12-18 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 2
			recordDate: 2012-12-18 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2012-12-18 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 4
			recordDate: 2012-12-17 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 5
			recordDate: 2012-11-26 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 6
			recordDate: 2012-11-26 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 7
			recordDate: 2012-11-26 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 8
			recordDate: 2012-11-26 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Provisional Application Expired
	appStatus_txt: Provisional Application Expired
	appStatusFacet: Provisional Application Expired
	patentNumber: 
	applIdStr: 61730024
	appl_id_txt: 61730024
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 61730024
	appSubCls: 
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-46900
	appType: Provisional
	appTypeFacet: Provisional
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 8
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|8}
	appStatusDate: 2013-12-02T01:33:29Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	APP_IND: 1
	firstInventorFile: Other
	applId: 61730024
	patentTitle: STREAMING RESTORE OF A DATABASE FROM A BACKUP SYSTEM
	appIntlPubNumber: 
	appConfrNumber: 6860
	appFilingDate: 2012-11-26T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: JAKUB  KULESZA
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: BELLEVUE, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: ANURAG WINDLASS GUPTA
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: ATHERTON, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: ZELAINE  FONG
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SAN CARLOS, 
			geoCode: CA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: DEEPAK  AGARWAL
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: REDMOND, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: ALEKSANDRAS  SURNA
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: REDMOND, 
			geoCode: WA
			country: (US)
			rankNo: 5
		№ 5
			nameLineOne: STEFANO  STEFANI
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: ISSAQUAH, 
			geoCode: WA
			country: (US)
			rankNo: 6
		№ 6
			nameLineOne: TUSHAR  JAIN
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SAN FRANCISCO, 
			geoCode: CA
			country: (US)
			rankNo: 7
	appCls: 
	_version_: 1580434066960809984
	lastUpdatedTimestamp: 2017-10-05T16:02:58.436Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: dd2929a0-c346-4dea-bfb2-0f0118a13a85
	responseHeader:
			status: 0
			QTime: 16
