DOCUMENT_BODY:
	appGrpArtNumber: 2163
	appGrpArtNumberFacet: 2163
	appStatus: Docketed New Case - Ready for Examination
	appStatus_txt: Docketed New Case - Ready for Examination
	appStatusFacet: Docketed New Case - Ready for Examination
	patentNumber: 
	applIdStr: 13776498
	appl_id_txt: 13776498
	appEarlyPubNumber: US20140244580A1
	appEntityStatus: UNDISCOUNTED
	id: 13776498
	appSubCls: 623000
	appLocation: ELECTRONIC
	appAttrDockNumber: 090204-0855763 (061300US)
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 107508
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2017-08-19T16:44:14Z
	LAST_MOD_TS: 2017-11-22T19:39:05Z
	appEarlyPubDate: 2014-08-28T04:00:00Z
	APP_IND: 1
	appExamName: MIAN, MUHAMMAD U
	appExamNameFacet: MIAN, MUHAMMAD U
	firstInventorFile: No
	appClsSubCls: 707/623000
	appClsSubClsFacet: 707/623000
	applId: 13776498
	patentTitle: PREDICTIVE STORAGE SERVICE
	appIntlPubNumber: 
	transactions:
		№ 0
			recordDate: 2017-11-21 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 1
			recordDate: 2017-11-21 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 2
			recordDate: 2017-11-13 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 3
			recordDate: 2017-08-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 4
			recordDate: 2017-08-19 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 5
			recordDate: 2017-08-15 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 6
			recordDate: 2017-08-15 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 7
			recordDate: 2017-08-04 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 8
			recordDate: 2017-07-26 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 9
			recordDate: 2017-06-20 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 10
			recordDate: 2017-06-20 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 11
			recordDate: 2017-05-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 12
			recordDate: 2017-05-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 13
			recordDate: 2017-05-17 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 14
			recordDate: 2017-05-13 00:00:00
			code: CTFR
			description: Final Rejection
		№ 15
			recordDate: 2017-05-12 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 16
			recordDate: 2017-05-12 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 17
			recordDate: 2017-03-29 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 18
			recordDate: 2017-03-29 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 19
			recordDate: 2017-03-28 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 20
			recordDate: 2017-03-10 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 21
			recordDate: 2017-03-10 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 22
			recordDate: 2017-03-06 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 23
			recordDate: 2017-02-27 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 24
			recordDate: 2017-01-24 00:00:00
			code: M865E
			description: Electronic request for Examiner Interview
		№ 25
			recordDate: 2016-12-13 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 26
			recordDate: 2016-12-13 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 27
			recordDate: 2016-11-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 28
			recordDate: 2016-11-29 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 29
			recordDate: 2016-11-29 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 30
			recordDate: 2016-11-17 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 31
			recordDate: 2016-11-17 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 32
			recordDate: 2016-11-17 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 33
			recordDate: 2016-09-14 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 34
			recordDate: 2016-09-14 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 35
			recordDate: 2016-09-14 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 36
			recordDate: 2016-08-03 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 37
			recordDate: 2016-08-03 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 38
			recordDate: 2016-07-20 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 39
			recordDate: 2016-07-20 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 40
			recordDate: 2016-07-11 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 41
			recordDate: 2016-07-05 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 42
			recordDate: 2016-06-11 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 43
			recordDate: 2016-06-11 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 44
			recordDate: 2016-04-28 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 45
			recordDate: 2016-04-28 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 46
			recordDate: 2016-04-28 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 47
			recordDate: 2016-04-25 00:00:00
			code: CTFR
			description: Final Rejection
		№ 48
			recordDate: 2016-04-22 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 49
			recordDate: 2016-03-18 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 50
			recordDate: 2016-03-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 51
			recordDate: 2016-01-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 52
			recordDate: 2015-12-31 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 53
			recordDate: 2015-12-10 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 54
			recordDate: 2015-12-04 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 55
			recordDate: 2015-10-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 56
			recordDate: 2015-10-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 57
			recordDate: 2015-10-01 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 58
			recordDate: 2015-09-26 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 59
			recordDate: 2015-07-31 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 60
			recordDate: 2015-07-31 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 61
			recordDate: 2015-07-27 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 62
			recordDate: 2015-07-27 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 63
			recordDate: 2015-06-15 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 64
			recordDate: 2015-06-10 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 65
			recordDate: 2015-06-10 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 66
			recordDate: 2015-04-27 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 67
			recordDate: 2015-04-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 68
			recordDate: 2015-04-27 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 69
			recordDate: 2015-04-20 00:00:00
			code: CTFR
			description: Final Rejection
		№ 70
			recordDate: 2015-04-16 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 71
			recordDate: 2015-04-15 00:00:00
			code: SA..
			description: Supplemental Response
		№ 72
			recordDate: 2015-04-14 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 73
			recordDate: 2015-04-14 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 74
			recordDate: 2015-01-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 75
			recordDate: 2015-01-26 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 76
			recordDate: 2015-01-09 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 77
			recordDate: 2015-01-06 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 78
			recordDate: 2015-01-06 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 79
			recordDate: 2014-10-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 80
			recordDate: 2014-10-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 81
			recordDate: 2014-10-24 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 82
			recordDate: 2014-10-20 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 83
			recordDate: 2014-10-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 84
			recordDate: 2014-10-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 85
			recordDate: 2014-10-09 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 86
			recordDate: 2014-10-09 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 87
			recordDate: 2014-10-09 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 88
			recordDate: 2014-08-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 89
			recordDate: 2014-08-28 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 90
			recordDate: 2014-07-31 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 91
			recordDate: 2014-03-10 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 92
			recordDate: 2014-03-10 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 93
			recordDate: 2014-03-10 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 94
			recordDate: 2014-03-07 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 95
			recordDate: 2014-03-07 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 96
			recordDate: 2014-03-06 00:00:00
			code: PG-PB-DT
			description: PG-Pub Notice of new or Revised projected publication date
		№ 97
			recordDate: 2014-02-25 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 98
			recordDate: 2013-04-22 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 99
			recordDate: 2013-04-19 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 100
			recordDate: 2013-03-27 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 101
			recordDate: 2013-03-27 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 102
			recordDate: 2013-03-27 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 103
			recordDate: 2013-03-27 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 104
			recordDate: 2013-03-27 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 105
			recordDate: 2013-03-26 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 106
			recordDate: 2013-02-27 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 107
			recordDate: 2013-02-26 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 108
			recordDate: 2013-02-25 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 109
			recordDate: 2013-02-25 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 110
			recordDate: 2013-02-25 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 111
			recordDate: 2013-02-25 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 112
			recordDate: 2013-02-25 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appConfrNumber: 9425
	appFilingDate: 2013-02-25T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Noah Anthony Eisner
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Menlo Park, 
			geoCode: CA
			country: (US)
			rankNo: 1
	appCls: 707
	_version_: 1584815607811932163
	lastUpdatedTimestamp: 2017-11-23T00:45:41.402Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 0a00969b-d87b-44b3-8fd5-1d76e5a8df75
	responseHeader:
			status: 0
			QTime: 18
