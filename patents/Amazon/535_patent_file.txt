DOCUMENT_BODY:
	appGrpArtNumber: 2833
	appGrpArtNumberFacet: 2833
	transactions:
		№ 0
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 1
			recordDate: 2015-01-27 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2015-01-27 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2015-01-07 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2014-12-22 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2014-12-22 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2014-12-15 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2014-12-15 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2014-09-29 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 9
			recordDate: 2014-09-27 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 10
			recordDate: 2014-09-27 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 11
			recordDate: 2014-09-24 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 12
			recordDate: 2014-06-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 13
			recordDate: 2014-03-11 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 14
			recordDate: 2013-06-26 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 15
			recordDate: 2013-06-04 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 16
			recordDate: 2013-06-04 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 17
			recordDate: 2013-06-04 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 18
			recordDate: 2013-06-04 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 19
			recordDate: 2013-06-04 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 20
			recordDate: 2013-04-28 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 21
			recordDate: 2013-04-26 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 22
			recordDate: 2013-04-25 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 23
			recordDate: 2013-04-25 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 24
			recordDate: 2013-04-25 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8939793
	applIdStr: 13870083
	appl_id_txt: 13870083
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13870083
	appSubCls: 573000
	appLocation: ELECTRONIC
	appAttrDockNumber: 08862.197 (L197)
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 14432
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2015-01-07T10:47:16Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-01-27T05:00:00Z
	APP_IND: 1
	appExamName: PAUMEN, GARY F
	appExamNameFacet: PAUMEN, GARY F
	firstInventorFile: Yes
	appClsSubCls: 439/573000
	appClsSubClsFacet: 439/573000
	applId: 13870083
	patentTitle: THROUGH-MOUNTED DEVICE CONNECTOR
	appIntlPubNumber: 
	appConfrNumber: 1613
	appFilingDate: 2013-04-25T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Valentin Shaun de la Fuente
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Jose, 
			geoCode: CA
			country: (US)
			rankNo: 1
	appCls: 439
	_version_: 1580433238172631040
	lastUpdatedTimestamp: 2017-10-05T15:49:48.059Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: a2cc947d-62d2-4cea-96b3-f2488704cc44
	responseHeader:
			status: 0
			QTime: 16
