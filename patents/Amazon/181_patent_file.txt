DOCUMENT_BODY:
	appGrpArtNumber: 2448
	appGrpArtNumberFacet: 2448
	transactions:
		№ 0
			recordDate: 2017-05-23 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9658983
	applIdStr: 13715863
	appl_id_txt: 13715863
	appEarlyPubNumber: 
	appEntityStatus:  
	id: 13715863
	appSubCls: 213000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-49100
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 6
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|6}
	appStatusDate: 2017-05-03T08:32:52Z
	LAST_MOD_TS: 2017-05-23T12:49:17Z
	patentIssueDate: 2017-05-23T04:00:00Z
	APP_IND: 1
	appExamName: BARRY, LANCE LEONARD
	appExamNameFacet: BARRY, LANCE LEONARD
	firstInventorFile: No
	appClsSubCls: 709/213000
	appClsSubClsFacet: 709/213000
	applId: 13715863
	patentTitle: LIFECYCLE SUPPORT FOR STORAGE OBJECTS HAVING MULTIPLE DURABILITY LEVELS SPECIFYING DIFFERENT NUMBERS OF VERSIONS
	appIntlPubNumber: 
	appConfrNumber: 2157
	appFilingDate: 2012-12-14T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: JEFFREY MICHAEL BARBER
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Praveen Kumar Gattu
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Redmond, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Derek Ernest Denny-Brown II
			nameLineTwo:  
			suffix: II
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Christopher Henning Elving
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Sunnyvale, 
			geoCode: CA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Carl Yates Perry
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 5
	appCls: 709
	_version_: 1580433206367223808
	lastUpdatedTimestamp: 2017-10-05T15:49:17.657Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 6127cd1c-1e1f-490f-8308-652c641ee1b3
	responseHeader:
			status: 0
			QTime: 15
