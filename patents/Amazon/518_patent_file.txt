DOCUMENT_BODY:
	appGrpArtNumber: 2442
	appGrpArtNumberFacet: 2442
	transactions:
		№ 0
			recordDate: 2017-01-24 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2017-01-24 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2017-01-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2017-01-04 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-12-19 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-12-15 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-12-13 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 7
			recordDate: 2016-12-13 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2016-12-13 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2016-11-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 10
			recordDate: 2016-11-14 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 11
			recordDate: 2016-11-14 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 12
			recordDate: 2016-11-07 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 13
			recordDate: 2016-11-07 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 14
			recordDate: 2016-11-04 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 15
			recordDate: 2016-11-03 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 16
			recordDate: 2016-11-03 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 17
			recordDate: 2016-10-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 18
			recordDate: 2016-10-17 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 19
			recordDate: 2016-10-11 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 20
			recordDate: 2016-10-11 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 21
			recordDate: 2016-10-10 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 22
			recordDate: 2016-10-07 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 23
			recordDate: 2016-10-07 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 24
			recordDate: 2016-10-07 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 25
			recordDate: 2016-09-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 26
			recordDate: 2016-09-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 27
			recordDate: 2016-09-13 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 28
			recordDate: 2016-09-09 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 29
			recordDate: 2016-09-06 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 30
			recordDate: 2016-09-06 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 31
			recordDate: 2016-08-30 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 32
			recordDate: 2016-08-30 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 33
			recordDate: 2016-08-29 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 34
			recordDate: 2016-08-29 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 35
			recordDate: 2016-08-29 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 36
			recordDate: 2016-08-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 37
			recordDate: 2016-08-04 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 38
			recordDate: 2016-07-30 00:00:00
			code: AFNE
			description: After Final Consideration Program Amendment too Extensive
		№ 39
			recordDate: 2016-07-30 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 40
			recordDate: 2016-07-28 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 41
			recordDate: 2016-07-26 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 42
			recordDate: 2016-04-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 43
			recordDate: 2016-04-29 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 44
			recordDate: 2016-04-29 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 45
			recordDate: 2016-04-26 00:00:00
			code: CTFR
			description: Final Rejection
		№ 46
			recordDate: 2016-04-25 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 47
			recordDate: 2016-04-25 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 48
			recordDate: 2016-02-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 49
			recordDate: 2016-02-18 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 50
			recordDate: 2015-11-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 51
			recordDate: 2015-11-18 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 52
			recordDate: 2015-11-18 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 53
			recordDate: 2015-11-13 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 54
			recordDate: 2015-11-13 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 55
			recordDate: 2015-11-13 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 56
			recordDate: 2015-11-13 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 57
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 58
			recordDate: 2015-04-10 00:00:00
			code: C602
			description: Oath or Declaration Filed (Including Supplemental)
		№ 59
			recordDate: 2015-03-26 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 60
			recordDate: 2015-03-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 61
			recordDate: 2015-03-11 00:00:00
			code: C602
			description: Oath or Declaration Filed (Including Supplemental)
		№ 62
			recordDate: 2015-01-14 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 63
			recordDate: 2014-11-05 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 64
			recordDate: 2014-11-05 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 65
			recordDate: 2014-10-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 66
			recordDate: 2014-10-16 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 67
			recordDate: 2014-04-22 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 68
			recordDate: 2014-04-22 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 69
			recordDate: 2014-04-22 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 70
			recordDate: 2014-04-16 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 71
			recordDate: 2014-02-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 72
			recordDate: 2013-07-17 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 73
			recordDate: 2013-05-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 74
			recordDate: 2013-05-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 75
			recordDate: 2013-05-29 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 76
			recordDate: 2013-05-29 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 77
			recordDate: 2013-05-29 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 78
			recordDate: 2013-05-28 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 79
			recordDate: 2013-05-28 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 80
			recordDate: 2013-04-18 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 81
			recordDate: 2013-04-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 82
			recordDate: 2013-04-18 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 83
			recordDate: 2013-04-16 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 84
			recordDate: 2013-04-16 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 85
			recordDate: 2013-04-16 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 86
			recordDate: 2013-04-16 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 87
			recordDate: 2013-04-16 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9553809
	applIdStr: 13864157
	appl_id_txt: 13864157
	appEarlyPubNumber: US20140310390A1
	appEntityStatus: UNDISCOUNTED
	id: 13864157
	appSubCls: 223000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-52400
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 6
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|6}
	appStatusDate: 2017-01-04T12:14:13Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-10-16T04:00:00Z
	patentIssueDate: 2017-01-24T05:00:00Z
	APP_IND: 1
	appExamName: NGUYEN, ANGELA
	appExamNameFacet: NGUYEN, ANGELA
	firstInventorFile: Yes
	appClsSubCls: 709/223000
	appClsSubClsFacet: 709/223000
	applId: 13864157
	patentTitle: ASYMMETRIC PACKET FLOW IN A DISTRIBUTED LOAD BALANCER
	appIntlPubNumber: 
	appConfrNumber: 1009
	appFilingDate: 2013-04-16T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: JAMES CHRISTOPHER SORENSON III
			nameLineTwo:  
			suffix: III
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: DOUGLAS STEWART LAURENCE
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: MERCER ISLAND, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: VENKATRAGHAVAN  SRINIVASAN
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: AKSHAY SUHAS VAIDYA
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: BOTHELL, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: FAN  ZHANG
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: BELLEVUE, 
			geoCode: WA
			country: (US)
			rankNo: 5
	appCls: 709
	_version_: 1580433237128249344
	lastUpdatedTimestamp: 2017-10-05T15:49:47.046Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: d87e7f50-ef6f-4911-a2f1-f087fb34a43a
	responseHeader:
			status: 0
			QTime: 17
