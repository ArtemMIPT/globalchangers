DOCUMENT_BODY:
	appGrpArtNumber: 2162
	appGrpArtNumberFacet: 2162
	transactions:
		№ 0
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 1
			recordDate: 2014-09-02 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2014-09-02 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2014-08-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2014-08-13 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2014-08-01 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2014-07-30 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2014-07-24 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2014-07-24 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 9
			recordDate: 2014-07-24 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 10
			recordDate: 2014-07-24 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 11
			recordDate: 2014-06-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 12
			recordDate: 2014-06-10 00:00:00
			code: MM327-O
			description: Mail PUBS Notice Requiring Inventors Oath or Declaration
		№ 13
			recordDate: 2014-06-06 00:00:00
			code: M327-O
			description: PUBS Notice Requiring Inventors Oath or Declaration
		№ 14
			recordDate: 2014-04-25 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 15
			recordDate: 2014-04-25 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 16
			recordDate: 2014-04-25 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 17
			recordDate: 2014-04-23 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 18
			recordDate: 2014-04-11 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 19
			recordDate: 2014-04-11 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 20
			recordDate: 2014-04-10 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 21
			recordDate: 2014-04-10 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 22
			recordDate: 2014-02-11 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 23
			recordDate: 2014-02-04 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 24
			recordDate: 2014-02-04 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 25
			recordDate: 2013-11-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 26
			recordDate: 2013-11-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 27
			recordDate: 2013-11-01 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 28
			recordDate: 2013-10-29 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 29
			recordDate: 2013-10-29 00:00:00
			code: A.PE
			description: Preliminary Amendment
		№ 30
			recordDate: 2013-10-29 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 31
			recordDate: 2013-05-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 32
			recordDate: 2013-05-20 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 33
			recordDate: 2013-05-20 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 34
			recordDate: 2013-05-20 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 35
			recordDate: 2013-05-17 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 36
			recordDate: 2013-05-17 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 37
			recordDate: 2013-04-09 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 38
			recordDate: 2013-04-03 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 39
			recordDate: 2013-04-01 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 40
			recordDate: 2013-04-01 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 41
			recordDate: 2013-04-01 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8825672
	applIdStr: 13854832
	appl_id_txt: 13854832
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13854832
	appSubCls: 749000
	appLocation: ELECTRONIC
	appAttrDockNumber: SEAZN.391C1
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 79502
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2014-08-13T11:31:55Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2014-09-02T04:00:00Z
	APP_IND: 1
	appExamName: NGUYEN, PHONG H
	appExamNameFacet: NGUYEN, PHONG H
	firstInventorFile: No
	appClsSubCls: 707/749000
	appClsSubClsFacet: 707/749000
	applId: 13854832
	patentTitle: SYSTEM AND METHOD FOR DETERMINING ORIGINALITY OF DATA CONTENT
	appIntlPubNumber: 
	appConfrNumber: 5340
	appFilingDate: 2013-04-01T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Jeffrey Matthew Bilger
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 707
	_version_: 1580433235023757316
	lastUpdatedTimestamp: 2017-10-05T15:49:45.042Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 8d2a2ffd-bf72-4810-a487-8f9a738c994e
	responseHeader:
			status: 0
			QTime: 17
