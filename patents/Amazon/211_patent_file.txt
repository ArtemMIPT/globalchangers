DOCUMENT_BODY:
	appGrpArtNumber: 2454
	appGrpArtNumberFacet: 2454
	transactions:
		№ 0
			recordDate: 2015-11-03 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-11-03 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-10-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-10-14 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-10-07 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-10-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2015-10-01 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 7
			recordDate: 2015-10-01 00:00:00
			code: MM327
			description: Mail Miscellaneous Communication to Applicant
		№ 8
			recordDate: 2015-09-27 00:00:00
			code: M327
			description: Miscellaneous Communication to Applicant - No Action Count
		№ 9
			recordDate: 2015-09-21 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 10
			recordDate: 2015-09-19 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 11
			recordDate: 2015-09-17 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 12
			recordDate: 2015-09-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 13
			recordDate: 2015-09-15 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 14
			recordDate: 2015-09-15 00:00:00
			code: MM327
			description: Mail Miscellaneous Communication to Applicant
		№ 15
			recordDate: 2015-09-07 00:00:00
			code: M327
			description: Miscellaneous Communication to Applicant - No Action Count
		№ 16
			recordDate: 2015-08-25 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 17
			recordDate: 2015-08-21 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 18
			recordDate: 2015-08-20 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 19
			recordDate: 2015-08-20 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 20
			recordDate: 2015-08-20 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 21
			recordDate: 2015-08-20 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 22
			recordDate: 2015-05-21 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 23
			recordDate: 2015-05-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 24
			recordDate: 2015-05-20 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 25
			recordDate: 2015-05-15 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 26
			recordDate: 2015-05-13 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 27
			recordDate: 2015-05-12 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 28
			recordDate: 2015-05-12 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 29
			recordDate: 2015-04-06 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 30
			recordDate: 2015-04-06 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 31
			recordDate: 2015-04-06 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 32
			recordDate: 2015-02-05 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 33
			recordDate: 2015-02-03 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 34
			recordDate: 2015-02-03 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 35
			recordDate: 2015-02-03 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 36
			recordDate: 2015-02-03 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 37
			recordDate: 2014-10-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 38
			recordDate: 2014-10-10 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 39
			recordDate: 2014-10-10 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 40
			recordDate: 2014-10-03 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 41
			recordDate: 2014-10-01 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 42
			recordDate: 2014-10-01 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 43
			recordDate: 2014-10-01 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 44
			recordDate: 2014-08-27 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 45
			recordDate: 2014-08-27 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 46
			recordDate: 2014-08-27 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 47
			recordDate: 2014-06-21 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 48
			recordDate: 2014-06-19 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 49
			recordDate: 2014-06-19 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 50
			recordDate: 2014-06-19 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 51
			recordDate: 2014-06-19 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 52
			recordDate: 2014-04-25 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 53
			recordDate: 2014-04-25 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 54
			recordDate: 2014-04-25 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 55
			recordDate: 2014-04-19 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 56
			recordDate: 2014-04-07 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 57
			recordDate: 2014-04-07 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 58
			recordDate: 2014-04-07 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 59
			recordDate: 2014-01-17 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 60
			recordDate: 2014-01-06 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 61
			recordDate: 2014-01-06 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 62
			recordDate: 2014-01-06 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 63
			recordDate: 2014-01-06 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 64
			recordDate: 2014-01-06 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 65
			recordDate: 2014-01-06 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 66
			recordDate: 2013-09-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 67
			recordDate: 2013-09-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 68
			recordDate: 2013-09-05 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 69
			recordDate: 2013-08-26 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 70
			recordDate: 2013-08-22 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 71
			recordDate: 2013-08-21 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 72
			recordDate: 2013-08-21 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 73
			recordDate: 2013-08-21 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 74
			recordDate: 2013-08-21 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 75
			recordDate: 2013-07-16 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 76
			recordDate: 2013-07-16 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 77
			recordDate: 2013-07-16 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 78
			recordDate: 2013-07-16 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 79
			recordDate: 2013-05-02 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 80
			recordDate: 2013-05-02 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 81
			recordDate: 2013-05-02 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 82
			recordDate: 2013-05-02 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 83
			recordDate: 2013-05-02 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 84
			recordDate: 2013-05-02 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 85
			recordDate: 2013-03-20 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 86
			recordDate: 2013-02-08 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 87
			recordDate: 2013-02-08 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 88
			recordDate: 2013-02-08 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 89
			recordDate: 2013-02-08 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 90
			recordDate: 2013-02-08 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 91
			recordDate: 2013-02-07 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 92
			recordDate: 2013-01-10 00:00:00
			code: L128
			description: Cleared by L&R (LARS)
		№ 93
			recordDate: 2012-12-21 00:00:00
			code: L198
			description: Referred to Level 2 (LARS) by OIPE CSR
		№ 94
			recordDate: 2012-12-20 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 95
			recordDate: 2012-12-20 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9178744
	applIdStr: 13722961
	appl_id_txt: 13722961
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13722961
	appSubCls: 202000
	appLocation: ELECTRONIC
	appAttrDockNumber: AM2-0095USC1
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 29150
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 16
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|16}
	appStatusDate: 2015-10-14T10:50:51Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-11-03T05:00:00Z
	APP_IND: 1
	appExamName: JEAN, FRANTZ B
	appExamNameFacet: JEAN, FRANTZ B
	firstInventorFile: No
	appClsSubCls: 709/202000
	appClsSubClsFacet: 709/202000
	applId: 13722961
	patentTitle: Delivery of Items for Consumption by a User Device
	appIntlPubNumber: 
	appConfrNumber: 4055
	appFilingDate: 2012-12-20T05:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Subram (Narsi)  Narasimhan
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Saratoga, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Gregg Elliott Zehr
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Palo Alto, 
			geoCode: CA
			country: (US)
			rankNo: 4
		№ 2
			nameLineOne: Michael V. Rykov
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 6
		№ 3
			nameLineOne: James C. Slezak
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 8
		№ 4
			nameLineOne: Richard  Moore
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 10
		№ 5
			nameLineOne: Thomas M.J. Fruchterman
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 12
		№ 6
			nameLineOne: Beryl  Tomay
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Newcastle, 
			geoCode: WA
			country: (US)
			rankNo: 14
		№ 7
			nameLineOne: John  Lattyak
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Los Gatos, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 8
			nameLineOne: Thomas A. Ryan
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Los Gatos, 
			geoCode: CA
			country: (US)
			rankNo: 3
		№ 9
			nameLineOne: Kenneth P. Kiraly
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Menlo Park, 
			geoCode: CA
			country: (US)
			rankNo: 5
		№ 10
			nameLineOne: Girish Bansilal Bajaj
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 7
		№ 11
			nameLineOne: Aviram  Zagorie
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 9
		№ 12
			nameLineOne: Kevin R. Cheung
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 11
		№ 13
			nameLineOne: Robert L. Goodwin
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Mercer Island, 
			geoCode: WA
			country: (US)
			rankNo: 13
		№ 14
			nameLineOne: Jon  Saxton
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Brooklyn, 
			geoCode: NY
			country: (US)
			rankNo: 15
	appCls: 709
	_version_: 1580433207766024193
	lastUpdatedTimestamp: 2017-10-05T15:49:19.049Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 08b71c7b-e732-4071-856a-34b5a913df41
	responseHeader:
			status: 0
			QTime: 17
