DOCUMENT_BODY:
	appGrpArtNumber: 2485
	appGrpArtNumberFacet: 2485
	transactions:
		№ 0
			recordDate: 2016-07-15 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2015-07-28 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2015-07-28 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2015-07-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2015-07-08 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2015-06-25 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2015-06-25 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2015-06-24 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2015-06-24 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2015-04-10 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2015-04-08 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2015-04-08 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 12
			recordDate: 2015-04-01 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 13
			recordDate: 2015-03-31 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 14
			recordDate: 2015-03-17 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 15
			recordDate: 2015-03-09 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 16
			recordDate: 2015-03-09 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 17
			recordDate: 2014-11-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 18
			recordDate: 2014-11-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 19
			recordDate: 2014-11-14 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 20
			recordDate: 2014-11-02 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 21
			recordDate: 2014-09-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 22
			recordDate: 2014-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 23
			recordDate: 2014-08-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 24
			recordDate: 2014-05-13 00:00:00
			code: WROIPE
			description: Application Return from OIPE
		№ 25
			recordDate: 2014-05-13 00:00:00
			code: ROIPE
			description: Application Return TO OIPE
		№ 26
			recordDate: 2014-04-16 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 27
			recordDate: 2014-04-14 00:00:00
			code: MM327
			description: Mail Miscellaneous Communication to Applicant
		№ 28
			recordDate: 2014-04-09 00:00:00
			code: M327
			description: Miscellaneous Communication to Applicant - No Action Count
		№ 29
			recordDate: 2014-04-03 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 30
			recordDate: 2013-03-09 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 31
			recordDate: 2013-01-10 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 32
			recordDate: 2012-12-18 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 33
			recordDate: 2012-12-18 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 34
			recordDate: 2012-12-18 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 35
			recordDate: 2012-12-17 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 36
			recordDate: 2012-12-10 00:00:00
			code: ADDFLFEE
			description: Additional Application Filing Fees
		№ 37
			recordDate: 2012-12-10 00:00:00
			code: OATHDECL
			description: A statement by one or more inventors satisfying the requirement under 35 USC 115, Oath of the Applic
		№ 38
			recordDate: 2012-10-15 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 39
			recordDate: 2012-10-15 00:00:00
			code: INCD
			description: Notice Mailed--Application Incomplete--Filing Date Assigned 
		№ 40
			recordDate: 2012-09-26 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 41
			recordDate: 2012-09-25 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 42
			recordDate: 2012-09-25 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 43
			recordDate: 2012-09-25 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 44
			recordDate: 2012-09-25 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9094670
	applIdStr: 13626620
	appl_id_txt: 13626620
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13626620
	appSubCls: 047000
	appLocation: ELECTRONIC
	appAttrDockNumber: 085101-526866.342NPUS00
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136608
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2015-07-08T11:26:09Z
	LAST_MOD_TS: 2017-05-11T09:32:48Z
	patentIssueDate: 2015-07-28T04:00:00Z
	APP_IND: 1
	appExamName: HUANG, FRANK F
	appExamNameFacet: HUANG, FRANK F
	firstInventorFile: No
	appClsSubCls: 348/047000
	appClsSubClsFacet: 348/047000
	applId: 13626620
	patentTitle: MODEL GENERATION AND DATABASE
	appIntlPubNumber: 
	appConfrNumber: 9532
	appFilingDate: 2012-09-25T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Paul J. Furio
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Kesler Williams Tanner
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Springville, 
			geoCode: UT
			country: (US)
			rankNo: 2
	appCls: 348
	_version_: 1580433187978346501
	lastUpdatedTimestamp: 2017-10-05T15:49:00.142Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 2555c4ea-fcf8-4402-9cd6-c81e59fd1ab9
	responseHeader:
			status: 0
			QTime: 13
