DOCUMENT_BODY:
	appGrpArtNumber: 2155
	appGrpArtNumberFacet: 2155
	transactions:
		№ 0
			recordDate: 2015-08-11 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-08-11 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-07-23 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-07-22 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-07-13 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-07-13 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2015-07-13 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 7
			recordDate: 2015-07-13 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 8
			recordDate: 2015-07-07 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 9
			recordDate: 2015-07-07 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 10
			recordDate: 2015-06-25 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 11
			recordDate: 2015-06-24 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 12
			recordDate: 2015-06-24 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 13
			recordDate: 2015-06-24 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 14
			recordDate: 2015-04-08 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 15
			recordDate: 2015-04-08 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 16
			recordDate: 2015-04-08 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 17
			recordDate: 2015-04-05 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 18
			recordDate: 2015-04-03 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 19
			recordDate: 2015-04-03 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 20
			recordDate: 2014-09-24 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 21
			recordDate: 2013-05-14 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 22
			recordDate: 2013-05-13 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 23
			recordDate: 2013-05-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 24
			recordDate: 2013-05-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 25
			recordDate: 2013-05-01 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 26
			recordDate: 2013-05-01 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 27
			recordDate: 2013-05-01 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 28
			recordDate: 2013-04-30 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 29
			recordDate: 2013-04-30 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 30
			recordDate: 2013-03-25 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 31
			recordDate: 2013-03-18 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 32
			recordDate: 2013-03-14 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 33
			recordDate: 2013-03-14 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9104707
	applIdStr: 13829375
	appl_id_txt: 13829375
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13829375
	appSubCls: 803000
	appLocation: ELECTRONIC
	appAttrDockNumber: AMAZ-0048
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 23377
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|2}
	appStatusDate: 2015-07-22T08:53:54Z
	LAST_MOD_TS: 2017-09-05T13:35:09Z
	patentIssueDate: 2015-08-11T04:00:00Z
	APP_IND: 1
	appExamName: TRAN, LOC
	appExamNameFacet: TRAN, LOC
	firstInventorFile: No
	appClsSubCls: 707/803000
	appClsSubClsFacet: 707/803000
	applId: 13829375
	patentTitle: Iterative Generation of Partial Column Schema
	appIntlPubNumber: 
	appConfrNumber: 3479
	appFilingDate: 2013-03-14T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Nicholas Alexander Allen
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 707
	_version_: 1580433229780877315
	lastUpdatedTimestamp: 2017-10-05T15:49:40.028Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 791bb353-5cc2-4f42-8948-5645f9c7ff30
	responseHeader:
			status: 0
			QTime: 19
