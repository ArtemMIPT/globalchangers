DOCUMENT_BODY:
	appGrpArtNumber: 2457
	appGrpArtNumberFacet: 2457
	transactions:
		№ 0
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 1
			recordDate: 2015-04-14 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2015-04-14 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2015-03-26 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2015-03-25 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2015-03-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2015-03-17 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 7
			recordDate: 2015-03-16 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 8
			recordDate: 2015-03-12 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 9
			recordDate: 2015-03-11 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 10
			recordDate: 2015-03-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 11
			recordDate: 2015-03-11 00:00:00
			code: MM327-B
			description: Mail PUB Notice of non-compliant IDS
		№ 12
			recordDate: 2015-03-09 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 13
			recordDate: 2015-03-09 00:00:00
			code: M327-B
			description: PUB Notice of non-compliant IDS
		№ 14
			recordDate: 2015-03-03 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 15
			recordDate: 2015-03-03 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 16
			recordDate: 2015-03-03 00:00:00
			code: DRWF
			description: Workflow - Drawings Finished
		№ 17
			recordDate: 2015-03-03 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 18
			recordDate: 2015-03-03 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 19
			recordDate: 2015-03-03 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 20
			recordDate: 2014-12-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 21
			recordDate: 2014-12-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 22
			recordDate: 2014-12-17 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 23
			recordDate: 2014-12-06 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 24
			recordDate: 2014-12-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 25
			recordDate: 2014-12-05 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 26
			recordDate: 2014-12-04 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 27
			recordDate: 2014-11-26 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 28
			recordDate: 2014-11-26 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 29
			recordDate: 2014-11-24 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 30
			recordDate: 2014-11-24 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 31
			recordDate: 2014-11-24 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 32
			recordDate: 2014-09-15 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 33
			recordDate: 2014-09-15 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 34
			recordDate: 2014-09-15 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 35
			recordDate: 2014-09-03 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 36
			recordDate: 2014-09-03 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 37
			recordDate: 2014-09-03 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 38
			recordDate: 2014-09-03 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 39
			recordDate: 2014-09-03 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 40
			recordDate: 2014-08-29 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 41
			recordDate: 2014-08-29 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 42
			recordDate: 2014-08-26 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 43
			recordDate: 2014-08-26 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 44
			recordDate: 2014-08-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 45
			recordDate: 2014-08-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 46
			recordDate: 2014-08-18 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 47
			recordDate: 2014-08-18 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 48
			recordDate: 2014-08-18 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 49
			recordDate: 2014-08-18 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 50
			recordDate: 2014-08-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 51
			recordDate: 2014-06-25 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 52
			recordDate: 2014-06-25 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 53
			recordDate: 2014-06-25 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 54
			recordDate: 2014-06-25 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 55
			recordDate: 2014-03-25 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 56
			recordDate: 2014-03-25 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 57
			recordDate: 2014-03-25 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 58
			recordDate: 2014-03-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 59
			recordDate: 2014-03-18 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 60
			recordDate: 2014-03-18 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 61
			recordDate: 2014-03-07 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 62
			recordDate: 2014-03-07 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 63
			recordDate: 2014-03-07 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 64
			recordDate: 2014-03-07 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 65
			recordDate: 2014-02-20 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 66
			recordDate: 2014-02-20 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 67
			recordDate: 2013-12-26 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 68
			recordDate: 2013-12-26 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 69
			recordDate: 2013-12-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 70
			recordDate: 2013-11-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 71
			recordDate: 2013-11-29 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 72
			recordDate: 2013-09-27 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 73
			recordDate: 2013-09-27 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 74
			recordDate: 2013-09-27 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 75
			recordDate: 2013-09-27 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 76
			recordDate: 2013-09-27 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 77
			recordDate: 2013-09-27 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 78
			recordDate: 2013-09-27 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 79
			recordDate: 2013-09-27 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 80
			recordDate: 2013-09-27 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 81
			recordDate: 2013-09-27 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 82
			recordDate: 2013-09-27 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 83
			recordDate: 2013-09-27 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 84
			recordDate: 2013-08-20 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 85
			recordDate: 2013-08-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 86
			recordDate: 2013-08-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 87
			recordDate: 2013-08-19 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 88
			recordDate: 2013-08-19 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 89
			recordDate: 2013-08-19 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 90
			recordDate: 2013-08-16 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 91
			recordDate: 2013-08-16 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 92
			recordDate: 2013-08-07 00:00:00
			code: FLFEE
			description: Payment of additional filing fee/Preexam
		№ 93
			recordDate: 2013-06-11 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 94
			recordDate: 2013-06-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 95
			recordDate: 2013-06-11 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 96
			recordDate: 2013-06-11 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 97
			recordDate: 2013-06-11 00:00:00
			code: INCD
			description: Notice Mailed--Application Incomplete--Filing Date Assigned 
		№ 98
			recordDate: 2013-05-09 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 99
			recordDate: 2013-05-07 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 100
			recordDate: 2013-05-06 00:00:00
			code: CLAIM
			description: Claim Preliminary Amendment
		№ 101
			recordDate: 2013-05-06 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 102
			recordDate: 2013-05-06 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9009286
	applIdStr: 13888283
	appl_id_txt: 13888283
	appEarlyPubNumber: US20130318153A1
	appEntityStatus: UNDISCOUNTED
	id: 13888283
	appSubCls: 223000
	appLocation: ELECTRONIC
	appAttrDockNumber: SEAZN.206C4
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 20995
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2015-03-25T11:43:15Z
	LAST_MOD_TS: 2017-08-30T14:31:45Z
	appEarlyPubDate: 2013-11-28T05:00:00Z
	patentIssueDate: 2015-04-14T04:00:00Z
	APP_IND: 1
	appExamName: JACOBS, LASHONDA T
	appExamNameFacet: JACOBS, LASHONDA T
	firstInventorFile: No
	appClsSubCls: 709/223000
	appClsSubClsFacet: 709/223000
	applId: 13888283
	patentTitle: LOCALITY BASED CONTENT DISTRIBUTION
	appIntlPubNumber: 
	appConfrNumber: 2027
	appFilingDate: 2013-05-06T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Swaminathan  Sivasubramanian
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Bradley E. Marshall
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bainbridge Island, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 2
			nameLineOne: David R. Richardson
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 709
	_version_: 1580433241953796096
	lastUpdatedTimestamp: 2017-10-05T15:49:51.653Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 8a5d78fb-c4aa-4695-8abb-3e524687bf44
	responseHeader:
			status: 0
			QTime: 18
