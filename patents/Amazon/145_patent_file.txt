DOCUMENT_BODY:
	appGrpArtNumber: 2198
	appGrpArtNumberFacet: 2198
	transactions:
		№ 0
			recordDate: 2015-08-18 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-08-18 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-07-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-07-29 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-07-15 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-07-13 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-07-10 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 7
			recordDate: 2015-07-10 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2015-07-10 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2015-06-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 10
			recordDate: 2015-06-17 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 11
			recordDate: 2015-06-17 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 12
			recordDate: 2015-06-15 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 13
			recordDate: 2015-06-11 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 14
			recordDate: 2015-06-10 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 15
			recordDate: 2015-06-09 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 16
			recordDate: 2015-06-09 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 17
			recordDate: 2015-06-09 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 18
			recordDate: 2015-04-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 19
			recordDate: 2015-04-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 20
			recordDate: 2015-04-24 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 21
			recordDate: 2015-04-21 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 22
			recordDate: 2015-04-20 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 23
			recordDate: 2015-04-20 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 24
			recordDate: 2015-03-18 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 25
			recordDate: 2015-03-18 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 26
			recordDate: 2015-03-13 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 27
			recordDate: 2015-03-13 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 28
			recordDate: 2015-02-09 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 29
			recordDate: 2015-02-09 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 30
			recordDate: 2015-02-09 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 31
			recordDate: 2015-02-05 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 32
			recordDate: 2015-01-30 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 33
			recordDate: 2015-01-16 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 34
			recordDate: 2015-01-12 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 35
			recordDate: 2015-01-12 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 36
			recordDate: 2014-12-01 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 37
			recordDate: 2014-12-01 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 38
			recordDate: 2014-10-30 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 39
			recordDate: 2014-10-30 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 40
			recordDate: 2014-10-30 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 41
			recordDate: 2014-10-20 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 42
			recordDate: 2014-01-17 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 43
			recordDate: 2013-08-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 44
			recordDate: 2013-03-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 45
			recordDate: 2013-03-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 46
			recordDate: 2013-03-04 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 47
			recordDate: 2013-03-04 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 48
			recordDate: 2013-02-22 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 49
			recordDate: 2013-01-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 50
			recordDate: 2013-01-09 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 51
			recordDate: 2013-01-09 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 52
			recordDate: 2013-01-08 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 53
			recordDate: 2012-12-05 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 54
			recordDate: 2012-12-04 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 55
			recordDate: 2012-12-04 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 56
			recordDate: 2012-12-04 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 57
			recordDate: 2012-12-04 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9110756
	applIdStr: 13705003
	appl_id_txt: 13705003
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13705003
	appSubCls: 172000
	appLocation: ELECTRONIC
	appAttrDockNumber: 90204-855754 (061200US)
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 107508
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2015-07-29T11:53:23Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-08-18T04:00:00Z
	APP_IND: 1
	appExamName: LUNA, ROBERTO E
	appExamNameFacet: LUNA, ROBERTO E
	firstInventorFile: No
	appClsSubCls: 717/172000
	appClsSubClsFacet: 717/172000
	applId: 13705003
	patentTitle: TAG-BASED DEPLOYMENT TO OVERLAPPING HOST SETS
	appIntlPubNumber: 
	appConfrNumber: 6851
	appFilingDate: 2012-12-04T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Jiaqi  Guo
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Granger, 
			geoCode: IN
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Matthew David Klein
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 717
	_version_: 1580433204001636353
	lastUpdatedTimestamp: 2017-10-05T15:49:15.477Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 580c0491-4c13-4537-b2b0-749b4885eba3
	responseHeader:
			status: 0
			QTime: 15
