DOCUMENT_BODY:
	appGrpArtNumber: 2455
	appGrpArtNumberFacet: 2455
	transactions:
		№ 0
			recordDate: 2017-01-24 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2017-01-24 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 2
			recordDate: 2017-01-24 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2017-01-05 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2017-01-04 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2016-12-14 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2016-12-14 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2016-12-12 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2016-12-12 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2016-10-20 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 10
			recordDate: 2016-10-20 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 11
			recordDate: 2016-10-20 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 12
			recordDate: 2016-10-15 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 13
			recordDate: 2016-09-16 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 14
			recordDate: 2016-09-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 15
			recordDate: 2016-09-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 16
			recordDate: 2016-09-12 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 17
			recordDate: 2016-09-07 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 18
			recordDate: 2016-09-07 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 19
			recordDate: 2016-06-06 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 20
			recordDate: 2016-05-24 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 21
			recordDate: 2016-02-25 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 22
			recordDate: 2016-02-25 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 23
			recordDate: 2016-02-25 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 24
			recordDate: 2016-02-21 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 25
			recordDate: 2015-11-17 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 26
			recordDate: 2015-11-16 00:00:00
			code: ELC.
			description: Response to Election / Restriction Filed
		№ 27
			recordDate: 2015-09-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 28
			recordDate: 2015-09-14 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 29
			recordDate: 2015-09-14 00:00:00
			code: MCTRS
			description: Mail Restriction Requirement
		№ 30
			recordDate: 2015-09-08 00:00:00
			code: CTRS
			description: Restriction/Election Requirement
		№ 31
			recordDate: 2015-01-14 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 32
			recordDate: 2014-02-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 33
			recordDate: 2013-07-17 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 34
			recordDate: 2013-05-30 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 35
			recordDate: 2013-05-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 36
			recordDate: 2013-05-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 37
			recordDate: 2013-05-30 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 38
			recordDate: 2013-05-30 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 39
			recordDate: 2013-05-29 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 40
			recordDate: 2013-05-29 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 41
			recordDate: 2013-04-28 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 42
			recordDate: 2013-04-26 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 43
			recordDate: 2013-04-24 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 44
			recordDate: 2013-04-24 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 45
			recordDate: 2013-04-24 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 46
			recordDate: 2013-04-24 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9553951
	applIdStr: 13869902
	appl_id_txt: 13869902
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13869902
	appSubCls: 201000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-51000
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|3}
	appStatusDate: 2017-01-04T12:52:24Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2017-01-24T05:00:00Z
	APP_IND: 1
	appExamName: KIM, EDWARD J
	appExamNameFacet: KIM, EDWARD J
	firstInventorFile: Yes
	appClsSubCls: 709/201000
	appClsSubClsFacet: 709/201000
	applId: 13869902
	patentTitle: SEMAPHORES IN DISTRIBUTED COMPUTING ENVIRONMENTS
	appIntlPubNumber: 
	appConfrNumber: 1330
	appFilingDate: 2013-04-24T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: LONG XUAN NGUYEN
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: MICHAEL BENJAMIN DEARDEUFF
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 709
	_version_: 1580433238154805251
	lastUpdatedTimestamp: 2017-10-05T15:49:48.036Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: a2cc947d-62d2-4cea-96b3-f2488704cc44
	responseHeader:
			status: 0
			QTime: 16
