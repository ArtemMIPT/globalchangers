DOCUMENT_BODY:
	appGrpArtNumber: 2665
	appGrpArtNumberFacet: 2665
	transactions:
		№ 0
			recordDate: 2016-08-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 1
			recordDate: 2016-08-17 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 2
			recordDate: 2016-08-16 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 3
			recordDate: 2016-02-23 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 4
			recordDate: 2016-02-23 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 5
			recordDate: 2016-02-04 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2016-02-03 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 7
			recordDate: 2016-01-22 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 8
			recordDate: 2016-01-22 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 9
			recordDate: 2016-01-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 10
			recordDate: 2016-01-21 00:00:00
			code: R48ACLT
			description: Letter Accepting Correction of Inventorship Under Rule 1.48
		№ 11
			recordDate: 2016-01-21 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 12
			recordDate: 2016-01-13 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 13
			recordDate: 2016-01-13 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 14
			recordDate: 2016-01-13 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 15
			recordDate: 2015-10-26 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 16
			recordDate: 2015-10-26 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 17
			recordDate: 2015-10-26 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 18
			recordDate: 2015-10-22 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 19
			recordDate: 2015-10-20 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 20
			recordDate: 2015-10-20 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 21
			recordDate: 2015-10-20 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 22
			recordDate: 2015-10-16 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 23
			recordDate: 2015-10-12 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 24
			recordDate: 2015-10-12 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 25
			recordDate: 2015-08-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 26
			recordDate: 2015-08-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 27
			recordDate: 2015-08-12 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 28
			recordDate: 2015-08-07 00:00:00
			code: CTFR
			description: Final Rejection
		№ 29
			recordDate: 2015-05-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 30
			recordDate: 2015-05-15 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 31
			recordDate: 2015-05-15 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 32
			recordDate: 2015-01-16 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 33
			recordDate: 2015-01-16 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 34
			recordDate: 2015-01-16 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 35
			recordDate: 2015-01-12 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 36
			recordDate: 2014-06-17 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 37
			recordDate: 2013-04-20 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 38
			recordDate: 2013-04-11 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 39
			recordDate: 2013-03-18 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 40
			recordDate: 2013-03-18 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 41
			recordDate: 2013-03-18 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 42
			recordDate: 2013-02-13 00:00:00
			code: L128
			description: Cleared by L&R (LARS)
		№ 43
			recordDate: 2013-02-13 00:00:00
			code: L198
			description: Referred to Level 2 (LARS) by OIPE CSR
		№ 44
			recordDate: 2013-02-11 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 45
			recordDate: 2013-02-11 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 46
			recordDate: 2013-02-11 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 47
			recordDate: 2013-02-11 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9269011
	applIdStr: 13764646
	appl_id_txt: 13764646
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13764646
	appSubCls: 103000
	appLocation: ELECTRONIC
	appAttrDockNumber: 085101-526914.392NPUS00
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136608
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 6
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|6}
	appStatusDate: 2016-02-03T10:47:13Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-02-23T05:00:00Z
	APP_IND: 1
	appExamName: GILLIARD, DELOMIA L
	appExamNameFacet: GILLIARD, DELOMIA L
	firstInventorFile: No
	appClsSubCls: 382/103000
	appClsSubClsFacet: 382/103000
	applId: 13764646
	patentTitle: GRAPHICAL REFINEMENT FOR POINTS OF INTEREST
	appIntlPubNumber: 
	appConfrNumber: 4456
	appFilingDate: 2013-02-11T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: AVNISH  SIKKA
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Acton, 
			geoCode: MA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: JAMES  SASSANO
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Somerville, 
			geoCode: MA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: SONJEEV  JAHAGIRDAR
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Cambridge, 
			geoCode: MA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: PENGCHENG  WU
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Marlborough, 
			geoCode: MA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: NICHOLAS RANDAL SOVICH
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Baltimore, 
			geoCode: MD
			country: (US)
			rankNo: 5
	appCls: 382
	_version_: 1580433216483885059
	lastUpdatedTimestamp: 2017-10-05T15:49:27.349Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 4619a0f3-746d-490b-bd96-48bc8c9d71bb
	responseHeader:
			status: 0
			QTime: 16
