DOCUMENT_BODY:
	appGrpArtNumber: 2162
	appGrpArtNumberFacet: 2162
	transactions:
		№ 0
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 1
			recordDate: 2014-09-02 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2014-09-02 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2014-08-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2014-08-13 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2014-08-06 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2014-07-29 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2014-07-22 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2014-07-22 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 9
			recordDate: 2014-07-22 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2014-07-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 11
			recordDate: 2014-07-08 00:00:00
			code: MM327-O
			description: Mail PUBS Notice Requiring Inventors Oath or Declaration
		№ 12
			recordDate: 2014-07-03 00:00:00
			code: M327-O
			description: PUBS Notice Requiring Inventors Oath or Declaration
		№ 13
			recordDate: 2014-05-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 14
			recordDate: 2014-05-09 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 15
			recordDate: 2014-05-09 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 16
			recordDate: 2014-05-05 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 17
			recordDate: 2014-05-05 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 18
			recordDate: 2014-04-21 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 19
			recordDate: 2014-04-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 20
			recordDate: 2014-04-14 00:00:00
			code: MEXIA
			description: Mail Applicant Initiated Interview Summary
		№ 21
			recordDate: 2014-04-11 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 22
			recordDate: 2014-04-10 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 23
			recordDate: 2014-04-01 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 24
			recordDate: 2014-04-01 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 25
			recordDate: 2014-01-10 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 26
			recordDate: 2014-01-10 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 27
			recordDate: 2014-01-10 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 28
			recordDate: 2014-01-07 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 29
			recordDate: 2013-09-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 30
			recordDate: 2013-09-10 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 31
			recordDate: 2013-08-02 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 32
			recordDate: 2013-06-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 33
			recordDate: 2013-06-14 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 34
			recordDate: 2013-06-14 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 35
			recordDate: 2013-06-13 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 36
			recordDate: 2013-06-13 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 37
			recordDate: 2013-05-14 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 38
			recordDate: 2013-05-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 39
			recordDate: 2013-05-08 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 40
			recordDate: 2013-05-08 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 41
			recordDate: 2013-05-08 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 42
			recordDate: 2013-05-08 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 43
			recordDate: 2013-05-08 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 44
			recordDate: 2013-05-08 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 45
			recordDate: 2013-05-08 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 46
			recordDate: 2013-05-08 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8825638
	applIdStr: 13890051
	appl_id_txt: 13890051
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13890051
	appSubCls: 722000
	appLocation: ELECTRONIC
	appAttrDockNumber: MIPS.421C1
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 20995
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2014-08-13T11:30:21Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2014-09-02T04:00:00Z
	APP_IND: 1
	appExamName: JAMI, HARES
	appExamNameFacet: JAMI, HARES
	firstInventorFile: No
	appClsSubCls: 707/722000
	appClsSubClsFacet: 707/722000
	applId: 13890051
	patentTitle: SYSTEM FOR GENERATING BEHAVIOR-BASED ASSOCIATIONS FOR MULTIPLE DOMAIN-SPECIFIC APPLICATIONS
	appIntlPubNumber: 
	appConfrNumber: 4438
	appFilingDate: 2013-05-08T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Dany Miguel Yannick Houde
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Jin Y. Yi
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seatte, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 2
			nameLineOne: Douglas Tak-Lai Wong
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
	appCls: 707
	_version_: 1580433242312409091
	lastUpdatedTimestamp: 2017-10-05T15:49:51.993Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 8a5d78fb-c4aa-4695-8abb-3e524687bf44
	responseHeader:
			status: 0
			QTime: 18
