DOCUMENT_BODY:
	appGrpArtNumber: 3625
	appGrpArtNumberFacet: 3625
	transactions:
		№ 0
			recordDate: 2013-12-31 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2013-12-31 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2013-12-12 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2013-12-11 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2013-12-04 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2013-11-26 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2013-11-26 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 7
			recordDate: 2013-11-14 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 8
			recordDate: 2013-11-11 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 9
			recordDate: 2013-11-11 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 10
			recordDate: 2013-11-11 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 11
			recordDate: 2013-11-11 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 12
			recordDate: 2013-11-11 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 13
			recordDate: 2013-09-05 00:00:00
			code: MM327-O
			description: Mail PUBS Notice Requiring Inventors Oath or Declaration
		№ 14
			recordDate: 2013-09-03 00:00:00
			code: M327-O
			description: PUBS Notice Requiring Inventors Oath or Declaration
		№ 15
			recordDate: 2013-08-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 16
			recordDate: 2013-08-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 17
			recordDate: 2013-08-12 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 18
			recordDate: 2013-08-07 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 19
			recordDate: 2013-07-23 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 20
			recordDate: 2013-06-07 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 21
			recordDate: 2013-06-04 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 22
			recordDate: 2013-05-17 00:00:00
			code: TI1050
			description: Transfer Inquiry to GAU
		№ 23
			recordDate: 2013-05-07 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 24
			recordDate: 2013-05-07 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 25
			recordDate: 2013-05-07 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 26
			recordDate: 2013-05-07 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 27
			recordDate: 2013-05-07 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 28
			recordDate: 2013-05-06 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 29
			recordDate: 2013-05-06 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 30
			recordDate: 2013-03-30 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 31
			recordDate: 2013-03-23 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 32
			recordDate: 2013-03-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 33
			recordDate: 2013-03-15 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 34
			recordDate: 2013-03-15 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 35
			recordDate: 2013-03-15 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 36
			recordDate: 2013-03-15 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8620768
	applIdStr: 13838857
	appl_id_txt: 13838857
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13838857
	appSubCls: 026100
	appLocation: ELECTRONIC
	appAttrDockNumber: 170101-1383
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|3}
	appStatusDate: 2013-12-11T11:14:46Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2013-12-31T05:00:00Z
	APP_IND: 1
	appExamName: AIRAPETIAN, MILA
	appExamNameFacet: AIRAPETIAN, MILA
	firstInventorFile: No
	appClsSubCls: 705/026100
	appClsSubClsFacet: 705/026100
	applId: 13838857
	patentTitle: METHOD AND SYSTEM FOR PRICE SUGGESTING USING ITEM-SPECIFIC ATTRIBUTES
	appIntlPubNumber: 
	appConfrNumber: 3335
	appFilingDate: 2013-03-15T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Gustava Eduardo Lopez
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Joel R. Spiegel
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Woodinville, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 705
	_version_: 1580433231750103041
	lastUpdatedTimestamp: 2017-10-05T15:49:41.927Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 46476086-7610-45b6-b7a3-ac4922c8865a
	responseHeader:
			status: 0
			QTime: 16
