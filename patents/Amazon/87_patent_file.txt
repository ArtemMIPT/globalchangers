DOCUMENT_BODY:
	appGrpArtNumber: 2138
	appGrpArtNumberFacet: 2138
	transactions:
		№ 0
			recordDate: 2017-09-07 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2016-10-04 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2016-10-04 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2016-09-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2016-09-14 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2016-09-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2016-09-01 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 7
			recordDate: 2016-09-01 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 8
			recordDate: 2016-08-31 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 9
			recordDate: 2016-08-27 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 10
			recordDate: 2016-08-25 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 11
			recordDate: 2016-08-24 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 12
			recordDate: 2016-08-23 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 13
			recordDate: 2016-08-23 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 14
			recordDate: 2016-08-23 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 15
			recordDate: 2016-08-23 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 16
			recordDate: 2016-08-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 17
			recordDate: 2016-08-11 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 18
			recordDate: 2016-08-11 00:00:00
			code: MM327
			description: Mail Miscellaneous Communication to Applicant
		№ 19
			recordDate: 2016-08-08 00:00:00
			code: M327
			description: Miscellaneous Communication to Applicant - No Action Count
		№ 20
			recordDate: 2016-08-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 21
			recordDate: 2016-08-05 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 22
			recordDate: 2016-08-04 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 23
			recordDate: 2016-08-04 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 24
			recordDate: 2016-07-01 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 25
			recordDate: 2016-07-01 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 26
			recordDate: 2016-06-30 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 27
			recordDate: 2016-06-29 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 28
			recordDate: 2016-06-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 29
			recordDate: 2016-06-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 30
			recordDate: 2016-06-06 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 31
			recordDate: 2016-05-31 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 32
			recordDate: 2016-05-28 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 33
			recordDate: 2016-05-28 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 34
			recordDate: 2016-05-28 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 35
			recordDate: 2016-05-25 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 36
			recordDate: 2016-04-22 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 37
			recordDate: 2016-04-22 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 38
			recordDate: 2016-04-21 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 39
			recordDate: 2016-04-21 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 40
			recordDate: 2016-04-21 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 41
			recordDate: 2016-04-21 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 42
			recordDate: 2016-04-21 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 43
			recordDate: 2016-02-12 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 44
			recordDate: 2016-02-03 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 45
			recordDate: 2015-11-30 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 46
			recordDate: 2015-11-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 47
			recordDate: 2015-11-27 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 48
			recordDate: 2015-11-20 00:00:00
			code: CTFR
			description: Final Rejection
		№ 49
			recordDate: 2015-09-06 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 50
			recordDate: 2015-09-02 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 51
			recordDate: 2015-06-04 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 52
			recordDate: 2015-06-03 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 53
			recordDate: 2015-06-03 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 54
			recordDate: 2015-05-31 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 55
			recordDate: 2015-05-31 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 56
			recordDate: 2015-05-05 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 57
			recordDate: 2015-05-05 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 58
			recordDate: 2015-05-04 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 59
			recordDate: 2015-05-04 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 60
			recordDate: 2015-05-04 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 61
			recordDate: 2015-03-24 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 62
			recordDate: 2015-03-17 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 63
			recordDate: 2015-03-17 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 64
			recordDate: 2015-01-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 65
			recordDate: 2015-01-09 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 66
			recordDate: 2015-01-09 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 67
			recordDate: 2015-01-06 00:00:00
			code: CTFR
			description: Final Rejection
		№ 68
			recordDate: 2014-11-05 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 69
			recordDate: 2014-11-03 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 70
			recordDate: 2014-10-16 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 71
			recordDate: 2014-10-14 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 72
			recordDate: 2014-10-14 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 73
			recordDate: 2014-08-04 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 74
			recordDate: 2014-08-04 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 75
			recordDate: 2014-08-04 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 76
			recordDate: 2014-08-04 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 77
			recordDate: 2014-08-01 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 78
			recordDate: 2014-07-27 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 79
			recordDate: 2014-05-20 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 80
			recordDate: 2014-05-15 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 81
			recordDate: 2013-11-18 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 82
			recordDate: 2013-11-12 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 83
			recordDate: 2013-05-17 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 84
			recordDate: 2012-12-17 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 85
			recordDate: 2012-12-07 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 86
			recordDate: 2012-12-07 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 87
			recordDate: 2012-12-07 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 88
			recordDate: 2012-12-07 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 89
			recordDate: 2012-11-14 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 90
			recordDate: 2012-11-13 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 91
			recordDate: 2012-11-13 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 92
			recordDate: 2012-11-13 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 93
			recordDate: 2012-11-13 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 94
			recordDate: 2012-11-13 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 95
			recordDate: 2012-11-13 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 96
			recordDate: 2012-11-13 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9460099
	applIdStr: 13675718
	appl_id_txt: 13675718
	appEarlyPubNumber: US20140136782A1
	appEntityStatus: UNDISCOUNTED
	id: 13675718
	appSubCls: 117000
	appLocation: ELECTRONIC
	appAttrDockNumber: AM2-1454US
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136607
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Seattle|WA|US|2}
	appStatusDate: 2016-09-14T12:32:38Z
	LAST_MOD_TS: 2017-09-07T10:52:43Z
	appEarlyPubDate: 2014-05-15T04:00:00Z
	patentIssueDate: 2016-10-04T04:00:00Z
	APP_IND: 1
	appExamName: FARROKH, HASHEM
	appExamNameFacet: FARROKH, HASHEM
	firstInventorFile: No
	appClsSubCls: 711/117000
	appClsSubClsFacet: 711/117000
	applId: 13675718
	patentTitle: DYNAMIC SELECTION OF STORAGE TIERS
	appIntlPubNumber: 
	appConfrNumber: 1764
	appFilingDate: 2012-11-13T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Nathan Bartholomew Thomas
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 711
	_version_: 1580433198109687811
	lastUpdatedTimestamp: 2017-10-05T15:49:09.826Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 683ba366-d23a-40da-948e-a872a584e98a
	responseHeader:
			status: 0
			QTime: 15
