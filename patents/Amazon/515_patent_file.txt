DOCUMENT_BODY:
	appGrpArtNumber: 2442
	appGrpArtNumberFacet: 2442
	transactions:
		№ 0
			recordDate: 2017-09-21 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 1
			recordDate: 2017-09-21 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 2
			recordDate: 2017-09-21 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 3
			recordDate: 2017-09-17 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 4
			recordDate: 2017-07-13 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 5
			recordDate: 2017-06-30 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 6
			recordDate: 2017-03-30 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 7
			recordDate: 2017-03-30 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 8
			recordDate: 2017-03-30 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 9
			recordDate: 2017-03-26 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 10
			recordDate: 2017-03-26 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 11
			recordDate: 2017-03-26 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 12
			recordDate: 2017-02-28 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 13
			recordDate: 2017-02-28 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 14
			recordDate: 2017-02-28 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 15
			recordDate: 2017-01-13 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 16
			recordDate: 2017-01-13 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 17
			recordDate: 2017-01-06 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 18
			recordDate: 2017-01-06 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 19
			recordDate: 2016-12-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 20
			recordDate: 2016-12-29 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 21
			recordDate: 2016-12-26 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 22
			recordDate: 2016-12-26 00:00:00
			code: AFNE
			description: After Final Consideration Program Amendment too Extensive
		№ 23
			recordDate: 2016-12-16 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 24
			recordDate: 2016-12-06 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 25
			recordDate: 2016-12-01 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 26
			recordDate: 2016-11-22 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 27
			recordDate: 2016-11-09 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 28
			recordDate: 2016-11-09 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 29
			recordDate: 2016-10-07 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 30
			recordDate: 2016-10-07 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 31
			recordDate: 2016-10-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 32
			recordDate: 2016-10-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 33
			recordDate: 2016-10-06 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 34
			recordDate: 2016-10-02 00:00:00
			code: CTFR
			description: Final Rejection
		№ 35
			recordDate: 2016-08-03 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 36
			recordDate: 2016-07-26 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 37
			recordDate: 2016-04-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 38
			recordDate: 2016-04-29 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 39
			recordDate: 2016-04-29 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 40
			recordDate: 2016-04-26 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 41
			recordDate: 2016-02-26 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 42
			recordDate: 2016-02-05 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 43
			recordDate: 2015-11-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 44
			recordDate: 2015-11-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 45
			recordDate: 2015-11-05 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 46
			recordDate: 2015-10-30 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 47
			recordDate: 2015-10-30 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 48
			recordDate: 2015-10-30 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 49
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 50
			recordDate: 2015-01-14 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 51
			recordDate: 2014-11-05 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 52
			recordDate: 2014-11-05 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 53
			recordDate: 2014-10-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 54
			recordDate: 2014-10-16 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 55
			recordDate: 2014-04-22 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 56
			recordDate: 2014-04-22 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 57
			recordDate: 2014-04-22 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 58
			recordDate: 2014-04-16 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 59
			recordDate: 2014-02-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 60
			recordDate: 2013-07-17 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 61
			recordDate: 2013-05-28 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 62
			recordDate: 2013-05-28 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 63
			recordDate: 2013-05-28 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 64
			recordDate: 2013-05-28 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 65
			recordDate: 2013-05-28 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 66
			recordDate: 2013-05-25 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 67
			recordDate: 2013-05-25 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 68
			recordDate: 2013-04-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 69
			recordDate: 2013-04-17 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 70
			recordDate: 2013-04-16 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 71
			recordDate: 2013-04-16 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 72
			recordDate: 2013-04-16 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 73
			recordDate: 2013-04-16 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 74
			recordDate: 2013-04-16 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Non Final Action Mailed
	appStatus_txt: Non Final Action Mailed
	appStatusFacet: Non Final Action Mailed
	patentNumber: 
	applIdStr: 13864138
	appl_id_txt: 13864138
	appEarlyPubNumber: US20140310417A1
	appEntityStatus: UNDISCOUNTED
	id: 13864138
	appSubCls: 226000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-52800
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 5
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|5}
	appStatusDate: 2017-09-19T14:14:23Z
	LAST_MOD_TS: 2017-09-22T11:46:09Z
	appEarlyPubDate: 2014-10-16T04:00:00Z
	APP_IND: 1
	appExamName: NGUYEN, ANGELA
	appExamNameFacet: NGUYEN, ANGELA
	firstInventorFile: Yes
	appClsSubCls: 709/226000
	appClsSubClsFacet: 709/226000
	applId: 13864138
	patentTitle: CONNECTION PUBLISHING IN A DISTRIBUTED LOAD BALANCER
	appIntlPubNumber: 
	appConfrNumber: 1538
	appFilingDate: 2013-04-16T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: JAMES CHRISTOPHER SORENSON III
			nameLineTwo:  
			suffix: III
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: DOUGLAS STEWART LAURENCE
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: MERCER ISLAND, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: VENKATRAGHAVAN  SRINIVASAN
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: SEATTLE, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: AKSHAY  VAIDYA
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 4
	appCls: 709
	_version_: 1580433237061140482
	lastUpdatedTimestamp: 2017-10-05T15:49:46.983Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: d87e7f50-ef6f-4911-a2f1-f087fb34a43a
	responseHeader:
			status: 0
			QTime: 17
