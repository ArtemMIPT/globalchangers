DOCUMENT_BODY:
	appGrpArtNumber: 2833
	appGrpArtNumberFacet: 2833
	transactions:
		№ 0
			recordDate: 2015-01-27 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-01-27 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-01-08 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-01-07 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2014-12-22 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2014-12-22 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2014-12-15 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2014-12-15 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2014-09-15 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2014-09-15 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2014-09-15 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2014-09-09 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2014-09-04 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 13
			recordDate: 2014-09-02 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 14
			recordDate: 2014-05-30 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 15
			recordDate: 2014-05-30 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 16
			recordDate: 2014-05-30 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 17
			recordDate: 2014-05-24 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 18
			recordDate: 2014-05-21 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 19
			recordDate: 2013-12-18 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 20
			recordDate: 2013-12-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 21
			recordDate: 2013-08-01 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 22
			recordDate: 2013-05-14 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 23
			recordDate: 2013-01-28 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 24
			recordDate: 2013-01-28 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 25
			recordDate: 2013-01-10 00:00:00
			code: C602
			description: Oath or Declaration Filed (Including Supplemental)
		№ 26
			recordDate: 2012-12-04 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 27
			recordDate: 2012-11-19 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 28
			recordDate: 2012-10-25 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 29
			recordDate: 2012-10-25 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 30
			recordDate: 2012-10-25 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 31
			recordDate: 2012-10-24 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 32
			recordDate: 2012-10-04 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 33
			recordDate: 2012-10-01 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 34
			recordDate: 2012-10-01 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 35
			recordDate: 2012-10-01 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 36
			recordDate: 2012-10-01 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8941256
	applIdStr: 13632766
	appl_id_txt: 13632766
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13632766
	appSubCls: 052000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-43800
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2015-01-07T12:02:10Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-01-27T05:00:00Z
	APP_IND: 1
	appExamName: GIRARDI, VANESSA MARY
	appExamNameFacet: GIRARDI, VANESSA MARY
	firstInventorFile: No
	appClsSubCls: 290/052000
	appClsSubClsFacet: 290/052000
	applId: 13632766
	patentTitle: ENERGY RECLAMATION FROM AIR-MOVING SYSTEMS
	appIntlPubNumber: 
	appConfrNumber: 6077
	appFilingDate: 2012-10-01T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Michael P. Czamara
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Brock R. Gardner
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Osvaldo P. Morales
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
	appCls: 290
	_version_: 1580433189323669508
	lastUpdatedTimestamp: 2017-10-05T15:49:01.462Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 27a4098c-12d4-49df-b6f6-358e3142263c
	responseHeader:
			status: 0
			QTime: 15
