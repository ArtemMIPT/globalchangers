DOCUMENT_BODY:
	appGrpArtNumber: 2457
	appGrpArtNumberFacet: 2457
	transactions:
		№ 0
			recordDate: 2016-01-26 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-01-26 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-01-07 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-01-06 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-12-17 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-12-17 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-12-14 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-12-14 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-09-30 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2015-09-30 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2015-09-30 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2015-09-27 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2015-09-24 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 13
			recordDate: 2015-09-24 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 14
			recordDate: 2015-09-18 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 15
			recordDate: 2015-09-18 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 16
			recordDate: 2015-09-18 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 17
			recordDate: 2015-09-03 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 18
			recordDate: 2015-08-27 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 19
			recordDate: 2015-06-10 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 20
			recordDate: 2015-06-10 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 21
			recordDate: 2015-06-10 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 22
			recordDate: 2015-06-03 00:00:00
			code: CTFR
			description: Final Rejection
		№ 23
			recordDate: 2015-04-23 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 24
			recordDate: 2015-04-17 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 25
			recordDate: 2015-01-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 26
			recordDate: 2015-01-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 27
			recordDate: 2015-01-23 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 28
			recordDate: 2015-01-18 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 29
			recordDate: 2014-08-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 30
			recordDate: 2014-08-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 31
			recordDate: 2013-01-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 32
			recordDate: 2013-01-14 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 33
			recordDate: 2013-01-08 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 34
			recordDate: 2013-01-08 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 35
			recordDate: 2013-01-08 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 36
			recordDate: 2013-01-07 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 37
			recordDate: 2012-12-10 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 38
			recordDate: 2012-12-06 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 39
			recordDate: 2012-12-06 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 40
			recordDate: 2012-12-06 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9246866
	applIdStr: 13706874
	appl_id_txt: 13706874
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13706874
	appSubCls: 204000
	appLocation: ELECTRONIC
	appAttrDockNumber: 3516-P6801
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 20551
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2016-01-06T13:39:49Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-01-26T05:00:00Z
	APP_IND: 1
	appExamName: LAI, MICHAEL C
	appExamNameFacet: LAI, MICHAEL C
	firstInventorFile: No
	appClsSubCls: 709/204000
	appClsSubClsFacet: 709/204000
	applId: 13706874
	patentTitle: ITEM RECOMMENDATION
	appIntlPubNumber: 
	appConfrNumber: 1054
	appFilingDate: 2012-12-06T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Adam Callahan Sanders
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 709
	_version_: 1580433204443086851
	lastUpdatedTimestamp: 2017-10-05T15:49:15.88Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 580c0491-4c13-4537-b2b0-749b4885eba3
	responseHeader:
			status: 0
			QTime: 15
