DOCUMENT_BODY:
	appGrpArtNumber: 2858
	appGrpArtNumberFacet: 2858
	transactions:
		№ 0
			recordDate: 2017-09-07 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2016-07-19 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2016-07-19 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2016-06-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2016-06-29 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2016-06-16 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2016-06-16 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2016-06-15 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2016-06-15 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2016-03-22 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2016-03-22 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2016-03-22 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 12
			recordDate: 2016-03-17 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 13
			recordDate: 2016-03-16 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 14
			recordDate: 2016-01-27 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 15
			recordDate: 2016-01-12 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 16
			recordDate: 2015-12-24 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 17
			recordDate: 2015-12-16 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 18
			recordDate: 2015-10-20 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 19
			recordDate: 2015-10-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 20
			recordDate: 2015-10-20 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 21
			recordDate: 2015-10-01 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 22
			recordDate: 2015-07-22 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 23
			recordDate: 2015-07-17 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 24
			recordDate: 2015-04-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 25
			recordDate: 2015-04-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 26
			recordDate: 2015-04-23 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 27
			recordDate: 2015-04-15 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 28
			recordDate: 2015-03-04 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 29
			recordDate: 2014-08-08 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 30
			recordDate: 2013-05-08 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 31
			recordDate: 2013-05-07 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 32
			recordDate: 2013-04-26 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 33
			recordDate: 2013-04-26 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 34
			recordDate: 2013-04-26 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 35
			recordDate: 2013-04-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 36
			recordDate: 2013-04-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 37
			recordDate: 2013-04-22 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 38
			recordDate: 2013-04-22 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 39
			recordDate: 2013-04-19 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 40
			recordDate: 2013-04-19 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 41
			recordDate: 2013-03-15 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 42
			recordDate: 2013-03-12 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 43
			recordDate: 2013-03-11 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 44
			recordDate: 2013-03-11 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 45
			recordDate: 2013-03-11 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9395400
	applIdStr: 13794172
	appl_id_txt: 13794172
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13794172
	appSubCls: 538000
	appLocation: ELECTRONIC
	appAttrDockNumber: AM2-0932US
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136607
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|4}
	appStatusDate: 2016-06-29T13:33:06Z
	LAST_MOD_TS: 2017-09-07T10:52:43Z
	patentIssueDate: 2016-07-19T04:00:00Z
	APP_IND: 1
	appExamName: MCANDREW, CHRISTOPHER P
	appExamNameFacet: MCANDREW, CHRISTOPHER P
	firstInventorFile: No
	appClsSubCls: 324/538000
	appClsSubClsFacet: 324/538000
	applId: 13794172
	patentTitle: TEST FIXTURE TO TEST DEVICE CONNECTORS
	appIntlPubNumber: 
	appConfrNumber: 9566
	appFilingDate: 2013-03-11T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Rashed Adnan Islam
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Jose, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Saket  Patil
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Sunnyvale, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Ian Charles Rust
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Palo Alto, 
			geoCode: CA
			country: (US)
			rankNo: 3
	appCls: 324
	_version_: 1580433222365347845
	lastUpdatedTimestamp: 2017-10-05T15:49:32.952Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 6073073c-cbc6-49b0-b9b5-714ce578e5c8
	responseHeader:
			status: 0
			QTime: 17
