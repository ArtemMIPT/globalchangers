DOCUMENT_BODY:
	appGrpArtNumber: 3744
	appGrpArtNumberFacet: 3744
	transactions:
		№ 0
			recordDate: 2017-02-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 1
			recordDate: 2017-02-22 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 2
			recordDate: 2017-02-22 00:00:00
			code: R48ACLT
			description: Letter Accepting Correction of Inventorship Under Rule 1.48
		№ 3
			recordDate: 2016-12-13 00:00:00
			code: N423
			description: Post Issue Communication - Certificate of Correction
		№ 4
			recordDate: 2016-03-08 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 5
			recordDate: 2016-03-08 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 6
			recordDate: 2016-02-18 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 7
			recordDate: 2016-02-17 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 8
			recordDate: 2016-02-02 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 9
			recordDate: 2016-02-01 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 10
			recordDate: 2016-01-26 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 11
			recordDate: 2016-01-26 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 12
			recordDate: 2015-11-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 13
			recordDate: 2015-11-09 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 14
			recordDate: 2015-11-09 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 15
			recordDate: 2015-11-04 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 16
			recordDate: 2015-10-29 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 17
			recordDate: 2015-10-23 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 18
			recordDate: 2015-10-12 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 19
			recordDate: 2015-10-12 00:00:00
			code: C614
			description: New or Additional Drawing Filed
		№ 20
			recordDate: 2015-09-04 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 21
			recordDate: 2015-08-31 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 22
			recordDate: 2015-08-19 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 23
			recordDate: 2015-08-19 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 24
			recordDate: 2015-08-19 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 25
			recordDate: 2015-08-13 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 26
			recordDate: 2014-09-10 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 27
			recordDate: 2013-12-17 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 28
			recordDate: 2013-08-09 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 29
			recordDate: 2013-02-04 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 30
			recordDate: 2013-01-28 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 31
			recordDate: 2013-01-28 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 32
			recordDate: 2013-01-28 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 33
			recordDate: 2013-01-28 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 34
			recordDate: 2013-01-28 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 35
			recordDate: 2013-01-28 00:00:00
			code: MPEN
			description: Mail Pre-Exam Notice
		№ 36
			recordDate: 2013-01-28 00:00:00
			code: FLRCPT.C
			description: Filing Receipt - Corrected
		№ 37
			recordDate: 2013-01-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 38
			recordDate: 2013-01-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 39
			recordDate: 2013-01-09 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 40
			recordDate: 2013-01-09 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 41
			recordDate: 2012-11-01 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 42
			recordDate: 2012-10-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 43
			recordDate: 2012-10-10 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 44
			recordDate: 2012-10-10 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 45
			recordDate: 2012-10-09 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 46
			recordDate: 2012-09-23 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 47
			recordDate: 2012-09-18 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 48
			recordDate: 2012-09-18 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 49
			recordDate: 2012-09-18 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9278142
	applIdStr: 13622321
	appl_id_txt: 13622321
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13622321
	appSubCls: 171000
	appLocation: ELECTRONIC
	appAttrDockNumber: 090204-0846383 (056600US)
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 107508
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2016-02-17T12:16:50Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-03-08T05:00:00Z
	APP_IND: 1
	appExamName: BRADFORD, JONATHAN
	appExamNameFacet: BRADFORD, JONATHAN
	firstInventorFile: No
	appClsSubCls: 062/171000
	appClsSubClsFacet: 062/171000
	applId: 13622321
	patentTitle: OPTIMUM CONDENSER WATER TEMPERATURE FOR WATER COOLED CHILLER
	appIntlPubNumber: 
	appConfrNumber: 1060
	appFilingDate: 2012-09-18T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Christopher A. Goodnow
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 062
	_version_: 1580433187012608001
	lastUpdatedTimestamp: 2017-10-05T15:48:59.237Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 2555c4ea-fcf8-4402-9cd6-c81e59fd1ab9
	responseHeader:
			status: 0
			QTime: 13
