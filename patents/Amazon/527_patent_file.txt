DOCUMENT_BODY:
	appGrpArtNumber: 2437
	appGrpArtNumberFacet: 2437
	transactions:
		№ 0
			recordDate: 2017-09-07 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2017-05-30 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2017-05-30 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 3
			recordDate: 2017-05-30 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 4
			recordDate: 2017-05-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2017-05-10 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 6
			recordDate: 2017-04-25 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 7
			recordDate: 2017-04-24 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 8
			recordDate: 2017-04-21 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2017-04-21 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2017-02-07 00:00:00
			code: MM327-W
			description: Mail PUBS Letter Withdrawing a Notice Requiring Inventors Oath or Declaration
		№ 11
			recordDate: 2017-02-03 00:00:00
			code: M327-W
			description: PUBS Letter Withdrawing a Notice Requiring Inventors Oath or Declaration
		№ 12
			recordDate: 2017-01-30 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 13
			recordDate: 2017-01-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 14
			recordDate: 2017-01-27 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 15
			recordDate: 2017-01-24 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 16
			recordDate: 2017-01-19 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 17
			recordDate: 2017-01-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 18
			recordDate: 2016-12-12 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 19
			recordDate: 2016-12-12 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 20
			recordDate: 2016-11-14 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 21
			recordDate: 2016-11-07 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 22
			recordDate: 2016-09-15 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 23
			recordDate: 2016-09-15 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 24
			recordDate: 2016-09-15 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 25
			recordDate: 2016-09-09 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 26
			recordDate: 2016-09-01 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 27
			recordDate: 2016-08-23 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 28
			recordDate: 2016-08-17 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 29
			recordDate: 2016-08-08 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 30
			recordDate: 2016-08-08 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 31
			recordDate: 2016-08-05 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 32
			recordDate: 2016-08-05 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 33
			recordDate: 2016-08-05 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 34
			recordDate: 2016-04-07 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 35
			recordDate: 2016-04-07 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 36
			recordDate: 2016-04-07 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 37
			recordDate: 2016-03-31 00:00:00
			code: CTFR
			description: Final Rejection
		№ 38
			recordDate: 2016-03-30 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 39
			recordDate: 2016-03-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 40
			recordDate: 2016-03-14 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 41
			recordDate: 2016-03-14 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 42
			recordDate: 2015-10-30 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 43
			recordDate: 2015-10-30 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 44
			recordDate: 2015-10-30 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 45
			recordDate: 2015-10-27 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 46
			recordDate: 2015-10-21 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 47
			recordDate: 2015-04-09 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 48
			recordDate: 2015-04-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 49
			recordDate: 2015-04-09 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 50
			recordDate: 2015-04-07 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 51
			recordDate: 2015-04-02 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 52
			recordDate: 2015-04-02 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 53
			recordDate: 2015-02-20 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 54
			recordDate: 2015-02-11 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 55
			recordDate: 2015-02-11 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 56
			recordDate: 2014-11-03 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 57
			recordDate: 2014-11-02 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 58
			recordDate: 2014-06-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 59
			recordDate: 2014-02-11 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 60
			recordDate: 2013-07-17 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 61
			recordDate: 2013-05-30 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 62
			recordDate: 2013-05-30 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 63
			recordDate: 2013-05-30 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 64
			recordDate: 2013-05-30 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 65
			recordDate: 2013-05-30 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 66
			recordDate: 2013-04-22 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 67
			recordDate: 2013-04-18 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 68
			recordDate: 2013-04-18 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 69
			recordDate: 2013-04-18 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 70
			recordDate: 2013-04-18 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9667649
	applIdStr: 13865782
	appl_id_txt: 13865782
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13865782
	appSubCls: 022000
	appLocation: ELECTRONIC
	appAttrDockNumber: AM2-1697US
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136607
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 7
	applicantsFacet: {|Amazon Technologies, Inc.||||Seattle|WA|US|7}
	appStatusDate: 2017-05-10T07:59:04Z
	LAST_MOD_TS: 2017-09-07T10:52:43Z
	patentIssueDate: 2017-05-30T04:00:00Z
	APP_IND: 1
	appExamName: LANIER, BENJAMIN E
	appExamNameFacet: LANIER, BENJAMIN E
	firstInventorFile: Yes
	appClsSubCls: 726/022000
	appClsSubClsFacet: 726/022000
	applId: 13865782
	patentTitle: Detecting Man-in-the-Middle and Denial-of-Service Attacks
	appIntlPubNumber: 
	appConfrNumber: 7332
	appFilingDate: 2013-04-18T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Frans Adriaan Lategan
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Johannesburg, 
			geoCode: 
			country: (ZA)
			rankNo: 1
		№ 1
			nameLineOne: Andries Petrus Johannes Dippenaar
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Cape Town, 
			geoCode: 
			country: (ZA)
			rankNo: 2
		№ 2
			nameLineOne: Marcin Piotr Kowalski
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Cape Town, 
			geoCode: 
			country: (ZA)
			rankNo: 3
		№ 3
			nameLineOne: Gina Louise Morris
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Cape Town, 
			geoCode: 
			country: (ZA)
			rankNo: 4
		№ 4
			nameLineOne: Anton Andre Eicher
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Hout Bay, 
			geoCode: 
			country: (ZA)
			rankNo: 5
		№ 5
			nameLineOne: Duncan Matthew Clough
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Cape Town, 
			geoCode: 
			country: (ZA)
			rankNo: 6
	appCls: 726
	_version_: 1580433237393539076
	lastUpdatedTimestamp: 2017-10-05T15:49:47.323Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: a2cc947d-62d2-4cea-96b3-f2488704cc44
	responseHeader:
			status: 0
			QTime: 16
