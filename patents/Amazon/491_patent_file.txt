DOCUMENT_BODY:
	appGrpArtNumber: 2454
	appGrpArtNumberFacet: 2454
	transactions:
		№ 0
			recordDate: 2016-09-06 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-09-06 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-08-18 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-08-17 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-08-08 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-08-05 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-08-04 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2016-08-04 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2016-05-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2016-05-18 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2016-05-18 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2016-05-13 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2016-05-09 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 13
			recordDate: 2016-04-14 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 14
			recordDate: 2016-03-03 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 15
			recordDate: 2016-03-03 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 16
			recordDate: 2016-02-25 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 17
			recordDate: 2016-02-22 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 18
			recordDate: 2016-02-19 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 19
			recordDate: 2016-02-19 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 20
			recordDate: 2016-02-19 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 21
			recordDate: 2015-10-22 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 22
			recordDate: 2015-10-22 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 23
			recordDate: 2015-10-22 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 24
			recordDate: 2015-10-18 00:00:00
			code: CTFR
			description: Final Rejection
		№ 25
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 26
			recordDate: 2015-08-24 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 27
			recordDate: 2015-08-21 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 28
			recordDate: 2015-08-21 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 29
			recordDate: 2015-03-27 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 30
			recordDate: 2015-03-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 31
			recordDate: 2015-03-27 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 32
			recordDate: 2015-03-16 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 33
			recordDate: 2015-03-04 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 34
			recordDate: 2015-03-04 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 35
			recordDate: 2014-11-26 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 36
			recordDate: 2014-11-26 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 37
			recordDate: 2014-11-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 38
			recordDate: 2014-10-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 39
			recordDate: 2014-10-03 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 40
			recordDate: 2014-10-02 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 41
			recordDate: 2014-04-02 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 42
			recordDate: 2014-04-02 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 43
			recordDate: 2014-04-02 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 44
			recordDate: 2014-03-28 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 45
			recordDate: 2014-02-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 46
			recordDate: 2013-07-17 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 47
			recordDate: 2013-05-21 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 48
			recordDate: 2013-05-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 49
			recordDate: 2013-05-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 50
			recordDate: 2013-05-21 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 51
			recordDate: 2013-05-21 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 52
			recordDate: 2013-05-20 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 53
			recordDate: 2013-05-20 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 54
			recordDate: 2013-04-09 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 55
			recordDate: 2013-04-03 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 56
			recordDate: 2013-04-02 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 57
			recordDate: 2013-04-02 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 58
			recordDate: 2013-04-02 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 59
			recordDate: 2013-04-02 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 60
			recordDate: 2013-04-02 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9438495
	applIdStr: 13855462
	appl_id_txt: 13855462
	appEarlyPubNumber: US20140297835A1
	appEntityStatus: UNDISCOUNTED
	id: 13855462
	appSubCls: 224000
	appLocation: ELECTRONIC
	appAttrDockNumber: 8904-90326-01
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 24197
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2016-08-17T08:48:25Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-10-02T04:00:00Z
	patentIssueDate: 2016-09-06T04:00:00Z
	APP_IND: 1
	appExamName: NGUYEN, ANH
	appExamNameFacet: NGUYEN, ANH
	firstInventorFile: Yes
	appClsSubCls: 709/224000
	appClsSubClsFacet: 709/224000
	applId: 13855462
	patentTitle: VISUALIZATION OF RESOURCES IN A DATA CENTER
	appIntlPubNumber: 
	appConfrNumber: 1360
	appFilingDate: 2013-04-02T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Willem Jacob Buys
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Cape Town, 
			geoCode: 
			country: (ZA)
			rankNo: 1
	appCls: 709
	_version_: 1580433235108691969
	lastUpdatedTimestamp: 2017-10-05T15:49:45.121Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 8d2a2ffd-bf72-4810-a487-8f9a738c994e
	responseHeader:
			status: 0
			QTime: 17
