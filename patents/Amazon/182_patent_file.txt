DOCUMENT_BODY:
	appGrpArtNumber: 2162
	appGrpArtNumberFacet: 2162
	transactions:
		№ 0
			recordDate: 2015-04-07 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-04-07 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-03-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-03-18 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-03-06 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-03-06 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-03-05 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-03-05 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2014-12-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2014-12-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2014-12-05 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2014-12-02 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2014-11-26 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 13
			recordDate: 2014-10-27 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 14
			recordDate: 2014-10-17 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 15
			recordDate: 2014-10-17 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 16
			recordDate: 2014-06-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 17
			recordDate: 2014-06-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 18
			recordDate: 2014-06-17 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 19
			recordDate: 2014-06-12 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 20
			recordDate: 2014-06-03 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 21
			recordDate: 2014-03-11 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 22
			recordDate: 2013-03-21 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 23
			recordDate: 2013-03-20 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 24
			recordDate: 2013-02-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 25
			recordDate: 2013-02-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 26
			recordDate: 2013-02-11 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 27
			recordDate: 2013-02-11 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 28
			recordDate: 2013-02-11 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 29
			recordDate: 2013-02-08 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 30
			recordDate: 2013-01-18 00:00:00
			code: L128
			description: Cleared by L&R (LARS)
		№ 31
			recordDate: 2012-12-19 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 32
			recordDate: 2012-12-19 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 33
			recordDate: 2012-12-19 00:00:00
			code: L198
			description: Referred to Level 2 (LARS) by OIPE CSR
		№ 34
			recordDate: 2012-12-14 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 35
			recordDate: 2012-12-14 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 36
			recordDate: 2012-12-14 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 37
			recordDate: 2012-12-14 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9002805
	applIdStr: 13715867
	appl_id_txt: 13715867
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13715867
	appSubCls: 692000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-49000
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 6
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|6}
	appStatusDate: 2015-03-18T10:55:44Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-04-07T04:00:00Z
	APP_IND: 1
	appExamName: JAMI, HARES
	appExamNameFacet: JAMI, HARES
	firstInventorFile: No
	appClsSubCls: 707/692000
	appClsSubClsFacet: 707/692000
	applId: 13715867
	patentTitle: CONDITIONAL STORAGE OBJECT DELETION
	appIntlPubNumber: 
	appConfrNumber: 1964
	appFilingDate: 2012-12-14T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: JEFFREY MICHAEL BARBER
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Praveen Kumar Gattu
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Redmond, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Christopher Henning Elving
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Sunnyvale, 
			geoCode: CA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Derek Ernest Denny-Brown II
			nameLineTwo:  
			suffix: II
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Carl Yates Perry
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 5
	appCls: 707
	_version_: 1580433206352543748
	lastUpdatedTimestamp: 2017-10-05T15:49:17.664Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 6127cd1c-1e1f-490f-8308-652c641ee1b3
	responseHeader:
			status: 0
			QTime: 15
