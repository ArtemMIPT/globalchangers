DOCUMENT_BODY:
	appGrpArtNumber: 2455
	appGrpArtNumberFacet: 2455
	transactions:
		№ 0
			recordDate: 2016-07-19 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 2
			recordDate: 2015-01-27 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 3
			recordDate: 2015-01-27 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 4
			recordDate: 2015-01-08 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2015-01-07 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 6
			recordDate: 2014-12-29 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 7
			recordDate: 2014-12-29 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 8
			recordDate: 2014-12-16 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2014-12-16 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 10
			recordDate: 2014-12-16 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 11
			recordDate: 2014-12-16 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 12
			recordDate: 2014-11-07 00:00:00
			code: MM327-O
			description: Mail PUBS Notice Requiring Inventors Oath or Declaration
		№ 13
			recordDate: 2014-11-05 00:00:00
			code: M327-O
			description: PUBS Notice Requiring Inventors Oath or Declaration
		№ 14
			recordDate: 2014-09-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 15
			recordDate: 2014-09-17 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 16
			recordDate: 2014-09-17 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 17
			recordDate: 2014-09-14 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 18
			recordDate: 2014-09-08 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 19
			recordDate: 2014-08-27 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 20
			recordDate: 2014-08-25 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 21
			recordDate: 2014-08-25 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 22
			recordDate: 2014-06-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 23
			recordDate: 2014-06-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 24
			recordDate: 2014-06-06 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 25
			recordDate: 2014-06-02 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 26
			recordDate: 2014-06-01 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 27
			recordDate: 2014-05-12 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 28
			recordDate: 2014-05-12 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 29
			recordDate: 2014-05-12 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 30
			recordDate: 2014-03-23 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 31
			recordDate: 2014-02-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 32
			recordDate: 2013-12-27 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 33
			recordDate: 2013-12-26 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 34
			recordDate: 2013-12-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 35
			recordDate: 2013-12-19 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 36
			recordDate: 2013-12-12 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 37
			recordDate: 2013-10-23 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 38
			recordDate: 2013-10-17 00:00:00
			code: MPTDI
			description: Mail-Petition Decision - Dismissed
		№ 39
			recordDate: 2013-10-16 00:00:00
			code: PTDI
			description: Petition Decision - Dismissed
		№ 40
			recordDate: 2013-09-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 41
			recordDate: 2013-09-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 42
			recordDate: 2013-09-23 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 43
			recordDate: 2013-09-20 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 44
			recordDate: 2013-09-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 45
			recordDate: 2013-09-19 00:00:00
			code: PG-PB-DT
			description: PG-Pub Notice of new or Revised projected publication date
		№ 46
			recordDate: 2013-09-13 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 47
			recordDate: 2013-09-13 00:00:00
			code: PET.
			description: Petition Entered
		№ 48
			recordDate: 2013-06-03 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 49
			recordDate: 2013-05-23 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 50
			recordDate: 2013-05-23 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 51
			recordDate: 2013-05-23 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 52
			recordDate: 2013-05-22 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 53
			recordDate: 2013-05-22 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 54
			recordDate: 2013-04-11 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 55
			recordDate: 2013-04-11 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 56
			recordDate: 2013-04-10 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 57
			recordDate: 2013-04-10 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 58
			recordDate: 2013-04-10 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 59
			recordDate: 2013-04-10 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 60
			recordDate: 2013-04-10 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 61
			recordDate: 2013-04-10 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 62
			recordDate: 2013-04-10 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 8943127
	applIdStr: 13860305
	appl_id_txt: 13860305
	appEarlyPubNumber: US20130346480A1
	appEntityStatus: UNDISCOUNTED
	id: 13860305
	appSubCls: 203000
	appLocation: ELECTRONIC
	appAttrDockNumber: 20346.0618.CNUS00
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136608
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 7
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|7}
	appStatusDate: 2015-01-07T10:19:50Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2013-12-26T05:00:00Z
	patentIssueDate: 2015-01-27T05:00:00Z
	APP_IND: 1
	appExamName: TRAN, PHILIP B
	appExamNameFacet: TRAN, PHILIP B
	firstInventorFile: No
	appClsSubCls: 709/203000
	appClsSubClsFacet: 709/203000
	applId: 13860305
	patentTitle: TECHNIQUES FOR CAPTURING DATA SETS
	appIntlPubNumber: 
	appConfrNumber: 1029
	appFilingDate: 2013-04-10T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Tate Andrew Certain
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Sachin  Jain
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bangalore, 
			geoCode: 
			country: (IN)
			rankNo: 2
		№ 2
			nameLineOne: James R. Hamilton
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Fiorenzo  Cattaneo
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Snoqualmie, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Danny  Wei
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 5
		№ 5
			nameLineOne: David Nolan Sunderland
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 6
	appCls: 709
	_version_: 1580433236023050246
	lastUpdatedTimestamp: 2017-10-05T15:49:45.999Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: d87e7f50-ef6f-4911-a2f1-f087fb34a43a
	responseHeader:
			status: 0
			QTime: 17
