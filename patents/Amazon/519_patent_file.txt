DOCUMENT_BODY:
	appGrpArtNumber: 2458
	appGrpArtNumberFacet: 2458
	transactions:
		№ 0
			recordDate: 2017-08-07 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 1
			recordDate: 2017-08-07 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 2
			recordDate: 2017-08-07 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 3
			recordDate: 2017-08-02 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 4
			recordDate: 2017-06-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 5
			recordDate: 2017-06-19 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 6
			recordDate: 2017-06-06 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 7
			recordDate: 2017-06-06 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 8
			recordDate: 2017-05-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 9
			recordDate: 2017-05-22 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 10
			recordDate: 2017-05-16 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 11
			recordDate: 2017-05-16 00:00:00
			code: AFNE
			description: After Final Consideration Program Amendment too Extensive
		№ 12
			recordDate: 2017-05-11 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 13
			recordDate: 2017-05-09 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 14
			recordDate: 2017-05-09 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 15
			recordDate: 2017-03-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 16
			recordDate: 2017-03-09 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 17
			recordDate: 2017-03-09 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 18
			recordDate: 2017-03-04 00:00:00
			code: CTFR
			description: Final Rejection
		№ 19
			recordDate: 2017-03-03 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 20
			recordDate: 2017-03-03 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 21
			recordDate: 2017-03-03 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 22
			recordDate: 2017-02-28 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 23
			recordDate: 2017-02-28 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 24
			recordDate: 2016-12-08 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 25
			recordDate: 2016-11-25 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 26
			recordDate: 2016-11-03 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 27
			recordDate: 2016-11-03 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 28
			recordDate: 2016-10-06 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 29
			recordDate: 2016-10-06 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 30
			recordDate: 2016-08-25 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 31
			recordDate: 2016-08-25 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 32
			recordDate: 2016-08-25 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 33
			recordDate: 2016-08-19 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 34
			recordDate: 2016-08-19 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 35
			recordDate: 2016-06-01 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 36
			recordDate: 2016-06-01 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 37
			recordDate: 2016-05-24 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 38
			recordDate: 2016-05-24 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 39
			recordDate: 2016-05-12 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 40
			recordDate: 2016-05-12 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 41
			recordDate: 2016-05-03 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 42
			recordDate: 2016-05-03 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 43
			recordDate: 2016-05-03 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 44
			recordDate: 2016-05-02 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 45
			recordDate: 2016-04-25 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 46
			recordDate: 2016-04-25 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 47
			recordDate: 2016-04-25 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 48
			recordDate: 2016-04-25 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 49
			recordDate: 2016-02-25 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 50
			recordDate: 2016-02-25 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 51
			recordDate: 2016-02-25 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 52
			recordDate: 2016-02-19 00:00:00
			code: CTFR
			description: Final Rejection
		№ 53
			recordDate: 2016-01-20 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 54
			recordDate: 2016-01-04 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 55
			recordDate: 2015-10-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 56
			recordDate: 2015-10-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 57
			recordDate: 2015-10-01 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 58
			recordDate: 2015-09-24 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 59
			recordDate: 2015-09-24 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 60
			recordDate: 2015-09-08 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 61
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 62
			recordDate: 2015-05-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 63
			recordDate: 2015-04-30 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 64
			recordDate: 2015-04-29 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 65
			recordDate: 2015-03-26 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 66
			recordDate: 2015-03-26 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 67
			recordDate: 2015-01-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 68
			recordDate: 2015-01-29 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 69
			recordDate: 2015-01-29 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 70
			recordDate: 2015-01-22 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 71
			recordDate: 2015-01-12 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 72
			recordDate: 2015-01-12 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 73
			recordDate: 2014-12-24 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 74
			recordDate: 2014-11-04 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 75
			recordDate: 2014-11-04 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 76
			recordDate: 2014-10-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 77
			recordDate: 2014-10-16 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 78
			recordDate: 2014-04-22 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 79
			recordDate: 2014-04-22 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 80
			recordDate: 2014-04-22 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 81
			recordDate: 2014-04-16 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 82
			recordDate: 2014-02-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 83
			recordDate: 2013-07-17 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 84
			recordDate: 2013-05-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 85
			recordDate: 2013-05-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 86
			recordDate: 2013-05-29 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 87
			recordDate: 2013-05-29 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 88
			recordDate: 2013-05-29 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 89
			recordDate: 2013-05-28 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 90
			recordDate: 2013-05-28 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 91
			recordDate: 2013-04-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 92
			recordDate: 2013-04-18 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 93
			recordDate: 2013-04-16 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 94
			recordDate: 2013-04-16 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 95
			recordDate: 2013-04-16 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 96
			recordDate: 2013-04-16 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 97
			recordDate: 2013-04-16 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Non Final Action Mailed
	appStatus_txt: Non Final Action Mailed
	appStatusFacet: Non Final Action Mailed
	patentNumber: 
	applIdStr: 13864162
	appl_id_txt: 13864162
	appEarlyPubNumber: US20140310391A1
	appEntityStatus: UNDISCOUNTED
	id: 13864162
	appSubCls: 223000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-52300
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|3}
	appStatusDate: 2017-08-03T17:36:03Z
	LAST_MOD_TS: 2017-08-08T06:19:23Z
	appEarlyPubDate: 2014-10-16T04:00:00Z
	APP_IND: 1
	appExamName: RECEK, JASON D
	appExamNameFacet: RECEK, JASON D
	firstInventorFile: Yes
	appClsSubCls: 709/223000
	appClsSubClsFacet: 709/223000
	applId: 13864162
	patentTitle: MULTIPATH ROUTING IN A DISTRIBUTED LOAD BALANCER
	appIntlPubNumber: 
	appConfrNumber: 1703
	appFilingDate: 2013-04-16T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: JAMES CHRISTOPHER SORENSON III
			nameLineTwo:  
			suffix: III
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: DOUGLAS STEWART LAURENCE
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Mercer Island, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 709
	_version_: 1580433237128249346
	lastUpdatedTimestamp: 2017-10-05T15:49:47.046Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: d87e7f50-ef6f-4911-a2f1-f087fb34a43a
	responseHeader:
			status: 0
			QTime: 17
