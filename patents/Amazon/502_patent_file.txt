DOCUMENT_BODY:
	appGrpArtNumber: 2491
	appGrpArtNumberFacet: 2491
	transactions:
		№ 0
			recordDate: 2017-01-03 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2017-01-03 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 2
			recordDate: 2017-01-03 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2016-12-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2016-12-14 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2016-12-08 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2016-12-08 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 7
			recordDate: 2016-12-08 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 8
			recordDate: 2016-12-07 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 9
			recordDate: 2016-12-03 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 10
			recordDate: 2016-12-03 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 11
			recordDate: 2016-12-02 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 12
			recordDate: 2016-11-28 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 13
			recordDate: 2016-11-28 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 14
			recordDate: 2016-11-28 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 15
			recordDate: 2016-11-28 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 16
			recordDate: 2016-08-26 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 17
			recordDate: 2016-08-26 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 18
			recordDate: 2016-08-26 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 19
			recordDate: 2016-08-24 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 20
			recordDate: 2016-08-13 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 21
			recordDate: 2016-08-13 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 22
			recordDate: 2016-08-08 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 23
			recordDate: 2016-06-14 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 24
			recordDate: 2016-03-02 00:00:00
			code: APBR
			description: Appeal Brief Review Complete
		№ 25
			recordDate: 2016-02-26 00:00:00
			code: T1OFF
			description: track 1 OFF
		№ 26
			recordDate: 2016-02-26 00:00:00
			code: AP.B
			description: Appeal Brief Filed
		№ 27
			recordDate: 2016-02-02 00:00:00
			code: MAPCP
			description: Mail Appeals conf. Proceed to BPAI
		№ 28
			recordDate: 2016-01-28 00:00:00
			code: APCP
			description: Pre-Appeals Conference Decision - Proceed to BPAI
		№ 29
			recordDate: 2015-11-30 00:00:00
			code: AP.C
			description: Request for Pre-Appeal Conference Filed
		№ 30
			recordDate: 2015-11-30 00:00:00
			code: N/AP
			description: Notice of Appeal Filed
		№ 31
			recordDate: 2015-09-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 32
			recordDate: 2015-09-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 33
			recordDate: 2015-09-01 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 34
			recordDate: 2015-08-25 00:00:00
			code: CTFR
			description: Final Rejection
		№ 35
			recordDate: 2015-06-27 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 36
			recordDate: 2015-06-26 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 37
			recordDate: 2015-06-25 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 38
			recordDate: 2015-06-22 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 39
			recordDate: 2015-06-22 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 40
			recordDate: 2015-03-31 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 41
			recordDate: 2015-03-31 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 42
			recordDate: 2015-03-31 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 43
			recordDate: 2015-03-25 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 44
			recordDate: 2015-03-12 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 45
			recordDate: 2015-03-05 00:00:00
			code: ELC.
			description: Response to Election / Restriction Filed
		№ 46
			recordDate: 2015-01-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 47
			recordDate: 2015-01-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 48
			recordDate: 2015-01-06 00:00:00
			code: MCTRS
			description: Mail Restriction Requirement
		№ 49
			recordDate: 2014-12-30 00:00:00
			code: CTRS
			description: Restriction/Election Requirement
		№ 50
			recordDate: 2014-06-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 51
			recordDate: 2014-02-11 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 52
			recordDate: 2013-07-26 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 53
			recordDate: 2013-07-26 00:00:00
			code: FLRCPT.C
			description: Filing Receipt - Corrected
		№ 54
			recordDate: 2013-07-17 00:00:00
			code: C602
			description: Oath or Declaration Filed (Including Supplemental)
		№ 55
			recordDate: 2013-07-17 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 56
			recordDate: 2013-05-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 57
			recordDate: 2013-05-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 58
			recordDate: 2013-05-22 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 59
			recordDate: 2013-05-22 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 60
			recordDate: 2013-05-22 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 61
			recordDate: 2013-05-21 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 62
			recordDate: 2013-05-21 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 63
			recordDate: 2013-04-10 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 64
			recordDate: 2013-04-08 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 65
			recordDate: 2013-04-08 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 66
			recordDate: 2013-04-08 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 67
			recordDate: 2013-04-08 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 68
			recordDate: 2013-04-08 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 69
			recordDate: 2013-04-08 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 70
			recordDate: 2013-04-08 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9537888
	applIdStr: 13858505
	appl_id_txt: 13858505
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13858505
	appSubCls: 023000
	appLocation: ELECTRONIC
	appAttrDockNumber: 170115-1100
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2016-12-14T10:38:02Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2017-01-03T05:00:00Z
	APP_IND: 1
	appExamName: DESROSIERS, EVANS
	appExamNameFacet: DESROSIERS, EVANS
	firstInventorFile: Yes
	appClsSubCls: 726/023000
	appClsSubClsFacet: 726/023000
	applId: 13858505
	patentTitle: PROXY SERVER-BASED MALWARE DETECTION
	appIntlPubNumber: 
	appConfrNumber: 6406
	appFilingDate: 2013-04-08T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: JESPER MIKAEL JOHANSSON
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Redmond, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: JON ARRON MCCLINTOCK
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 2
			nameLineOne: ANDREW JAY ROTHS
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Kenmore, 
			geoCode: WA
			country: (US)
			rankNo: 3
	appCls: 726
	_version_: 1580433235727351810
	lastUpdatedTimestamp: 2017-10-05T15:49:45.717Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: d87e7f50-ef6f-4911-a2f1-f087fb34a43a
	responseHeader:
			status: 0
			QTime: 17
