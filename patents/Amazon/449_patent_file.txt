DOCUMENT_BODY:
	appGrpArtNumber: 2492
	appGrpArtNumberFacet: 2492
	transactions:
		№ 0
			recordDate: 2016-11-15 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-11-15 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-10-27 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-10-26 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-10-17 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-10-14 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2016-10-13 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2016-10-13 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2016-07-27 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2016-07-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2016-07-27 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2016-07-22 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2016-07-08 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 13
			recordDate: 2016-05-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 14
			recordDate: 2016-05-09 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 15
			recordDate: 2016-02-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 16
			recordDate: 2016-02-09 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 17
			recordDate: 2016-02-09 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 18
			recordDate: 2016-01-27 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 19
			recordDate: 2016-01-27 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 20
			recordDate: 2016-01-22 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 21
			recordDate: 2015-12-12 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 22
			recordDate: 2015-10-26 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 23
			recordDate: 2015-10-26 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 24
			recordDate: 2015-08-26 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 25
			recordDate: 2015-08-26 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 26
			recordDate: 2015-08-26 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 27
			recordDate: 2015-08-20 00:00:00
			code: CTFR
			description: Final Rejection
		№ 28
			recordDate: 2015-08-03 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 29
			recordDate: 2015-07-27 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 30
			recordDate: 2015-07-27 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 31
			recordDate: 2015-02-26 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 32
			recordDate: 2015-02-26 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 33
			recordDate: 2015-02-26 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 34
			recordDate: 2015-02-18 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 35
			recordDate: 2015-02-04 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 36
			recordDate: 2015-02-02 00:00:00
			code: ELC.
			description: Response to Election / Restriction Filed
		№ 37
			recordDate: 2015-02-02 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 38
			recordDate: 2014-09-04 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 39
			recordDate: 2014-09-04 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 40
			recordDate: 2014-09-04 00:00:00
			code: MCTRS
			description: Mail Restriction Requirement
		№ 41
			recordDate: 2014-08-28 00:00:00
			code: CTRS
			description: Restriction/Election Requirement
		№ 42
			recordDate: 2014-08-05 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 43
			recordDate: 2014-04-30 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 44
			recordDate: 2014-01-07 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 45
			recordDate: 2013-05-17 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 46
			recordDate: 2013-05-13 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 47
			recordDate: 2013-05-13 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 48
			recordDate: 2013-05-13 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 49
			recordDate: 2013-05-13 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 50
			recordDate: 2013-05-13 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 51
			recordDate: 2013-05-11 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 52
			recordDate: 2013-05-11 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 53
			recordDate: 2013-04-08 00:00:00
			code: L128
			description: Cleared by L&R (LARS)
		№ 54
			recordDate: 2013-03-25 00:00:00
			code: L198
			description: Referred to Level 2 (LARS) by OIPE CSR
		№ 55
			recordDate: 2013-03-18 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 56
			recordDate: 2013-03-14 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 57
			recordDate: 2013-03-14 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9497023
	applIdStr: 13830308
	appl_id_txt: 13830308
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13830308
	appSubCls: 277000
	appLocation: ELECTRONIC
	appAttrDockNumber: 101058.000053
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 23377
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2016-10-26T09:20:04Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-11-15T05:00:00Z
	APP_IND: 1
	appExamName: PERUNGAVOOR, VENKATANARAY
	appExamNameFacet: PERUNGAVOOR, VENKATANARAY
	firstInventorFile: No
	appClsSubCls: 380/277000
	appClsSubClsFacet: 380/277000
	applId: 13830308
	patentTitle: MULTIPLY-ENCRYPTED MESSAGE FOR FILTERING
	appIntlPubNumber: 
	appConfrNumber: 6573
	appFilingDate: 2013-03-14T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Nicholas Alexander Allen
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 380
	_version_: 1580433229910900739
	lastUpdatedTimestamp: 2017-10-05T15:49:40.173Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 791bb353-5cc2-4f42-8948-5645f9c7ff30
	responseHeader:
			status: 0
			QTime: 19
