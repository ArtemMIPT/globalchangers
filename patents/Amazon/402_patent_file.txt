DOCUMENT_BODY:
	appGrpArtNumber: 2656
	appGrpArtNumberFacet: 2656
	transactions:
		№ 0
			recordDate: 2016-07-15 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2015-07-28 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2015-07-28 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2015-07-09 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2015-07-08 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2015-06-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2015-06-30 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 7
			recordDate: 2015-06-30 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 8
			recordDate: 2015-06-29 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 9
			recordDate: 2015-06-18 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 10
			recordDate: 2015-06-18 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 11
			recordDate: 2015-06-18 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 12
			recordDate: 2015-06-16 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 13
			recordDate: 2015-06-12 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 14
			recordDate: 2015-06-12 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 15
			recordDate: 2015-06-11 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 16
			recordDate: 2015-06-11 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 17
			recordDate: 2015-06-11 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 18
			recordDate: 2015-03-27 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 19
			recordDate: 2015-03-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 20
			recordDate: 2015-03-24 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 21
			recordDate: 2015-03-19 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 22
			recordDate: 2015-03-14 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 23
			recordDate: 2015-03-12 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 24
			recordDate: 2015-03-11 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 25
			recordDate: 2015-03-11 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 26
			recordDate: 2015-03-03 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 27
			recordDate: 2015-02-24 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 28
			recordDate: 2015-02-24 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 29
			recordDate: 2015-01-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 30
			recordDate: 2015-01-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 31
			recordDate: 2015-01-12 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 32
			recordDate: 2015-01-07 00:00:00
			code: CTFR
			description: Final Rejection
		№ 33
			recordDate: 2015-01-07 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 34
			recordDate: 2014-12-26 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 35
			recordDate: 2014-12-17 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 36
			recordDate: 2014-12-17 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 37
			recordDate: 2014-12-17 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 38
			recordDate: 2014-12-17 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 39
			recordDate: 2014-12-08 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 40
			recordDate: 2014-12-03 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 41
			recordDate: 2014-12-03 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 42
			recordDate: 2014-09-18 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 43
			recordDate: 2014-09-18 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 44
			recordDate: 2014-09-18 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 45
			recordDate: 2014-09-15 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 46
			recordDate: 2014-09-14 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 47
			recordDate: 2014-07-24 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 48
			recordDate: 2013-11-20 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 49
			recordDate: 2013-11-20 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 50
			recordDate: 2013-11-20 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 51
			recordDate: 2013-08-06 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 52
			recordDate: 2013-07-31 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 53
			recordDate: 2013-07-30 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 54
			recordDate: 2013-07-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 55
			recordDate: 2013-07-30 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 56
			recordDate: 2013-07-29 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 57
			recordDate: 2013-07-29 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 58
			recordDate: 2013-07-18 00:00:00
			code: CORRSPEC
			description: Applicant has submitted a new specification to correct Corrected Papers problems
		№ 59
			recordDate: 2013-06-27 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 60
			recordDate: 2013-06-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 61
			recordDate: 2013-06-27 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 62
			recordDate: 2013-06-27 00:00:00
			code: INCR
			description: Notice of Incomplete Reply
		№ 63
			recordDate: 2013-06-27 00:00:00
			code: MPEN
			description: Mail Pre-Exam Notice
		№ 64
			recordDate: 2013-06-18 00:00:00
			code: ADDFLFEE
			description: Additional Application Filing Fees
		№ 65
			recordDate: 2013-06-18 00:00:00
			code: CORRSPEC
			description: Applicant has submitted a new specification to correct Corrected Papers problems
		№ 66
			recordDate: 2013-06-18 00:00:00
			code: CORRDRW
			description: Applicant has submitted new drawings to correct Corrected Papers problems
		№ 67
			recordDate: 2013-04-18 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 68
			recordDate: 2013-04-18 00:00:00
			code: CPAP
			description: Corrected Paper
		№ 69
			recordDate: 2013-04-18 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 70
			recordDate: 2013-03-16 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 71
			recordDate: 2013-03-13 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 72
			recordDate: 2013-03-12 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 73
			recordDate: 2013-03-12 00:00:00
			code: SPECIFIC
			description: A  document that contains, at least in part, a written description of an invention, and of the manne
		№ 74
			recordDate: 2013-03-12 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 75
			recordDate: 2013-03-12 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9094576
	applIdStr: 13797394
	appl_id_txt: 13797394
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13797394
	appSubCls: 014070
	appLocation: ELECTRONIC
	appAttrDockNumber: 085101-526952.432NPUS00
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136608
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2015-07-08T11:09:44Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-07-28T04:00:00Z
	APP_IND: 1
	appExamName: TIEU, BINH KIEN
	appExamNameFacet: TIEU, BINH KIEN
	firstInventorFile: No
	appClsSubCls: 348/014070
	appClsSubClsFacet: 348/014070
	applId: 13797394
	patentTitle: RENDERED AUDIOVISUAL COMMUNICATION
	appIntlPubNumber: 
	appConfrNumber: 6386
	appFilingDate: 2013-03-12T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Kenneth Mark Karakotsios
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Jose, 
			geoCode: CA
			country: (US)
			rankNo: 1
	appCls: 348
	_version_: 1580433222915850245
	lastUpdatedTimestamp: 2017-10-05T15:49:33.495Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: cc915edb-47a7-45a5-937d-e5065d213ba1
	responseHeader:
			status: 0
			QTime: 16
