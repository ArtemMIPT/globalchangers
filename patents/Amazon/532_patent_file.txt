DOCUMENT_BODY:
	appGrpArtNumber: 2626
	appGrpArtNumberFacet: 2626
	transactions:
		№ 0
			recordDate: 2016-08-03 00:00:00
			code: C.ADB
			description: Correspondence Address Change
		№ 1
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 2
			recordDate: 2015-07-07 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 3
			recordDate: 2015-07-07 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 4
			recordDate: 2015-06-18 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2015-06-17 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 6
			recordDate: 2015-06-03 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 7
			recordDate: 2015-06-02 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 8
			recordDate: 2015-06-01 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2015-06-01 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2015-03-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 11
			recordDate: 2015-03-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 12
			recordDate: 2015-03-13 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 13
			recordDate: 2015-03-10 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 14
			recordDate: 2015-03-07 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 15
			recordDate: 2015-03-04 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 16
			recordDate: 2015-01-06 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 17
			recordDate: 2015-01-02 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 18
			recordDate: 2015-01-02 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 19
			recordDate: 2015-01-02 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 20
			recordDate: 2015-01-02 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 21
			recordDate: 2015-01-02 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 22
			recordDate: 2014-12-19 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 23
			recordDate: 2014-12-12 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 24
			recordDate: 2014-12-12 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 25
			recordDate: 2014-09-26 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 26
			recordDate: 2014-09-26 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 27
			recordDate: 2014-09-26 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 28
			recordDate: 2014-09-24 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 29
			recordDate: 2014-08-12 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 30
			recordDate: 2014-08-08 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 31
			recordDate: 2014-03-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 32
			recordDate: 2014-03-06 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 33
			recordDate: 2013-06-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 34
			recordDate: 2013-06-10 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 35
			recordDate: 2013-05-31 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 36
			recordDate: 2013-05-31 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 37
			recordDate: 2013-05-31 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 38
			recordDate: 2013-05-30 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 39
			recordDate: 2013-05-30 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 40
			recordDate: 2013-04-24 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 41
			recordDate: 2013-04-22 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 42
			recordDate: 2013-04-22 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 43
			recordDate: 2013-04-22 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 44
			recordDate: 2013-04-22 00:00:00
			code: IEXX
			description: Initial Exam Team nn
		№ 45
			recordDate: 2013-04-22 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9075435
	applIdStr: 13867988
	appl_id_txt: 13867988
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13867988
	appSubCls: 156000
	appLocation: ELECTRONIC
	appAttrDockNumber: 085101-527023.503NPUS00
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 136608
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2015-06-17T12:07:48Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-07-07T04:00:00Z
	APP_IND: 1
	appExamName: LEE, NICHOLAS J
	appExamNameFacet: LEE, NICHOLAS J
	firstInventorFile: Yes
	appClsSubCls: 345/156000
	appClsSubClsFacet: 345/156000
	applId: 13867988
	patentTitle: CONTEXT-AWARE NOTIFICATIONS
	appIntlPubNumber: 
	appConfrNumber: 2608
	appFilingDate: 2013-04-22T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Isaac Scott Noble
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Soquel, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Matthew Paul Bell
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Sunol, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Guenael Thomas Strutt
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Jose, 
			geoCode: CA
			country: (US)
			rankNo: 3
	appCls: 345
	_version_: 1580433237801435137
	lastUpdatedTimestamp: 2017-10-05T15:49:47.69Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: a2cc947d-62d2-4cea-96b3-f2488704cc44
	responseHeader:
			status: 0
			QTime: 16
