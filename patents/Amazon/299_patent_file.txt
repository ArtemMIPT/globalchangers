DOCUMENT_BODY:
	appGrpArtNumber: 2491
	appGrpArtNumberFacet: 2491
	transactions:
		№ 0
			recordDate: 2017-01-17 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 1
			recordDate: 2017-01-17 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 2
			recordDate: 2017-01-17 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 3
			recordDate: 2017-01-17 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 4
			recordDate: 2016-12-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2016-12-28 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 6
			recordDate: 2016-12-28 00:00:00
			code: FIDS
			description: Workflow - Informational Disclosure Statement - Finish
		№ 7
			recordDate: 2016-12-21 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 8
			recordDate: 2016-12-08 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 9
			recordDate: 2016-12-02 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 10
			recordDate: 2016-12-01 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 11
			recordDate: 2016-11-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 12
			recordDate: 2016-11-30 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 13
			recordDate: 2016-11-30 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 14
			recordDate: 2016-11-28 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 15
			recordDate: 2016-11-28 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 16
			recordDate: 2016-11-22 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 17
			recordDate: 2016-11-22 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 18
			recordDate: 2016-11-21 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 19
			recordDate: 2016-11-18 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 20
			recordDate: 2016-11-18 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 21
			recordDate: 2016-11-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 22
			recordDate: 2016-09-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 23
			recordDate: 2016-09-06 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 24
			recordDate: 2016-09-06 00:00:00
			code: MCNOA
			description: Mailing Corrected Notice of Allowability
		№ 25
			recordDate: 2016-08-31 00:00:00
			code: CNOA
			description: Corrected Notice of Allowability
		№ 26
			recordDate: 2016-08-31 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 27
			recordDate: 2016-08-30 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 28
			recordDate: 2016-08-26 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 29
			recordDate: 2016-08-26 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 30
			recordDate: 2016-08-26 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 31
			recordDate: 2016-08-24 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 32
			recordDate: 2016-08-24 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 33
			recordDate: 2016-08-24 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 34
			recordDate: 2016-08-23 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 35
			recordDate: 2016-08-22 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 36
			recordDate: 2016-08-18 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 37
			recordDate: 2016-08-18 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 38
			recordDate: 2016-08-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 39
			recordDate: 2016-08-16 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 40
			recordDate: 2016-08-16 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 41
			recordDate: 2016-08-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 42
			recordDate: 2016-08-11 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 43
			recordDate: 2016-05-23 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 44
			recordDate: 2016-05-11 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 45
			recordDate: 2016-05-11 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 46
			recordDate: 2016-05-11 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 47
			recordDate: 2016-05-11 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 48
			recordDate: 2016-05-11 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 49
			recordDate: 2016-04-05 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 50
			recordDate: 2016-03-30 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 51
			recordDate: 2016-01-11 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 52
			recordDate: 2016-01-11 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 53
			recordDate: 2016-01-11 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 54
			recordDate: 2016-01-06 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 55
			recordDate: 2015-12-16 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 56
			recordDate: 2015-10-20 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 57
			recordDate: 2015-10-20 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 58
			recordDate: 2015-10-20 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 59
			recordDate: 2015-10-16 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 60
			recordDate: 2015-10-16 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 61
			recordDate: 2015-10-16 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 62
			recordDate: 2015-10-15 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 63
			recordDate: 2015-10-15 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 64
			recordDate: 2015-10-13 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 65
			recordDate: 2015-09-11 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 66
			recordDate: 2015-09-04 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 67
			recordDate: 2015-07-15 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 68
			recordDate: 2015-07-15 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 69
			recordDate: 2015-07-15 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 70
			recordDate: 2015-07-09 00:00:00
			code: CTFR
			description: Final Rejection
		№ 71
			recordDate: 2015-06-29 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 72
			recordDate: 2015-06-29 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 73
			recordDate: 2015-06-29 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 74
			recordDate: 2015-06-29 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 75
			recordDate: 2015-05-05 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 76
			recordDate: 2015-05-05 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 77
			recordDate: 2015-03-22 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 78
			recordDate: 2015-03-02 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 79
			recordDate: 2015-03-02 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 80
			recordDate: 2015-02-23 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 81
			recordDate: 2015-02-23 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 82
			recordDate: 2014-12-19 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 83
			recordDate: 2014-12-15 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 84
			recordDate: 2014-12-15 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 85
			recordDate: 2014-12-12 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 86
			recordDate: 2014-12-12 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 87
			recordDate: 2014-09-16 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 88
			recordDate: 2014-09-16 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 89
			recordDate: 2014-08-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 90
			recordDate: 2014-08-29 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 91
			recordDate: 2014-08-29 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 92
			recordDate: 2014-08-24 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 93
			recordDate: 2014-08-19 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 94
			recordDate: 2014-08-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 95
			recordDate: 2014-08-14 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 96
			recordDate: 2014-07-28 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 97
			recordDate: 2014-07-28 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 98
			recordDate: 2014-02-20 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 99
			recordDate: 2014-02-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 100
			recordDate: 2014-02-20 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 101
			recordDate: 2014-02-11 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 102
			recordDate: 2014-02-07 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 103
			recordDate: 2013-10-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 104
			recordDate: 2013-10-24 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 105
			recordDate: 2013-10-21 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 106
			recordDate: 2013-07-15 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 107
			recordDate: 2013-04-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 108
			recordDate: 2013-04-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 109
			recordDate: 2013-04-30 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 110
			recordDate: 2013-04-30 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 111
			recordDate: 2013-04-12 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 112
			recordDate: 2013-03-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 113
			recordDate: 2013-03-11 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 114
			recordDate: 2013-03-11 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 115
			recordDate: 2013-03-08 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 116
			recordDate: 2013-02-13 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 117
			recordDate: 2013-02-12 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 118
			recordDate: 2013-02-12 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 119
			recordDate: 2013-02-12 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 120
			recordDate: 2013-02-12 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9547771
	applIdStr: 13764995
	appl_id_txt: 13764995
	appEarlyPubNumber: US20140230007A1
	appEntityStatus: UNDISCOUNTED
	id: 13764995
	appSubCls: 001000
	appLocation: ELECTRONIC
	appAttrDockNumber: 0097749-100US0
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 113507
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 5
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|5}
	appStatusDate: 2016-12-28T17:00:09Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-08-14T04:00:00Z
	patentIssueDate: 2017-01-17T05:00:00Z
	APP_IND: 1
	appExamName: POTRATZ, DANIEL B
	appExamNameFacet: POTRATZ, DANIEL B
	firstInventorFile: No
	appClsSubCls: 726/001000
	appClsSubClsFacet: 726/001000
	applId: 13764995
	patentTitle: POLICY ENFORCEMENT WITH ASSOCIATED DATA
	appIntlPubNumber: 
	appConfrNumber: 1075
	appFilingDate: 2013-02-12T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Gregory Branchek Roth
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Matthew James Wren
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Eric Jason Brandwine
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Haymarket, 
			geoCode: VA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Brian Irl Pratt
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 4
	appCls: 726
	_version_: 1580433216530022404
	lastUpdatedTimestamp: 2017-10-05T15:49:27.376Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 4619a0f3-746d-490b-bd96-48bc8c9d71bb
	responseHeader:
			status: 0
			QTime: 16
