DOCUMENT_BODY:
	appGrpArtNumber: 2626
	appGrpArtNumberFacet: 2626
	transactions:
		№ 0
			recordDate: 2016-03-15 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-03-15 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-02-25 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-02-24 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-02-09 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2016-02-09 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2016-02-09 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 7
			recordDate: 2016-02-08 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 8
			recordDate: 2016-02-02 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 9
			recordDate: 2016-02-02 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 10
			recordDate: 2016-02-02 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 11
			recordDate: 2015-11-16 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 12
			recordDate: 2015-11-16 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 13
			recordDate: 2015-11-16 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 14
			recordDate: 2015-11-10 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 15
			recordDate: 2015-11-10 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 16
			recordDate: 2015-11-03 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 17
			recordDate: 2015-10-21 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 18
			recordDate: 2015-10-21 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 19
			recordDate: 2015-09-09 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 20
			recordDate: 2015-09-01 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 21
			recordDate: 2015-07-21 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 22
			recordDate: 2015-07-21 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 23
			recordDate: 2015-07-21 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 24
			recordDate: 2015-07-06 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 25
			recordDate: 2015-04-22 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 26
			recordDate: 2015-04-22 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 27
			recordDate: 2015-04-21 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 28
			recordDate: 2015-04-21 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 29
			recordDate: 2015-04-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 30
			recordDate: 2015-04-15 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 31
			recordDate: 2015-04-09 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 32
			recordDate: 2015-04-09 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 33
			recordDate: 2015-04-06 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 34
			recordDate: 2015-04-06 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 35
			recordDate: 2015-04-05 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 36
			recordDate: 2015-04-03 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 37
			recordDate: 2015-04-03 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 38
			recordDate: 2015-02-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 39
			recordDate: 2015-02-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 40
			recordDate: 2015-02-13 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 41
			recordDate: 2015-02-08 00:00:00
			code: CTFR
			description: Final Rejection
		№ 42
			recordDate: 2015-02-02 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 43
			recordDate: 2015-01-26 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 44
			recordDate: 2015-01-02 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 45
			recordDate: 2014-12-16 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 46
			recordDate: 2014-12-16 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 47
			recordDate: 2014-10-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 48
			recordDate: 2014-10-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 49
			recordDate: 2014-10-24 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 50
			recordDate: 2014-10-20 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 51
			recordDate: 2014-07-25 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 52
			recordDate: 2014-03-31 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 53
			recordDate: 2014-02-04 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 54
			recordDate: 2013-06-26 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 55
			recordDate: 2013-05-24 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 56
			recordDate: 2013-04-24 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 57
			recordDate: 2013-04-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 58
			recordDate: 2013-04-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 59
			recordDate: 2013-04-24 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 60
			recordDate: 2013-04-24 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 61
			recordDate: 2013-04-23 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 62
			recordDate: 2013-04-23 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 63
			recordDate: 2013-03-20 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 64
			recordDate: 2013-03-16 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 65
			recordDate: 2013-03-16 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 66
			recordDate: 2013-03-14 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 67
			recordDate: 2013-03-14 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9285905
	applIdStr: 13803573
	appl_id_txt: 13803573
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13803573
	appSubCls: 173000
	appLocation: ELECTRONIC
	appAttrDockNumber: 579-7026
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 109263
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: RENO, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||RENO|NV|US|4}
	appStatusDate: 2016-02-24T12:04:35Z
	LAST_MOD_TS: 2017-05-16T11:18:00Z
	patentIssueDate: 2016-03-15T04:00:00Z
	APP_IND: 1
	appExamName: TAYLOR JR, DUANE N
	appExamNameFacet: TAYLOR JR, DUANE N
	firstInventorFile: No
	appClsSubCls: 345/173000
	appClsSubClsFacet: 345/173000
	applId: 13803573
	patentTitle: ACTUATOR COUPLED DEVICE CHASSIS
	appIntlPubNumber: 
	appConfrNumber: 3993
	appFilingDate: 2013-03-14T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: DAVID CHARLES BUUCK
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: PRUNEDALE, 
			geoCode: CA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: TIFFANY ANN YUN
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: FREMONT, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: SUKWON  NOH
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: CUPERTINO, 
			geoCode: CA
			country: (US)
			rankNo: 3
	appCls: 345
	_version_: 1580433224284241924
	lastUpdatedTimestamp: 2017-10-05T15:49:34.804Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 15a4ef0e-add3-4358-9cd3-7d1e69d9afa9
	responseHeader:
			status: 0
			QTime: 18
