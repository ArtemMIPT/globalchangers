DOCUMENT_BODY:
	appGrpArtNumber: 2441
	appGrpArtNumberFacet: 2441
	transactions:
		№ 0
			recordDate: 2015-10-27 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-10-27 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 2
			recordDate: 2015-10-27 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2015-10-08 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2015-10-07 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2015-09-23 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2015-09-22 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2015-09-21 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 8
			recordDate: 2015-09-21 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 9
			recordDate: 2015-06-19 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2015-06-19 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2015-06-19 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 12
			recordDate: 2015-06-16 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 13
			recordDate: 2015-06-15 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 14
			recordDate: 2015-06-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 15
			recordDate: 2014-11-05 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 16
			recordDate: 2014-02-19 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 17
			recordDate: 2013-06-28 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 18
			recordDate: 2013-05-17 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 19
			recordDate: 2013-05-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 20
			recordDate: 2013-05-17 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 21
			recordDate: 2013-05-17 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 22
			recordDate: 2013-05-17 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 23
			recordDate: 2013-05-16 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 24
			recordDate: 2013-05-16 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 25
			recordDate: 2013-04-09 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 26
			recordDate: 2013-04-03 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 27
			recordDate: 2013-04-01 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 28
			recordDate: 2013-04-01 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 29
			recordDate: 2013-04-01 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 30
			recordDate: 2013-04-01 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 31
			recordDate: 2013-04-01 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9172621
	applIdStr: 13854653
	appl_id_txt: 13854653
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13854653
	appSubCls: 224000
	appLocation: ELECTRONIC
	appAttrDockNumber: 5924-51800
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 35690
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2015-10-07T11:22:27Z
	LAST_MOD_TS: 2017-09-11T19:00:24Z
	patentIssueDate: 2015-10-27T04:00:00Z
	APP_IND: 1
	appExamName: KATSIKIS, KOSTAS J
	appExamNameFacet: KATSIKIS, KOSTAS J
	firstInventorFile: Yes
	appClsSubCls: 709/224000
	appClsSubClsFacet: 709/224000
	applId: 13854653
	patentTitle: UNIFIED ACCOUNT METADATA MANAGEMENT
	appIntlPubNumber: 
	appConfrNumber: 2706
	appFilingDate: 2013-04-01T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: ANDRIES PETRUS JOHANNES DIPPENAAR
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Cape Town, 
			geoCode: 
			country: (ZA)
			rankNo: 1
	appCls: 709
	_version_: 1580433235000688646
	lastUpdatedTimestamp: 2017-10-05T15:49:44.995Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 8d2a2ffd-bf72-4810-a487-8f9a738c994e
	responseHeader:
			status: 0
			QTime: 17
