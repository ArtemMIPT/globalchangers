DOCUMENT_BODY:
	appGrpArtNumber: 2447
	appGrpArtNumberFacet: 2447
	transactions:
		№ 0
			recordDate: 2016-05-09 00:00:00
			code: N423
			description: Post Issue Communication - Certificate of Correction
		№ 1
			recordDate: 2015-10-20 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 2
			recordDate: 2015-10-20 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2015-10-19 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 4
			recordDate: 2015-10-19 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 5
			recordDate: 2015-10-02 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 6
			recordDate: 2015-09-30 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 7
			recordDate: 2015-09-23 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 8
			recordDate: 2015-09-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 9
			recordDate: 2015-09-22 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 10
			recordDate: 2015-09-22 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 11
			recordDate: 2015-09-17 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 12
			recordDate: 2015-09-17 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 13
			recordDate: 2015-09-11 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 14
			recordDate: 2015-09-08 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 15
			recordDate: 2015-09-08 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 16
			recordDate: 2015-09-08 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 17
			recordDate: 2015-09-08 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 18
			recordDate: 2015-06-22 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 19
			recordDate: 2015-06-22 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 20
			recordDate: 2015-06-22 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 21
			recordDate: 2015-06-17 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 22
			recordDate: 2015-06-13 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 23
			recordDate: 2015-06-13 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 24
			recordDate: 2015-06-08 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 25
			recordDate: 2015-06-08 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 26
			recordDate: 2015-05-14 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 27
			recordDate: 2015-05-11 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 28
			recordDate: 2015-05-11 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 29
			recordDate: 2015-04-09 00:00:00
			code: MEXAC
			description: Mail Interview Summary - Applicant Initiated - Conference
		№ 30
			recordDate: 2015-04-04 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 31
			recordDate: 2015-04-04 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 32
			recordDate: 2015-04-04 00:00:00
			code: EXAC
			description: Interview Summary - Applicant Initiated - Conference
		№ 33
			recordDate: 2015-02-12 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 34
			recordDate: 2015-02-12 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 35
			recordDate: 2015-02-12 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 36
			recordDate: 2015-02-08 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 37
			recordDate: 2015-02-07 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 38
			recordDate: 2015-02-07 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 39
			recordDate: 2014-12-18 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 40
			recordDate: 2014-12-18 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 41
			recordDate: 2014-12-18 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 42
			recordDate: 2014-07-24 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 43
			recordDate: 2013-04-03 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 44
			recordDate: 2013-04-03 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 45
			recordDate: 2013-04-03 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 46
			recordDate: 2013-04-03 00:00:00
			code: FLRCPT.R
			description: Filing Receipt - Replacement
		№ 47
			recordDate: 2013-02-04 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 48
			recordDate: 2013-01-31 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 49
			recordDate: 2013-01-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 50
			recordDate: 2013-01-22 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 51
			recordDate: 2013-01-22 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 52
			recordDate: 2013-01-19 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 53
			recordDate: 2012-12-26 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 54
			recordDate: 2012-12-20 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 55
			recordDate: 2012-12-19 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 56
			recordDate: 2012-12-19 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 57
			recordDate: 2012-12-19 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 58
			recordDate: 2012-12-19 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 59
			recordDate: 2012-12-19 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9166862
	applIdStr: 13720888
	appl_id_txt: 13720888
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13720888
	appSubCls: 203000
	appLocation: ELECTRONIC
	appAttrDockNumber: SEAZN.753A6
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 79502
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 7
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|7}
	appStatusDate: 2015-09-30T09:29:18Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-10-20T04:00:00Z
	APP_IND: 1
	appExamName: NDIAYE, CHEIKH T
	appExamNameFacet: NDIAYE, CHEIKH T
	firstInventorFile: No
	appClsSubCls: 709/203000
	appClsSubClsFacet: 709/203000
	applId: 13720888
	patentTitle: DISTRIBUTED CACHING SYSTEM
	appIntlPubNumber: 
	appConfrNumber: 9933
	appFilingDate: 2012-12-19T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Melissa Elaine Davis
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Edmonds, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Antoun Joubran Kanawati
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Mukul Vijay Karnik
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Redmond, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Kal Lyndon McFate
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Lake Forest Park, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Vishal  Parakh
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 5
		№ 5
			nameLineOne: Alexander Julian Tribble
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 6
	appCls: 709
	_version_: 1580433207358128129
	lastUpdatedTimestamp: 2017-10-05T15:49:18.661Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 08b71c7b-e732-4071-856a-34b5a913df41
	responseHeader:
			status: 0
			QTime: 17
