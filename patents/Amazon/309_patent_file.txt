DOCUMENT_BODY:
	appGrpArtNumber: 2835
	appGrpArtNumberFacet: 2835
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9054439
	applIdStr: 13768613
	appl_id_txt: 13768613
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13768613
	appSubCls: 679010
	appLocation: ELECTRONIC
	appAttrDockNumber: 25396-0243
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 29052
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2015-05-20T12:48:52Z
	LAST_MOD_TS: 2017-10-31T23:36:30Z
	patentIssueDate: 2015-06-09T04:00:00Z
	APP_IND: 1
	appExamName: BUTTAR, MANDEEP S
	appExamNameFacet: BUTTAR, MANDEEP S
	firstInventorFile: No
	appClsSubCls: 361/679010
	appClsSubClsFacet: 361/679010
	applId: 13768613
	patentTitle: STRUCTURES FOR SEAMLESS COUPLING OF DEVICE MEMBERS
	appIntlPubNumber: 
	transactions:
		№ 0
			recordDate: 2015-06-09 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-06-09 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-05-21 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-05-20 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-05-08 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-05-07 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-05-06 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-05-06 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-02-20 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2015-02-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2015-02-20 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2015-02-15 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2015-02-09 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 13
			recordDate: 2015-02-02 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 14
			recordDate: 2015-01-27 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 15
			recordDate: 2014-12-31 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 16
			recordDate: 2014-12-17 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 17
			recordDate: 2014-12-17 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 18
			recordDate: 2014-11-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 19
			recordDate: 2014-11-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 20
			recordDate: 2014-11-06 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 21
			recordDate: 2014-11-03 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 22
			recordDate: 2014-07-29 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 23
			recordDate: 2013-10-31 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 24
			recordDate: 2013-09-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 25
			recordDate: 2013-09-17 00:00:00
			code: TI1050
			description: Transfer Inquiry to GAU
		№ 26
			recordDate: 2013-09-11 00:00:00
			code: TI1050
			description: Transfer Inquiry to GAU
		№ 27
			recordDate: 2013-07-30 00:00:00
			code: TI1050
			description: Transfer Inquiry to GAU
		№ 28
			recordDate: 2013-07-30 00:00:00
			code: TI1050
			description: Transfer Inquiry to GAU
		№ 29
			recordDate: 2013-07-17 00:00:00
			code: TI1050
			description: Transfer Inquiry to GAU
		№ 30
			recordDate: 2013-05-22 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 31
			recordDate: 2013-04-23 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 32
			recordDate: 2013-03-18 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 33
			recordDate: 2013-03-18 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 34
			recordDate: 2013-03-18 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 35
			recordDate: 2013-03-11 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 36
			recordDate: 2013-02-19 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 37
			recordDate: 2013-02-15 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 38
			recordDate: 2013-02-15 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 39
			recordDate: 2013-02-15 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appConfrNumber: 6792
	appFilingDate: 2013-02-15T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Marc Rene Walliser
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Francisco, 
			geoCode: CA
			country: (US)
			rankNo: 1
	appCls: 361
	_version_: 1582836809778331652
	lastUpdatedTimestamp: 2017-11-01T04:33:32.55Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 4e446174-b94c-4fff-9a7f-c35223ddaa58
	responseHeader:
			status: 0
			QTime: 18
