DOCUMENT_BODY:
	appGrpArtNumber: 2458
	appGrpArtNumberFacet: 2458
	transactions:
		№ 0
			recordDate: 2015-09-15 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-09-15 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-08-26 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 3
			recordDate: 2015-08-05 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 4
			recordDate: 2015-08-05 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 5
			recordDate: 2015-08-03 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 6
			recordDate: 2015-08-03 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 7
			recordDate: 2015-06-11 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 8
			recordDate: 2015-06-11 00:00:00
			code: MM327
			description: Mail Miscellaneous Communication to Applicant
		№ 9
			recordDate: 2015-06-09 00:00:00
			code: M327
			description: Miscellaneous Communication to Applicant - No Action Count
		№ 10
			recordDate: 2015-06-09 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 11
			recordDate: 2015-05-26 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 12
			recordDate: 2015-05-22 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 13
			recordDate: 2015-05-22 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 14
			recordDate: 2015-05-22 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 15
			recordDate: 2015-05-22 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 16
			recordDate: 2015-05-22 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 17
			recordDate: 2015-05-22 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 18
			recordDate: 2015-05-22 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 19
			recordDate: 2015-05-15 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 20
			recordDate: 2015-05-08 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 21
			recordDate: 2015-05-08 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 22
			recordDate: 2015-05-07 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 23
			recordDate: 2015-05-07 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 24
			recordDate: 2015-05-07 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 25
			recordDate: 2015-05-07 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 26
			recordDate: 2015-05-07 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 27
			recordDate: 2015-05-06 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 28
			recordDate: 2015-04-15 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 29
			recordDate: 2015-04-15 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 30
			recordDate: 2015-04-15 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 31
			recordDate: 2015-03-26 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 32
			recordDate: 2015-03-23 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 33
			recordDate: 2014-12-22 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 34
			recordDate: 2014-12-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 35
			recordDate: 2014-12-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 36
			recordDate: 2014-12-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 37
			recordDate: 2014-12-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 38
			recordDate: 2014-12-15 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 39
			recordDate: 2014-12-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 40
			recordDate: 2014-12-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 41
			recordDate: 2014-12-15 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 42
			recordDate: 2014-12-05 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 43
			recordDate: 2014-12-05 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 44
			recordDate: 2014-12-05 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 45
			recordDate: 2014-10-27 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 46
			recordDate: 2014-10-27 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 47
			recordDate: 2013-11-05 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 48
			recordDate: 2013-11-05 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 49
			recordDate: 2013-11-05 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 50
			recordDate: 2013-10-09 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 51
			recordDate: 2013-10-09 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 52
			recordDate: 2013-10-09 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 53
			recordDate: 2013-08-23 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 54
			recordDate: 2013-07-23 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 55
			recordDate: 2013-07-23 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 56
			recordDate: 2013-07-23 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 57
			recordDate: 2013-07-11 00:00:00
			code: LET.
			description: Miscellaneous Incoming Letter
		№ 58
			recordDate: 2013-07-11 00:00:00
			code: A.PE
			description: Preliminary Amendment
		№ 59
			recordDate: 2013-06-20 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 60
			recordDate: 2013-06-20 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 61
			recordDate: 2013-06-20 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 62
			recordDate: 2013-06-04 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 63
			recordDate: 2013-04-16 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 64
			recordDate: 2013-04-16 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 65
			recordDate: 2013-04-16 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 66
			recordDate: 2013-04-08 00:00:00
			code: FLFEE
			description: Payment of additional filing fee/Preexam
		№ 67
			recordDate: 2013-02-08 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 68
			recordDate: 2013-02-08 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 69
			recordDate: 2013-02-08 00:00:00
			code: INCD
			description: Notice Mailed--Application Incomplete--Filing Date Assigned 
		№ 70
			recordDate: 2013-01-10 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 71
			recordDate: 2013-01-04 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 72
			recordDate: 2013-01-04 00:00:00
			code: CLAIM
			description: Claim Preliminary Amendment
		№ 73
			recordDate: 2013-01-04 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 74
			recordDate: 2013-01-04 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 75
			recordDate: 2013-01-04 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9137102
	applIdStr: 13734789
	appl_id_txt: 13734789
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13734789
	appSubCls: 227000
	appLocation: ELECTRONIC
	appAttrDockNumber: 120137.635C3
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 500
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 4
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|4}
	appStatusDate: 2015-08-26T09:43:15Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-09-15T04:00:00Z
	APP_IND: 1
	appExamName: DINH, KHANH Q
	appExamNameFacet: DINH, KHANH Q
	firstInventorFile: No
	appClsSubCls: 709/227000
	appClsSubClsFacet: 709/227000
	applId: 13734789
	patentTitle: USING VIRTUAL NETWORKING DEVICES TO MANAGE ROUTING COMMUNICATIONS BETWEEN CONNECTED COMPUTER NETWORKS
	appIntlPubNumber: 
	appConfrNumber: 1920
	appFilingDate: 2013-01-04T05:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Eric Jason Brandwine
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Haymarket, 
			geoCode: VA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Kevin Christopher Miller
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Herndon, 
			geoCode: VA
			country: (US)
			rankNo: 1
		№ 2
			nameLineOne: Andrew J. Doane
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Veinna, 
			geoCode: VA
			country: (US)
			rankNo: 3
	appCls: 709
	_version_: 1580433210304626691
	lastUpdatedTimestamp: 2017-10-05T15:49:21.474Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 38365122-7fb9-41d5-b2d7-e4dacdf0b560
	responseHeader:
			status: 0
			QTime: 16
