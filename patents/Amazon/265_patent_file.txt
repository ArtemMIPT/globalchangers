DOCUMENT_BODY:
	appGrpArtNumber: 2172
	appGrpArtNumberFacet: 2172
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9507480
	applIdStr: 13751245
	appl_id_txt: 13751245
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13751245
	appSubCls: 746000
	appLocation: ELECTRONIC
	appAttrDockNumber: 170114-1610
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 3
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|3}
	appStatusDate: 2016-11-09T13:19:28Z
	LAST_MOD_TS: 2017-10-30T10:50:56Z
	patentIssueDate: 2016-11-29T05:00:00Z
	APP_IND: 1
	appExamName: BRAHMACHARI, MANDRITA
	appExamNameFacet: BRAHMACHARI, MANDRITA
	firstInventorFile: No
	appClsSubCls: 715/746000
	appClsSubClsFacet: 715/746000
	applId: 13751245
	patentTitle: INTERFACE OPTIMIZATION APPLICATION
	appIntlPubNumber: 
	transactions:
		№ 0
			recordDate: 2016-11-29 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-11-29 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2016-11-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2016-11-09 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2016-10-27 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2016-10-27 00:00:00
			code: TCPB
			description: Printer Rush- No mailing
		№ 6
			recordDate: 2016-10-27 00:00:00
			code: MN271
			description: Mail Response to 312 Amendment (PTO-271)
		№ 7
			recordDate: 2016-10-26 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 8
			recordDate: 2016-10-21 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 9
			recordDate: 2016-10-21 00:00:00
			code: N271
			description: Response to Amendment under Rule 312
		№ 10
			recordDate: 2016-10-19 00:00:00
			code: PUBTC
			description: Pubs Case Remand to TC
		№ 11
			recordDate: 2016-10-18 00:00:00
			code: A.NA
			description: Amendment after Notice of Allowance (Rule 312)
		№ 12
			recordDate: 2016-10-18 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 13
			recordDate: 2016-10-18 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 14
			recordDate: 2016-08-16 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 15
			recordDate: 2016-08-16 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 16
			recordDate: 2016-08-16 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 17
			recordDate: 2016-08-12 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 18
			recordDate: 2016-06-29 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 19
			recordDate: 2016-06-20 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 20
			recordDate: 2016-05-05 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 21
			recordDate: 2016-04-27 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 22
			recordDate: 2016-03-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 23
			recordDate: 2016-03-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 24
			recordDate: 2016-03-24 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 25
			recordDate: 2016-03-08 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 26
			recordDate: 2015-08-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 27
			recordDate: 2015-08-19 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 28
			recordDate: 2015-08-14 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 29
			recordDate: 2015-08-14 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 30
			recordDate: 2015-08-11 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 31
			recordDate: 2015-08-11 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 32
			recordDate: 2015-08-07 00:00:00
			code: AFAC
			description: After Final Consideration Program Additional Consideration and/or updated search
		№ 33
			recordDate: 2015-08-07 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 34
			recordDate: 2015-07-31 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 35
			recordDate: 2015-07-31 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 36
			recordDate: 2015-07-23 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 37
			recordDate: 2015-07-21 00:00:00
			code: RAFC
			description: PILOT- Request for After Final Consideration Program
		№ 38
			recordDate: 2015-07-21 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 39
			recordDate: 2015-04-23 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 40
			recordDate: 2015-04-23 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 41
			recordDate: 2015-04-23 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 42
			recordDate: 2015-04-17 00:00:00
			code: CTFR
			description: Final Rejection
		№ 43
			recordDate: 2015-02-12 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 44
			recordDate: 2015-02-10 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 45
			recordDate: 2015-02-04 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 46
			recordDate: 2015-01-30 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 47
			recordDate: 2015-01-30 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 48
			recordDate: 2015-01-30 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 49
			recordDate: 2014-11-06 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 50
			recordDate: 2014-11-06 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 51
			recordDate: 2014-11-06 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 52
			recordDate: 2014-10-31 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 53
			recordDate: 2014-08-25 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 54
			recordDate: 2013-05-13 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 55
			recordDate: 2013-03-20 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 56
			recordDate: 2013-02-22 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 57
			recordDate: 2013-02-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 58
			recordDate: 2013-02-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 59
			recordDate: 2013-02-22 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 60
			recordDate: 2013-02-22 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 61
			recordDate: 2013-02-21 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 62
			recordDate: 2013-01-31 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 63
			recordDate: 2013-01-28 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 64
			recordDate: 2013-01-28 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 65
			recordDate: 2013-01-28 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appConfrNumber: 1046
	appFilingDate: 2013-01-28T05:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Allan Poon Hui
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Redmond, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Douglas Andrew Hungarter
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
	appCls: 715
	_version_: 1582694957820411907
	lastUpdatedTimestamp: 2017-10-30T14:58:51.986Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 043f6711-5f32-4b47-91a2-1be2571e614b
	responseHeader:
			status: 0
			QTime: 16
