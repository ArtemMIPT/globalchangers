DOCUMENT_BODY:
	appGrpArtNumber: 2449
	appGrpArtNumberFacet: 2449
	transactions:
		№ 0
			recordDate: 2015-08-18 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-08-18 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-07-30 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-07-29 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-07-17 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-07-17 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-07-14 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-07-14 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-04-27 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2015-04-27 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2015-04-27 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2015-04-18 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2015-04-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 13
			recordDate: 2015-04-17 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 14
			recordDate: 2015-04-17 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 15
			recordDate: 2015-04-16 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 16
			recordDate: 2015-04-16 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 17
			recordDate: 2015-03-27 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 18
			recordDate: 2015-01-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 19
			recordDate: 2015-01-29 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 20
			recordDate: 2015-01-27 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 21
			recordDate: 2015-01-23 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 22
			recordDate: 2015-01-23 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 23
			recordDate: 2015-01-23 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 24
			recordDate: 2014-12-17 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 25
			recordDate: 2014-09-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 26
			recordDate: 2014-09-19 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 27
			recordDate: 2014-03-20 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 28
			recordDate: 2014-03-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 29
			recordDate: 2014-03-20 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 30
			recordDate: 2014-03-12 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 31
			recordDate: 2014-02-18 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 32
			recordDate: 2013-06-18 00:00:00
			code: D5001
			description: Dispatch from OIPE to Corps - U-P-R-D Application
		№ 33
			recordDate: 2013-05-02 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 34
			recordDate: 2013-05-02 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 35
			recordDate: 2013-05-02 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 36
			recordDate: 2013-05-02 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 37
			recordDate: 2013-05-02 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 38
			recordDate: 2013-05-01 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 39
			recordDate: 2013-05-01 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 40
			recordDate: 2013-03-27 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 41
			recordDate: 2013-03-22 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 42
			recordDate: 2013-03-15 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 43
			recordDate: 2013-03-15 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9112827
	applIdStr: 13836540
	appl_id_txt: 13836540
	appEarlyPubNumber: US20140280482A1
	appEntityStatus: UNDISCOUNTED
	id: 13836540
	appSubCls: 224000
	appLocation: ELECTRONIC
	appAttrDockNumber: 110.0331
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 103182
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2015-07-29T10:44:21Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-09-18T04:00:00Z
	patentIssueDate: 2015-08-18T04:00:00Z
	APP_IND: 1
	appExamName: WON, MICHAEL YOUNG
	appExamNameFacet: WON, MICHAEL YOUNG
	firstInventorFile: No
	appClsSubCls: 709/224000
	appClsSubClsFacet: 709/224000
	applId: 13836540
	patentTitle: DETERMINING APPROPRIATE BROWSING APPLICATIONS FOR SELECTED NETWORK RESOURCES
	appIntlPubNumber: 
	appConfrNumber: 5275
	appFilingDate: 2013-03-15T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Jay Austin Crosley
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Redmond, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 709
	_version_: 1580433231278243843
	lastUpdatedTimestamp: 2017-10-05T15:49:41.432Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 791bb353-5cc2-4f42-8948-5645f9c7ff30
	responseHeader:
			status: 0
			QTime: 19
