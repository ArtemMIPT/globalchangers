DOCUMENT_BODY:
	appGrpArtNumber: 3625
	appGrpArtNumberFacet: 3625
	transactions:
		№ 0
			recordDate: 2015-10-13 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-10-13 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-09-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-09-23 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-09-14 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-09-12 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-09-10 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-09-10 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2015-09-02 00:00:00
			code: C600
			description: Supplemental Papers - Oath or Declaration
		№ 9
			recordDate: 2015-07-14 00:00:00
			code: MM327-O
			description: Mail PUBS Notice Requiring Inventors Oath or Declaration
		№ 10
			recordDate: 2015-07-10 00:00:00
			code: M327-O
			description: PUBS Notice Requiring Inventors Oath or Declaration
		№ 11
			recordDate: 2015-06-24 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 12
			recordDate: 2015-06-24 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 13
			recordDate: 2015-06-24 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 14
			recordDate: 2015-06-20 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 15
			recordDate: 2015-06-18 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 16
			recordDate: 2015-06-18 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 17
			recordDate: 2015-06-18 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 18
			recordDate: 2015-06-18 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 19
			recordDate: 2015-06-18 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 20
			recordDate: 2015-06-18 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 21
			recordDate: 2015-05-21 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 22
			recordDate: 2015-05-15 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 23
			recordDate: 2015-05-15 00:00:00
			code: DIST
			description: Terminal Disclaimer Filed
		№ 24
			recordDate: 2015-05-15 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 25
			recordDate: 2015-04-30 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 26
			recordDate: 2015-04-30 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 27
			recordDate: 2015-04-30 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 28
			recordDate: 2015-04-07 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 29
			recordDate: 2015-04-02 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 30
			recordDate: 2015-04-02 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 31
			recordDate: 2015-03-04 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 32
			recordDate: 2015-03-04 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 33
			recordDate: 2015-03-04 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 34
			recordDate: 2015-02-20 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 35
			recordDate: 2015-02-20 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 36
			recordDate: 2015-02-20 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 37
			recordDate: 2015-02-13 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 38
			recordDate: 2015-02-13 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 39
			recordDate: 2015-02-13 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 40
			recordDate: 2015-01-28 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 41
			recordDate: 2015-01-21 00:00:00
			code: ELC.
			description: Response to Election / Restriction Filed
		№ 42
			recordDate: 2015-01-12 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 43
			recordDate: 2015-01-07 00:00:00
			code: EXIA
			description: Interview Summary- Applicant Initiated
		№ 44
			recordDate: 2015-01-07 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 45
			recordDate: 2014-12-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 46
			recordDate: 2014-12-01 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 47
			recordDate: 2014-12-01 00:00:00
			code: MCTRS
			description: Mail Restriction Requirement
		№ 48
			recordDate: 2014-11-24 00:00:00
			code: CTRS
			description: Restriction/Election Requirement
		№ 49
			recordDate: 2014-07-10 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 50
			recordDate: 2014-07-10 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 51
			recordDate: 2014-07-10 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 52
			recordDate: 2014-02-12 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 53
			recordDate: 2014-02-12 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 54
			recordDate: 2014-02-05 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 55
			recordDate: 2013-06-07 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 56
			recordDate: 2013-04-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 57
			recordDate: 2013-04-24 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 58
			recordDate: 2013-04-24 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 59
			recordDate: 2013-04-24 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 60
			recordDate: 2013-04-24 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 61
			recordDate: 2013-04-23 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 62
			recordDate: 2013-04-23 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 63
			recordDate: 2013-04-12 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 64
			recordDate: 2013-04-12 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 65
			recordDate: 2013-04-12 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 66
			recordDate: 2013-03-18 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 67
			recordDate: 2013-03-14 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 68
			recordDate: 2013-03-13 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 69
			recordDate: 2013-03-13 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9159106
	applIdStr: 13799877
	appl_id_txt: 13799877
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13799877
	appSubCls: 027200
	appLocation: ELECTRONIC
	appAttrDockNumber: 90204-897863 (073410US)
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 107508
	applicants:
		№ 0
			nameLineOne: AMAZON TECHNOLOGIES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 8
	applicantsFacet: {|AMAZON TECHNOLOGIES, INC.||||Reno|NV|US|8}
	appStatusDate: 2015-09-23T10:57:21Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2015-10-13T04:00:00Z
	APP_IND: 1
	appExamName: ALLEN, WILLIAM J
	appExamNameFacet: ALLEN, WILLIAM J
	firstInventorFile: No
	appClsSubCls: 705/027200
	appClsSubClsFacet: 705/027200
	applId: 13799877
	patentTitle: SYSTEMS AND METHODS FOR FABRICATING PRODUCTS ON DEMAND
	appIntlPubNumber: 
	appConfrNumber: 4609
	appFilingDate: 2013-03-13T04:00:00Z
	firstNamedApplicant:  AMAZON TECHNOLOGIES, INC.
	firstNamedApplicantFacet:  AMAZON TECHNOLOGIES, INC.
	inventors:
		№ 0
			nameLineOne: Gregory J. Meyers
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Newcastle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: Bryan K. Beatty
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Issaquah, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 2
			nameLineOne: Jeffrey B. Slosberg
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 6
		№ 3
			nameLineOne: Colin  Bodell
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 4
			nameLineOne: Jeremy E. Powers
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Vashon, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 5
			nameLineOne: Michael G. Curtis
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Sammamish, 
			geoCode: WA
			country: (US)
			rankNo: 5
		№ 6
			nameLineOne: Leon L. Au
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 7
	appCls: 705
	_version_: 1580433223359397889
	lastUpdatedTimestamp: 2017-10-05T15:49:33.928Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: cc915edb-47a7-45a5-937d-e5065d213ba1
	responseHeader:
			status: 0
			QTime: 16
