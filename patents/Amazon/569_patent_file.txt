DOCUMENT_BODY:
	appGrpArtNumber: 3628
	appGrpArtNumberFacet: 3628
	transactions:
		№ 0
			recordDate: 2017-07-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 1
			recordDate: 2017-07-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 2
			recordDate: 2017-07-13 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 3
			recordDate: 2017-07-10 00:00:00
			code: CTFR
			description: Final Rejection
		№ 4
			recordDate: 2017-07-03 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 5
			recordDate: 2017-07-03 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 6
			recordDate: 2017-06-26 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 7
			recordDate: 2017-06-14 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 8
			recordDate: 2017-06-14 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 9
			recordDate: 2017-04-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 10
			recordDate: 2017-04-05 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 11
			recordDate: 2017-04-05 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 12
			recordDate: 2017-03-28 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 13
			recordDate: 2017-03-28 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 14
			recordDate: 2016-12-19 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 15
			recordDate: 2016-12-19 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 16
			recordDate: 2016-12-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 17
			recordDate: 2016-12-19 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 18
			recordDate: 2016-12-15 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 19
			recordDate: 2016-12-15 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 20
			recordDate: 2016-12-15 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 21
			recordDate: 2016-12-12 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 22
			recordDate: 2016-12-12 00:00:00
			code: MCTAV
			description: Mail Advisory Action (PTOL - 303)
		№ 23
			recordDate: 2016-12-07 00:00:00
			code: CTAV
			description: Advisory Action (PTOL-303)
		№ 24
			recordDate: 2016-12-05 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 25
			recordDate: 2016-12-02 00:00:00
			code: A.NE
			description: Response after Final Action
		№ 26
			recordDate: 2016-12-02 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 27
			recordDate: 2016-08-05 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 28
			recordDate: 2016-08-04 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 29
			recordDate: 2016-08-04 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 30
			recordDate: 2016-07-29 00:00:00
			code: CTFR
			description: Final Rejection
		№ 31
			recordDate: 2016-05-09 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 32
			recordDate: 2016-04-29 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 33
			recordDate: 2016-04-29 00:00:00
			code: XT/G
			description: Request for Extension of Time - Granted
		№ 34
			recordDate: 2015-11-13 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 35
			recordDate: 2015-11-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 36
			recordDate: 2015-11-13 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 37
			recordDate: 2015-10-09 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 38
			recordDate: 2015-10-07 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 39
			recordDate: 2015-10-05 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 40
			recordDate: 2015-08-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 41
			recordDate: 2015-06-08 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 42
			recordDate: 2015-01-23 00:00:00
			code: RCAP
			description: Reference capture on IDS
		№ 43
			recordDate: 2015-01-23 00:00:00
			code: M844
			description: Information Disclosure Statement (IDS) Filed
		№ 44
			recordDate: 2015-01-23 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 45
			recordDate: 2014-11-14 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 46
			recordDate: 2014-11-13 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 47
			recordDate: 2014-10-29 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 48
			recordDate: 2014-10-28 00:00:00
			code: C.AD
			description: Correspondence Address Change
		№ 49
			recordDate: 2014-10-07 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 50
			recordDate: 2014-05-08 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 51
			recordDate: 2014-05-08 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 52
			recordDate: 2014-05-08 00:00:00
			code: PG-RQST
			description: PG-Pub Request
		№ 53
			recordDate: 2014-05-05 00:00:00
			code: RESC
			description: Rescind Nonpublication Request for Pre Grant Publication
		№ 54
			recordDate: 2014-02-24 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 55
			recordDate: 2014-02-21 00:00:00
			code: TI1050
			description: Transfer Inquiry to GAU
		№ 56
			recordDate: 2013-06-24 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 57
			recordDate: 2013-06-13 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 58
			recordDate: 2013-06-13 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 59
			recordDate: 2013-06-13 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 60
			recordDate: 2013-06-13 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 61
			recordDate: 2013-06-13 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 62
			recordDate: 2013-06-12 00:00:00
			code: FTFS
			description: FITF set to YES - revise initial setting
		№ 63
			recordDate: 2013-06-12 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 64
			recordDate: 2013-05-14 00:00:00
			code: L128
			description: Cleared by L&R (LARS)
		№ 65
			recordDate: 2013-05-13 00:00:00
			code: L198
			description: Referred to Level 2 (LARS) by OIPE CSR
		№ 66
			recordDate: 2013-05-08 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 67
			recordDate: 2013-05-08 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 68
			recordDate: 2013-05-08 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 69
			recordDate: 2013-05-08 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Final Rejection Mailed
	appStatus_txt: Final Rejection Mailed
	appStatusFacet: Final Rejection Mailed
	patentNumber: 
	applIdStr: 13889622
	appl_id_txt: 13889622
	appEarlyPubNumber: US20140333761A1
	appEntityStatus: UNDISCOUNTED
	id: 13889622
	appSubCls: 007350
	appLocation: ELECTRONIC
	appAttrDockNumber: 110.0333
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 103182
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 2
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|2}
	appStatusDate: 2017-07-11T17:45:20Z
	LAST_MOD_TS: 2017-07-27T17:30:46Z
	appEarlyPubDate: 2014-11-13T05:00:00Z
	APP_IND: 1
	appExamName: SINGH, RUPANGINI
	appExamNameFacet: SINGH, RUPANGINI
	firstInventorFile: Yes
	appClsSubCls: 705/007350
	appClsSubClsFacet: 705/007350
	applId: 13889622
	patentTitle: AUTOMATED ITEM RETURN MACHINES
	appIntlPubNumber: 
	appConfrNumber: 9817
	appFilingDate: 2013-05-08T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: Richard Franklin Porter
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 1
	appCls: 705
	_version_: 1580433242233765890
	lastUpdatedTimestamp: 2017-10-05T15:49:51.921Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 8a5d78fb-c4aa-4695-8abb-3e524687bf44
	responseHeader:
			status: 0
			QTime: 18
