DOCUMENT_BODY:
	appGrpArtNumber: 2165
	appGrpArtNumberFacet: 2165
	transactions:
		№ 0
			recordDate: 2016-05-31 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2016-05-31 00:00:00
			code: CCRDY
			description: Application ready for PDX access by participating foreign offices
		№ 2
			recordDate: 2016-05-31 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 3
			recordDate: 2016-05-12 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 4
			recordDate: 2016-05-11 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 5
			recordDate: 2016-05-03 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 6
			recordDate: 2016-04-29 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 7
			recordDate: 2016-04-28 00:00:00
			code: REAS
			description: Response to Reasons for Allowance
		№ 8
			recordDate: 2016-04-28 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 9
			recordDate: 2016-04-28 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 10
			recordDate: 2016-01-29 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 11
			recordDate: 2016-01-29 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 12
			recordDate: 2016-01-29 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 13
			recordDate: 2016-01-25 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 14
			recordDate: 2016-01-21 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 15
			recordDate: 2016-01-03 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 16
			recordDate: 2016-01-03 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 17
			recordDate: 2015-12-28 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 18
			recordDate: 2015-12-28 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 19
			recordDate: 2015-12-11 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 20
			recordDate: 2015-12-08 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 21
			recordDate: 2015-10-28 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 22
			recordDate: 2015-10-28 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 23
			recordDate: 2015-10-28 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 24
			recordDate: 2015-10-23 00:00:00
			code: CTFR
			description: Final Rejection
		№ 25
			recordDate: 2015-10-13 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 26
			recordDate: 2015-10-08 00:00:00
			code: MEXAT
			description: Mail Interview Summary - Applicant Initiated - Telephonic
		№ 27
			recordDate: 2015-10-07 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 28
			recordDate: 2015-10-07 00:00:00
			code: C602
			description: Oath or Declaration Filed (Including Supplemental)
		№ 29
			recordDate: 2015-10-02 00:00:00
			code: EXAT
			description: Interview Summary - Applicant Initiated - Telephonic
		№ 30
			recordDate: 2015-07-09 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 31
			recordDate: 2015-07-09 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 32
			recordDate: 2015-07-08 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 33
			recordDate: 2015-07-01 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 34
			recordDate: 2015-06-24 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 35
			recordDate: 2015-06-17 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 36
			recordDate: 2014-08-15 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 37
			recordDate: 2014-08-15 00:00:00
			code: FLRCPT.C
			description: Filing Receipt - Corrected
		№ 38
			recordDate: 2014-04-16 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 39
			recordDate: 2013-08-15 00:00:00
			code: A.PE
			description: Preliminary Amendment
		№ 40
			recordDate: 2013-06-03 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 41
			recordDate: 2013-06-03 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 42
			recordDate: 2013-06-03 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 43
			recordDate: 2013-06-03 00:00:00
			code: PA..
			description: Change in Power of Attorney (May Include Associate POA)
		№ 44
			recordDate: 2013-06-03 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 45
			recordDate: 2013-06-03 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 46
			recordDate: 2013-05-31 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 47
			recordDate: 2013-05-31 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 48
			recordDate: 2013-05-13 00:00:00
			code: A.PE
			description: Preliminary Amendment
		№ 49
			recordDate: 2013-04-24 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 50
			recordDate: 2013-04-22 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 51
			recordDate: 2013-04-22 00:00:00
			code: NPRQ
			description: PGPubs nonPub Request
		№ 52
			recordDate: 2013-04-22 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 53
			recordDate: 2013-04-22 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 54
			recordDate: 2013-04-22 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 55
			recordDate: 2013-04-22 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9355134
	applIdStr: 13867450
	appl_id_txt: 13867450
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 13867450
	appSubCls: 737000
	appLocation: ELECTRONIC
	appAttrDockNumber: 170106-1351
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 71247
	applicants:
		№ 0
			nameLineOne: Amazon Technologies, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Reno, 
			geoCode: NV
			country: (US)
			rankNo: 8
	applicantsFacet: {|Amazon Technologies, Inc.||||Reno|NV|US|8}
	appStatusDate: 2016-05-11T11:33:55Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	patentIssueDate: 2016-05-31T04:00:00Z
	APP_IND: 1
	appExamName: HICKS, MICHAEL J
	appExamNameFacet: HICKS, MICHAEL J
	firstInventorFile: No
	appClsSubCls: 707/737000
	appClsSubClsFacet: 707/737000
	applId: 13867450
	patentTitle: FACILITATING DATA REDISTRIBUTION IN DATABASE SHARDING
	appIntlPubNumber: 
	appConfrNumber: 8915
	appFilingDate: 2013-04-22T04:00:00Z
	firstNamedApplicant:  Amazon Technologies, Inc.
	firstNamedApplicantFacet:  Amazon Technologies, Inc.
	inventors:
		№ 0
			nameLineOne: JOSEPH  MAGERRAMOV
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Seattle, 
			geoCode: WA
			country: (US)
			rankNo: 2
		№ 1
			nameLineOne: MIN  ZHU
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 4
		№ 2
			nameLineOne: ALESSANDRO  GHERARDI
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 6
		№ 3
			nameLineOne: WEINAN  WANG
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 1
		№ 4
			nameLineOne: MAXYM  KHARCHENKO
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bellevue, 
			geoCode: WA
			country: (US)
			rankNo: 3
		№ 5
			nameLineOne: AARON DREW ALEXANDER KUJAT
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Issaquah, 
			geoCode: WA
			country: (US)
			rankNo: 5
		№ 6
			nameLineOne: JASON CURTIS JENKS
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Lynnwood, 
			geoCode: WA
			country: (US)
			rankNo: 7
	appCls: 707
	_version_: 1580433237701820417
	lastUpdatedTimestamp: 2017-10-05T15:49:47.604Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: a2cc947d-62d2-4cea-96b3-f2488704cc44
	responseHeader:
			status: 0
			QTime: 16
