DOCUMENT_BODY:
	transactions:
		№ 0
			recordDate: 2014-03-16 00:00:00
			code: EXPRO
			description: EXPIRED PROVISIONAL
		№ 1
			recordDate: 2013-04-22 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 2
			recordDate: 2013-04-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2013-04-22 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 4
			recordDate: 2013-04-20 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 5
			recordDate: 2013-04-20 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 6
			recordDate: 2013-03-20 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 7
			recordDate: 2013-03-20 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 8
			recordDate: 2013-03-15 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 9
			recordDate: 2013-03-15 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Provisional Application Expired
	appStatus_txt: Provisional Application Expired
	appStatusFacet: Provisional Application Expired
	patentNumber: 
	applIdStr: 61788340
	appl_id_txt: 61788340
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 61788340
	appSubCls: 
	appLocation: ELECTRONIC
	appAttrDockNumber: 114826-52201
	appType: Provisional
	appTypeFacet: Provisional
	appCustNumber: 86738
	applicants:
		№ 0
			nameLineOne: Wal-Mart Stores, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bentonville, 
			geoCode: AR
			country: (US)
			rankNo: 2
	applicantsFacet: {|Wal-Mart Stores, Inc.||||Bentonville|AR|US|2}
	appStatusDate: 2014-03-17T00:48:29Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	APP_IND: 1
	firstInventorFile: Other
	applId: 61788340
	patentTitle: SYSTEMS AND METHODS FOR SAVING A REMAINDER OF CASH TRANSACTIONS
	appIntlPubNumber: 
	appConfrNumber: 7698
	appFilingDate: 2013-03-15T04:00:00Z
	firstNamedApplicant:  Wal-Mart Stores, Inc.
	firstNamedApplicantFacet:  Wal-Mart Stores, Inc.
	inventors:
		№ 0
			nameLineOne: John K. Collier
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Rogers, 
			geoCode: AR
			country: (US)
			rankNo: 1
	appCls: 
	_version_: 1580434071075422212
	lastUpdatedTimestamp: 2017-10-05T16:03:02.359Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 7cb9fe90-660c-4df5-8392-e78318daac97
	responseHeader:
			status: 0
			QTime: 12
