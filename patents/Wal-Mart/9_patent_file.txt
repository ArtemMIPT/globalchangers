DOCUMENT_BODY:
	transactions:
		№ 0
			recordDate: 2014-03-16 00:00:00
			code: EXPRO
			description: EXPIRED PROVISIONAL
		№ 1
			recordDate: 2013-05-06 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 2
			recordDate: 2013-05-06 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2013-05-06 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 4
			recordDate: 2013-05-03 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 5
			recordDate: 2013-05-03 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 6
			recordDate: 2013-04-02 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 7
			recordDate: 2013-03-21 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 8
			recordDate: 2013-03-15 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 9
			recordDate: 2013-03-15 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Provisional Application Expired
	appStatus_txt: Provisional Application Expired
	appStatusFacet: Provisional Application Expired
	patentNumber: 
	applIdStr: 61789710
	appl_id_txt: 61789710
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 61789710
	appSubCls: 
	appLocation: ELECTRONIC
	appAttrDockNumber: 114826-53601
	appType: Provisional
	appTypeFacet: Provisional
	appCustNumber: 86738
	applicants:
		№ 0
			nameLineOne: Wal-Mart Stores, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bentonville, 
			geoCode: AR
			country: (US)
			rankNo: 4
	applicantsFacet: {|Wal-Mart Stores, Inc.||||Bentonville|AR|US|4}
	appStatusDate: 2014-03-17T01:22:30Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	APP_IND: 1
	firstInventorFile: Other
	applId: 61789710
	patentTitle: DISCREPANCY ANALYSIS OF RFID READS TO DETERMINE LOCATIONS
	appIntlPubNumber: 
	appConfrNumber: 4809
	appFilingDate: 2013-03-15T04:00:00Z
	firstNamedApplicant:  Wal-Mart Stores, Inc.
	firstNamedApplicantFacet:  Wal-Mart Stores, Inc.
	inventors:
		№ 0
			nameLineOne: Nicholaus Adam Jones
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Fayetteville, 
			geoCode: AR
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Jarrod Lee Bourlon
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Centerton, 
			geoCode: AR
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Thomas Edward Stiefel
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bentonville, 
			geoCode: AR
			country: (US)
			rankNo: 3
	appCls: 
	_version_: 1580434071188668421
	lastUpdatedTimestamp: 2017-10-05T16:03:02.469Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 7cb9fe90-660c-4df5-8392-e78318daac97
	responseHeader:
			status: 0
			QTime: 12
