DOCUMENT_BODY:
	transactions:
		№ 0
			recordDate: 2014-05-25 00:00:00
			code: EXPRO
			description: EXPIRED PROVISIONAL
		№ 1
			recordDate: 2013-07-22 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 2
			recordDate: 2013-07-22 00:00:00
			code: FLRCPT.C
			description: Filing Receipt - Corrected
		№ 3
			recordDate: 2013-06-10 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 4
			recordDate: 2013-06-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 5
			recordDate: 2013-06-10 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 6
			recordDate: 2013-06-07 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 7
			recordDate: 2013-06-07 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 8
			recordDate: 2013-05-25 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 9
			recordDate: 2013-05-25 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 10
			recordDate: 2013-05-24 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 11
			recordDate: 2013-05-24 00:00:00
			code: BIG.
			description: ENTITY STATUS SET TO UNDISCOUNTED (INITIAL DEFAULT SETTING OR STATUS CHANGE)
		№ 12
			recordDate: 2013-05-24 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Provisional Application Expired
	appStatus_txt: Provisional Application Expired
	appStatusFacet: Provisional Application Expired
	patentNumber: 
	applIdStr: 61827283
	appl_id_txt: 61827283
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 61827283
	appSubCls: 
	appLocation: ELECTRONIC
	appAttrDockNumber: 114826-54801
	appType: Provisional
	appTypeFacet: Provisional
	appCustNumber: 86738
	applicants:
		№ 0
			nameLineOne: Wal-Mart Stores, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bentonville, 
			geoCode: AR
			country: (US)
			rankNo: 4
	applicantsFacet: {|Wal-Mart Stores, Inc.||||Bentonville|AR|US|4}
	appStatusDate: 2014-05-26T00:36:02Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	APP_IND: 1
	firstInventorFile: Other
	applId: 61827283
	patentTitle: Systems and Methods for Recommending Products
	appIntlPubNumber: 
	appConfrNumber: 7595
	appFilingDate: 2013-05-24T04:00:00Z
	firstNamedApplicant:  Wal-Mart Stores, Inc.
	firstNamedApplicantFacet:  Wal-Mart Stores, Inc.
	inventors:
		№ 0
			nameLineOne: Julia  Kaplan
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Redwood City, 
			geoCode: AR
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Alice Au Quan
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Francisco, 
			geoCode: CA
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Zoltan Rajeczy von Burian
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: San Francisco, 
			geoCode: CA
			country: (US)
			rankNo: 3
	appCls: 
	_version_: 1580434073695813640
	lastUpdatedTimestamp: 2017-10-05T16:03:04.859Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 7cb9fe90-660c-4df5-8392-e78318daac97
	responseHeader:
			status: 0
			QTime: 12
