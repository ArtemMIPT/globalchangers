DOCUMENT_BODY:
	appGrpArtNumber: 3684
	appGrpArtNumberFacet: 3684
	transactions:
		№ 0
			recordDate: 2015-04-21 00:00:00
			code: PGM/
			description: Recordation of Patent Grant Mailed
		№ 1
			recordDate: 2015-04-21 00:00:00
			code: PTAC
			description: Patent Issue Date Used in PTA Calculation
		№ 2
			recordDate: 2015-04-02 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2015-04-01 00:00:00
			code: WPIR
			description: Issue Notification Mailed
		№ 4
			recordDate: 2015-03-17 00:00:00
			code: D1935
			description: Dispatch to FDC
		№ 5
			recordDate: 2015-03-17 00:00:00
			code: PILS
			description: Application Is Considered Ready for Issue
		№ 6
			recordDate: 2015-03-16 00:00:00
			code: N084
			description: Issue Fee Payment Verified
		№ 7
			recordDate: 2015-03-16 00:00:00
			code: IFEE
			description: Issue Fee Payment Received
		№ 8
			recordDate: 2014-12-17 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 9
			recordDate: 2014-12-16 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 10
			recordDate: 2014-12-16 00:00:00
			code: MN/=.
			description: Mail Notice of Allowance
		№ 11
			recordDate: 2014-12-12 00:00:00
			code: N/=.
			description: Notice of Allowance Data Verification Completed
		№ 12
			recordDate: 2014-12-11 00:00:00
			code: EX.R
			description: Reasons for Allowance
		№ 13
			recordDate: 2014-12-11 00:00:00
			code: EX.A
			description: Examiner's Amendment Communication
		№ 14
			recordDate: 2014-11-20 00:00:00
			code: EXET
			description: Interview Summary - Examiner Initiated - Telephonic
		№ 15
			recordDate: 2014-11-20 00:00:00
			code: EXIE
			description: Interview Summary - Examiner Initiated
		№ 16
			recordDate: 2014-10-03 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 17
			recordDate: 2014-10-03 00:00:00
			code: ABN9
			description: Disposal for a RCE / CPA / R129
		№ 18
			recordDate: 2014-09-19 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 19
			recordDate: 2014-09-19 00:00:00
			code: PG-ISSUE
			description: PG-Pub Issue Notification
		№ 20
			recordDate: 2014-09-17 00:00:00
			code: RCEX
			description: Request for Continued Examination (RCE)
		№ 21
			recordDate: 2014-09-17 00:00:00
			code: BRCE
			description: Workflow - Request for RCE - Begin
		№ 22
			recordDate: 2014-06-19 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 23
			recordDate: 2014-06-19 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 24
			recordDate: 2014-06-19 00:00:00
			code: MCTFR
			description: Mail Final Rejection (PTOL - 326)
		№ 25
			recordDate: 2014-06-16 00:00:00
			code: CTFR
			description: Final Rejection
		№ 26
			recordDate: 2014-06-13 00:00:00
			code: IDSC
			description: Information Disclosure Statement considered
		№ 27
			recordDate: 2014-06-12 00:00:00
			code: EIDS.
			description: Electronic Information Disclosure Statement
		№ 28
			recordDate: 2014-06-12 00:00:00
			code: WIDS
			description: Information Disclosure Statement (IDS) Filed
		№ 29
			recordDate: 2014-05-19 00:00:00
			code: FWDX
			description: Date Forwarded to Examiner
		№ 30
			recordDate: 2014-05-13 00:00:00
			code: A...
			description: Response after Non-Final Action
		№ 31
			recordDate: 2014-05-13 00:00:00
			code: C614
			description: New or Additional Drawing Filed
		№ 32
			recordDate: 2014-02-14 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 33
			recordDate: 2014-02-13 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 34
			recordDate: 2014-02-13 00:00:00
			code: MCTNF
			description: Mail Non-Final Rejection
		№ 35
			recordDate: 2014-02-10 00:00:00
			code: CTNF
			description: Non-Final Rejection
		№ 36
			recordDate: 2013-12-11 00:00:00
			code: DOCK
			description: Case Docketed to Examiner in GAU
		№ 37
			recordDate: 2013-09-30 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 38
			recordDate: 2013-07-12 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 39
			recordDate: 2013-07-12 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 40
			recordDate: 2013-07-12 00:00:00
			code: FLRCPT.U
			description: Filing Receipt - Updated
		№ 41
			recordDate: 2013-07-11 00:00:00
			code: FTFI
			description: FITF set to NO - revise initial setting
		№ 42
			recordDate: 2013-07-11 00:00:00
			code: PGPC
			description: Sent to Classification Contractor
		№ 43
			recordDate: 2013-06-29 00:00:00
			code: ADDFLFEE
			description: Additional Application Filing Fees
		№ 44
			recordDate: 2013-06-29 00:00:00
			code: CORRDRW
			description: Applicant has submitted new drawings to correct Corrected Papers problems
		№ 45
			recordDate: 2013-05-01 00:00:00
			code: ELC_RVW
			description: Electronic Review
		№ 46
			recordDate: 2013-04-29 00:00:00
			code: EML_NTF
			description: Email Notification
		№ 47
			recordDate: 2013-04-29 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 48
			recordDate: 2013-04-29 00:00:00
			code: CPAP
			description: Corrected Paper
		№ 49
			recordDate: 2013-04-29 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 50
			recordDate: 2013-03-21 00:00:00
			code: L194
			description: Cleared by OIPE CSR
		№ 51
			recordDate: 2013-03-16 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 52
			recordDate: 2013-03-14 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 53
			recordDate: 2013-03-14 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Patented Case
	appStatus_txt: Patented Case
	appStatusFacet: Patented Case
	patentNumber: 9015069
	applIdStr: 13804905
	appl_id_txt: 13804905
	appEarlyPubNumber: US20140279269A1
	appEntityStatus: UNDISCOUNTED
	id: 13804905
	appSubCls: 026810
	appLocation: ELECTRONIC
	appAttrDockNumber: 114826-52902 (168US01)
	appType: Utility
	appTypeFacet: Utility
	appCustNumber: 86738
	applicants:
		№ 0
			nameLineOne: Wal-Mart Stores, Inc.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bentonville, 
			geoCode: AR
			country: (US)
			rankNo: 6
	applicantsFacet: {|Wal-Mart Stores, Inc.||||Bentonville|AR|US|6}
	appStatusDate: 2015-04-01T09:49:05Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	appEarlyPubDate: 2014-09-18T04:00:00Z
	patentIssueDate: 2015-04-21T04:00:00Z
	APP_IND: 1
	appExamName: BARGEON, BRITTANY E
	appExamNameFacet: BARGEON, BRITTANY E
	firstInventorFile: No
	appClsSubCls: 705/026810
	appClsSubClsFacet: 705/026810
	applId: 13804905
	patentTitle: SYSTEM AND METHOD FOR ORDER PROCESSING USING CUSTOMER LOCATION INFORMATION
	appIntlPubNumber: 
	appConfrNumber: 1356
	appFilingDate: 2013-03-14T04:00:00Z
	firstNamedApplicant:  Wal-Mart Stores, Inc.
	firstNamedApplicantFacet:  Wal-Mart Stores, Inc.
	inventors:
		№ 0
			nameLineOne: Travis  Brantley
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Pea Ridge, 
			geoCode: AR
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Bryan  Finster
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Centerton, 
			geoCode: AR
			country: (US)
			rankNo: 2
		№ 2
			nameLineOne: Ben  Rowse
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bentonville, 
			geoCode: AR
			country: (US)
			rankNo: 3
		№ 3
			nameLineOne: Khem  Chander
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bentonville, 
			geoCode: AR
			country: (US)
			rankNo: 4
		№ 4
			nameLineOne: Anthony G. Wind
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Gravette, 
			geoCode: AR
			country: (US)
			rankNo: 5
	appCls: 705
	_version_: 1580433224557920256
	lastUpdatedTimestamp: 2017-10-05T15:49:35.059Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 7cb9fe90-660c-4df5-8392-e78318daac97
	responseHeader:
			status: 0
			QTime: 12
