DOCUMENT_BODY:
	transactions:
		№ 0
			recordDate: 2014-03-16 00:00:00
			code: EXPRO
			description: EXPIRED PROVISIONAL
		№ 1
			recordDate: 2013-05-10 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 2
			recordDate: 2013-05-10 00:00:00
			code: EML_NTR
			description: Email Notification
		№ 3
			recordDate: 2013-05-10 00:00:00
			code: FLRCPT.O
			description: Filing Receipt
		№ 4
			recordDate: 2013-05-09 00:00:00
			code: COMP
			description: Application Is Now Complete
		№ 5
			recordDate: 2013-05-09 00:00:00
			code: OIPE
			description: Application Dispatched from OIPE
		№ 6
			recordDate: 2013-04-09 00:00:00
			code: L128
			description: Cleared by L&R (LARS)
		№ 7
			recordDate: 2013-04-01 00:00:00
			code: L198
			description: Referred to Level 2 (LARS) by OIPE CSR
		№ 8
			recordDate: 2013-03-20 00:00:00
			code: SCAN
			description: IFW Scan & PACR Auto Security Review
		№ 9
			recordDate: 2013-03-15 00:00:00
			code: APPERMS
			description: Applicants have given acceptable permission for participating foreign 
		№ 10
			recordDate: 2013-03-15 00:00:00
			code: IEXX
			description: Initial Exam Team nn
	appStatus: Provisional Application Expired
	appStatus_txt: Provisional Application Expired
	appStatusFacet: Provisional Application Expired
	patentNumber: 
	applIdStr: 61788696
	appl_id_txt: 61788696
	appEarlyPubNumber: 
	appEntityStatus: UNDISCOUNTED
	id: 61788696
	appSubCls: 
	appLocation: ELECTRONIC
	appAttrDockNumber: 114826-53301
	appType: Provisional
	appTypeFacet: Provisional
	appCustNumber: 86738
	applicants:
		№ 0
			nameLineOne: WAL-MART STORES, INC.
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Bentonville, 
			geoCode: AR
			country: (US)
			rankNo: 3
	applicantsFacet: {|WAL-MART STORES, INC.||||Bentonville|AR|US|3}
	appStatusDate: 2014-03-17T01:15:34Z
	LAST_MOD_TS: 2017-04-05T08:22:16Z
	APP_IND: 1
	firstInventorFile: Other
	applId: 61788696
	patentTitle: RFID READER LOCATION SELF-DISCOVERY
	appIntlPubNumber: 
	appConfrNumber: 7156
	appFilingDate: 2013-03-15T04:00:00Z
	firstNamedApplicant:  WAL-MART STORES, INC.
	firstNamedApplicantFacet:  WAL-MART STORES, INC.
	inventors:
		№ 0
			nameLineOne: Nicholaus Adam Jones
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Fayetteville, 
			geoCode: AR
			country: (US)
			rankNo: 1
		№ 1
			nameLineOne: Alvin Scott Taulbee
			nameLineTwo:  
			suffix: 
			streetOne: 
			streetTwo: 
			city: Springdale, 
			geoCode: AR
			country: (US)
			rankNo: 2
	appCls: 
	_version_: 1580434071123656706
	lastUpdatedTimestamp: 2017-10-05T16:03:02.405Z
METADATA:
	indexLastUpdatedDate: Sat Nov 25 14:40:32 EST 2017
	queryId: 7cb9fe90-660c-4df5-8392-e78318daac97
	responseHeader:
			status: 0
			QTime: 12
