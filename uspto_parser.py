from uspto.peds.client import UsptoPatentExaminationDataSystemClient
import os

client = UsptoPatentExaminationDataSystemClient()


current_dir = os.getcwd()
companies = ['Amazon', 'Target', 'Wal-Mart', 'Sears', 'Home Depot', 'Lowe\'s', 'CVSPharmacy', 'Kroger', 'Best Buy', 'Costco', 'Walgreens', 'MEIJER', 'Safeway']


if __name__ == '__main__':

	for company in companies:

		company_directory = current_dir + '/patents/' + company
		if not os.path.exists(company_directory):
			os.mkdir(company_directory)

		expression = 'firstNamedApplicant:(' + company + ')'
		filter     = 'appFilingDate:[1997-01-01T00:00:00Z TO 2017-11-24T23:59:59Z]'
		initial_result = client.search(expression, filter=filter, rows=20)
		
		found = initial_result.get('numFound')
		max_rows = 20
		steps = int(found/max_rows) + 1

		for i in range(steps):
			result = client.search(expression, filter=filter, start = i*20, rows=20)

			numFound_file = company_directory + "/numFound.txt"
			start_file = company_directory + "/start.txt"
			

			with open(numFound_file, 'w') as n:
				n.write(str(result.get('numFound')))

			with open(start_file, 'w') as s:
				s.write(str(result.get('start')))

			for j in range(len(result.get('docs'))):
				patents_file_path = company_directory + '/' + str(j + (i*20)) + '_patent_file.txt'
				result_dict = result.get('docs')[j]
				result_dict_keys = result.get('docs')[j].keys()

				str_result = 'DOCUMENT_BODY:\n'

				for item1 in result_dict_keys:
					if item1 == 'transactions' or item1 == 'inventors' or item1 == 'applicants':
						str_result += '\t' + item1 + ":\n"
						t = 0
						for item2 in result_dict.get(item1):
							str_result += "\t\t№ " + str(t) + "\n"
							t += 1
							for item3 in item2.keys():
								str_result += "\t\t\t" + item3 + ": " + str(item2.get(item3)) + "\n"
					else:
						str_result += '\t' + item1 + ": " + str(result.get('docs')[j].get(item1)) + "\n"
				str_result += 'METADATA:\n'

				for key1 in result.get('metadata').keys():
					if(key1 == 'responseHeader'):
						str_result += '\t' + key1 +':' + '\n'
						for key2 in result.get('metadata').get(key1):
							str_result += '\t\t\t' + key2 + ': ' + str(result.get('metadata').get(key1).get(key2)) + '\n'
					else:
						str_result += '\t' + key1 + ': ' + str(result.get('metadata').get(key1)) + '\n'

				with open(patents_file_path, 'w') as p:
					p.write(str_result)